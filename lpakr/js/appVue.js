var app = new Vue({
  el: '#app',
  data: function(){
		return {
			products: [],
      currentPage: 0
		}
	},
  methods: {
    getProducts: function(){
      var self;
			self = this;
      axios({
          method: 'get',
          url: 'http://dev.ankr.dkonto.pl/wp-json/acf/v3/options/options/product_box'
        })
        .then(function(response) {
          self.products = self.chunk(response.data.product_box, 12);
        });
    },
    chunk: function(arr, len){
      var chunks = [],
          i = 0,
          n = arr.length;
        while (i < n) {
          chunks.push(arr.slice(i, i += len));
        }
        return chunks;
    },
    hideBtn: function(){
      // hide prev btn
      if(this.currentPage == 0){
        $('#prevBtn').addClass('hidden');
      }else{
        $('#prevBtn').removeClass('hidden');
      }
      // hide next btn
      if(this.currentPage == this.products.length - 1){
        $('#nextBtn').addClass('hidden');
      }else{
        $('#nextBtn').removeClass('hidden');
      }
    },
    nextPage: function(el){
      if(this.products.length - 1 != this.currentPage){
        this.currentPage++;
        $(window).scrollTop($('#app').offset().top - 80);
      }
      this.hideBtn();
    },
    prevPage: function(el){
      if(this.currentPage > 0){
        $(window).scrollTop($('#app').offset().top - 80);
        this.currentPage--;
      }
      this.hideBtn();
    }
  },
  mounted: function(){
    this.getProducts();
    this.hideBtn();
  }
})
