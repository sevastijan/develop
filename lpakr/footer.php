<footer class="footer">
  <div class="content-wrapper">
    <div class="box">
      <p class="m-typo m-typo_text">
        Przed wyborem nagrody należy zapoznać się szczegółowo z regulaminem dostępnym na naszej stronie www
        <br> pod adresem <a href="#">ankara.sanok.regulamin</a> <br>
        <span>
          produkty pokazane przy odbiorze mogą nieznacznie różnić się wyglądem od przedstawionych w katlaogu. przygotowanie katalogu nie stanowi
          oferty hanfdlowej. produkty w katalogu będą stale powiększane o asortyment. Ilość produktów ważna do wyczeprania zapasów.
        </span>
      </p>
    </div>
  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . '/js/app.js'?>"></script>
<script src="<?php echo get_stylesheet_directory_uri() . '/js/appVue.js'?>"></script>
<?php wp_footer(); ?>
</body>
</html>
