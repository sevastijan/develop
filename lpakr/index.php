<?php get_header(); ?>


<section class="products-boxes">
  <div id="app" class="content-wrapper">

      <div v-for="product in products[currentPage]" class="box">
        <div :class=" 'points ' + product.color">
          <div class="wrapper">
            <p class="m-typo m-typo_primary">
                {{ product.points }}
            </p>
            <p class="m-typo m-typo_primary">
              pkt
            </p>
          </div>
        </div>
        <img :src="product.image" alt="" class="img">
        <p class="m-typo m-typo_text">
          {{ product.name }}
        </p>
        <p class="m-typo m-typo_text">
          {{ product.category }}
        </p>
      </div>

      <div class="pagination">
        <p @click="prevPage" id="prevBtn" class="btn">poprzednie</p>
        <p @click="nextPage" id="nextBtn" class="btn">następne</p>
      </div>


  </div>
</section>


<?php get_footer(); ?>
