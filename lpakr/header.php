<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <link href="<?php echo get_stylesheet_directory_uri() . '/css/style.css'?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great|Poppins:400,700" rel="stylesheet">
  <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

  <header class="header clearfix">
    <div class="content-wrapper">
      <div class="two-boxes clearfix">
        <div class="box">
          <img src="<?php echo PB_THEME_URL; ?>images/brand-logo.png" alt="brand-logo" class="brand-logo">
        </div>
        <div class="box">
          <p class="m-typo m-typo_text">
            program lojalnościowy
          </p>
          <h1 class="m-typo m-typo_primary">
            zbieraj punkty! Odbieraj nagrody!
          </h1>
          <p class="m-typo m-typo_secondary">
            kupuj produkty w Ankarze, zbieraj punkty na kartę i wymieniaj je na nagrody z naszego katalogu
          </p>
        </div>
      </div>
    </div>
  </header>
