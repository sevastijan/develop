<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <link href="<?php echo get_stylesheet_directory_uri() . '/css/style.css'?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:300i,700" rel="stylesheet">
  <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <nav class="navbar">
      <div class="content-wrapper">
        <ul id="js-menu-list" class="list">
          <li id="js-primary-item" class="list_item">
            <div class="box">
                <img src="<?php echo PB_THEME_URL; ?>images/menu-item-img.png" alt="navbar-icon" class="navbar-icon">
                <span class="typo typo_secondary">Sprzątanie</span>
            </div>
            </li>
          <li id="js-secondary-item" class="list_item">
            <div class="box">
                <img src="<?php echo PB_THEME_URL; ?>images/menu-item-img2.png" alt="navbar-icon" class="navbar-icon">
                <span class="typo typo_secondary">Odkurzanie</span>
            </div>
            </li>
          <li id="js-tertiary-item" class="list_item">
            <div class="box">
                <img src="<?php echo PB_THEME_URL; ?>images/menu-item-img3.png" alt="navbar-icon" class="navbar-icon">
                <span class="typo typo_secondary">Usługi dodatkowe</span>
            </div>
            </li>
          <li id="js-quaternary-item" class="list_item">
            <div class="box">
                <img src="<?php echo PB_THEME_URL; ?>images/menu-item-img4.png" alt="navbar-icon" class="navbar-icon">
                <span class="typo typo_secondary">Kontakt</span>
            </div>
            </li>
          <li class="list_item">
            <div class="box">
              <a href="tel://+48513301408" class="typo typo_secondary">
                <img src="<?php echo PB_THEME_URL; ?>images/menu-item-img5.png" alt="navbar-icon" class="navbar-icon">
                <span>tel. 513 301 408</span>
              </a>
            </div>
            </li>
        </ul>
        <a href="#">
            <img src="<?php echo PB_THEME_URL; ?>images/brand-logo.png" alt="brand-logo" class="brand-logo navbar-logo">
        </a>
        <div id="js-burger" class="burger">
          <span class="burger_item"></span>
          <span class="burger_item"></span>
          <span class="burger_item"></span>
        </div>
      </div>
    </nav>
    <header class="header">
      <div class="content-wrapper">
        <div class="two-boxes">
          <div class="box">
            <img src="<?php echo PB_THEME_URL; ?>images/hero.png" alt="hero" class="hero">
          </div>
          <div class="box">
            <p class="typo typo_text">
              <span>Pan Cacy!</span> To młoda, energiczna firma wyspecjalizowana w usługach porządkowych.
              Naszą pracę wykonujemy z najwyższą starannością.
              Wierzymy, że zadowolenie naszych klientów to przepustka do dalszego rozwoju. <br>
              Stawiając na wysoką jakość, korzystamy z nowoczesnego,
              specjalistycznego sprzętu renomowanej marki HYLA. <br>
              Zapraszamy do współpracy!
            </p>
          </div>
        </div>
      </div>
    </header>
