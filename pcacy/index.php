<?php get_header(); ?>
<section class="post-wrapper content-wrapper clearfix">
  <?php if( have_rows('offer_post', 'options') ):?>
    <?php while ( have_rows('offer_post', 'options') ) : the_row();?>
      <div id="js-<?php the_sub_field('category', 'options'); ?>" class="post clearfix">
        <div class="post_head">
          <img src="<?php the_sub_field('heading_image', 'options'); ?>" alt="post-img" class="post_head-img">
          <div class="content">
            <h3 class="typo typo_primary">
                <?php the_sub_field('heading', 'options'); ?>
            </h3>
            <p class="typo typo_text">
              <?php the_sub_field('heading_description', 'options'); ?>
            </p>
          </div>
        </div>
        <div class="post_body">
          <div class="two-boxes">
            <?php if( have_rows('offer', 'options') ):?>
              <?php while ( have_rows('offer', 'options') ) : the_row();?>
                <div class="box">
                  <h6 class="typo typo_secondary">
                    <?php the_sub_field('offer_title', 'options'); ?>
                  </h6>
                  <img src="<?php the_sub_field('offer_image', 'options'); ?>" alt="post-img" class="post-body-img">
                  <p class="typo typo_text">
                      <?php the_sub_field('offer_description', 'options'); ?>
                </div>
              <?php endwhile;?>
            <?php endif;?>
          </div>
        </div>
      </div>
    <?php endwhile;?>
  <?php endif;?>
</section>
<?php get_footer(); ?>
