<footer id="js-footer" class="footer">
  <div class="content-wrapper">
    <div class="col-3">
      <h6 class="typo typo_secondary">
        Kontakt
      </h6>
      <p class="typo typo_text">
        Przemyslaw Mrozowski <br>
        Medyka 179,<br>
        37-732 Medyka, <br>
        woj.podkarpackie<br>
      </p>
      <a href="#">
        <img src="<?php echo PB_THEME_URL; ?>images/brand-logo.png" alt="brand-logo" class="brand-logo">
      </a>
    </div>
    <div class="col-3">
      <?php echo do_shortcode( '[contact-form-7 id="35" title="Formularz 1"]' ); ?>
    </div>
    <div class="col-3">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2575.0715248836877!2d22.936338315708056!3d49.803522979391246!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473b7a049ecc5c2f%3A0x3ae5218e7cf40688!2sMedyka+179%2C+37-732+Medyka!5e0!3m2!1spl!2spl!4v1517332146584" frameborder="0" allowfullscreen></iframe>
    </div>
    <p class="typo typo_text author">projekt: <a href="https://pawelblonski.pl/" target="_blank" title="Rysownik i ilustrator Paweł Błoński - Studio grafiki i ilustracji">PawelBlonski.pl</a></p>
  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() . '/js/app.js'?>"></script>
<?php wp_footer(); ?>
</body>
</html>
