// burger menu
$("#js-burger").click(function(){
    $("#js-menu-list").toggleClass("is-active");
    $("body").toggleClass("is-active");
});

// hide menu after click
$(".list_item").find('span').click(function(){
    $("body").removeClass("is-active");
    $("#js-menu-list").removeClass("is-active");
});

// nav scroll
$("#js-primary-item").find('span, img').click(function() {
    $('html, body').animate({
        scrollTop: $("#js-primary").offset().top - 140
    }, 2000);
});
$("#js-secondary-item").find('span, img').click(function() {
    $('html, body').animate({
        scrollTop: $("#js-secondary").offset().top - 140
    }, 2000);
});
$("#js-tertiary-item").find('span, img').click(function() {
    $('html, body').animate({
        scrollTop: $("#js-tertiary").offset().top - 140
    }, 2000);
});
$("#js-quaternary-item").find('span, img').click(function() {
    $('html, body').animate({
        scrollTop: $("#js-footer").offset().top
    }, 2000);
});
// fixed navbar
var num = 20,
    show = true;
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
       $('.navbar, .navbar-logo, .burger, .list_item').addClass('reduced');
    } else {
       $('.navbar, .navbar-logo, .burger, .list_item').removeClass('reduced');
    }
});
