<?php
/**
 * Wordpress template created for "Pawelblonski.pl"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Single article
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>

<?php 

    $classes = get_body_class();
    $marginClass = '';
    if (in_array('tales-template-default',$classes)) {
        $marginClass = 'tales-article';
    } 

?>

<article class="article <?php echo $marginClass; ?>">
    <section class="subpage-heading">
        <div class="content-wrapper narrow">
             <img src="<?php echo PB_THEME_URL; ?>/images/subpage-heading.png" alt="subpage-img" class="subpage-heading_img">
        <h1 class="typo typo_primary title">
            <?php the_title(); ?>
        </h1>
        <?php     
            $classes = get_body_class(); 
            $moreArticlesHref = get_field('main_btn_more', 'options');
            $moreArticlesImage = PB_THEME_URL . "images/subpage-heading1.png";
            $moreArticlesHeader = '
                <h6 class="typo typo_primary realizations">
                    <a href="' . $moreArticlesHref . '">
                        Więcej artykułów
                    </a>
                </h6>
                <img src="' . $moreArticlesImage . '" alt="subpage-img" class="subpage-heading_img">
            ';
            if (in_array('post-template-default',$classes)) {
                echo $moreArticlesHeader;
            } 
        ?>
        </div>
    </section>

    <div class="content-wrapper article">
        <?php 
            $classes = get_body_class(); 
            $shortcode = get_field('article_gallery');


            the_content();

            echo do_shortcode($shortcode);  

            if (in_array('post-template-default',$classes)) {
                $moreArticles = require(THEME_DIR.'/_includes/_more-articles-btn.php');
                echo $moreArticles;
            }

        ?>
    </div>
</article>


<?php get_footer(); ?>
