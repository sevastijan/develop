<section class="offer">
    <div class="content-wrapper">
    <?php if ( have_rows('main_offer', 'options') ) : ?>
        <?php while( have_rows('main_offer', 'options') ) : the_row(); ?>
        <article class="offer-box">
            <a href="<?php the_sub_field('main_offer-link', 'options'); ?>">
                <section class="offer-box_heading">
                    <h4 class="typo typo_primary">
                        <?php the_sub_field('main_offer-heading', 'options'); ?>
                    </h4>
                </section>
                <figure class="offer-box_img" style="background-image:url('<?php the_sub_field('main_offer-img', 'options'); ?>');">
                </figure>
                <section class="offer-box_excerpt">
                    <div class="typo typo_text">
                        <?php the_sub_field('main_offer-descr', 'options'); ?>
                    </div>
                </section>
            </a>
        </article>
        <?php endwhile; ?>
    <?php endif; ?>
    </div>
</section>