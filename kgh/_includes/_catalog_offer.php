<section class="catalog parallax js-parallax">
    <div class="catalog_container">
        <div class="catalog_text typo typo_text">
            <p>Specjalizujemy się w realizacji projektów związanych z produkcją opakowań tekturowych dla branży owocowo – warzywnej.</p>
            <p>Dzięki posiadaniu zaplecza technicznego oraz magazynowego możemy zaoferować szeroki wachlarz wzorów. Nasi pracownicy pomogą dobrać opakowania odpowiadające Państwa wymaganiom – realizujemy projekty dla najbardziej wymagających klientów.</p>
        </div>
        <div class="catalog_cta typo typo_text">
            <a href="#" class="btn btn_primary">Zobacz katalog produktów</a>
        </div>
    </div>
</section>