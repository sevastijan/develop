<section class="header-slider">
    <div id="js-headerSlider" class="header-slider-wrapper ">

    <div class="box content-wrapper">
       <div class="header-slider_box">
            <figure class="header-slider_box_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/mainslider-bottle.png" alt="" class="header-slider_box_item-img">
            </figure>
            <figure class="header-slider_box_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/mainslider-bottle.png" alt="" class="header-slider_box_item-img">
            </figure>
        </div>
        <div class="header-slider_box">
            <h1 class="typo typo_primary">
                Lemoniady Qeency
            </h1>
            <p class="typo typo_text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget fringilla ipsum. Suspendisse tincidunt diam augue. Fusce condimentum bibendum tortor non feugiat. Praesent vestibulum nulla non mauris auctor, eu imperdiet est interdum. Fusce mattis ullamcorper 
            </p>
        </div>
        <div class="header-slider_box">
            <figure class="header-slider_box_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/mainslider-bottle.png" alt="" class="header-slider_box_item-img">
            </figure>
            <figure class="header-slider_box_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/mainslider-bottle.png" alt="" class="header-slider_box_item-img">
            </figure>
        </div>
    </div>

    <div class="box content-wrapper">
       <div class="header-slider_box">
            <figure class="header-slider_box_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/mainslider-bottle.png" alt="" class="header-slider_box_item-img">
            </figure>
            <figure class="header-slider_box_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/mainslider-bottle.png" alt="" class="header-slider_box_item-img">
            </figure>
        </div>
        <div class="header-slider_box">
            <h1 class="typo typo_primary">
                Aloe vera
            </h1>
            <p class="typo typo_text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget fringilla ipsum. Suspendisse tincidunt diam augue. Fusce condimentum bibendum tortor non feugiat. Praesent vestibulum nulla non mauris auctor, eu imperdiet est interdum. Fusce mattis ullamcorper 
            </p>
        </div>
        <div class="header-slider_box">
            <figure class="header-slider_box_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/mainslider-bottle.png" alt="" class="header-slider_box_item-img">
            </figure>
            <figure class="header-slider_box_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/mainslider-bottle.png" alt="" class="header-slider_box_item-img">
            </figure>
        </div>
    </div>

    </div>
</section>