<section class="realizations-slider">
    <div id="js-realizationsSlider" class="header-slider-wrapper">
        <div class="header-slider-wrapper_item">
            <img src="<?php echo PB_THEME_URL; ?>/images/slider1.png" alt="" class="header-slider-wrapper_item-img">
        </div>
        <div class="header-slider-wrapper_item">
            <img src="<?php echo PB_THEME_URL; ?>/images/slider2.png" alt="" class="header-slider-wrapper_item-img">
        </div>
        <div class="header-slider-wrapper_item">
            <img src="<?php echo PB_THEME_URL; ?>/images/slider3.png" alt="" class="header-slider-wrapper_item-img">
        </div>
        <div class="header-slider-wrapper_item">
            <img src="https://pawelblonski.pl/wp-content/uploads/2017/11/lina-1000px_full_size.jpg" alt="" class="header-slider-wrapper_item-img">
        </div>
        <div class="header-slider-wrapper_item">
            <img src="https://pawelblonski.pl/wp-content/uploads/2018/02/lunapark_0000_WildWest-podglad-72dpi.jpg" alt="" class="header-slider-wrapper_item-img">
        </div>
        <div class="header-slider-wrapper_item">
            <img src="https://pawelblonski.pl/wp-content/uploads/2017/11/800x800_full_size.jpg" alt="" class="header-slider-wrapper_item-img">
        </div>
    </div>
</section>