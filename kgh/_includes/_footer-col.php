<div class="content-wrapper">
    <div class="footer-boxes">
            <div class="footer-boxes_box">
            <?php echo do_shortcode( '[contact-form-7 id="5" title="Formularz kontaktowy"]' ); ?>
        </div>
        <div class="footer-boxes_box">
            <div class="text-wrapper">
                <div id="js-dynamic-dept" class="typo typo_text">
                <p class="typo typo_text">
                    Siedziba Firmy<br />
                    KGH Polska
                </p>
                <p class="typo typo_text">
                    ul. Chałubińskiego 8, <br>
                    00-613 Warszawa, <br>
                    NIP: 7010405475, <br>
                    Tel: <a href="tel:+48226250316">+48 22 625 03 16</a>,<br />
                    Fax: +48 22 830 00 39
                </p>
                </div>
            </div>
        </div>
        <div class="footer-boxes_box">
            <iframe src="https://snazzymaps.com/embed/90810" width="100%" height="288px" style="border:none;"></iframe>
        </div>
</div>
</div>