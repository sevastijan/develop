
<section class="subpage-listing">
    <section class="subpage-heading">
        <div class="content-wrapper narrow">
             <img src="<?php echo PB_THEME_URL; ?>/images/subpage-heading.png" alt="subpage-img" class="subpage-heading_img">
        <h1 class="typo typo_primary title">
            <?php the_field('subpage_title'); ?>
        </h1>
        <h6 id="realizationsBtn" class="typo typo_primary realizations">
            Zobacz realizacje
        </h6>
        <img src="<?php echo PB_THEME_URL; ?>/images/subpage-heading1.png" alt="subpage-img" class="subpage-heading_img">
        </div>
    </section>
    <div class="content-wrapper narrow">
        <div class="typo typo_text subpage-listing_heading">
            <?php the_content(); ?>
        </div>
    <?php if ( have_rows('subpage_articles') ) : ?>
        <?php while( have_rows('subpage_articles') ) : the_row(); ?>
            <article class="subpage-listing-row">
                <div class="subpage-listing-row_content">
                    <h2 class="typo typo_primary">
                        <?php the_sub_field('subpage_articles-title'); ?>
                    </h2>
                    <div class="typo typo_text">
                        <?php the_sub_field('subpage_articles-descr'); ?>
                    </div>
                </div>
                <figure class="subpage-listing-row_img" style="background-image:url('<?php the_sub_field('subpage_articles-img'); ?>');"></figure>
            </article>
        <?php endwhile; ?>
    <?php endif; ?>
    </div>
</section>