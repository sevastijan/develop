// main page articles listing

<section class="articles main">
    <div class="content-wrapper">
        
        <?php 
            $classes = get_body_class(); 
            $loop = new WP_Query( array('posts_per_page' => 3, 'paged' => $paged ) );
            if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <article class="articles-item">
                        <a class="articles-item-href" href="<?php the_permalink(); ?>">
                            <figure class="articles-item_img" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></figure>
                            <div class="articles-item_content">
                                <h2 class="typo typo_primary">
                                    <?php the_title(); ?>
                                </h2>
                                <p class="typo typo_text">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tempus nec nibh et rutrum. Nam sit amet purus non libero bibendum luctus. Maecenas convallis ultrices lectus. In eu iaculis elit. Integer finibus enim mi, sed euismod lorem cursus bibendum. Sed urna dolor, laoreet id lacus non, lobortis iaculis massa. Quisque vehicula metus eget lorem fermentum consequat. Vestibulum id sem felis. Nulla malesuada velit nec diam mattis venenatis. Morbi ut orci at lectus dictum hendrerit vitae sit
                                </p>
                            </div>
                        </a>
                    </article>
                <?php endwhile;
            endif;
            wp_reset_postdata();

            require(THEME_DIR.'/_includes/_more-articles-btn.php');

        ?>
    </div>
</section>