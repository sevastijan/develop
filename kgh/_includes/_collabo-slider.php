<section class="collabo-slider">
    <div id="js-collaboSlider" class="header-slider-wrapper">
    <?php if ( have_rows('partners_slider', 'options') ) : ?>
        <?php while( have_rows('partners_slider', 'options') ) : the_row(); ?>
            <div class="header-slider-wrapper_item">
                <figure class="header-slider-wrapper_item-box">
                    <img src="<?php the_sub_field('partners_slider-item', 'options'); ?>" alt="" class="header-slider-wrapper_item-box-img">
                </figure>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
    </div>
</section>