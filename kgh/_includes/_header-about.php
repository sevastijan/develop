<div class="header-nav_about">
    <a href="#">
        <p class="typo typo_primary">
            czytaj więcej o mnie.
        </p>
        <div class="header-nav_about-img" style="background-image:url('https://static.goldenline.pl/user_photo/185/user_3398585_9a5707_huge.jpg');"></div>
    </a>
    <p class="typo typo_primary">
        Cześć, nazywam się Paweł! Jestem zawodowym rysownikiem, ilustratorem
        i projektantem. W branży artystycznej działam już ponad 15 lat. <br />
        Co mogę dla ciebie zrobić ? 
    </p>
</div>