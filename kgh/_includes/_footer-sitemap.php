<div class="sitemap">
    <div class="content-wrapper">
        <?php
        wp_nav_menu( array(
            'theme_location' => 'secondary',
            'menu_class' => 'sitemap-list',
            'container'=> 'ul'
        ));
        ?>
        <?php
        wp_nav_menu( array(
            'theme_location' => 'tertiary',
            'menu_class' => 'sitemap-list',
            'container'=> 'ul'
        ));
        ?>
        <?php
        wp_nav_menu( array(
            'theme_location' => 'quaternary',
            'menu_class' => 'sitemap-list',
            'container'=> 'ul'
        ));
        ?>
        <figure class="sitemap-logo">
            <a href="http://dev.pb2018.dkonto.pl/">
                <img src="" alt="" class="sitemap-logo_img">
            </a> 
        </figure>
    </div>
</div>











