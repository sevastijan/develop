<?php if ( have_rows('realizations_slider', 'options') ) : ?>
    <section class="realizations-slider">
        <div id="js-certsSlider" class="header-slider-wrapper">
            <?php while( have_rows('realizations_slider', 'options') ) : the_row(); ?>
                <div class="header-slider-wrapper_item">
                    <a href="<?php the_sub_field('realizations_slider-url', 'options') ?>">
                        <figure class="header-slider-wrapper_item-box">
                            <img src="<?php the_sub_field('realizations_slider-img', 'options'); ?>" alt="" class="header-slider-wrapper_item-box-img">
                        </figure>
                        <h3 class="typo typo_primary">
                            <?php the_sub_field('realizations_slider-heading', 'options'); ?>
                        </h3>
                        <div class="typo typo_text">
                            <?php the_sub_field('realizations_slider-descr', 'options'); ?>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
        </div>
    </section>
<?php endif; ?>