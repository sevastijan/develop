
<section class="comics-listing">

    <section class="subpage-heading">
        <div class="content-wrapper">
             <img src="<?php echo PB_THEME_URL; ?>/images/subpage-heading.png" alt="subpage-img" class="subpage-heading_img">
        <h1 class="typo typo_primary title">
            Komiksy reklamowe
        </h1>
        <h6 class="typo typo_primary realizations">
            <a href="#">
                Zobacz realizacje
            </a>
        </h6>
        <img src="<?php echo PB_THEME_URL; ?>/images/subpage-heading1.png" alt="subpage-img" class="subpage-heading_img">
        </div>
    </section>
    <div class="content-wrapper">

        <article class="comics-listing-row">
            <div class="comics-listing-row_content">
                <h2 class="typo typo_primary">
                    Kreatywna reklama o wielkiej mocy.
                </h2>
                <p class="typo typo_text">
                    W dzisiejszych czasach komiks przeżywa swoje odrodzenie. Dobra kreska w połączeniu z ciekawym scenariuszem przyciągają uwagę coraz szerszej grupy odbiorców. Co więcej, komiks mimowolnie wzbudza zainteresowanie nawet tych osób, które nigdy wcześniej nie miały z nim styczności. Fakt ten został dostrzeżony przez rysowników, ilustratorów i innych twórców działających w branży kreatywnej, a zwłaszcza w reklamie. Potencjał komiksu jako skutecznej formy reklamy zaczęli także dostrzegać właściciele firm, decydujący się na promocję swoich produktów i usług za pomocą komiksu. I słusznie, bo komiks w reklamie może dziś zdziałać cuda.
                </p>
            </div>
            <figure class="comics-listing-row_img" style="background-image:url('http://dev.pb2018.dkonto.pl/wp-content/themes/pb2018//images/offer-img.png');"></figure>
        </article>

        <article class="comics-listing-row">
            <div class="comics-listing-row_content">
                <h2 class="typo typo_primary">
                    Potencjał komiksu reklamowego.
                </h2>
                <p class="typo typo_text">
                    Komiks reklamowy jest wyróżniającą się i o wiele atrakcyjniejszą od tradycyjnej reklamy formą przekazu informacji. Komiksem opowiesz historię swojej firmy, komiksem wypromujesz swój produkt i usługę. Komiksem reklamowym zdziałasz o wiele więcej od konkrecji korzystającej z powszechnej formy reklamy – drukowanej czy webowej. A co najważniejsze, za pośrednictwem komiksu reklamowego o wiele szybciej dotrzesz do klientów i zdobędziesz ich zaufanie. Komiks posiada bowiem niezwykłą moc oddziaływania na ludzkie emocje.
                </p>
            </div>
            <figure class="comics-listing-row_img" style="background-image:url('http://dev.pb2018.dkonto.pl/wp-content/themes/pb2018//images/offer-img.png');"></figure>
        </article>

    </div>
</section>