<?php
/**
 * Wordpress template created for "KGH Polska"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Grupa produktów
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>


<section class="products-group">
    <div class="content-wrapper narrow">
        <div class="products-group_heading">
            <img src="<?php echo PB_THEME_URL; ?>/images/smoothie-title.png" alt="Products group title">
        </div>
        <div class="products-group_showbox">
            <img src="<?php echo PB_THEME_URL; ?>/images/great-smoothie.png" alt="Products name">
            <img src="<?php echo PB_THEME_URL; ?>/images/smoothie-bottles.png" alt="Products bottles">
        </div>
        <div class="typo typo_text">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur quis enim vitae purus auctor congue. Vestibulum dictum nunc a aliquet convallis. Nulla fermentum urna et mauris mollis elementum. Morbi suscipit, mi id fringilla varius, mi augue viverra diam, id 
            </p>
        </div>
        <ul class="products-group-list">
            <li class="products-group-list_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/bottle.png" alt="Bottle">
                <div class="products-group-list_item-descr">
                    <div class="typo typo_text">
                        <p>
                            <span>Nazwa produktu:</span> Jabłko Mango Dynia SMOTHIE GREAT
                        </p>
                        <p>
                            <span>Rodzaj:</span> Smoothie owocowe
                        </p>
                        <p>
                            <span>Składniki:</span> przecier jabłkowy (29,5%), przecier mango (25%), 
                            przecier z dyni (25%), sok jabłkowy (20%), sok z cytryny (0,5%), 
                            substancja wzbogacająca: witamina C (kwas L-askorbinowy).
                        </p>
                        <p>
                            <span>Pojemność:</span> 300 ml
                        </p>
                    </div>
                </div>
            </li>
            <li class="products-group-list_item">
                <img src="<?php echo PB_THEME_URL; ?>/images/bottle.png" alt="Bottle">
                <div class="products-group-list_item-descr">
                    <div class="typo typo_text">
                        <p>
                            <span>Nazwa produktu:</span> Jabłko Mango Dynia SMOTHIE GREAT
                        </p>
                        <p>
                            <span>Rodzaj:</span> Smoothie owocowe
                        </p>
                        <p>
                            <span>Składniki:</span> przecier jabłkowy (29,5%), przecier mango (25%), 
                            przecier z dyni (25%), sok jabłkowy (20%), sok z cytryny (0,5%), 
                            substancja wzbogacająca: witamina C (kwas L-askorbinowy).
                        </p>
                        <p>
                            <span>Pojemność:</span> 300 ml
                        </p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>


<?php get_footer(); ?>
