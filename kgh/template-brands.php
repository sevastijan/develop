<?php
/**
 * Wordpress template created for "KGH Polska"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Nasze Marki
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>


<section class="brands">
    <section class="subpage-heading">
        <div class="content-wrapper narrow w-brands">
            <h1 class="typo typo_primary title">
                Nasze Marki        
            </h1>
        </div>
        <div class="content-wrapper narrow">
            <div class="subpage-heading_brand">
                <ul class="subpage-heading_brand-list">
                    <li class="subpage-heading_brand-list-item">
                        <img src="<?php echo PB_THEME_URL; ?>/images/qeency.png" alt="brand-logo">
                    </li>
                    <li class="subpage-heading_brand-list-item">
                        <img src="<?php echo PB_THEME_URL; ?>/images/great.png" alt="brand-logo">
                    </li>
                </ul>
            </div>
        </div>   
    </section>

    <div class="post ">
        <div class="content-wrapper narrow">
            <div class="row">
                <div class="post-img">
                    <img src="<?php echo PB_THEME_URL; ?>/images/qeency-big.png" alt="brand-logo">
                </div>
                <div class="typo typo_text">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur quis enim vitae purus auctor congue. Vestibulum dictum nunc a aliquet convallis. Nulla fermentum urna et mauris mollis elementum. Morbi suscipit, mi id fringilla varius, mi augue viverra diam, id suscipit lectus purus vel dui. Donec malesuada odio eu ullamcorper ornare. C
                    </p>
                </div>
                <div class="post-img-large">
                    <img src="<?php echo PB_THEME_URL; ?>/images/post-large.png" alt="large-img">
                </div>
            </div>
            <div class="row">
                <div class="post-img">
                    <img src="<?php echo PB_THEME_URL; ?>/images/great-large.png" alt="brand-logo">
                </div>
                <div class="typo typo_text">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur quis enim vitae purus auctor congue. Vestibulum dictum nunc a aliquet convallis. Nulla fermentum urna et mauris mollis elementum. Morbi suscipit, mi id fringilla varius, mi augue viverra diam, id suscipit lectus purus vel dui. Donec malesuada odio eu ullamcorper ornare. C
                    </p>
                </div>
                <div class="post-img-large">
                    <img src="<?php echo PB_THEME_URL; ?>/images/great-post.png" alt="large-img">
                </div>
            </div>
        </div>
    </div>

</section>


<?php get_footer(); ?>
