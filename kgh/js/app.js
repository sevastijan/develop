// burger
$("#js-burger, #js-navListClose").click(function(){
    $("#js-navList").toggleClass("is-active");
    $("body").toggleClass("is-active");
});


// hide menu after click
$(".list_item").click(function(){
    $("body").removeClass("is-active");
    $("#js-menu-list").removeClass("is-active");
});


// header slider
$('#js-headerSlider').slick({
    dots: true,
    infinite: true,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
    nextArrow: '<i class="slider-arrows right"></i>',
    prevArrow: '<i class="slider-arrows left"></i>',
});

$("#js-certsSlider").slick({
  infinite: true,
  dots: true,
  slidesToShow: 4,
  centerMode: true,
  nextArrow: '<i class="slider-arrows right"></i>',
  prevArrow: '<i class="slider-arrows left"></i>',
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        centerMode: false
      }
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        centerMode: true
      }
    },
    {
      breakpoint: 1550,
      settings: {
        slidesToShow: 3,
        centerMode: true
      }
    }
  ]
});

// realizations slider
$("#js-realizationsSlider").slick({
  infinite: true,
  slidesToShow: 4,
  centerMode: true,
  nextArrow: '<i class="slider-arrows right"></i>',
  prevArrow: '<i class="slider-arrows left"></i>',
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        centerMode: false
      }
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        centerMode: true
      }
    },
    {
      breakpoint: 1550,
      settings: {
        slidesToShow: 3,
        centerMode: true
      }
    }
  ]
});

// collabo slider
$('#js-collaboSlider').slick({
    infinite: true,
    slidesToShow: 5,
    centerMode: true,
    nextArrow: '<i class="slider-arrows right"></i>',
    prevArrow: '<i class="slider-arrows left"></i>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        },
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 1550,
            settings: {
                slidesToShow: 3
            }
        }
    ]
});


// realization scroll

$("#realizationsBtn").click(function() {
    var rel = $("#realizations").offset().top;
    
    $(window).animate({ 
        scrollTop: rel - 10  
    }, "slow");
    
});


// fixed navbar
var num = 60,
  show = true;


$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('.header-nav').addClass('reduced');
        $("#js-navWrapper").addClass("desktop-fixed");
    } else {
        $('.header-nav').removeClass('reduced');
        $("#js-navWrapper").removeClass("desktop-fixed");
    }
});



var images = [].slice.call(document.querySelectorAll('.js-parallax'));

function getViewportHeight() {
    var a = document.documentElement.clientHeight, b = window.innerHeight;
    return a < b ? b : a;
}

function getViewportScroll() {
    if (typeof window.scrollY != 'undefined') {
        return window.scrollY;
    }
    if (typeof pageYOffset != 'undefined') {
        return pageYOffset;
    }
    var doc = document.documentElement;
    doc = doc.clientHeight ? doc : document.body;
    return doc.scrollTop;
}

function doParallax() {
    var el, elOffset, elHeight,
        offset = getViewportScroll(),
        vHeight = getViewportHeight();

    for (var i in images) {
        el = images[i];
        elOffset = el.offsetTop;
        elHeight = el.offsetHeight;

        if ((elOffset > offset + vHeight) || (elOffset + elHeight < offset)) { continue; }

        el.style.backgroundPosition = '50% ' + Math.round((elOffset - offset) * 3 / 8) + 'px';
    }
}
$(window).on('scroll', doParallax);


$('#js-department').on('change', function(){
    var selectedOption = this.value;
    var element = $('#departments');
    $(element[0].attributes).each(function () {
        if (selectedOption === this.value) {
            var newDeptData = this.name.replace('name', 'descr');
            $('#js-dynamic-dept').html(element.attr(newDeptData));
        }
    });
})
