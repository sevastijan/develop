  <footer class="footer">

    <?php require(THEME_DIR.'/_includes/_footer-col.php'); ?>


    <?php require(THEME_DIR.'/_includes/_footer-sitemap.php'); ?>


    <?php require(THEME_DIR.'/_includes/_copyright-bar.php'); ?>

  </footer>
  
  <?php if( have_rows('dzialy', 'options') ): ?>
    <div id="departments"
      <?php $loop = 0; while ( have_rows('dzialy', 'options') ) : the_row(); ?>
        data-department-<?php echo $loop; ?>-name="<?php the_sub_field('nazwa_do_porownania');?>"
        data-department-<?php echo $loop; ?>-descr="<?php the_sub_field('informacje_o_dziale'); ?>'"
    
      <?php $loop++; endwhile; ?>
    >
  </div>
  <?php endif; ?>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/jquery.scrollto@2.1.2/jquery.scrollTo.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . '/js/app.js'?>"></script>
    <?php wp_footer(); ?>
</body>
</html>