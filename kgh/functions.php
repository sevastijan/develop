<?php

if(!defined('THEME_DIR')) {
define('THEME_DIR', get_theme_root(). '/' .get_template() . '/');
}

if(!defined('PB_THEME_URL')) {
define('PB_THEME_URL', WP_CONTENT_URL. '/themes/' .get_template() . '/');
}

  /**
  *  Register settings page
  *
  */
  if( function_exists('acf_add_options_page')) {
  	acf_add_options_page(array(
  		'page_title' 	=> 'Ustawienia',
        'post_id' => 'options',
  		'menu_title'	=> 'Ustawienia',
  		'menu_slug' 	=> 'module-settings',
  		'capability'	=> 'edit_posts',
  		'redirect'		=> false
  	));
  }

  register_nav_menus(array(
    'primary' => 'Main navigation'
  ));
  register_nav_menus(array(
    'secondary' => 'Footer col-0'
  ));
  register_nav_menus(array(
    'tertiary' => 'Footer col-1'
  ));
  register_nav_menus(array(
    'quaternary' => 'Footer col-2'
  ));
  add_theme_support( 'post-thumbnails' );

  // function create_post_type() {
  //   register_post_type( 'tales',
  //     array(
  //       'labels' => array(
  //         'name' => __( 'Bajki' ),
  //         'singular_name' => __( 'Branding' )
  //       ),
  //       'public' => true,
  //       'has_archive' => true,
	// 	'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
  //     )
  //   );
  // }
  // add_action( 'init', 'create_post_type' );



  function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a.*>)?\s*(<img.*\/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
  }

  add_filter('the_content', 'filter_ptags_on_images');

?>
