<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset') ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php wp_title(); ?>
	</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri() . '/css/style.css'?>" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <header class="header">
        <div class="content-wrapper">
            <section class="header-nav">
				
				<?php require(THEME_DIR.'/_includes/_nav-list.php'); ?>
				
				<?php require(THEME_DIR.'/_includes/_burger.php'); ?>
				
            </section>
        </div>
    </header>