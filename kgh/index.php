<?php get_header(); ?>


    <?php require(THEME_DIR.'/_includes/_header-slider.php'); ?>

    <?php require(THEME_DIR.'/_includes/_offer.php'); ?>

    <section class="subpage-heading no-realizations">
        <div class="content-wrapper">
            <img src="<?php echo PB_THEME_URL; ?>/images/kgh-brand.png ?>" alt="subpage-img" class="subpage-heading_img">
            <h1 class="typo typo_primary title">
                Opakowania
            </h1>
        </div>
    </section>

    <?php require(THEME_DIR.'/_includes/_catalog_offer.php'); ?>

    <section class="subpage-heading no-realizations">
        <div class="content-wrapper">
            <img src="<?php echo PB_THEME_URL; ?>/images/kgh-brand.png ?>" alt="subpage-img" class="subpage-heading_img">
            <h1 class="typo typo_primary title">
                Aktualności
            </h1>
        </div>
    </section>

    <?php require(THEME_DIR.'/_includes/_realizations-slider.php'); ?>

    <section class="subpage-heading no-realizations">
        <div class="content-wrapper">
            <img src="<?php echo PB_THEME_URL; ?>/images/kgh-brand.png ?>" alt="subpage-img" class="subpage-heading_img">
            <h1 class="typo typo_primary title">
                Nagrody, Certyfikaty, Wyróżnienia
            </h1>
        </div>
    </section>

    <?php require(THEME_DIR.'/_includes/_certs-slider.php'); ?>

    <section class="subpage-heading no-realizations">
        <div class="content-wrapper">
            <img src="<?php echo PB_THEME_URL; ?>/images/kgh-brand.png ?>" alt="subpage-img" class="subpage-heading_img">
            <h1 class="typo typo_primary title">
                Partnerzy
            </h1>
        </div>
    </section>

    <?php require(THEME_DIR.'/_includes/_collabo-slider.php'); ?>

<?php get_footer(); ?>