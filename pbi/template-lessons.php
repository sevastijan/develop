<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Lessons index
 * @package WordPress
 *
 */

 get_header('title');  ?>

 <main>
 	<div class="academy">
 		<!-- <a href="<?php echo esc_url( home_url( '/akademia' ) ); ?>" class="return">Wróć</a> -->
 		<div class="academy-container">
 			<h1 class="academy__title">Lekcje PBI</h1>
 			<div class="academy__lesson">
				<?php
					$index = 1;
          $lessons = new WP_Query(array(
              'post_type' => 'lessons',
              'posts_per_page' => -1,
              'order' => 'asc',
              'orderby' => 'menu_order'
            ));
        ?>
        <?php while($lessons->have_posts()) : $lessons->the_post(); ?>
 				<div class="academy__lesson-item">
 					<h1 class="academy__lesson-number academy__lesson-number--<?php echo $index; ?>">
            <?php
              if($index < 10) {
                echo '0' . $index;
              } else {
                echo $index;
              }
             ?>
          </h1>
 					<div class="academy__lesson-content">
 						<h3 class="academy__lesson-title">Lekcja</h3>
 						<p class="academy__lesson-text"><?php the_title(); ?></p>
						<a href="<?php the_permalink(); ?>">
	 						<div class="academy__lesson-button">
								<img src="<?php echo THEME_URL; ?>public/img/arrow-small.png" alt="" class="academy__lesson-arrow">
							</div>
						</a>
 					</div>
 				</div>
				<?php $index++; endwhile; ?>
 			</div>
 	</div>
 </main>

  <?php get_footer(); ?>
