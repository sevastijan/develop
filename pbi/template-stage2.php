<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Szkolenia poziom 2
 * @package WordPress
 *
 */

 get_header('title');  ?>

 <main class="program">
	 <div class="program__intro-left">
		 <h3 class="program__title">Stopień II - Odświeżenie certyfikatu</h3>
		 <p class="program__text">
			 Szkolenie aktualizujące
wiedzę i potwierdzające
odświeżenie certyfikatu – co
dwa lata
		 </p>
		 <div class="training__info">
			 <h3 class="panel__title">Zakres szkolenia</h3>
			 <ul class="panel__list">
				 <li class="panel__list-item"><span>Najnowsze trendy w polskim internecie</span></li>
				 <li class="panel__list-item"><span>Przypomnienie najważniejszych
wskaź ników w badanu widowni
internetowej</span></li>
				 <li class="panel__list-item"><span>Nowe funkcjonalności, nowe
wskaźniki w badaniu Gemius/PBI
wprowadzone w cią gu ostatnich dwóch
lat od daty realizacji aktualnego
szkolenia</span></li>
				 <li class="panel__list-item"><span>Przykłady analiz oparte na nowych
funkcjonalnościach i ich praktyczne
zastosowanie</span></li>
			 </ul>
			 <h3 class="training__price"> CENA: </h3>
			 <p class="training__price-text">Cena: 200 PLN/osoba </p>
			 <p class="training__price-text">Klienci firmy Gemius w danym roku kalendarzowym – rabat 100 proc. </p>
		 </div>
		 <?php echo do_shortcode('[contact-form-7 id="968" title="Formularz 1"]'); ?>
		 </div>
 </main>

  <?php get_footer(); ?>
