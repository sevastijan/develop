<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: PBI
 * @package WordPress
 *
 */

 get_header('title'); the_post();?>

 <main class="main-about">
   <img src="<?php echo THEME_URL; ?>public/img/pbi/pbi-yellow-circle.png" alt="" class="main-about__img">
    <section class="about">
      <div class="about--wrapper">
        <div class="about-intro">
          <h1 class="about-intro__title">Kim jesteśmy?</h1>
          <?php the_content(); ?>
        </div>
        <?php if( have_rows('section') ): while ( have_rows('section') ) : the_row(); ?>
         <div class="about-owners">
           <h1 class="about-owners__title"><?php the_sub_field('section_name'); ?></h1>
           <div class="about-owners--wrapper">
             <?php if( have_rows('images') ): while ( have_rows('images') ) : the_row(); ?>
             <div class="about-owners__logo-box">
               <div class="about-owners__logo-box--wrapper">
               <div class="about-owners__logo">
                 <img src="<?php the_sub_field('image'); ?>" alt="" class="about-owners__logo--img">
               </div>
               </div>
             </div>
            <?php endwhile; endif; ?>
           </div>
         </div>
       <?php endwhile; endif; ?>
      </div>
    </section>
 </main>

<?php get_footer(); ?>
