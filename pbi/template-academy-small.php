<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Academy small
 * @package WordPress
 *
 */

 get_header('title');  ?>
<main>
  <div class="intro">
    <div class="intro__wrapper">
      <div class="intro__wrapper--inside">
        <a href="<?php echo esc_url( home_url( '/szkolenia' ) ); ?>">
          <div class="intro__box">
            <img src="<?php echo THEME_URL; ?>public/img/article-icon.png" alt="" class="intro__box-icon">
            <div class="intro__box-title--container">
              <h3 class="intro__box-title">Szkolenia</h3>
            </div>
            <p class="intro__box-text"><a href="<?php echo esc_url( home_url( '/szkolenia' ) ); ?>">Poznaj nasze szkolenia <br> i zapisz się na wybrane</a></p>
          </div>
        </a>
        <a href="<?php echo esc_url( home_url( '/lekcje' ) ); ?>">
          <div class="intro__box">
            <img src="<?php echo THEME_URL; ?>public/img/research-icon.svg" alt="" class="intro__box-icon">
            <div class="intro__box-title--container">
              <h3 class="intro__box-title">Lekcje</h3>
            </div>
            <p class="intro__box-text"><a href="<?php echo esc_url( home_url( '/lekcje' ) ); ?>">Naucz się jak korzystać <br> z narzędzi Gemius PBI</a></p>
          </div>
        </a>
        <a href="<?php echo esc_url( home_url( '/prezentacje' ) ); ?>">
          <div class="intro__box">
            <img src="<?php echo THEME_URL; ?>public/img/mobile-icon.svg" alt="" class="intro__box-icon">
            <div class="intro__box-title--container">
              <!-- Zmieniony tekst -->
              <h3 class="intro__box-title">Wystąpienia</h3>
            </div>
            <p class="intro__box-text"><a href="<?php echo esc_url( home_url( '/prezentacje' ) ); ?>">Bądź na bieżąco z naszymi<br> wystąpieniami konferencyjnymi</a></p>
          </div>
        </a>
      </div>
    </div>
  </div>
</main>
<?php get_footer(); ?>
