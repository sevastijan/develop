<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Program szkoleniowy
 * @package WordPress
 *
 */

 get_header('title'); the_post(); ?>

 <main class="program">
     <div class="program__intro">
     <div class="program__intro-left">
       <h3 class="program__title">Program Szkoleniowy</h3>
       <p class="program__text">
       Szkolimy z posługiwania się wynikami badania Gemius/PBI oraz obsługi aplikacji Gemius Explorer. Szkolenia prowadzone są w dwóch formach: otwartej dla wszystkich zainteresowanych, i zamkniętej, realizowanej dla pracowników jednej firmy. Tematy poruszane na szkoleniu wiążą się z badaniami widowni internetowej oraz posługiwaniem się wynikami tych badao. Zaprezentowane zostaną także podstawowe funkcjonalności aplikacji Gemius Explorer. Szkolenie trwa 4 godziny i kooczy się testem – ci, którzy go zdadzą, otrzymują certyfikat Gemius/PBI.
       </p>
     </div>
     <div class="training">
       <h3 class="program__title--third">Rodzaje Szkoleń</h3>
       <h2 class="program__title--grey">Szkolenia Otwarte</h1>
         <div class="training-categories__boxes">
           <div class="training-categories__box">
             <img src="<?php echo THEME_URL; ?>public/img/program/program-badacze.svg" alt="" class="training-categories__box-icon">
             <h5 class="training-categories__box-title">Badacze</h5>
           </div>
           <div class="training-categories__box">
             <img src="<?php echo THEME_URL; ?>public/img/program/program-specjalisci.svg" alt="" class="training-categories__box-icon">
             <h5 class="training-categories__box-title">Specjaliści<Br> marketingu i PR</h5>
           </div>
           <div class="training-categories__box">
             <img src="<?php echo THEME_URL; ?>public/img/program/program-studenci.svg" alt="" class="training-categories__box-icon">
             <h5 class="training-categories__box-title">Studenci</h5>
           </div>
           <div class="training-categories__box">
             <img src="<?php echo THEME_URL; ?>public/img/program/program-sluchacze.svg" alt="" class="training-categories__box-icon">
             <h5 class="training-categories__box-title">Inne osoby<br> zainteresowane<br> badaniem Gemius PBI</h5>
           </div>
         </div>
         <h2 class="program__title--grey--padding">Szkolenia Zamknięte</h1>
           <div class="training-categories__boxes">
             <div class="training-categories__box">
               <img src="<?php echo THEME_URL; ?>public/img/program/program-domy.svg" alt="" class="training-categories__box-icon">
               <h5 class="training-categories__box-title">Domy<br>Mediowe</h5>
             </div>
             <div class="training-categories__box">
               <img src="<?php echo THEME_URL; ?>public/img/program/program-dzialy.svg" alt="" class="training-categories__box-icon">
               <h5 class="training-categories__box-title">Działy Marketingu</h5>
             </div>
             <div class="training-categories__box">
               <img src="<?php echo THEME_URL; ?>public/img/program/program-dzialypr.svg" alt="" class="training-categories__box-icon">
               <h5 class="training-categories__box-title">Działy PR</h5>
             </div>
             <div class="training-categories__box">
               <img src="<?php echo THEME_URL; ?>public/img/program/program-dzialybadan.svg" alt="" class="training-categories__box-icon">
               <h5 class="training-categories__box-title">Działy badań</h5>
             </div>
             <div class="training-categories__box">
               <img src="<?php echo THEME_URL; ?>public/img/program/program-poratelinternetowe.svg" alt="" class="training-categories__box-icon">
               <h5 class="training-categories__box-title">Portale internetowe</h5>
             </div>
             <div class="training-categories__box">
               <img src="<?php echo THEME_URL; ?>public/img/program/program-ecommerce.svg" alt="" class="training-categories__box-icon">
               <h5 class="training-categories__box-title">E-commerce</h5>
             </div>
             <div class="training-categories__box">
               <img src="<?php echo THEME_URL; ?>public/img/program/program-agencjeinteraktywne.svg" alt="" class="training-categories__box-icon">
               <h5 class="training-categories__box-title">Agencje interaktywne</h5>
             </div>
             <div class="training-categories__box">
               <img src="<?php echo THEME_URL; ?>public/img/program/program-agencjereklamowe.svg" alt="" class="training-categories__box-icon">
               <h5 class="training-categories__box-title">Agencje reklamowe</h5>
             </div>
           </div>
     </div>
     <div class="certificate">
       <h3 class="certificate__title">Korzyści / Certyfikat PBI</h3>
       <div class="certificate__content">
         <div class="certificate__content-left">
           <h3 class="certificate__content-title">Dla osób indywidualnych</h3>
           <p class="certificate__content-text">potwierdzenie umiejętności posługiwania się
           wynikami badania Gemius/PBI oraz aplikacją Gemius Explorer
           <br><br>
           pozyskanie wiedzy na temat pomiaru widowni internetowej
           i planowania kampanii w mediach internetowych
            <br><br>
           imienny dyplom wystawiony dla konkretnej osoby</p>
         </div>
         <div class="certificate__content-right">
           <h3 class="certificate__content-title certificate__content-title--second">Dla Firm</h3>
           <p class="certificate__content-text">
             zorganizowany program szkoleo dla dużej grupy pracowników
               <br><br>
             branżowy dokument poświadczający umiejętności
             pracowników oraz kandydatów
               <br><br>
             zachęta do regularnego aktualizowania wiedzy przez pracowników
           </p>
         </div>
       </div>
     </div>
     <div class="gemius-steps">
       <h1 class="gemius-steps__title">Zakres Tematyczny i Ceny</h1>
       <div class="gemius-steps__content">
         <div class="gemius-steps__content-box">
           <h3 class="gemius-steps__content-title">01</h3>
           <h3 class="gemius-steps__content-title--small">Stopień I</h3>
           <img src="<?php echo THEME_URL; ?>public/img/gemius-explorer/gemius-arrow-right.svg" alt="" class="gemius-steps__content-arrow">
           <p class="gemius-steps__content-text">Poziom podstawowy – dla tych,<br> którzy dopiero chcą się nauczyć<br> posługiwania się badaniem<br> widowni internetowej
           </p>
           <a href="/stopien-i/" class="gemius-steps__content-button">Zobacz szczegóły</a>
         </div>
         <div class="gemius-steps__content-box">
           <h3 class="gemius-steps__content-title">02</h3>
           <h3 class="gemius-steps__content-title--small">Stopień II</h3>
           <img src="<?php echo THEME_URL; ?>public/img/gemius-explorer/gemius-arrow-right.svg" alt="" class="gemius-steps__content-arrow">
           <p class="gemius-steps__content-text">Poziom zaawansowany – dla tych,<br> którzy posiadają już jakieś<br> umiejętności w zakresie badania<br> widowni internetowej
           </p>
           <a href="/stopien-ii/" class="gemius-steps__content-button">Zobacz szczegóły</a>
         </div>
         <!-- <div class="gemius-steps__content-box">
             <h3 class="gemius-steps__content-title">03</h3>
             <h3 class="gemius-steps__content-title--small">Stopień III</h3>
             <img src="<?php echo THEME_URL; ?>public/img/gemius-explorer/gemius-arrow-right.svg" alt="" class="gemius-steps__content-arrow">
           <p class="gemius-steps__content-text">Szkolenie aktualizujące wiedzę i<br> potwierdzające odświeżenie<br> certyfikatu
             – co dwa lata
           </p>
             <a href="" class="gemius-steps__content-button">Zobacz szczegóły</a>
         </div> -->
       </div>
     </div>
     <div class="download__experts">
       <h1 class="download__experts-title">Trenerzy</h1>
       <div class="download__experts--wrapper">
         <?php  if( have_rows('trenerzy') ): while ( have_rows('trenerzy') ) : the_row(); ?>
           <div class="download__experts-box">
             <div class="download__experts-box--wrapper download__experts-box--wrapper-first">
               <div class="download__experts-image--wrapper">
                 <img src="<?php the_sub_field('photo'); ?>" alt="" class="download__experts-box__image">
               </div>
              <h3 class="download__experts-box__name"><?php the_sub_field('name'); ?></h3>
              <p class="download__experts-box__topics"><?php the_sub_field('opis'); ?></p>
            </div>
           </div>
       <?php endwhile; endif; ?>
       </div>
     </div>
     <div class="contact__us-contact">
       <h1 class="contact__us-contact__title">Kontakt</h1>
       <div class="contact__us-contact__information">
         <p class="contact__us-contact__paragraph">
           Polskie Badania Internetu Sp. z o.o.<br>
           Al. Jerozolimskie 65/79, pok. 3.175<br>
           00-697 Warszawa<br>
           tel. (48) 22 630 72 68<br>
           fax. (48) 22 630 72 67<br>
           biuro@pbi.org.pl
         </p>
       </div>
       <h1 class="contact__us-contact__title">Dane Rejestrowe</h1>
       <div class="contact__us-contact__information">
         <p class="contact__us-contact__paragraph">
           NIP: 525-21-89-123<br>
           Regon: 016416840<br>
           Kapitał zakładowy: 611 400,00 zł<br>
           KRS 0000026069
         </p>
       </div>
     </div>
       <h3 class="program-faq__title program-faq__title--contact">Dojazd i kontakt</h3>
   </main>
   <div class="contact__us-map">
     <section id="map" class="map__img">
     </section>
     <div class="map">
       <div class="map__contact">
         <div class="map__contact-content">
           <div class="map__contact-left">
             <h1 class="map__contact-title">Kontakt</h1>
             <p class="map__contact-text">
               Polskie Badania Internetu Sp. z o.o.<br>
               Al. Jerozolimskie 65/79, pok. 3.175<br>
               00-697 Warszawa<br>
               tel. (48) 22 630 72 68<br>
               fax. (48) 22 630 72 67<br>
               biuro@pbi.org.pl
             </p>
           </div>
           <div class="map__contact-right">
             <h1 class="map__contact-title">Dane Rejestrowe</h1>
             <p class="map__contact-text">
               NIP: 525-21-89-123<br>
               Regon: 016416840<br>
               Kapitał zakładowy: 611 400,00 zł<br>
               KRS 0000026069
             </p>
           </div>
         </div>
       </div>
     </div>
   </div>
   <main class="program program--second">
     <?php if( have_rows('organizacje') || have_rows('uczelnie') ): ?>
     <div class="program-partners">
       <h3 class="program-partners__title">Uczestnicy Instytucjonalni</h3>
       <?php if( have_rows('organizacje') ): ?>
       <h3 class="program-partners__title--small">Organizacje</h3>
       <div class="program-partners__boxes">
         <?php while ( have_rows('organizacje') ) : the_row(); ?>
           <div class="program-partners__box">
             <img src="<?php the_sub_field('logo'); ?>" alt="" class="program-partners__box-logo">
           </div>
         <?php endwhile;?>
       </div>
       <?php endif; ?>
       <?php if( have_rows('uczelnie') ): ?>
       <h3 class="program-partners__title--small">Uczelnie</h3>
       <div class="program-partners__boxes">
         <?php while ( have_rows('uczelnie') ) : the_row(); ?>
           <div class="program-partners__box">
             <img src="<?php the_sub_field('logo'); ?>" alt="" class="program-partners__box-logo">
           </div>
         <?php endwhile;?>
       </div>
       <?php endif; ?>
       </div>
       <?php endif; ?>
     </div>
<?php $acvitateCerts = false; if($acvitateCerts) : ?>

     <?php

     $certs = new WP_Query(array(
         'post_type' => 'certs'
     ));

     if($certs->have_posts()) : while($certs->have_posts()) : $certs->the_post();
        $certID = get_field('id');
         $certificates[] = $certID ;

     endwhile; endif;?>

     <div class="verify" data-certificates='<?php echo json_encode($certificates); ?>'>
       <form class="verify-certificate">
         <h3 class="verify-certificate__title">Zweryfikuj swój certyfikat</h3>
         <input type="text" placeholder="Podaj nr certyfikatu" class="verify-certificate__input">
         <div class="verify-certificate__button">Ok</div>
       </form>
     </div>


     <?php
   endif;

     $postsPerPage = get_query_var('posts', 3);

     $opinions = new WP_Query(array(
         'post_type' => 'opinions',
         'posts_per_page' => $postsPerPage
     ));

     if($opinions->have_posts()) : ?>

     <div class="opinions">
       <h3 class="opinions__title">Opinie uczestników</h3>
       <div id="opinions-container">
       <?php while($opinions->have_posts()) : $opinions->the_post(); ?>

         <div class="opinions__container">
           <div class="opinions__container-left">
              <img src="<?php the_field('photo'); ?>" alt="" class="opinions__container-photo">
           </div>
           <div class="opinions__container-right">
             <h3 class="opinions__container-title"><?php the_field('name'); ?></h3>
             <p class="opinions__container-text"><?php the_field('opinia'); ?></p>
           </div>
         </div>

       <?php endwhile; ?>
      </div>
       <div class="opinions__container-button--wrapper">
         <a id="loadmore-opinions" class="opinions__container-button"> Pokaż więcej</a>
       </div>
     </div>
   <?php endif; ?>
   <?php
   $faqPerPage = get_query_var('posts', 3);
   $faqs = new WP_Query(array(
       'post_type' => 'faq',
       'posts_per_page' => $faqPerPage
   ));

   if($faqs->have_posts()) :
   ?>
     <div class="program-faq">
       <h3 class="program-faq__title">Najczęściej zadawane pytania</h3>
       <div id="faqs-container">
       <?php while($faqs->have_posts()) : $faqs->the_post(); ?>
         <p class="program-faq__text">
           <?php the_field('question'); ?>
         </p>
       <?php endwhile; ?>
       </div>
       <div class="program-faq__button--wrapper">
         <a id="loadmore-faqs" class="program-faq__button"> Pokaż więcej</a>
       </div>
     </div>
   <?php endif; ?>
 </main>

 <?php get_footer(); ?>
