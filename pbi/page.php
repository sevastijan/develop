<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
  get_header('title'); the_post(); ?>

  <main class="single-article">
    <div class="single-article--wrapper">
      <article class="single-article__main">
        <h3 class="single-article__title">
          <?php the_title(); ?>
        </h3>
        <div class="single-article__details">
          <span class="single-article__author"><?php the_author(); ?>/
          </span>
          <span class="single-article__date"><?php the_date(); ?>/
          </span>
        </div>
        <?php if(has_post_thumbnail()) : ?>
         <div class="single-article__image">
          <?php
            the_post_thumbnail('full', ['class' => 'single-article__image--intro']);
            $thumbnailArray = wp_get_attachment(get_post_thumbnail_id($post->ID));
          ?>
          <p class="single-article__image-author"><?php echo $thumbnailArray['description']; ?></p>
          </div>
        <?php endif; ?>
        <div class="single-article__body">
          <?php the_content(); ?>
        </div>
      </article>
    </div>
  </main>

  <?php get_footer(); ?>
