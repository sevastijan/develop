<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: ENG
 * @package WordPress
 *
 */

 get_header('title'); the_post();?>
 <div class="en-intro__container">  <h1 class="en-intro__title">About us</h1></div>
 <main class="en">
   <p class="en-intro__text">PBI supports market with knowledge to make better business decisions in the digital environment. For this purpose the organization maintains and provides the results of the standard of internet audience measurement Gemius / PBI and is conducting other researches. The data are used by representatives of publishers, media houses, research departments of large companies and interactive agencies. PBI contributes also to the development of the digital market, sharing the expertise and establishing cooperation with partner organizations with similar business profile.</p>
   <h3 class="en__title">Our owners are the biggest media companies in Poland:</h3>
   <div class="en__boxes">
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/owners/zpr.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/owners/agora.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/owners/allegro.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/owners/axel.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/owners/interia.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="http://pbi.org.pl/wp-content/uploads/2017/03/Logo-Onet-2017.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/owners/polsat.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/owners/press.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/owners/wp.png" alt="" class="en__box-img">
     </div>


   </div>
   <h3 class="en__title">Our products and services</h3>
     <p class="en__text">Research Gemius/PBI is the standard of internet audience measurement in Poland, conducted by us in cooperation with Gemius. It is used by the owners of websites to measure traffic, to make comparisons with competing companies and prepare advertising offers. Thanks to internet audience measurement advertisers may compare offers of a variety of publishers.
     Research Gemius/PBI is hybrid research, combining measurement of traffic online (site-centric) and measurement of panelists behaviour (user-centric). It measures both the traffic to websites, audio and video consumption, and mobile applications. Population consists of people aged 7 years and more. The indicators used in the study are the number of Internet users, , time spent on sites, number of page views and derivatives. There are also demographic data users. The survey results can be accessed using the Gemius Explorer solutions or in form of data reports available at our office.
     We offer also archived data from the Megapanel research, conducted by us between 2005-2015 in cooperation with Gemius. </p>
     <h3 class="en__title--big">Partnerships</h3>
   <div class="en__boxes">
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/partnerzy/gemius.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/partnerzy/iab.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/partnerzy/i-com.png" alt="" class="en__box-img">
     </div>
     <div class="en__box">
       <img src="<?php echo THEME_URL; ?>public/img/partnerzy/emro.png" alt="" class="en__box-img">
     </div>
   </div>
   <h3 class="en__title"></h3>
   <div class="contact__us">
     <h1 class="contact__us-title">Contact us</h1>
     <p class="contact__us-text">Do you have questions about our research to us or ideas you want to share? Contact us!</p>
     <div class="contact__us--wrapper">
       <div class="contact__us-box">
         <div class="contact__us-box--wrapper">
           <img src="<?php echo THEME_URL; ?>public/img/kontakt/amiotk-kontakt.png" alt="" class="contact__us-box__image">
           <div class="contact__us-box--details">
             <h3 class="contact__us-box__name">Anna</h3>
             <h4 class="contact__us-box__surname">Miotk</h4>
             <h5 class="contact__us-box__area">Media, universities and conferences </h5>
           </div>
       </div>
         <p class="contact__us-box__text">Do you need data from the Gemius/PBI research for article, broadcast of scientific research? Do you want to  invite our speaker to your conference or you are interested in our media patronage? Contact me.
         </p>
         <p class="contact__us-box__mail">a.miotk@pbi.org.pl</p>
       </div>
       <div class="contact__us-box">
         <div class="contact__us-box--wrapper">
           <img src="http://pbi.org.pl/wp-content/uploads/2017/01/pliszka-2.png" alt="" class="contact__us-box__image">
           <div class="contact__us-box--details">
             <h3 class="contact__us-box__name">Sławomir </h3>
             <h4 class="contact__us-box__surname">Pliszka</h4>
             <h5 class="contact__us-box__area">Data from research Gemius/PBI or archival data from Megapanel research</h5>
           </div>
       </div>
         <p class="contact__us-box__text">Do you want to buy data from the Gemius/PBI research or conduct a study? I will answer your questions</p>
         <p class="contact__us-box__mail">biuro@pbi.org.pl </p>
       </div>
       <div class="contact__us-box">
         <div class="contact__us-box--wrapper">
           <img src="<?php echo THEME_URL; ?>public/img/kontakt/msztyber-kontakt.png" alt="" class="contact__us-box__image">
           <div class="contact__us-box--details">
             <h3 class="contact__us-box__name">Monika</h3>
             <h4 class="contact__us-box__surname">Sztyber</h4>
             <h5 class="contact__us-box__area">Office</h5>
           </div>
         </div>
         <p class="contact__us-box__text">Do you need administrative help? <bR> Write to me.</p>
         <p class="contact__us-box__mail">m.sztyber@pbi.org.pl</p>
       </div>
   </div>
 </main>
 <div class="contact__us-map">
   <div class="contact__us-contact">
     <h1 class="contact__us-contact__title">Our address</h1>
     <div class="contact__us-contact__information">
       <p class="contact__us-contact__paragraph">
         Polskie Badania Internetu Sp. z o.o.<br>
         Al. Jerozolimskie 65/79, pok. 3.175<br>
         00-697 Warsaw, Poland<br>
         tel. (48) 22 630 72 68<br>
         fax. (48) 22 630 72 67<br>
         biuro@pbi.org.pl
       </p>
     </div>
   </div>
   <section id="map" class="map__img">
   </section>
   <div class="map">
     <div class="map__contact">
       <div class="map__contact-content">
         <div class="map__contact-left">
           <h1 class="map__contact-title">Our address</h1>
           <p class="map__contact-text">
             Polskie Badania Internetu Sp. z o.o.<br>
             Al. Jerozolimskie 65/79, pok. 3.175<br>
             00-697 Warsaw, Poland<br>
             tel. (48) 22 630 72 68<br>
             fax. (48) 22 630 72 67<br>
             biuro@pbi.org.pl
           </p>
         </div>
       </div>
     </div>
   </div>
 </div>
<?php get_footer(); ?>
