<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
  get_header('title'); the_post(); ?>

  <main class="single-article">
    <div class="single-article--wrapper">
      <article class="single-article__main">
          <h3 class="single-article__title">
            <?php the_title(); ?>
          </h3>
        <div class="single-article__details">
          <span class="single-article__author"><?php the_author(); ?>/
          </span>
          <span class="single-article__date"><?php the_date(); ?>/
          </span>
          <span class="single-article__tag"><?php foreach((get_the_category()) as $category) { echo $category->cat_name; } ?>
          </span>
          <!-- AddToAny BEGIN -->
          <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="padding: 10px 0;">
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_google_plus"></a>
            <a class="a2a_button_pinterest"></a>
            <a class="a2a_button_email"></a>
            <a class="a2a_button_linkedin"></a>
            <a class="a2a_button_tumblr"></a>
          </div>
          <script async src="https://static.addtoany.com/menu/page.js"></script>
          <!-- AddToAny END -->
        </div>
        <!-- <?php if(has_post_thumbnail()) : ?>
         <div class="single-article__image">
          <?php
            the_post_thumbnail('full', ['class' => 'single-article__image--intro']);
            $thumbnailArray = wp_get_attachment(get_post_thumbnail_id($post->ID));
          ?>
          <p class="single-article__image-author"><?php echo $thumbnailArray['description']; ?></p>
          </div>
        <?php endif; ?> -->
        <div class="single-article__body">
          <?php the_content(); ?>
        </div>
        <?php if (comments_open()) : ?>
          <div id="disqus_thread"></div>
          <script>

          (function() {
          var d = document, s = d.createElement('script');
          s.src = '//pbi-1.disqus.com/embed.js';
          s.setAttribute('data-timestamp', +new Date());
          (d.head || d.body).appendChild(s);
          })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

        <?php endif; ?>
      </article>
      <aside class="single-article__aside related-articles">
        <h3 class="related-articles__title">Powiązane teksty:</h3>
        <?php
          $news = new WP_Query(array(
              'post_type' => 'post',
              'posts_per_page' => 3
            ));
        ?>
        <?php while($news->have_posts()) : $news->the_post(); ?>
        <div class="related__article related-article__box">
          <a href="<?php the_permalink(); ?>"><h3 class="related-article__title"><?php the_title(); ?></h3></a>
          <div class="related-article__image">
            <?php the_post_thumbnail('large', ['class' => 'related-article__image-photo']); ?>
          </div>
        </div>
      <?php endwhile; ?>
      </aside>
    </div>
  </main>

  <?php get_footer(); ?>
