<main class="research-faq">
	<div class="research-faq__accordion-1">
		<div id="container">
			<section id="accordion">
				<div>
					<input type="checkbox" id="check-1">
					<label class="research-faq__title research-faq__title--1" for="check-1">
						Pomiar serwisów www - FAQ<i class="itranslate"></i></label>
					<article class="research-faq__article">
						<h3 class="research-faq__article-title">Co to jest pomiar widowni internetowej?</h3>
						<p class="research-faq__article-text">Pomiar widowni internetowej to jeden z typów badań audytoriów mediowych. Jest to badanie, w którym gromadzone są dane na temat zasięgu i oglądalności serwisów internetowych oraz dane socjo-demograficzne dotyczące widowni tych serwisów. Dane te można wykorzystywać przy tworzeniu ofert reklamowych oraz planowaniu i ocenie skuteczności kampanii reklamowych.  Pozwalają one na dokonywanie porównań, które nie byłoby możliwe, gdyby każdy właściciel serwisu www posługiwał się innym systemem pomiaru.</p>
					<h3 class="research-faq__article-title">Co oznacza, że badanie Gemius/PBI jest standardem pomiaru widowni internetowej na polskim rynku?</h3>
					<p class="research-faq__article-text">Badanie Gemius/PBI zostało stworzone przez PBI i Gemius w porozumieniu z przedstawicielami rynku internetowego (właściciele serwisów internetowych, domy mediowe, agencje reklamowe itd.) i jest stosowane przez podmioty rynku jako wspólne badanie. </p>
				<h3 class="research-faq__article-title">Dlaczego niektóre witryny nie są widoczne w wynikach badania?</h3>
				<p class="research-faq__article-text">Pomiar dokonywany jest za pośrednictwem aplikacji pomiarowej zainstalowanej na komputerach panelistów. W wynikach pojawiają się witryny  zgłoszone do badanie przez wydawców lub sieci reklamowe, lub takie, na których popularność wśród panelistów na tyle istotnie wzrasta w porównaniu z poprzednim miesiącem, że dodajemy je do badania z własnej inicjatywy.</p>
			<h3 class="research-faq__article-title">Jak duży musi być ruch na witrynie, aby była ona widoczna w wynikach badania Gemius/PBI?</h3>
			<p class="research-faq__article-text">W wynikach badania pokazywanie wyników jest uzależnione od liczby panelistów zanotowanych na danym węźle. Ten próg to 300 panelistów. Wyniki węzłów poniżej 300 panelistów pozostają ukryte (w wynikach pokazana jest „-„)</p>
		<h3 class="research-faq__article-title">Jak mierzycie korzystanie z aplikacji mobilnych?</h3>
		<p class="research-faq__article-text">Łączymy dwa rodzaje pomiaru: site-centric (aplikacje, które są oskryptowane za pomocą narzędzi firmy Gemius) oraz user-centric (aplikacje nieoskryptowane – estymacja wyników jedynie na podstawie tego, co na swoich smartfonach i tabletach uruchamiają członkowie panelu badawczego).</p>
		<h3 class="research-faq__article-title">Czym się różni wskaźnik Real Users od wskaźnika Unikalni użytkownicy (Unique Users)?</h3>
		<div class="research-faq__article-numbers--wrapper">
		<h3 class="research-faq__article-numbers--title">Różnice pomiędzy  wskaźnikami RU i UU są następujące:</h3>
		<ol>
			<li> <span><p>Real Users - określa liczbę osób (realnych użytkowników internetu</p>, a nie cookies, przeglądarek czy komputerów), które w danym miesiącu odwiedziły witrynę. </span></li>
			<li><span> <p>Unique Users (unikalni użytkownicy)</p> -  określa liczbę cookies, które w danym okresie zarejestrowano na witrynie. Cookie jest to mały plik, zapisywany przez stronę internetową na komputerze użytkownika internetu.</span> </li>
		</ol>
				</div>
		<p class="research-faq__article-text">Liczba realnych użytkowników (RU) nie odpowiada liczbie cookies (UU) z wielu powodów. Pierwszym z nich jest zjawisko "kasowalności cookies". Polega ono na tym, że niektórzy internauci świadomie bądź przypadkowo kasują co pewien czas zarejestrowane na swoich komputerach cookie. Są przez to rejestrowani wiele razy w badanym okresie. Przykładowo: internauta, który codziennie kasuje cookie na swoim komputerze, jest rozpoznawany w okresie jednego miesiąca jako trzydziestu różnych unique users czyli cookie. Innym powodem powstawania różnic pomiędzy liczbą realnych użytkowników (RU) a liczbą cookies (UU) na badanej witrynie jest współkorzystanie wielu użytkowników z tego samego komputera (tego samego cookie) oraz korzystanie z internetu przez tych samych użytkowników w wielu miejscach (korzystanie z wielu komputerów).</p>
		<h3 class="research-faq__article-title">Skąd biorą się różnice w liczbie użytkowników mojej witryny w Google Analytics i badaniu Gemius/PBI?</h3>
						<p class="research-faq__article-text">Przede wszystkim należy pamiętać , że liczba RU to nie to samo co wskaźnik UU w Google Analytics. Obecność witryny w wynikach badania zależy od liczby panelistów zanotowanych na danym węźle. Dane do wyników prezentowanych w badaniu Gemius/PBI czerpiemy ze skryptów pomiarowych Gemius. Różnice miedzy wynikami zliczonymi przez skrypty Gemius i a tymi z Google Analytics mogę wynikać z wielu powodów. Poniżej najistotniejsze: różne metodologie pomiaru samplowania danych (Google Analitics od 500 tys sesji., GemiusPrism od 1 mln zdarzeń). pomiary oparte na różnych typach cookies (GA – 1st party cookies, GP – 3rd party cookies), różnice w oskryptowaniu (np. nieoskryptowanie niektórych podstron którymś rodzajem skryptów).</p>
		<h3 class="research-faq__article-title"> Co oznacza pojęcie Realny Użytkownik?</h3>
						<p class="research-faq__article-text">Realny użytkownik jest odpowiednikiem internauty. Wskaźnik ten pozwala oszacować liczbę internautów odwiedzających daną witrynę. Pozbywamy się wówczas problemu kasowalności cookies i nieprzyjmowania cookies – co utrudnia analizę za pomocą narzędzi web analytics Dla każdej audytowanej witryny liczba RU jest wyznaczana w oparciu o zachowanie wszystkich użytkowników odwiedzających daną stronę.</p>
		<h3 class="research-faq__article-title"> Gdzie mogę obejrzeć dane demo?</h3>
		<a href="http://pbi.org.pl/badania/wyniki-badan/" class="research-faq__article-link">Demo</a>
						<p class="research-faq__article-text">Instrukcja jak obejrzeć dane demonstracyjne znajduję się tu: </p>
					<a href="http://www.audience.gemius.com/pl/wyniki-badania/demo-data/" class="research-faq__article-link">www.audience.gemius.com/pl/wyniki-badania/demo-data/</a>
		<h3 class="research-faq__article-title">    Co mi daje audyt?</h3>
			<p class="research-faq__article-text">Dane dla węzłów audytowanych bazują na danych site-centric (zgodność wskaźników typu odsłony i czas to 1%). Również dane soc-dem są oparte na  dużym panelu o liczności 130 tys panelistów. Dla węzłów nieaudytowanych jest to software-panel o liczności 11 tys. Dodatkowo dla węzłów objętych audytem w danych miesięcznych pokazywana jest liczba BrowserID – liczba unikalnych użytkowników zidentyfikowanych za pomocą cookiesa lub identyfikatora zapisywanego w przestrzeni local-storage przeglądarki. Dla węzłów audytowanych pokazywana jest również liczba odsłon i czas poniżej progu panelistów.</p>
			<h3 class="research-faq__article-title">Kiedy publikowane są dane z badania?</h3>
				<p class="research-faq__article-text">Dane dzienne za poprzedni dzień/dni publikowane są w dni robocze do godziny 14. Dane tygodniowe (poniedziałek-niedziela) publikowane są w pierwszy dzień roboczy po tygodniu badanym do godziny 14 Dane miesięczne publikowane są w ciągu 3 dni roboczych po 5 dniu miesiąca.</p>
				<h3 class="research-faq__article-title">    Jakie urządzenia badacie?</h3>
				<p class="research-faq__article-text">W wynikach badania pokazywany jest ruch z urządzeń typu PC, telefonów i tabletów. W panelu badawczym mamy użytkowników tych 3 typów urządzeń. Dane ze skryptów pomiarowych zbieramy ze wszystkich urządzeń, jednak w badaniu jest uwzględniany tylko ruch z wcześniej wymienionych.</p>
				<h3 class="research-faq__article-title">Czym się różni badanie Megapanel od obecnego badania Gemius/PBI?</h3>
				<p class="research-faq__article-text">Badanie Megapanel było prowadzone w latach 2004-2015. Obecne badanie Gemius/PBI jest prowadzone od stycznia 2016r. Obejmuje urządzienia typu PC i mobile (Megapanel tylko PC). Dostarcza dane dzienne, miesięczne i tygodniowe (Megapanel – dane miesięczne i tygodniowe). dane są dostępne krótko po okresie badanym (Metodologia badania Megapanel wymagała odczekania okresu 28 dni po badanym okresie do rozpoczęcia obliczeń)</p>
				<h3 class="research-faq__article-title"> Jakie wskaźniki można porównywać z badaniem Megapanel?</h3>
				<p class="research-faq__article-text">Badanie Gemius PBI jest badaniem dużo bardziej rozwiniętym. Obejmuje oprócz komputerów PC również urządzenia mobilne i dla tych urządzeń wyznaczane jest RU.</p>
				<table class="research-faq__table">
					<tr>
						<th>Badanie Gemius/PBI</th>
						<th>Megapanel</th>
					</tr>
					<tr>
						<td>odsłony na platformie PC</td>
						<td>odsłony krajowe niemobilne</td>
					</tr>
					<tr>
						<td>czas na platformie PC</td>
						<td>czas krajowy niemobilny</td>
					</tr>
					<tr>
						<td>odsłony na platformie Mobile</td>
						<td>odsłony krajowe mobilne</td>
					</tr>
					<tr>
						<td>czas na platformie Mobile</td>
						<td>czas krajowy mobilny</td>
					</tr>
					<tr>
						<td>RU danego węzła na PC / RU Internetu na PC - należy uwzględnić fakt, że od marca 2016 rozdzielony został ruch na platformie PC na work i home.</td>
						<td>zasięg</td>
					</tr>
				</table>
				<h3 class="research-faq__article-title">Jak otrzymać dostęp do wyników?</h3>
				<p class="research-faq__article-text">Prosimy o kontakt z wybraną osobą:</p>
				<a href="http://pbi.org.pl/badania/dane-kontaktowe/" class="research-faq__article-link research-faq__article-link--semibold">KONTAKT</a>
				<h3 class="research-faq__article-title">Jakie dane znajdę w wynikach?</h3>
				<a href="http://pbi.org.pl/badania/definicje-i-wskazniki/" class="research-faq__article-link research-faq__article-link--semibold">WSKAŹNIKI</a>
				<h3 class="research-faq__article-title"> Kto korzysta z wyników badania</h3>
				<p class="research-faq__article-text">Z wyników badania Gemius/PBi korzystają różnorodne podmioty związane z branżą internetową, reklamodawcy, agencje reklamowe i domy mediowe – do szacowania potencjału reklamowego stron www i planowania kampanii online; wydawcy, właściciele serwisów internetowych – do szacowania swojego udziału w rynku, porównywania się z konkurencją oraz tworzenia cenników reklamowych; działy marketingu i badawcze największych firm z różnych sektorów – do analiz rynkowych, porównań z konkurencją i oceny skuteczności działań marketingowych inwestorzy i konsultanci działający w środowisku startupowym – do analiz rynkowych i szacowania potencjału rozwiązań internetowych; naukowcy – w opracowaniach naukowych i projektach badawczych; media – w tworzeniu rzetelnych i wiarygodnych publikacji oraz wzmianek w mediach.</p>
				<h3 class="research-faq__article-title">Dlaczego tak długo się pobierają dane?</h3>
				<p class="research-faq__article-text">Pliki z wynikami mają znaczny rozmiar i pobranie ich na komputer użytkownika może zająć trochę czasu. Szybkość pobierania danych jest również uzależniona od jakości połączenia internetowego. Przy pierwszym korzystaniu z  aplikacji analitycznej, zalecamy zalogowanie się do widoku kalendarza i pozostawienia włączonego programu GemiusExplorer. Dane są pobierane w ustalonej kolejności – od najnowszych, od dziennych, przez tygodniowe, do miesięcznych.</p>
				<h3 class="research-faq__article-title">Czy mogę robić wykresy w aplikacji analitycznej?</h3>
				<p class="research-faq__article-text">Aplikacja analityczna umożliwia eksport danych w żądanym formacie (xls, csv ) – dzięki temu możliwe jest analizowanie i dowolna wizualizacja danych w specjalistycznym oprogramowaniu.</p>
				<h3 class="research-faq__article-title">Kto to jest panelista fuzyjny?</h3>
				<p class="research-faq__article-text">Do gromadzenia danych do badania korzystamy z różnych źródeł wiedzy o użytkownikach – paneli użytkowników różnych typów urządzeń (BrowserID-paneliści – osoby, którzy wypełnili ankietę rekrutacyjną, software-paneliści – osoby, które dodatkowo zainstalowały oprogramowanie pomiarowe). Bardzo ważny jest również panel kalibracyjny, którego uczestnicy pozwalają mierzyć swoją aktywność na wszystkich swoich urządzeniach. Każdy z tych zbiorów panelistów dostarcza inną część informacji do danych wynikowych. W procesie produkcji powstaje panel, który posiada wszystkie zebrane informacje: informacje o aktywności na węzłach nieaudytowanych, dużo dokładniejsze informacje o aktywności użytkowników węzłów audytowanych i informacje o współużytkowaniu urządzeń.</p>
				<h3 class="research-faq__article-title">Czy mogę robić wykresy w aplikacji analitycznej?</h3>
				<p class="research-faq__article-text">Aplikacja analityczna umożliwia eksport danych w żądanym formacie (xls, csv ) – dzięki temu możliwe jest analizowanie i dowolna wizualizacja danych w specjalistycznym oprogramowaniu.</p>
				<h3 class="research-faq__article-title">Dlaczego dla mojej strony widzę różne wyniki w badaniu Gemius/PBI i w narzędziu GemiusPrism?</h3>
				<p class="research-faq__article-text">W wynikach badania uwzględniamy tylko ruch krajowy z urządzeń typu PC, telefon, tablet. W GemiusPrism widoczny jest cały ruch zebrany za pomocą skryptów.</p>
	</article>
				</div>
			</section>
		</div>
		<div id="container">
			<section id="accordion">
				<div>
					<input type="checkbox" id="check-2">
					<label class="research-faq__title research-faq__title--2" for="check-2">Pomiar aplikacji mobilnych - FAQ<i class="itranslate"></i></label>
					<article class="research-faq__article">
						<h3 class="research-faq__article-title">Jak mierzycie korzystanie z aplikacji mobilnych?</h3>
						<p class="research-faq__article-text">Łączymy dwa rodzaje pomiaru: site-centric (aplikacje, które są oskryptowane za pomocą narzędzi firmy Gemius) oraz user-centric (aplikacje nieoskryptowane – estymacja wyników jedynie na podstawie tego, co na swoich smartfonach i tabletach uruchamiają członkowie panelu badawczego).</p>
						<h3 class="research-faq__article-title">Jakie aplikacje uwzględnia Wasz pomiar?</h3>
						<p class="research-faq__article-text">W pomiarze uwzględniamy aplikacje zgłoszone przez wydawców do pomiaru – zarówno oskryptowane jak i nieoskryptowane. Pomiarem objęte zostały również najpopularniejsze aplikacje nieoskryptowane, nawet jeżeli nie zostały zgłoszone do badania przez ich właściciela.</p>
						<h3 class="research-faq__article-title">Jakie warunki muszą być spełnione aby aplikacja była widoczna w wynikach badania?</h3>
						<p class="research-faq__article-text">Dane dla aplikacji są widoczne w wynikach badania, jeśli daną aplikację uruchomiło co najmniej 300 panelistów.</p>
						<h3 class="research-faq__article-title">Jaka jest definicja realnego użytkownika?</h3>
						<p class="research-faq__article-text">Realny użytkownik to taki, który choć raz skorzystał w danym okresie z aplikacji.</p>
						<h3 class="research-faq__article-title">Dlaczego liczba użytkowników w Waszym badaniu różni się czasem od liczby użytkowników aplikacji podawanej przez właściciela aplikacji?</h3>
						<div class="research-faq__article-numbers--wrapper">
							<h3 class="research-faq__article-numbers--title">Dla aplikacji audytowanych różnice mogą wynikać z faktu, że:</h3>
							<ol>
								<li><span> <p>Zliczania danych site-centric tylko dla wersji oskryptowanych</p>. Aplikacja może mieć wpięte kody badania tylko dla części systemów operacyjnych. Firma badawcza rekomenduje oskryptowanie wszystkich wersji obecnych na rynku, jednak to od wydawcy zależy , które wersje zostaną oskryptowane.</span></li>
								<li><span> <p>Dane wydawców to liczby urządzeń, </p> natomiast w wynikach badania na tej podstawie są estymowane liczby Realnych Użytkowników. Z naszego panelu wiemy, ile średnio osób korzysta z  danego typu urządzenia.</span></li>
								<li><span> <p>Propagacja </p> - zbieramy dane od użytkowników, którzy posiadają oskryptowaną wersję aplikacji. Na rynku funkcjonują jeszcze wersje nieoskryptowane. Odsetek starych, niezaktualizowanych aplikacji stopniowo będzie się zmniejszał.</span></li>
							</ol>
							<h3 class="research-faq__article-numbers--title">Dla aplikacji nieaudytowanych różnice mogą wynikać z tego że:</h3>
							<ol>
								<li><span> <p>Estymujemy wyniki na podstawie panelu badawczego.</p>Panelistą może zostać w tym momencie tylko użytkownik telefonu z systemem Android, więc estymujemy użytkowników tylko tego systemu.</span></li>
							</ol>
							<h3 class="research-faq__article-numbers--title">Dla obu rodzajów aplikacji różnice mogą wynikać też z tego że:</h3>
							<ol>
								<li><span style="font-weight: bold;">Uwzględniamy korzystanie</span> <span>z danej aplikacji tylko z terenu Polski a nie z zagranicy.</span></li>
								<li><span>Właściciele aplikacji mogą podawać różne miary różniące się od naszej definicji realnego użytkownika np. liczbę pobranych ze sklepu aplikacji lub liczbę zainstalowanych aplikacji (nawet jeżeli nie są w ogóle lub prawie w ogóle używane).</span></li>
							</ol>
							<!-- Normal text -->
							<h3 class="research-faq__article-title">Jakie systemy operacyjne urządzeń mobilnych uwzględniacie w pomiarze?</h3>
							<p class="research-faq__article-text">Dla aplikacji oskryptowanych uwzględniamy wszystkie najpopularniejsze systemy: Android, iOS i Windows Phone.
							Dla aplikacji nieoskryptowanych dane estymujemy na panelu internautów który stanowią posiadacze smartfonów i tabletów z systemem operacyjnym Android. To najpopularniejszy wśród użytkowników system – jak wynika z danych dla oskryptowanych witryn www 84 proc. użytkowników ma urządzenia z systemem Android, 12 proc. – z systemem iOS, a 4 proc. – z systemem Windows Phone. </p>
							<h3 class="research-faq__article-title">Czy może zdarzyć się tak,  że aplikacja działająca w tle albo tylko generująca jakiś transfer, bez świadomego uruchamiania jej przez użytkownika, jest uwzględniana w wynikach?</h3>
							<p class="research-faq__article-text">W przypadku aplikacji nieaudytowanych pomiar jest oparty na panelu. Tam gdzie jest to możliwe sprawdzana jest widoczność aplikacji na ekranie i tylko taka akcja jest liczona jako skorzystanie z aplikacji. Pozostaje niewielka część panelistów, w przypadku których nie ma technicznej możliwości sprawdzania widoczności aplikacji na ekranie – wówczas jako użycie uznawana jest obecność uruchomionego procesu aplikacji w pamięci urządzenia.</p>
							<h3 class="research-faq__article-title">W jaki sposób udostępniacie wyniki badania?</h3>
							<p class="research-faq__article-text">Wyniki naszego badania dostępne są dla posiadaczy licencji na oprogramowanie Gemius Explorer lub w formie odpłatnych raportów. Do celów niekomercyjnych (publikacje w mediach, dane na potrzeby artykułów tworzonych przez dziennikarzy) udostępniamy ranking top 20 aplikacji lub zestawienie, w którym znajduje się maksymalnie 20 aplikacji. </p>
						</div>
					</article>
				</div>
			</section>
		</div>
	</div>

</main>
