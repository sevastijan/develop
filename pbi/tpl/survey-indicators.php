<main class="research-indicators">
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--first">Definicje używane w badaniu Gemius/PBI</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-user.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Użytkownik (real user)</div>
			<div class="research-indicators__box-text">estymowana liczba osób, którzy wykonali w danym miesiącu przynajmniej jedną odsłonę w Internecie</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/odslona.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Odsłona</div>
			<div class="research-indicators__box-text">wczytanie dokumentu www z wybranej witryny internetowej widziane jako odwołanie do specjalnego skryptu badawczego udostępnianego przez Gemius SA.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-czas.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Czas</div>
			<div class="research-indicators__box-text">czas mierzony w sekundach pomiędzy odsłonami</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wizyta.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Wizyta</div>
			<div class="research-indicators__box-text">seria odsłon na danej witrynie, pomiędzy którymi nie wystąpiła przerwa dłuższa niż 30 minut</div>
		</div>
		</div>
	</div>
	</div>
	<!-- End of container -->
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title">Wskaźniki</h3>
		<h5 class="research-indicators__title--second">Wskaźniki podstawowe</h3>
			<div class="research-indicators__boxes">
				<div class="research-indicators__box">
					<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-user.svg" alt="" class="research-indicators__box-icon">
					<div class="research-indicators__box-title">Użytkownik (real user)</div>
					<div class="research-indicators__box-text">estymowana liczba internautów z wybranej grupy celowej, którzy dokonali przynajmniej jednej odsłony na wybranym węźle w danym miesiącu</div>
				</div>
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-odslony.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Odsłona</div>
					<div class="research-indicators__box-text">liczba odsłon wygenerowanych przez wybraną grupę celową na wybranych węzłach w wybranym okresie czasu</div>
				</div>
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-czas.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Czas</div>
					<div class="research-indicators__box-text">suma czasów spędzonych przez użytkowników (real user) na wybranym węźle; wskaźnik ten podawany jest w godzinach i obejmuje tylko czas trwania odsłon krajowych</div>
				</div>
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-zasieg.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Wizyta</div>
					<div class="research-indicators__box-text">stosunek liczby użytkowników, którzy dokonali przynajmniej jednej odsłony na wybranym węźle w wybranym okresie czasu do całkowitej liczby internautów w miesiącu, do którego należy wybrany okres czasu</div>
				</div>
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wizyty.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Wizyta</div>
					<div class="research-indicators__box-text">liczba wizyt wygenerowanych przez użytkowników na wybranych węzłach w określonym przedziale czasowym</div>
				</div>
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wezel.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Wizyta</div>
					<div class="research-indicators__box-text">rodzaj analizowanej treści – witryna, serwis, agregat lub grupa witryn, aplikacja</div>
				</div>
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-idwezla.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Wizyta</div>
					<div class="research-indicators__box-text">unikalny identyfikator węzła</div>
				</div>
			</div>
		</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Średnie</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-sredniaodslon.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Średnia liczba odsłon na użytkownika</div>
			<div class="research-indicators__box-text">liczba odsłon na wybranym węźle wygenerowana przez przeciętnego użytkownika z wybranej grupy celowej w wybranym okresie czasu</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-sredniczas.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Średni czas na użytkownika</div>
			<div class="research-indicators__box-text">całkowity czas jaki przeciętny użytkownik z wybranej grupy celowej spędził na wybranym węźle w wybranym okresie czasu</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-sredniczaswizyty.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Średni czas trwania wizyty</div>
			<div class="research-indicators__box-text">średni czas trwania wizyty dla wybranego węzła w określonym przedziale czasowym</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-sredniczasodslony.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Średni czas odsłony</div>
			<div class="research-indicators__box-text">średni czas pomiędzy dwoma odsłonami w ramach jednej wizyty wygenerowany przez wybraną grupę celową na wybranych węzłach w wybranym okresie czasu</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-sredniczasuruchomienia.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Średni czas uruchomienia na użytkownika</div>
			<div class="research-indicators__box-text">średni czas na użytkownika, w którym wybrana aplikacja była uruchomiona na komputerach użytkowników w danej grupie celowej w zdefiniowanym czasie</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-srednialiczbawizyt.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Średnia liczba wizyt na użytkownika</div>
			<div class="research-indicators__box-text">średnia liczba wizyt wygenerowanych przez użytkownika należącego do wybranej grupy celowej na wybranym węźle w określonym przedziale czasowym</div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Dopasowanie</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-dopasowanieuzytkownikow.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Dopasowanie użytkowników</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników grupy celowej do liczby wszystkich użytkowników na wybranym węźle w wybranym okresie czasu</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-dopasowanieodslon.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Dopasowanie odsłon</div>
			<div class="research-indicators__box-text">stosunek liczby odsłon wygenerowanych przez grupę celową do liczby odsłon wygenerowanych przez wszystkich użytkowników na wybranym węźle w wybranym okresie czasu</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-dopasowanieczasu.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Dopasowanie czasu</div>
			<div class="research-indicators__box-text">stosunek czasu, jaki użytkownicy z wybranej grupy celowej spędzili na wybranym węźle w określonym przedziale czasowym</div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Współoglądalność</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wspologladalnosc.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Współoglądalność</div>
			<div class="research-indicators__box-text">liczba użytkowników, którzy byli na każdym z wybranych węzłów w wybranym czasie</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wspologladalnosc2.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Współoglądalność %</div>
			<div class="research-indicators__box-text">udział liczby użytkowników, którzy byli na każdym z wybranych węzłów w wybranym okresie czasu, do liczby wszystkich użytkowników, którzy byli na wybranym węźle w wybranym okresie czasu</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-maxwspologladalnosc.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Maksymalna współoglądalność</div>
			<div class="research-indicators__box-text">liczba użytkowników w danej grupie celowej, którzy odwiedzili dany węzeł oraz przynajmniej jeden z pozostałych wybranych węzłów w zdefiniowanym czasie</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-maxwspologladalnosc2.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Maksymalna współoglądalność %</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników, którzy odwiedzili dany węzeł oraz przynajmniej jeden z pozostałych wybranych węzłów w zdefiniowanym czasie do liczby użytkowników, którzy odwiedzili dany węzeł w zdefiniowanym czasie</div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Udział</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-udzialuzytkownikow.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Udział użytkowników</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników grupy celowej danego węzła do liczby użytkowników grupy celowej wybranych węzłów w wybranym okresie czasu, podany w procentach</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-udzialodslon.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Udział odsłon </div>
			<div class="research-indicators__box-text">stosunek liczby odsłon wygenerowanych na wybranym węźle przez użytkowników z grupy celowej do liczby odsłon wygenerowanych na wybranych węzłach przez użytkowników grupy celowej w wybranym okresie czasu, podany w procentach</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-udzialczasu.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Udział czasu</div>
			<div class="research-indicators__box-text">stosunek czasu spędzonego przez grupę celową na wybranym węźle do łącznego czasu spędzonego na wybranych węzłach przez użytkowników grupy celowej w wybranym okresie czasu, podany w procentach</div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Affinity Index</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-affinityindex.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Affinity Index</div>
			<div class="research-indicators__box-text">stosunek wartości dopasowania użytkowników grupy celowej dla wybranego węzła, w określonym okresie do wartości dopasowania użytkowników grupy celowej dla całego internetu (wszystkich witryn objętych badaniem)
<br><br><span class="affinity-index__span">
Możliwe jest również przedstawienie affinity index względem grupy referencyjnej. Wskaźnik ten określony jest mianem „względne”
</span>
			</div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Wskaźniki dla aplikacji</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-uruchomioneaplikacje.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Uruchomiane aplikacje</div>
			<div class="research-indicators__box-text">
liczba użytkowników w danej grupie celowej, którzy mieli uruchomiony proces danej aplikacji w zdefiniowanym okresie.
			</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-useriaplikacje.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Użytkownicy i uruchomione aplikacje</div>
			<div class="research-indicators__box-text">
suma liczby użytkowników w danej grupie celowej, którzy odwiedzili wybrany węzeł w zdefiniowanym okresie oraz liczby użytkowników w danej grupie celowej, którzy mieli uruchomiony proces danej aplikacji w zdefiniowanym okresie
			</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-czasuruchomienia.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Czas uruchomienia</div>
			<div class="research-indicators__box-text">
czas, w którym wybrana aplikacja była uruchomiona na komputerach użytkowników w danej grupie celowej w zdefiniowanym okresie; wskaźnik ten jest wyrażony w godzinach
			</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-uruchomioneaplikacje2.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Uruchomiane aplikacje %</div>
			<div class="research-indicators__box-text">
stosunek liczby użytkowników z danej grupy celowej, którzy mieli uruchomiony proces danej aplikacji w zdefiniowanym okresie do liczby wszystkich internautów z danej grupy celowej
			</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-uzytkownicyiaplikacje2.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Użytkownicy i uruchomione aplikacje %</div>
			<div class="research-indicators__box-text">
stosunek sumy liczby użytkowników w danej grupie celowej, którzy odwiedzili wybrany węzeł w zdefiniowanym okresie oraz liczby użytkowników w danej grupie celowej, którzy mieli uruchomiony proces danej aplikacji w zdefiniowanym okresie, do liczby wszystkich internautów z danej grupy celowej
			</div>
		</div>
		</div>
	</div>
	</div>

</main>
