<main class="research-how">
		<h3 class="research-how__title">Łączymy pomiary user-centric i site-centric</h3>
		<div class="research-how__intro">
			<div class="research-how__intro--first">
				<div class="research-how__intro-container research-how__intro-container--title">
				<h1 class="research-how__intro-title">User-centric</h1>
			</div>
			<div class="research-how__intro-container research-how__intro-container--text">
				<p class="research-how__intro-text">zbiera informacje o zachowaniu internautów biorących udział w badaniu - odwiedzaniu witryn internetowych i korzystaniu z aplikacji. Dane na temat aktywności panelistów są zbierane za pomocą specjalnego oprogramowania raportującego NetPanel (software panel), zaś na witrynach audytowanych dodatkowo przy udziale skryptów zliczających (BID panel). Informacje z obu typów paneli łączone są w procesie fuzji, w wyniku której powstaje wspólny panel fuzyjny.</p>
			</div>
			</div>
			<div class="research-how__intro--second">
				<div class="research-how__intro-container research-how__intro-container--title">
				<h1 class="research-how__intro-title">Site-centric</h1>
				</div>
				<div class="research-how__intro-container research-how__intro-container--text">
				<p class="research-how__intro-text">(gemiusPrism) opiera się na technologii BID, pozwalającej na identyfikowanie użytkowników monitorowanych witryn bez naruszania ich prywatności. Dane uzyskiwane są za pomocą skryptów zliczających umieszczonych w kodzie html witryn audytowanych, czyli włączonych do badania. Informacje o liczbie użytkowników oraz dokonywanych przez nich odsłonach przesyłane są automatycznie do centrum obliczeniowego Gemius, gdzie następuje bieżąca aktualizacja wyników. Ponieważ badanie gemiusPrism (zbieranie informacji o ruchu na witrynach) jest pomiarem obejmującym pełną aktywność użytkowników audytowanych witryn (ma charakter spisowy), jest to metoda pozbawiona typowych błędów statystycznych badań na próbach. Dzięki temu wyniki Badania Gemius/PBI są dla witryn poddanych audytowi site-centric maksymalnie zbliżone do rzeczywistości pod względem liczby odsłon i spędzonego na witrynach czasu.</p>
				</div>
			</div>
		</div>
	<div class="research-how__content research-how__devices">
		<h3 class="research-how__title--secondary">Uwzględniamy typy urządzeń</h3>
		<div class="research-how__devices-boxes">
			<div class="research-how__devices-box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/pc.svg" alt="" class="research-how__devices-icon">
				<h5 class="research-how__devices-title">PC</h5>
			</div>
			<div class="research-how__devices-box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/mobile.svg" alt="" class="research-how__devices-icon">
				<h5 class="research-how__devices-title">Telefon</h5>
			</div>
			<div class="research-how__devices-box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/tablet.svg" alt="" class="research-how__devices-icon">
				<h5 class="research-how__devices-title">Tablet</h5>
			</div>
		</div>
	</div>
	<div class="research-how__content research-how__move">
		<h3 class="research-how__title--secondary">Ruch z PC dzielimy na ruch z pracy i ruch z domu</h3>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/pracadom.png" alt="" class="research-how__content-infographic">
	</div>
	<div class="research-how__content research-how__user">
		<h3 class="research-how__title--secondary">Rozpoznajemy tego samego użytkownika na różnych urządzeniach</h3>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/userdevices.png" alt="" class="research-how__content-infographic">
	</div>
	<div class="research-how__content research-how__reach">
		<h3 class="research-how__title--secondary">Dzięki temu pokazujemy wspólny zasięg poszczególnych witryn na wszystkich platformach</h3>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/zasieg.png" alt="" class="research-how__content-infographic">
	</div>
	<div class="research-how__content research-how__details">
		<h3 class="research-how__details-title">
			User-centric
		</h3>
		<p class="research-how__details-text">
			Dane o odwiedzanych witrynach/ używanych aplikacjach pochodzące od użytkowników internetu, którzy są uczestnikami panelu badawczego Gemius/PBI (ew. naszego panelu badawczego)
		</p>
		<h3 class="research-how__details-title">
			Site-centric
		</h3>
		<p class="research-how__details-text">
			Informacje pochodzące ze stron internetowych objętych audytem – dane obejmujące cały ruch na tych stronach  przesyłane do firmy badawczej za pomocą skryptów zliczających
		</p>
		<h3 class="research-how__details-title">
			BPS – Behavioral Panel Synthesis
		</h3>
		<p class="research-how__details-text">
			Panele na poszczególnych platformach mają część wspólną – panel kalibracyjny. To grupa osób, u których pomiarem objęte są wszystkie używane przez nich urządzenia
			<br><br>
			Na podstawie zachowania tych osób algorytm BPS  jest w stanie z dużym prawdopodobieństwem wskazać tych samych użytkowników na różnych urządzeniach. Dzięki temu możliwe jest pokazanie wspólnego zasięgu poszczególnych witryn na wszystkich platformach
		</p>
	</div>
</main>
