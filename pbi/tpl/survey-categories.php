<main class="categorisation">
		<div class="categorisation__intro">
			<div class="categorisation__title categorisation__title--trees">Struktura prezentowanych wyników</div>
			<p class="categorisation__text categorisation__text--trees">Wyniki prezentujemy za pomocą tzw. drzewek</p>
			<div class="trees">
				<p class="trees__text">Najważniejszym drzewkiem jest drzewko mediów. Ma ono określoną strukturze</p>
				<ul class="trees__list">
					<li class="trees__list-item"><span>Grupa</span></li>
					<li class="trees__list-item"><span>Witryna</span></li>
					<li class="trees__list-item"><span>Serwis</span></li>
				</ul>
					<p class="trees__text">Podstawową jednostką w drzewku jest witryna – to wszystkie podstrony dostępne w obrębie danej domeny.<br> Witryny mogą być połączone w grupy witryn mające jednego właściciela.
					Witryny mogą być połączone w grupy witryn mające jednego właściciela.<br><br>
					Serwis to część witryny stanowiąca spójną całość, możliwa do zidentyfikowania przez końcowego odbiorcę jako ten konkretny serwis. Nie każda witryna musi mieć wydzielone serwisy. Serwisy wydzielamy we współpracy z wydawcami. <br><br>
					Analizę wyników ułatwiają drzewa agregatów prezentujące węzły  pogrupowane według różnych kryteriów: <br>
					</p>
				<ul class="trees__list">
					<li class="trees__list-item"><span>Tematyka - drzewo tematyczne,</span> </li>
					<li class="trees__list-item"><span>Funkcja - drzewo funkcjonalne</span></li>
					<li class="trees__list-item"><span>Przynależność reklamowa - drzewo sieci reklamowych</span></li>
				</ul>
				<h3 class="trees__group-title">Drzewo tematyczne</h3>
				<p class="trees__group-text">Drzewo tematyczne odzwierciedla przynależność witryny do konkretnej kategorii tematycznej.</p>
				<h3 class="trees__group-title">Drzewo funkcjonalne</h3>
				<p class="trees__group-text">Drzewo funkcjonalne pokazuje przynależność witryny do konkretnej kategorii funkcjonalnej.  </p>
				<h3 class="trees__group-title">Drzewo sieci</h3>
				<p class="trees__group-text">Drzewo sieci ułatwia identyfikację do jakiej sieci  reklamowej należy dany węzeł. Pozwala określić potencjał reklamowy danej sieci.</p>
			</div>
		<h3 class="categorisation__title">Kategoryzacja</h3>
		<p class="categorisation__text">Dzielimy witryny na z góry określone kategorie tematyczne i funkcjonalne</p>
	</div>
	<div class="general-categories">
		<h3 class="general-categories__title">Ogólne kategorie tematyczne</h3>
		<div class="general-categories__boxes">
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/biznes.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Biznes, finanse, prawo</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/budownictwo.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Budownictwo,<br> nieruchomości</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/edukacja.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Edukacja</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/erotyka.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Erotyka</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/informacje.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Informacje i<br> publicystyka</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/kultura.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Kultura i rozrywka</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/motoryzacja.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Motoryzacja</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/technologie.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Nowe technologie</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/praca.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Praca</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/sport.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Sport</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/styl.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Styl życia</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/turystyka.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Turystyka</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/wielotematyczne.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Wielotematyczne</h5>
			</div>
		</div>
	</div>
	<div class="general-categories">
		<h3 class="general-categories__title">Ogólne kategorie funkcjonalne</h3>
		<div class="general-categories__boxes">
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/ecommerce.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">E-commerce</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/firmowe.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Firmowe</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/hosting.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Hosting</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/komunikacja.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Komunikacja</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/publiczne.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Publiczne</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/spolecznosci.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Społeczności</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/wyszukiwarki.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Wyszukiwarki, mapy i lokalizatory</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/serwisy.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Serwisy ogłoszeniowe</h5>
			</div>
			<div class="general-categories__box">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/funkcja.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title">Funkcja informacyjna</h5>
			</div>
		</div>

		<div class="categoristation__container">
			<h3 class="categorisation__title--secondary">Kategoryzujemy witryny i aplikacje według określonych zasad</h3>
			<p class="categorisation__paragraph">
				Witryny objęte przez nas badaniem przez nas kategoryzowane i grupowane według określonych zasad.<br> Zasady te zostały ustalone przez twórców badania i nie mogą być modyfikowane na bieżąco na potrzeby <br>konkretnych serwisów.
			</p>
				<ul class="categorisation__container-list__documents">
					<li class="categorisation__container-list__documents--item">
						<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Ogolne_zasady_kategoryzacji.pdf">Ogólne zasady kategoryzacji  stron i aplikacji</a>
					</li>
					<li class="categorisation__container-list__documents--item">
						<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Opis_kategorii_tematycznych_i_funkcjonalnych_w_badaniu_Gemius_PBI.pdf">Opis kategorii tematycznych i funkcjonalnych stron i aplikacji</a>
					</li>
					<li class="categorisation__container-list__documents--item">
						<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Kategoryzacja_materialow_audio_i_wideo_w_badaniu_Gemius_PBI.pdf">Kategoryzacja wideo/audio</a>
					</li>
				</ul>
			</div>

		<div class="categoristation__container">
			<h3 class="categorisation__title--grouping ">Grupowanie</h3>
			<h3 class="categorisation__title--light">Dokumenty dotyczące grupowania</h3>
			<ul class="categorisation__container-list__documents">
				<li class="categorisation__container-list__documents--item"><a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Regulamin_prowadzenia_Rejestru_Grup_od_1.08.2016.pdf">Regulamin grupowania</a></li>
				<li class="categorisation__container-list__documents--item"><a href="https://files.gemius.com/index.php/s/wzlQNRHMCw7Fkfw">Rejestr Grup</a></li>
				<li class="categorisation__container-list__documents--item"><a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Wniosek_o_wpis_do_Rejestru_wzor_od_1.08.2016.docx">Wniosek o utworzenie nowej grupy właścicielskiej</a></li>
				<li class="categorisation__container-list__documents--item"><a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Wniosek_aktualizacyjny.docx">Wniosek aktualizacyjny – dodanie do grupy dodatkowych domen lub aplikacji</a></li>
				<li class="categorisation__container-list__documents--item"><a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Wniosek_o_usuniecie_wpisu_w_Rejestrze.docx">Wniosek o usunięcie grupy właścicielskiej</a></li>
				<li class="categorisation__container-list__documents--item"><a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Zadanie_weryfikacji_decyzji.doc">Żądanie weryfikacji</a></li>
				<li class="categorisation__container-list__documents--item"><a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/OSWIADCZENIE_rozdzial_III_ust.1_pkt.a.docx">Oświadczenie z rozdziału 3 ust 1 pkt a</a></li>
				<li class="categorisation__container-list__documents--item"><a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/OSWIADCZENIE_rozdzial_III_ust.1_pkt.b.docx">Oświadczenie z rozdziału 3 ust 1 pkt b</a></li>
				<li class="categorisation__container-list__documents--item"><a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/OSWIADCZENIE_rozdzial_III_ust.1_pkt.c.docx">Oświadczenie z rozdziału 3 ust 1 pkt c</a></li>
			</ul>
		</div>
		<div class="categorisation__details">
			<h3 class="categorisation__details-title">Po co grupujemy i kategoryzujemy serwisy?</h3>
			<ul class="trees__list">
				<li class="trees__list-item"><span>by dokładnie odwierciedlić strukturę właścicielską rynku online w Polsce</span></li>
				<li class="trees__list-item"><span>aby przedstawić dane danych o łącznej oglądalności witryn, aplikacji czy playerów należących do jednego wydawcy lub jednej grupy wydawców</span></li>
				<li class="trees__list-item"><span>by wydawcy mogli oszacować potencjał reklamowy swoich zasobów</span></li>
				<li class="trees__list-item"><span>aby reklamodawcy mogli łatwiej planować kampanie online</span></li>
			</ul>
		</div>
		<!-- <div class="categoristation__container">
			<h3 class="categorisation__paragraph--big">Jak dołączyć do badania Gemius/PBI?</h3>
		</div>
		<div class="general-categories__boxes">
			<div class="general-categories__box general-categories__box--last">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/biznes.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title general-categories__box-title--last">Jestem właścicielem serwisu internetowego</h5>
			</div>
			<div class="general-categories__box general-categories__box--last">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/budownictwo.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title general-categories__box-title--last">Jestem właścicielem aplikacji</h5>
			</div>
			<div class="general-categories__box general-categories__box--last">
				<img src="<?php echo THEME_URL; ?>/public/img/badanie/kategoria.svg" alt="" class="general-categories__box-icon">
				<h5 class="general-categories__box-title general-categories__box-title--last">Jestem właścicielem treści wideo/audio</h5>
			</div>
		</div> -->
	</div>
</main>
