<main class="research-indicators">
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--first">Definicje używane w badaniu Gemius/PBI</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-user.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Użytkownik (real user)</div>
			<div class="research-indicators__box-text">estymowana liczba osób, którzy wykonali w danym miesiącu przynajmniej jedną odsłonę w Internecie</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/odslona.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Odsłona</div>
			<div class="research-indicators__box-text">wczytanie dokumentu www z wybranej witryny internetowej widziane jako odwołanie do specjalnego skryptu badawczego udostępnianego przez Gemius SA.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-czas.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Czas</div>
			<div class="research-indicators__box-text">czas mierzony w sekundach pomiędzy odsłonami</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wizyta.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Wizyta</div>
			<div class="research-indicators__box-text">seria odsłon na danej witrynie, pomiędzy którymi nie wystąpiła przerwa dłuższa niż 30 minut</div>
		</div>
    <div class="research-indicators__box">
    <img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-odslony.svg" alt="" class="research-indicators__box-icon">
    <div class="research-indicators__box-title">Unikalne identyfikatory przeglądarek</div>
      <div class="research-indicators__box-text">Iiczba unikalnych identyfikatorów przeglądarek, które odwiedziły dany węzeł w wybranym okresie</div> 
    </div>
		</div>
	</div>
	</div>
	<!-- End of container -->
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title">Wskaźniki</h3>
		<h5 class="research-indicators__title--second">Wskaźniki podstawowe</h3>
			<div class="research-indicators__boxes">
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-czas.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Czas</div>
					<div class="research-indicators__box-text">czas spędzony przez użytkowników w danej grupie celowej na wybranym węźle(ach) w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w godzinach.</div>
				</div>
        <div class="research-indicators__box">
          <img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-odslony.svg" alt="" class="research-indicators__box-icon">
          <div class="research-indicators__box-title">Odsłony</div>
          <div class="research-indicators__box-text">liczba zdarzeń polegających na wyświetleniu przez użytkowników w zdefiniowanym okresie, strony w przeglądarce internetowej lub nowych treści w aplikacji internetowej.</div>
        </div>
        <div class="research-indicators__box">
          <img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-user.svg" alt="" class="research-indicators__box-icon">
          <div class="research-indicators__box-title">Użytkownicy (real users)</div>
          <div class="research-indicators__box-text">liczba użytkowników Internetu w danej grupie celowej, którzy odwiedzili (wygenerowali co najmniej jedną odsłonę) wybrany węzeł(y) w zdefiniowanym okresie. Wskaźnik ten odnosi się do rzeczywistej liczby osób, a nie komputerów, plików cookie czy adresów IP.</div>
        </div>
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-zasieg.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Wizyty</div>
					<div class="research-indicators__box-text">Wizyty – Liczba wizyt wygenerowanycch przez użytkowników z danej grupy celowej na danym węźle (węzłach) w zdefiniowanym okresie. Wizyta to  seria odsłon na danej domenie, pomiędzy którymi nie wystąpiła przerwa dłuższa niż 30 minut.</div>
				</div>
				<div class="research-indicators__box">
				<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wizyty.svg" alt="" class="research-indicators__box-icon">
				<div class="research-indicators__box-title">Zasięg wśród internautów</div>
					<div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy odwiedzili wybrany węzeł(y) w zdefiniowanym okresie do liczby wszystkich użytkowników Internetu w danej grupie celowej, w danym miesiącu. Wskaźnik ten wyrażony jest w procentach. </div>
				</div>
			</div>
		</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Średnie</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-sredniczas.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Średni czas na użytkownika</div>
			<div class="research-indicators__box-text">średni czas spędzony przez użytkownika w danej grupie celowej na wybranym węźle(ach) w zdefiniowanym okresie. </div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-sredniczasodslony.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Średni czas trwania odsłony</div>
			<div class="research-indicators__box-text">średni czas pomiędzy dwiema kolejnymi odsłonami wygenerowanymi przez użytkowników w danej grupie celowej na wybranym węźle(ach) w zdefiniowanym okresie. </div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-sredniczasuruchomienia.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Średni czas uruchomienia na aplikację</div>
			<div class="research-indicators__box-text">średni czas na każdą aktywną aplikację, w którym wybrana aplikacja(e) była aktywna (uruchomiona) na komputerach użytkowników w danej grupie celowej w zdefiniowanym okresie.</div>
		</div>
		<div class="research-indicators__box">
  		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-srednialiczbawizyt.svg" alt="" class="research-indicators__box-icon">
  		<div class="research-indicators__box-title">Średnia liczba wizyt na użytkownika</div>
  			<div class="research-indicators__box-text">średnia liczba wizyt przechodzących przez wybrany węzeł(y), wygenerowanych przez użytkownika w danej grupie celowej w zdefiniowanym okresie. </div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Dopasowanie</h3>
	<div class="research-indicators__boxes">
    <div class="research-indicators__box">
      <img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-dopasowanieczasu.svg" alt="" class="research-indicators__box-icon">
      <div class="research-indicators__box-title">Dopasowanie czasu</div>
      <div class="research-indicators__box-text">stosunek czasu spędzonego przez użytkowników w danej grupie celowej na wybranym węźle (węzłach) w zdefiniowanym okresie do całkowitego czasu spędzonego przez użytkowników na wybranym węźle(ach) w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
    </div>
    <div class="research-indicators__box">
      <img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-dopasowanieczasu.svg" alt="" class="research-indicators__box-icon">
      <div class="research-indicators__box-title">Dopasowanie czasu (względne)</div>
      <div class="research-indicators__box-text">stosunek czasu spędzonego przez użytkowników w danej grupie celowej na wybranym węźle(ach) w zdefiniowanym okresie do całkowitego czasu spędzonego przez użytkowników w zdefiniowanej grupy referencyjnej na wybranym węźle(ach) w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
    </div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-dopasowanieodslon.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Dopasowanie odsłon</div>
			<div class="research-indicators__box-text">stosunek liczby odsłon wygenerowanych przez użytkowników w danej grupie celowej na wybranym węźle (węzłach) w zdefiniowanym okresie do całkowitej liczby odsłon wygenerowanych przez użytkowników na wybranym węźle(ach) w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-dopasowanieodslon.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Dopasowanie odsłon (względne)</div>
			<div class="research-indicators__box-text">stosunek liczby odsłon wygenerowanych przez użytkowników w danej grupie celowej na wybranym węźle(ach) w zdefiniowanym okresie do całkowitej liczby odsłon wygenerowanych przez użytkowników w zdefiniowanej grupie referencyjnej na wybranym węźle(ach) w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.  </div>
		</div>
    <div class="research-indicators__box">
      <img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-dopasowanieuzytkownikow.svg" alt="" class="research-indicators__box-icon">
      <div class="research-indicators__box-title">Dopasowanie użytkowników (względne)</div>
      <div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy odwiedzili wybrany węzeł(y) w zdefiniowanym okresie do całkowitej liczby użytkowników w zdefiniowanej grupie referencyjnej, którzy odwiedzili wybrany węzeł(y) w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
    </div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Współoglądalność</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wspologladalnosc.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Współoglądalność</div>
			<div class="research-indicators__box-text">liczba użytkowników w danej grupie celowej, którzy odwiedzili każdy z zaznaczonych węzłów w zdefiniowanym okresie.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-wspologladalnosc2.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Współoglądalność %</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy odwiedzili każdy z zaznaczonych węzłów w zdefiniowanym okresie do liczby użytkowników w danej grupie celowej, którzy odwiedzili dany węzeł w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-maxwspologladalnosc.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Maksymalna współoglądalność</div>
			<div class="research-indicators__box-text">liczba użytkowników z danej grupy celowej, którzy odwiedzili wybrany węzeł w określonym przedziale czasowym, oraz co najmniej jeden z pozostałych wybranych węzłów.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-maxwspologladalnosc2.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Maksymalna współoglądalność %</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy odwiedzili dany węzeł oraz przynajmniej jeden z pozostałych zaznaczonych węzłów w zdefiniowanym okresie do liczby użytkowników w danej grupie celowej, którzy odwiedzili dany węzeł w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-maxwspologladalnosc.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Współoglądalność (H%)</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy odwiedzili węzły wybrane w danym wierszu i kolumnie w zdefiniowanym okresie do liczby użytkowników w danej grupie celowej, którzy odwiedzili węzeł wybrany w danym wierszu w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-maxwspologladalnosc2.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Współoglądalność (V%)</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy odwiedzili węzły wybrane w danym wierszu i kolumnie w zdefiniowanym okresie do liczby użytkowników w danej grupie celowej, którzy odwiedzili węzeł wybrany w danej kolumnie w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Udział</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-udzialuzytkownikow.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Udział użytkowników</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy odwiedzili dany węzeł w zdefiniowanym okresie do liczby użytkowników w danej grupie celowej, którzy odwiedzili co najmniej jeden z wybranych węzłów w zdefiniowanym okresie. Wskaźnik te wyrażony jest w procentach.</div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-udzialodslon.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Udział odsłon </div>
			<div class="research-indicators__box-text">stosunek liczby odsłon wygenerowanych przez użytkowników w danej grupie celowej na danym węźle w zdefiniowanym okresie do liczby odsłon wygenerowanych przez użytkowników w danej grupie celowej na wszystkich wybranych węzłach w zdefiniowanym okresie. Wskaźnik te wyrażony jest w procentach. </div>
		</div>
		<div class="research-indicators__box">
		<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-udzialczasu.svg" alt="" class="research-indicators__box-icon">
		<div class="research-indicators__box-title">Udział czasu</div>
			<div class="research-indicators__box-text">stosunek czasu spędzonego przez użytkowników w danej grupie celowej na danym węźle w zdefiniowanym okresie do czasu spędzonego przez użytkowników w danej grupie celowej na wszystkich wybranych węzłach w zdefiniowanym okresie. Wskaźnik te wyrażony jest w procentach.</div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Affinity Index</h3>
	<div class="research-indicators__boxes">
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-affinityindex.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Affinity Index wśród internautów</div>
			<div class="research-indicators__box-text">stosunek wartości dopasowania użytkowników w danej grupie celowej, dla wybranego węzła(ów), w zdefiniowanym okresie do wartości dopasowania użytkowników w danej grupie celowej, dla całego Internetu (wszystkich witryn objętych badaniem), w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.
			</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-affinityindex.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Affinity Index wśród internautów (względne)</div>
			<div class="research-indicators__box-text">stosunek wartości względnego dopasowania użytkowników w danej grupie celowej, dla wybranego węzła(ów), w zdefiniowanym okresie do wartości względnego dopasowania użytkowników w danej grupie celowej, dla całego Internetu (wszystkich witryn objętych badaniem), w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.
			</div>
		</div>
		</div>
	</div>
	</div>
	<div class="research-indicators__container">
		<div class="research-indicators__container--wrapper">
		<h3 class="research-indicators__title--small">Wskaźniki dla aplikacji</h3>
	<div class="research-indicators__boxes">
    <div class="research-indicators__box">
      <img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-czasuruchomienia.svg" alt="" class="research-indicators__box-icon">
      <div class="research-indicators__box-title">Czas uruchomienia</div>
      <div class="research-indicators__box-text">
        czas, w którym wybrana aplikacja(e) była aktywna (uruchomiona) na komputerach użytkowników w danej grupie celowej w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w godzinach.
      </div>
    </div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-uruchomioneaplikacje.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Uruchomione aplikacje</div>
			<div class="research-indicators__box-text">liczba użytkowników w danej grupie celowej, którzy mieli uruchomiony proces(y) danej(ych) aplikacji w zdefiniowanym okresie.</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-uruchomioneaplikacje2.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Uruchomione aplikacje %</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy mieli uruchomiony proces(y) danej(ch) aplikacji w zdefiniowanym okresie, do liczby wszystkich użytkowników Internetu w danej grupie celowej, w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-useriaplikacje.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Użytkownicy i uruchomione aplikacje</div>
			<div class="research-indicators__box-text">liczba użytkowników w danej grupie celowej, którzy w zdefiniowanym okresie odwiedzili wybrane węzły lub mieli uruchomione procesy wybranych aplikacji.
			</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-uruchomioneaplikacje2.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Uruchomiane aplikacje %</div>
			<div class="research-indicators__box-text">stosunek liczby użytkowników w danej grupie celowej, którzy w zdefiniowanym okresie odwiedzili wybrane węzły lub mieli uruchomione procesy wybranych aplikacji, do liczby wszystkich użytkowników Internetu w danej grupie celowej, w zdefiniowanym okresie. Wskaźnik ten wyrażony jest w procentach.
			</div>
		</div>
		<div class="research-indicators__box">
			<img src="<?php echo THEME_URL; ?>public/img/badanie/indicator-uzytkownicyiaplikacje2.svg" alt="" class="research-indicators__box-icon">
			<div class="research-indicators__box-title">Użytkownicy i uruchomione aplikacje %</div>
			<div class="research-indicators__box-text">
stosunek sumy liczby użytkowników w danej grupie celowej, którzy odwiedzili wybrany węzeł w zdefiniowanym okresie oraz liczby użytkowników w danej grupie celowej, którzy mieli uruchomiony proces danej aplikacji w zdefiniowanym okresie, do liczby wszystkich internautów z danej grupy celowej
			</div>
		</div>
		</div>
	</div>
	</div>

</main>
