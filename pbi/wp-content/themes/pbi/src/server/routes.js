module.exports = (app) => {
  app.get('/', function (req, res) {
    res.render('index')
  })
  app.get('/raporty', function (req, res) {
    res.render('sub-raporty')
  })
  app.get('/raporty/raport', function (req, res) {
    res.render('sub-reports-single-report')
  })
  app.get('/badania', function (req, res) {
    res.render('sub-badania')
  })
  app.get('/akademia/lekcje', function (req, res) {
    res.render('sub-akademia-lekcje')
  })
  app.get('/akademia/prezentacje', function (req, res) {
    res.render('sub-akademia-prezentacje')
  })
  app.get('/akademia/wystapienie', function (req, res) {
    res.render('sub-akademia-wystapienie')
  })
  app.get('/gemius', function (req, res) {
    res.render('sub-gemius')
  })
  app.get('/pbi', function (req, res) {
    res.render('sub-pbi')
  })
  app.get('/aktualnosci', function (req, res) {
    res.render('sub-news')
  })
  app.get('/media', function (req, res) {
    res.render('sub-media-for-press')
  })
  app.get('/media/download', function (req, res) {
    res.render('sub-media-download')
  })
  app.get('/aktualnosci/artykul', function (req, res) {
    res.render('sub-news-single-article')
  })
  app.get('/kontakt', function (req, res) {
    res.render('sub-contact')
  })
}
