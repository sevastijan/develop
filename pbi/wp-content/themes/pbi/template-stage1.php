<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Szkolenia poziom 1
 * @package WordPress
 *
 */

 get_header('title');  ?>

 <main class="program">
 	<div class="program__intro-left">
 		<h3 class="program__title">Stopień I</h3>
 		<p class="program__text">
 			Poziom podstawowy – dla tych, którzy dopiero chcą się nauczyd posługiwania się badaniem widowni
 internetowej
 		</p>
 		<div class="training__info">
 			<h3 class="panel__title">Zakres szkolenia</h3>
 			<ul class="panel__list">
 				<li class="panel__list-item"><span>Aktualne trendy w polskim internecie</span></li>
 				<li class="panel__list-item"><span>Standard pomiaru widowni internetowej</span></li>
 				<li class="panel__list-item"><span>Metodologia badania</span></li>
 				<li class="panel__list-item"><span>Najważniejsze wskaźniki- definicje,
 znaczenie, interpretacja</span></li>
 				<li class="panel__list-item"><span>Przykładowe analizy</span></li>
 				<li class="panel__list-item"><span>Przykładowe zastosowania wyników
 badania w praktyce</span></li>
 			</ul>
 			<h3 class="training__price"> CENA: </h3>
 			<p class="training__price-text">Cena: 200 PLN/osoba </p>
 			<p class="training__price-text">Klienci firmy Gemius w danym roku kalendarzowym – rabat 100 proc. </p>
 		</div>
 		<?php echo do_shortcode('[contact-form-7 id="968" title="Formularz 1"]'); ?>
 		</div>
 	</div>
 </main>

  <?php get_footer(); ?>
