<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Form
 * @package WordPress
 *
 */
get_header('title'); the_post(); ?>
	<main>
		<section class="form-container">
			<div class="toggle__nav">
				<input id="toggle__checkbox" type="checkbox">
				<label for="toggle__checkbox"><span>Menu Toggle</span></label>
				<?php bem_menu('sub_gemius', 'toggle__list'); ?>
			</div>
			<div class="aside-nav">
				<?php bem_menu('sub_gemius', 'aside-nav'); ?>
			</div>
			<div class="form-container--wrapper">
				<?php the_content(); ?>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
