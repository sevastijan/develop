<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Gemius Explorer
 * @package WordPress
 *
 */

 get_header('title'); the_post();?>

 <main>
   <section class="gemius">
     <div class="toggle__nav">
       <input id="toggle__checkbox" type="checkbox">
       <label for="toggle__checkbox">
         <span>Menu Toggle</span>
       </label>
       <?php bem_menu('sub_gemius', 'toggle__list'); ?>
     </div>
     <div class="aside-nav">
       <?php bem_menu('sub_gemius', 'aside-nav'); ?>
     </div>
     <div class="gemius-content">
         <div class="gemius-intro__content">
           <div class="gemius-content--wrapper">
           <div class="single-report__main-paragraph" style="padding-top: 60px;">
             Dane z badania Gemius/PBI są dostępne również w formie raportów. To propozycja dla tych, którzy nie potrzebują regularnego dostępu do pełnej wersji badania, a chcieliby otrzymać zestaw wybranych danych. Nasze raporty mają format plików xls z danymi – przykłady można zobaczyć poniżej. W razie dodatkowych pytań zapraszamy do kontaktu!
           </div>
           <h1 class="gemius-content__main-title">Badanie Gemius PBI</h1>
           <div class="gemius-box gemius-box--wsparcie">
             <img src="<?php echo THEME_URL; ?>public/img/gemius-box--1.svg" alt="" class="gemius-box__icon gemius-box__icon--1">
             <img src="<?php echo THEME_URL; ?>public/img/gemius-explorer/gemius-box-1--black.svg" alt="" class="gemius-box__icon--mobile gemius-box__icon--1">
             <h3 class="gemius-box__title">Wsparcie Twoich <br> decyzji biznesowych</h3>
           </div>
           <div class="gemius-box gemius-box--wiarygodne">
             <img src="<?php echo THEME_URL; ?>public/img/gemius-box--2.svg" alt="" class="gemius-box__icon gemius-box__icon--2">
             <img src="<?php echo THEME_URL; ?>public/img/gemius-explorer/gemius-box-2--black.svg" alt="" class="gemius-box__icon--mobile gemius-box__icon--2">
             <h3 class="gemius-box__title">Wiarygodne <br> i rzetelne dane</h3>
           </div>
           <div class="gemius-box gemius-box--standard">
             <img src="<?php echo THEME_URL; ?>public/img/gemius-box--3.svg" alt="" class="gemius-box__icon gemius-box__icon--3">
             <img src="<?php echo THEME_URL; ?>public/img/gemius-explorer/gemius-box-3--black.svg" alt="" class="gemius-box__icon--mobile gemius-box__icon--3">
             <h3 class="gemius-box__title">Standard pomiaru <br> widowni internetowej</h3>
           </div>
         </div>
       </div>
       <div class="gemius-possibilities">
         <div class="gemius-possibilities__title"><?php the_field('out_capabilities_headline'); ?></div>
         <div class="gemius-possibilities__content">
           <div class="gemius-possibilities__content--wrapper">
             <?php if( have_rows('out_capabilities') ): while ( have_rows('out_capabilities') ) : the_row(); ?>
             <div class="gemius-possibilities__box">
               <div class="gemius-possibilities__box-content">
                 <h3 class="gemius-possibilities__box-title"><?php the_sub_field('capabilities'); ?></h3>
               </div>
             </div>
             <?php endwhile; endif; ?>
           </div>
         </div>
       </div>
       <div class="gemius-promoted" style="height: auto;">
         <div class="gemius-promoted--wrapper">
           <div class="gemius-promoted__box">
             <div class="gemius-promoted__content">
               <img src="<?php echo THEME_URL; ?>public/img/gemius-promocircle.png" alt="" class="gemius-promoted__content-img">
               <img src="<?php echo THEME_URL; ?>public/img/gemius-explorer/gemius-yellow-circle.png" alt="" class="gemius-promoted__content-img--mobile">
               <h3 class="gemius-promoted__content-title"><?php the_field('our_promoted_headline'); ?></h3>
               <p class="gemius-promoted__content-text">
                 <?php $index = 1; if( have_rows('out_promoted') ): while ( have_rows('out_promoted') ) : the_row(); ?>
                 <div>
                   <span class="gemius-promoted__content-text--line"><span class="gemius-promoted__content-text--number"><?php echo $index; ?>.</span> <?php the_sub_field('promoted'); ?></span>
                    <span style="font-size: 13px; display: block; padding-left: 26px;"><?php the_sub_field('descr'); ?></span>
                    <?php if(get_sub_field('plik')) : ?>
                      <span style="font-size: 12px; padding-left: 26px;" class="small">Przykładowy raport > <a href="<?php the_sub_field('plik'); ?>">Pobierz</a></span>
                    <?php endif; ?>
                    </div>
                <?php $index++; endwhile; endif; ?>
                 </p>
             </div>
           </div>
         </div>
       </div>
       <div class="gemius-steps">
         <h1 class="gemius-steps__title">Cztery proste kroki </h1>
         <h3 class="gemius-steps__subtitle">do uzyskania raportu z danymi z badania Gemius/PBI</h3>
         <div class="gemius-steps__content">
           <?php $index = 1; if( have_rows('steps') ): while ( have_rows('steps') ) : the_row(); ?>
             <div class="gemius-steps__content-box">
               <h3 class="gemius-steps__content-title">0<?php echo $index; ?></h3>
               <img src="<?php echo THEME_URL; ?>public/img/gemius-explorer/gemius-arrow-right.svg" alt="" class="gemius-steps__content-arrow">
               <p class="gemius-steps__content-text"><?php the_sub_field('step'); ?></p>
             </div>
           <?php $index++; endwhile; endif;?>
         </div>
         <div class="gemius-steps__content-send">
           <a href="mailto:biuro@pbi.org.pl" class="gemius-steps__content-button">
             <span>Wyślij zapytanie</span>
           </a>
         </div>
       </div>
     </div>
   </section>
 </main>

<?php get_footer(); ?>
