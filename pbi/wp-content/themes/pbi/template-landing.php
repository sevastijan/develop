<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Landing
 * @package WordPress
 *
 */

 get_header('title'); the_post(); ?>

<main class="panel">
 <div class="panel--wrapper">
 <p class="panel-intro__text">Przyczyń się do rozwijania wiedzy na temat polskiego internetu. Weź udział w badaniu Gemius/PBI. Zostań uczestnikiem największego badania internetowego w Polsce.</p>
 <h3 class="panel__title">Badanie Gemius/PBI:</h3>
 <ul class="panel__list">
   <li class="panel__list-item"><span>Największe w Polsce badanie internetu.</span></li>
   <li class="panel__list-item"><span>Standard pomiaru widowni internetowej - umożliwia porównywanie ruchu w różnych witrynach www.</span></li>
   <li class="panel__list-item"><span>Ponad 8000 badanych stron.</span></li>
   <li class="panel__list-item"><span>Innowacyjny pomiar aplikacji mobilnych.</span></li>
 </ul>
 <h3 class="panel__title">Odbiorcy badania Gemius/PBI</h3>
 <ul class="panel__list">
   <li class="panel__list-item"><span>Właściciele serwisów internetowych,</span></li>
   <li class="panel__list-item"><span> Reklamodawcy i podmioty obsługujące reklamodawców,</span></li>
   <li class="panel__list-item"><span>Dziennikarze,</span></li>
   <li class="panel__list-item"><span>Naukowcy,</span></li>
   <li class="panel__list-item"><span>My wszyscy -  ciekawi świata internauci.</span></li>
 </ul>
 <h3 class="panel__title">Jak to działa:</h3>
 <ul class="panel__list">
   <li class="panel__list-item"><span>Łączymy pomiar ruchu na stronach www (site-centric) i zachowań panelistów (user-centric)</span></li>
   <li class="panel__list-item"><span>Dane, które gromadzimy to adresy stron www (czas spędzony na stronie).</span></li>
   <li class="panel__list-item"><span>Twoje dane pozostają anonimowe – nie wiemy jaka konkretna osoba jakie strony odwiedza. Zebrane dane używane są jedynie w zbiorczych zestawieniach.</span></li>
   <li class="panel__list-item"><span>Na adres e-mail, który podajesz przy rejestracji, wysyłamy tylko informacje związane z badaniem.</span></li>
   <li class="panel__list-item"><span>W podziękowaniu  za uczestnictwo w badaniu organizujemy konkursy z atrakcyjnymi nagrodami.</span></li>
   <li class="panel__list-item"><span>W razie potrzeby zapewniamy pomoc techniczną i merytoryczną drogą mailową i telefoniczną.</span></li>
 </ul>
 <h3 class="panel__title">Kim są Paneliści biorący udział w badaniu?</h3>
 <ul class="panel__list">
 <li class="panel__list-item"><span>Internauci, którzy wypełnili ankietę i zarejestrowali się jako uczestnicy badania (tzw. cookie-paneliści). </span></li>
 <li class="panel__list-item"><span> Paneliści posiadający zainstalowany program netPanel (tzw. software-paneliści).</span></li>
 </ul>
 <h3 class="panel__title">Jak wygląda proces rejestracji?</h3>
 <ul class="panel__list--numbers">
   <li class="panel__list-item"><span><p>01.</p>Kliknięciem odpowiadasz na nasze zaproszenie do udziału w badaniu Gemius/PBI, które wyświetla się losowo wybranym Internautom w wielu miejscach w internecie.</span></li>
   <li class="panel__list-item"><span><p>02.</p> Wypełniasz formularz rejestracyjny.</span>
     <ul class="panel__list-inner"><li><span>W tym momencie zostajesz tzw.  cookie-panelistą.</span></li></ul>
   </li>
   <li class="panel__list-item"><span><p>03.</p>Po wypełnieniu formularza, będziesz mógł zainstalować program netPanel.</span></li>
   <li class="panel__list-item"><span><p>04.</p> Instalujesz na swoim komputerze oprogramowanie mierzące.</span></li>
   <ul class="panel__list-inner"><li><span>W tym momencie zostajesz software-panelistą.</span></li></ul>
   <li class="panel__list-item"><span><p>05.</p>GOTOWE!</span></li>
 </ul>
 <h3 class="panel__title">Jaki sprzęt musisz posiadać?</h3>
 <ul class="panel__list">
   <li class="panel__list-item"><span>Komputer  z systemem operacyjnym Windows oraz co najmniej jedną  z najpopularniejszych przeglądarek internetowych (Opera, Mozilla Firefox, Google Chrome, Internet Explorer).</span></li>
 </ul>
 <h3 class="panel__title">Jak działa oprogramowanie mierzące? </h3>
 <ul class="panel__list">
   <li class="panel__list-item"><span>Zbiera informacje podczas Twojej aktywności na komputerze.</span></li>
   <li class="panel__list-item"><span>Program działa w tle – nie obciąża systemu, jego praca jest nieodczuwalna dla stabilności działania urządzenia.</span></li>
   <li class="panel__list-item"><span>Każdy użytkownik komputera może korzystać z własnego konta badawczego.</span></li>
   <li class="panel__list-item"><span>Zebrane dane przesyłane są automatycznie - mają niewielki rozmiar.</span></li>
 </ul>
 <h3 class="panel__title">Jakie dane zbieramy?</h3>
 <ul class="panel__list">
   <li class="panel__list-item"><span>Anonimową aktywność użytkownika na stronach internetowych.</span></li>
 </ul>
 <h3 class="panel__title">Co da Ci udział w badaniu Gemius/PBI?</h3>
 <ul class="panel__list">
   <li class="panel__list-item"><span>Masz swój udział w rozwoju wiedzy na temat polskiego internetu.</span></li>
   <li class="panel__list-item"><span>Tworzysz z nami największe i najbardziej opiniotwórcze badanie widowni internetowej w Polsce.</span></li>
   <li class="panel__list-item"><span>Masz możliwość sprawdzenia swojej wiedzy w konkursach z cennymi nagrodami rzeczowymi.</span></li>
 </ul>
 <div class="panel__list-join">
   <h3 class="panel__list-join__title">Dołącz do badania już teraz</h3>
 </div>
 <div class="panel__list-join__contact">
   <p class="panel__list-join__title--contact">Kontakt </p>
   <p class="panel__list-join__email">Email: netpanel@gemius.pl </p>
   <p class="panel__list-join__mobile">Tel: 22 378 30 60</p>
 </div>

 </div>
</main>

  <?php get_footer(); ?>
