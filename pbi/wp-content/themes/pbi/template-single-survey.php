<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Badanie
 * @package WordPress
 *
 */

 get_header('title');
 the_post();
 get_template_part('tpl/survey', get_field('select_template'));
 get_footer();

?>
