<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

  /**
  *  Define root theme url's
  *
  */
  if (!defined('THEME_DIR')) {
      define('THEME_DIR', get_theme_root().'/'.get_template().'/');
  }
  if (!defined('THEME_URL')) {
      define('THEME_URL', WP_CONTENT_URL.'/themes/'.get_template().'/');
  }

  /**
  *  Import custom post types
  *
  */
  require_once(THEME_DIR.'/includes/post-types.php');
  require_once(THEME_DIR.'/includes/walkers.php');

  /**
  *  Register menus
  *
  */
  register_nav_menus(array(
    'main_menu' => 'Menu główne',
    'footer_1' => 'Footer 1',
    'footer_2' => 'Footer 2',
    'footer_3' => 'Footer 3',
    'footer_4' => 'Footer 4',
    'footer_5' => 'Footer 5',
    'footer_6' => 'Footer 6',
    'footer_7' => 'Footer 7',
    'footer_8' => 'Footer 8',
    'sub_gemius' => 'SUB Gemius Explorer',
    'sub_media' => 'SUB Media'
  ));

  /**
  *  Custom post excerpt
  *
  */
  function the_excerpt_max_charlength($charlength) {
      echo cutText(get_the_excerpt(), $charlength);
  }
  function cutText($text, $maxLength) {
      $maxLength++;
      $return = '';
      if (mb_strlen($text) > $maxLength) {
          $subex = mb_substr($text, 0, $maxLength - 5);
          $exwords = explode(' ', $subex);
          $excut = -(mb_strlen($exwords[count($exwords) - 1]));
          if ($excut < 0) {
              $return = mb_substr($subex, 0, $excut);
          } else {
              $return = $subex;
          }
          $return .= '...';
      } else {
          $return = $text;
      }
      return $return;
  }

  /**
  *  Theme thumbnails
  *
  */
  add_theme_support('post-thumbnails', array('post', 'media'));

  /**
  *  Allow SVG Icons
  *
  */
  function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');

  /**
  *  Register settings page
  *
  */
  if( function_exists('acf_add_options_page')) {
  	acf_add_options_page(array(
  		'page_title' 	=> 'Ustawienia',
  		'menu_title'	=> 'Ustawienia',
  		'menu_slug' 	=> 'module-settings',
  		'capability'	=> 'edit_posts',
  		'redirect'		=> false
  	));
  }

  /**
  *  Custom queries
  *
  */
  function custom_query_vars_filter($vars) {
    $vars[] = 'posts';
    $vars[] .= 'typ';
    $vars[] .= 'order';
    $vars[] .= 'orderby';
    $vars[] .= 's';
    $vars[] .= 'keyword';

    return $vars;
  }
  add_filter('query_vars', 'custom_query_vars_filter');

  function wp_get_attachment( $attachment_id ) {
  $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
  }

  if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
  function my_jquery_enqueue() {
     wp_deregister_script('jquery');
     wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", false, null);
     wp_enqueue_script('jquery');
  }

  
  /**
  *  Custom numeric pagination
  *
  */
  function the_numeric_pagination() {

    if( is_singular() )
      return;

    global $wp_query;

    if( $wp_query->max_num_pages <= 1 )
      return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    if ( $paged >= 1 )
      $links[] = $paged;

    if ( $paged >= 3 ) {
      $links[] = $paged - 1;
      $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
      $links[] = $paged + 2;
      $links[] = $paged + 1;
    }

    echo '<div class="pagination-box"><ul class="pagination-ul">' . "\n";

    if ( get_previous_posts_link() )
      printf( '<li class="pagination-li">%s</li>' . "\n", get_previous_posts_link('<span class="label-prev arrow"></span>') );

    if ( ! in_array( 1, $links ) ) {
      $class = 1 == $paged ? 'active' : '';

      printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

      if ( ! in_array( 2, $links ) )
        echo '<li>…</li>';
    }

    sort( $links );
    foreach ( (array) $links as $link ) {
      $class = $paged == $link ? 'active' : '';
      printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    if ( ! in_array( $max, $links ) ) {
      if ( ! in_array( $max - 1, $links ) )
        echo '<li>…</li>' . "\n";

      $class = $paged == $max ? 'active' : '';
      printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    if ( get_next_posts_link() )
      printf( '<li class="pagination-li">%s</li>' . "\n", get_next_posts_link('<span class="label-next arrow"></span>') );

    echo '</ul></div>' . "\n";
  }

?>
