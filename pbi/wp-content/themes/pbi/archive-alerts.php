<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 get_header('title'); ?>
 <main class="search">
   <?php
     $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
     $media = new WP_Query(array(
         'post_type' => 'alerts'
       ));
     if($media->have_posts()) : while($media->have_posts()) : $media->the_post();
   ?>
   <div class="search-box">
     <div class="search-box__content">
       <?php if(get_field('data_publikacji')) : ?><p class="search-box__content-date"><?php the_field('data_publikacji'); ?> </p><?php endif; ?>
       <?php if(get_field('czasopismo')) : ?>
         <p class="search-box__content-medium"><?php the_field('czasopismo'); ?></p>
         <span class="search-box__content-break"> / </span>
       <?php endif; ?>
       <?php if(get_field('autor')) : ?><p class="search-box__content-author"><?php the_field('autor'); ?> </p><?php endif; ?>
       <h3 class="search-box__content-title search-box__content-title--media">
         <a href="<?php the_permalink(); ?>">
           <?php the_title(); ?>
          </a>
       </h3>
       <p class="search-box__content-text">
         <?php the_excerpt_max_charlength(190); ?>
       </p>
     </div>
   </div>
   <?php endwhile; endif; ?>
 </main>
<?php get_footer(); ?>