<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
  get_header('title'); the_post(); global $post;

  if(get_field('post_type') == 'inny') : ?>

  <main class="single-article">
    <div class="single-article--wrapper">
      <article class="single-article__main">
        <h3 class="single-article__title">
          <?php the_title(); ?>
        </h3>
        <div class="single-article__details">
          <span class="single-article__author"><?php the_author(); ?>/
          </span>
          <span class="single-article__date"><?php the_date(); ?>
          </span>
        </div>
        <div class="single-article__image">
          <?php
            the_post_thumbnail('full', ['class' => 'single-article__image--intro']);
          ?>
        </div>
        <div class="single-article__body">
          <?php the_content(); ?>
          <a class="measure-steps__link" target="_blank" style="padding-left: 0;" href="<?php the_field('link_do_publikacji'); ?>">Zobacz publikację</a>
        </div>
      </article>
      <aside class="single-article__aside related-articles">
        <h3 class="related-articles__title">Powiązane teksty:</h3>
        <?php
          $news = new WP_Query(array(
              'post_type' => 'post',
              'posts_per_page' => 3
            ));
        ?>
        <?php while($news->have_posts()) : $news->the_post(); ?>
        <div class="related__article related-article__box">
          <h3 class="related-article__title"><?php the_title(); ?></h3>
          <div class="related-article__image">
            <?php the_post_thumbnail('large', ['class' => 'related-article__image-photo']); ?>
          </div>
        </div>
      <?php endwhile; ?>
      </aside>
    </div>
  </main>

<?php else : ?>

  <main>
 	 <div class="single-presentation">
 		 <h1 class="single-presentation__title"><?php the_title(); ?></h1>
 		 <div class="single-presentation__aside">
 			 <ul>
 				 <li class="single-presentation__aside-details">Autor: <span><?php the_field('author'); ?></span></li>
 				 <li class="single-presentation__aside-details">Data dodania: <span><?php echo get_the_date(); ?></span></li>
         <?php if(get_field('download_file')) : ?>
   				<a href="<?php the_field('download_file'); ?>">
    				 <div class="single-presentation__aside-download single-presentation__aside-download--desktop">
      				 <span class="single-presentation__aside-link">Pobierz prezentację<span>
    				 </div>
  				</a>
        <?php endif; ?>
 			 </ul>
 		 </div>
 		 <div class="single-presentation__main">
 			 <div class="single-presentation__main-paragraph">
 				 <?php the_content(); ?>
 			 </div>
 		 </div>
 		 <a href="<?php the_field('download_file'); ?>">
 			 <div class="single-presentation__aside-download single-presentation__aside-download--mobile">
 				 <span class="single-presentation__aside-link">Pobierz prezentację<span>
 				</div>
 			</a>
 	 </div>
  </main>

<?php endif; ?>
  <?php get_footer(); ?>
