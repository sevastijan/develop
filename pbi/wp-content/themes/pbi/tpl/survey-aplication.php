<main class="measure">
	<p class="measure-intro__text">Dzięki dołączeniu do pomiaru aplikacji mobilnych realizowanego w badaniu Gemius/PBI zyskasz rzetelne i wiarygodne dane na temat wykorzystania Twojej aplikacji przez internautów. Poznasz również parametry demograficzne odbiorców swojej aplikacji. Stworzysz przejrzyste materiały dla reklamodawców. <br>
Dołączenie do badania wiąże się z przystąpieniem do audytu site-centric, oferowanego przez firmę Gemius, w ramach badania Gemius/PBI, oraz z podpisaniem standardowej umowy z Gemiusem. Wyniki pomiaru są prezentowane w aplikacji Gemius Explorer. Ponadto właściciel aplikacji otrzymuje dostęp do interfejsu Gemius Prism, w którym część wskaźników dla aplikacji jest widoczna tylko dla niego. Poniżej znajdziesz informacje, jak wygląda cały proces.</p>
	<div class="measure-steps">
		<h1 class="measure-steps__title">Jakie są dalsze kroki?</h1>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/pomiar-01.png" alt="" class="measure-steps__number">
		<h3 class="measure-steps__title--secondary">Dołączasz do <a href="<?php echo esc_url( home_url( '/badania/pomiar-serwisow-www' ) ); ?>">pomiaru serwisów www</a></h3>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/pomiar-02.png" alt="" class="measure-steps__number measure-steps__number--2">
	<h3 class="measure-steps__title--secondary">Skryptujesz aplikacje według wskazówek z poniższej dokumentacji</h3>
	</div>
	<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/zasady_skryptowania_aplikacji_mobilnych_w_badaniu_Gemius_PBI.pdf" class="measure-steps__link">Instrukcja i zasady skryptowania aplikacji mobilnych</a>
	<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/gemius_PBI_mobile_plugin_ios.zip" class="measure-steps__link">Dokumentacja dla systemu operacyjnego iOS</a>
	<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/gemius_PBI_mobile_plugin_android.zip" class="measure-steps__link">Dokumentacja dla systemu operacyjnego Android</a>
	<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/AMP_rekomendacja_skyptowania_PL.pdf" class="measure-steps__link">Rekomendacja sposobu skryptowania stron mobilnych zbudowanych przy użyciu technologii Google Accelerated Mobile Pages (AMP)</a>
	<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/FIA_rekomendacja_skyptowania_PL.pdf" class="measure-steps__link">Rekomendacja sposobu skryptowania stron mobilnych zbudowanych przy użyciu technologii Facebook Instant Articles (FIA)</a>
	
	<!-- <p class="measure-steps__text--regular">
		Aplikacje na inne systemy operacyjne niż iOS i Android, należy oskryptować według zasad zawartych<br> w dokumentach:
	</p>

	<a href="" class="measure-steps__link">Zasady skryptowania aplikacji bez użycia SDK</a>
	<a href="" class="measure-steps__link">Format łańcucha UserAgent dla aplikacji</a> -->
	<!-- 24.02 doszedl ten paragraf -->
	<p class="measure-steps__text--regular">
			Pytania? Skontaktuj się z Działem Sprzedaży firmy Gemius: pl-audyt@gemius.com
	</p>
	<!-- <div class="measure-steps__contact">
		<img src="" alt="" class="measure-steps__contact-photo">
		<p class="measure-steps__contact-text">Pytania? Skontaktuj się z Michałem Włodarczykiem z firmy Gemius:<br>
			<span>michal.maciej.wlodarczyk@gemius.com</span>
		</p>
	</div>
	<p class="measure-steps__contact-text--mobile">Pytania? Skontaktuj się z Michałem Włodarczykiem z firmy Gemius:<br>
	</p>
		<p class="measure-mail">michal.maciej.wlodarczyk@gemius.com</p> -->
</main>
