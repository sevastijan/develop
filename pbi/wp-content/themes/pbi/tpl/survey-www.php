<main class="measure">
	<!-- 24.02 poprawiony tekst -->
	<p class="measure-intro__text">Dzięki dołączeniu do pomiaru serwisów www zyskasz wiarygodne i rzetelne dane na temat ruchu w swoim serwisie. Poznasz lepiej odbiorców Twojej witryny . Porównasz je także do danych dla innych witryn internetowych.  W końcu – stworzysz przejrzystą ofertę dla swoich reklamodawców.  <br>
Dołączenie do badania wiąże się z przystąpieniem do audytu site-centric, oferowanego przez firmę Gemius, w ramach badania Gemius/PBI, oraz z podpisaniem standardowej umowy z Gemiusem. Wyniki pomiaru są prezentowane w aplikacji Gemius Explorer. Ponadto właściciel aplikacji otrzymuje dostęp do interfejsu Gemius Prism, w którym część wskaźników dla witryn jest widoczna tylko dla niego. Poniżej znajdziesz informacje na temat całego procesu.</p>
	<div class="measure-steps">
		<h1 class="measure-steps__title">Jakie są dalsze kroki?</h1>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/pomiar-01.png" alt="" class="measure-steps__number">
		<h3 class="measure-steps__title--secondary">Kontaktujesz się z przedstawicielem Gemiusa wysyłając maila na adres pl-audyt@
gemius.com i ustalacie szczegóły.</h3>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/pomiar-02.png" alt="" class="measure-steps__number measure-steps__number--2">
	<h3 class="measure-steps__title--secondary">Podpisujesz umowę.</h3>
	<img src="<?php echo THEME_URL; ?>public/img/badanie/pomiar-03.png" alt="" class="measure-steps__number measure-steps__number--2">
<h3 class="measure-steps__title--secondary">Skryptujesz swój serwis www</h3>
<!-- 24.02 dodany jeden paragraf, wyrzucone na gorze inne rzeczy do sprawdzenia-->
		<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/reguly_umieszczania_skryptow_audytu_site_centric_badanie_Gemius_PBI.pdf" class="measure-steps__link">Dokumentacja</a>
		<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/AMP_rekomendacja_skyptowania_PL.pdf" class="measure-steps__link">Rekomendacja sposobu skryptowania stron mobilnych zbudowanych przy użyciu technologii Google Accelerated Mobile Pages (AMP)</a>
		<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/FIA_rekomendacja_skyptowania_PL.pdf" class="measure-steps__link">Rekomendacja sposobu skryptowania stron mobilnych zbudowanych przy użyciu technologii Facebook Instant Articles (FIA)</a>
		<p class="measure-steps__text--regular measure-steps__text--last">
			Pytania? Skontaktuj się z Działem Sprzedaży firmy Gemius: pl-audyt@gemius.com
		</p>

	</div>
	<!-- 24.02 to tez do usuniecia -->
	<!-- <a href="" class="measure-steps__link">Instrukcja i zasady skryptowania aplikacji mobilnych</a>
	<p class="measure-steps__text--regular">
		Aplikacje na inne systemy operacyjne niż iOS i Android, należy oskryptować według zasad zawartych<br> w dokumentach:
	</p>
	<a href="" class="measure-steps__link">Zasady skryptowania aplikacji bez użycia SDK</a>
	<a href="" class="measure-steps__link">Format łańcucha UserAgent dla aplikacji</a>
	<div class="measure-steps__contact">
		<img src="" alt="" class="measure-steps__contact-photo">
		<p class="measure-steps__contact-text">Pytania? Skontaktuj się z Michałem Włodarczykiem z firmy Gemius:<br>
			<span>michal.maciej.wlodarczyk@gemius.com</span>
		</p>
	</div>
	<p class="measure-steps__contact-text--mobile">Pytania? Skontaktuj się z Działem Sprzedaży firmy Gemius: pl-audyt@gemius.com<br>
	</p>
		<p class="measure-mail">michal.maciej.wlodarczyk@gemius.com</p> -->
</main>
