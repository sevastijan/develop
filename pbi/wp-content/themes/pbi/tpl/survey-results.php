<main class="research-results">
	<p class="research-results__title--start">Wyniki badania Gemius/PBI prezentowane są za pomocą programu<br> Gemius Explorer.</p>
	<h3 class="research-results__title--secondary">Główny widok programu Gemius Explorer</h3>
	<img src="<?php echo THEME_URL; ?>public/img/badanie/wyniki-badania.png" alt="" class="research-results__icon">
	<p class="research-results__description">
		W pakiecie z aplikacją Gemius Explorer otrzymasz szkolenie dotyczące jej działania. Możesz również<br>
		 skorzystać z naszych <a href="<?php echo esc_url( home_url( '/lekcje' ) ); ?>"><span>lekcji</span></a>
	</p>

<div class="research-results__demo">
		<h3 class="research-results__demo-title">Chcesz wypróbować naszą aplikację?</h3>
		<h3 class="research-results__demo-step">Pobierz wersję demonstracyjną</h3>
		<a href="http://download.gemius.com/gexplorer/gemiusExplorer5.2.exe" class="research-results__demo-download">Pobierz wersję demo</a>
		<h3 class="research-results__demo-step">Z menu Plik wybierz opcję „Otwórz dane overnight”</h3>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/overnight.png" alt="" class="research-results__demo-image">
		<h3 class="research-results__demo-step">Zaloguj się</h3>
		<h5 class="research-results__demo-program">login: pl:demo</h5>
		<h5 class="research-results__demo-program">hasło: demo</h5>
		<p class="research-results__demo-program--description">Otworzy się kalendarz. Przejdź do widoku lipca i wybierz odpowiedni dzień lub<br> dni z dostępnego zakresu dat (zaznaczony pogrubioną czcionką).</p>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/kalendarz.png" alt="" class="research-results__demo-program--image">
		<div class="measure-steps__contact">
			<img src="" alt="" class="measure-steps__contact-photo">
			<p class="measure-steps__contact-text">Pytania? Skontaktuj się z Michałem Włodarczykiem z firmy Gemius:<br>
				<span>michal.maciej.wlodarczyk@gemius.com</span>
			</p>
		</div>
		<p class="measure-steps__contact-text--mobile">Pytania? Skontaktuj się z Michałem Włodarczykiem z firmy Gemius:<br>
		</p>
		<p class="measure-mail">michal.maciej.wlodarczyk@gemius.com</p>
</div>
</main>
