<main class="about-research">
    <div class="about-research__intro">
    <h3 class="about-research__title">O badaniu</h3>
    <p class="about-research__text">
      Badanie Gemius/PBI jest standardem pomiaru widowni internetowej w Polsce. Jest wykorzystywane przez właścicieli serwisów internetowych do mierzenia ruchu w serwisach, porównywania się z konkurencyjnymi firmami i konstruowania ofert reklamowych. Reklamodawcy na bazie pomiaru widowni internetowej mogą z kolei porównywać parametry ofert różnych wydawców.<br><br>

      Badanie Gemius/PBI to badanie hybrydowe, łączące pomiar ruchu online (site-centric) i pomiar zachowań panelistów (user-centic). Mierzy zarówno ruch na stronach internetowych, konsumpcję treści audio i wideo oraz aplikacje mobilne. Uniwers demograficzny stanową osoby w wieku 7 lat i więcej. Wskaźniki stosowane w badaniu to liczba internautów, zasięg, czas spędzany na witrynach i liczba odsłon oraz wskaźniki pochodne. Dostępne są również dane demograficzne użytkowników. Dostęp do wyników badania można uzyskać za pomocą licencjonowanego oprogramowania Gemius Explorer.
    </p>
    </div>
    <div class="about-research--wrapper">
    <h3 class="about-research__title--secondary">Główne funkcjonalności</h3>
    <div class="icon-container icon-container--first">
      <div class="icon-box">
        <h3 class="icon-box__title">Dane<br> Dzienne</h3>
        <img src="<?php echo THEME_URL; ?>public/img/badanie/danedzienne.svg" alt="" class="icon-box__icon">
      </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Dane<br> tygodniowe</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/danetygodniowe.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Dane<br> miesięczne</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/danemiesieczne.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Podział na platformy<br> PC/mobile</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/podzial.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Podział <br>praca/dom</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/pracadom.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Drzewko agregatów</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/drzewko.svg" alt="" class="icon-box__icon">
    </div>
    </div>
      <h3 class="about-research__title--secondary">Kto korzysta z naszych danych</h3>
    <div class="icon-container">
      <div class="icon-box">
        <h3 class="icon-box__title">Branża<br> reklamowa</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/reklamowa.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Właściciel<br> serwisu www</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/wlasciciele.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Marketing<br> i badania</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/marketing.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Inwestorzy<br> i konsultanci</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/inwestorzy.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Naukowcy</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/naukowcy.svg" alt="" class="icon-box__icon">
    </div>
      <div class="icon-box">
        <h3 class="icon-box__title">Media</h3>
      <img src="<?php echo THEME_URL; ?>public/img/badanie/media.svg" alt="" class="icon-box__icon">
    </div>
  </div>
</main>
