<main class="how-join">
<p class="how-join__text">Jeśli jesteś zainteresowany dołączeniem do badania Gemius/PBI, wybierz produkt, który chcesz dołączyć do badania. Dowiesz się, jakie kolejne kroki musisz wykonać, aby móc rozpocząć pomiar.</p>
<div class="research-how__devices-boxes">
<div class="research-how__devices-box research-how__devices-box--how">
	<img src="<?php echo THEME_URL; ?>public/img/badanie/wlasciciel-serwisu.svg" alt="" class="research-how__devices-icon research-how__devices-icon--how">
	<h5 class="research-how__devices-title research-how__devices-title--how"><a href="<?php echo esc_url( home_url( '/badania/pomiar-serwisow-www/' ) ); ?>">Jestem właścicielem serwisu internetowego</a></h5>
</div>
<div class="research-how__devices-box research-how__devices-box--how">
	<img src="<?php echo THEME_URL; ?>public/img/badanie/wlasciciel-aplikacji.svg" alt="" class="research-how__devices-icon research-how__devices-icon--how">
	<h5 class="research-how__devices-title research-how__devices-title--how"><a href="<?php echo esc_url( home_url( '/badania/pomiar-aplikacji-mobilnych/' ) ); ?>">Jestem właścicielem aplikacji</a></h5>
</div>
<div class="research-how__devices-box research-how__devices-box--how">
	<img src="<?php echo THEME_URL; ?>public/img/badanie/wlasciciel-audio.svg" alt="" class="research-how__devices-icon research-how__devices-icon--how">
	<h5 class="research-how__devices-title research-how__devices-title--how"><a href="<?php echo esc_url( home_url( '/badania/pomiar-wideo-audio/' ) ); ?>">Jestem właścicielem treści wideo/audio</a></h5>
</div>
</div>
</main>
