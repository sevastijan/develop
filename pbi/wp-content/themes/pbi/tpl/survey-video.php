<main class="measure">
	<p class="measure-intro__text">Dołączenie do badania Gemius/PBI umożliwi Ci pokazanie potencjału treści wideo i audio niezależnie od platformy (PC, mobile) oraz niezależnie od formy dystrybucji (playera w stronie www, aplikacji desktopowej czy na urządzenia mobilne). Wyniki są opisywane takimi wskaźnikami jak np. liczba użytkowników czy odtworzeń. Możesz również analizować profile demograficzne widzów/słuchaczy.<br><br>
Pomiar multimediów wiąże się z przystąpieniem audytu site-centric, oferowanego przez firmę Gemius w ramach badania Gemius/PBI. Wyniki pomiaru będą prezentowane w aplikacji Gemius Explorer. Ponadto otrzymasz dostęp do interfejsu Gemius Prism, w którym część wskaźników dla treści strumieniowych będzie widoczna tylko dla Ciebie. Poniżej znajdziesz informacje, jak wygląda procedura.</p>
	<div class="measure-steps">
		<h1 class="measure-steps__title">Jakie są dalsze kroki?</h1>
		<img src="<?php echo THEME_URL; ?>public/img/badanie/pomiar-01.png" alt="" class="measure-steps__number">
		<h3 class="measure-steps__title--secondary">Dołączasz do <a href="<?php echo esc_url( home_url( '/badania/pomiar-serwisow-www' ) ); ?>">pomiaru serwisów www</a></h3>
		<!-- 24.02 usuniete warunki i zgloszenia -->
		<img src="<?php echo THEME_URL; ?>public/img/badanie/pomiar-02.png" alt="" class="measure-steps__number measure-steps__number--2">
	<h3 class="measure-steps__title--secondary">Skryptujesz swoje treści wideo/audio</h3>
	</div>
	<a href="http://pliki.gemius.pl/audyt_gemius_pl/ON/dokumenty/Pomiar_tresci_audio_i_wideo_na_stronach_WWW_w_badaniu_gemius_pbi.pdf" class="measure-steps__link">Dokumentacja</a>
	<!-- <a href="" class="measure-steps__link">Skryptowanie streamu w aplikacjach bez użycia SDK</a> -->
	<!-- 24.02 doszedl jeden paragraf, na dole wykomentowane -->
	<p class="measure-steps__text--regular">
			Pytania? Skontaktuj się z Działem Sprzedaży firmy Gemius: pl-audyt@gemius.com
	</p>
	<!-- <div class="measure-steps__contact">
		<img src="" alt="" class="measure-steps__contact-photo">
		<p class="measure-steps__contact-text">Pytania? Skontaktuj się z Michałem Włodarczykiem z firmy Gemius:<br>
			<span>michal.maciej.wlodarczyk@gemius.com</span>
		</p>
	</div>
	<p class="measure-steps__contact-text--mobile">Pytania? Skontaktuj się z Michałem Włodarczykiem z firmy Gemius:<br>
	</p>
		<p class="measure-mail">michal.maciej.wlodarczyk@gemius.com</p> -->
</main>
