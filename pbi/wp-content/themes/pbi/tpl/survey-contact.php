<main class="gemius-contact">
	<?php if( have_rows('contact_persons_group') ): while ( have_rows('contact_persons_group') ) : the_row(); ?>
		<div class="gemius-contact__title<?php the_sub_field('extra_class'); ?>"><?php the_sub_field('title'); ?></div>
		<div class="gemius-contact__boxes gemius-contact__boxes<?php the_sub_field('extra_class'); ?>">
			<?php if( have_rows('person') ): while ( have_rows('person') ) : the_row(); ?>
				<div class="gemius-contact__box">
					<img src="<?php the_sub_field('photo'); ?>" alt="" class="gemius-contact__icon">
					<h3 class="gemius-contact__name"><?php the_sub_field('name'); ?></h3>
					<h4 class="gemius-contact__function"><?php the_sub_field('function'); ?></h4>
					<h4 class="gemius-contact__task"><?php the_sub_field('description'); ?></h4>
					<h5 class="gemius-contact__mail"><a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></h5>
				</div>
			<?php endwhile; endif; ?>
		</div>
	<?php endwhile; endif; ?>
</main>
