<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 get_header('title');  ?>
	<?php if(isset($_GET['s'])) : ?>
	 <div class="search-intro__container">
	 	<h1 class="search-intro__title--first">Wyniki dla</h1>
	 	<h1 class="search-intro__title"><?php echo $_GET['s']; ?></h1>
	 </div>
 <?php endif; ?>
 <main class="search">
	 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 	<div class="search-box">
 		<div class="search-box__content">
 			<p class="search-box__content-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
 			<p class="search-box__content-text"><?php the_excerpt_max_charlength(165); ?></p>
 		</div>
 	</div>
<?php endwhile; else : ?>
	<h1 class="search-intro__title--first">Brak wyników spełniających Twoje oczekiwania</h1>
<?php endif; ?>
 </main>

  <?php get_footer(); ?>
