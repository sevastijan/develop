<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 get_header('title');?>
 <main class="error">
   <img class="error__image--mobile" src="<?php echo THEME_URL; ?>public/img/404--mobile.png" alt="">
   <img class="error__image" src="<?php echo THEME_URL; ?>public/img/404.png" alt="">
 </main>
<?php get_footer(); ?>
