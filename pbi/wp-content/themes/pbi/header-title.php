<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
  ?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Polskie Badania Internetu</title>
  <meta name="description" content="PBI">
  <meta name="author" content="SitePoint">
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

  <?php if(get_field('og_image')) : ?>
    <meta property="og:image" content="<?php the_field('og_image'); ?>" />
  <?php elseif (!is_single()) : ?>
    <meta property="og:image" content="http://pbi.org.pl/wp-content/uploads/2017/03/ogtags.jpg" />
  <?php endif; ?>
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/badania.css">
  <?php if(!is_post_type_archive('reports')) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/news.css">
  <?php endif; ?>
  <?php if(is_page(48)) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/kontakt.css">
  <?php endif; ?>
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/normalize.css">
  <?php if(is_post_type_archive('post') || is_singular('reports') || is_home() || is_singular() || is_post_type_archive('press')) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/news.css">
  <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/pbi.css">
  <?php if(!is_page(915)) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/gemius.css">
  <?php endif; ?>
  <?php if(is_page(327)) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/media.css">
  <?php endif; ?>
  <?php if(is_post_type_archive('reports') || is_singular('reports') ) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/raporty.css">
  <?php endif; ?>
  <?php if(is_page(265) || is_singular('lessons') || is_page(308) || is_page(824) || is_page(964) || is_page(966) || is_page(915) || is_singular('press') || is_singular('alerts') || is_post_type_archive('press') || is_singular('media')) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/akademia.css">
  <?php endif; ?>
  <?php if(is_404() || is_page(440) || is_search() || is_post_type_archive('alerts') || is_post_type_archive('media')) : ?>
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>public/stylesheets/en.css">
  <?php endif; ?>
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL;?>style.css">
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>public/stylesheets/improvements.css">

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-8203377-1', 'auto');
    ga('send', 'pageview');

  </script>

  <?php wp_head(); ?>
</head>
<body>
  <header class="header">
    <div class="header__navigation">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <img class="header__logo" src="<?php echo THEME_URL; ?>public/img/pbi-logo-final.svg" alt="">
      </a>
      <nav class="nav">
        <?php bem_menu('main_menu', 'nav'); ?>
      </nav>
      <div class="nav-toggle"><span></span></div>
      <nav class="nav-mobile visible">
        <img class="nav-mobile__logo" src="<?php echo THEME_URL; ?>public/img/logo-pbi-white.svg" alt="">
        <div class="nav-mobile__search--wrapper">
          <a class="nav-mobile__english" href="<?php echo esc_url( home_url( '/en' ) ); ?>">
            <h3>en</h3>
          </a>
          <div class="search__mobile--wrapper">
            <form id="js-search-form-mobile" action="<?php echo esc_url( home_url( '/' ) ); ?>">
              <input id="search__mobile-input" class="search__mobile-input" type="checkbox">
              <label for="search__mobile-input">
                <img src="<?php echo THEME_URL; ?>public/img/search-white.png" class="header__navigation-search-mobile" alt="">
              </label>
              <input class="inner__mobile" name="s" type="text"></input>
            </form>
          </div>
      </div>
        <?php bem_menu('main_menu', 'nav-mobile'); ?>
         <a href="https://www.facebook.com/PBIorgpl/"><img src="<?php echo THEME_URL; ?>public/img/fb-hd.png" alt="" class="nav-mobile__facebook"></a>
         <a href="https://www.linkedin.com/company/2095978"><img src="<?php echo THEME_URL; ?>public/img/in-hd.png" alt="" class="nav-mobile__linkedin"></a>
      </nav>

      <div class="social-wrapper">
        <a class="a2a_button_facebook" target="_blank" href="https://www.facebook.com/PBIorgpl/" rel="nofollow noopener"><span class="a2a_svg a2a_s__default a2a_s_facebook" style="background-color: rgb(59, 89, 152);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M17.78 27.5V17.008h3.522l.527-4.09h-4.05v-2.61c0-1.182.33-1.99 2.023-1.99h2.166V4.66c-.375-.05-1.66-.16-3.155-.16-3.123 0-5.26 1.905-5.26 5.405v3.016h-3.53v4.09h3.53V27.5h4.223z"></path></svg></span><span class="a2a_label">Facebook</span></a>
        <a class="a2a_button_pinterest" target="" href='#' rel="nofollow noopener"><span class="a2a_svg a2a_s__default a2a_s_pinterest" style="background-color: rgb(189, 8, 28);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M16.539 4.5c-6.277 0-9.442 4.5-9.442 8.253 0 2.272.86 4.293 2.705 5.046.303.125.574.005.662-.33.061-.231.205-.816.27-1.06.088-.331.053-.447-.191-.736-.532-.627-.873-1.439-.873-2.591 0-3.338 2.498-6.327 6.505-6.327 3.548 0 5.497 2.168 5.497 5.062 0 3.81-1.686 7.025-4.188 7.025-1.382 0-2.416-1.142-2.085-2.545.397-1.674 1.166-3.48 1.166-4.689 0-1.081-.581-1.983-1.782-1.983-1.413 0-2.548 1.462-2.548 3.419 0 1.247.421 2.091.421 2.091l-1.699 7.199c-.505 2.137-.076 4.755-.039 5.019.021.158.223.196.314.077.13-.17 1.813-2.247 2.384-4.324.162-.587.929-3.631.929-3.631.46.876 1.801 1.646 3.227 1.646 4.247 0 7.128-3.871 7.128-9.053.003-3.918-3.317-7.568-8.361-7.568z"></path></svg></span><span class="a2a_label">Pinterest</span></a>
        <a class="a2a_button_linkedin" target="_blank" href="https://www.linkedin.com/company/2095978" rel="nofollow noopener"><span class="a2a_svg a2a_s__default a2a_s_linkedin" style="background-color: rgb(0, 123, 181);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M6.227 12.61h4.19v13.48h-4.19V12.61zm2.095-6.7a2.43 2.43 0 0 1 0 4.86c-1.344 0-2.428-1.09-2.428-2.43s1.084-2.43 2.428-2.43m4.72 6.7h4.02v1.84h.058c.56-1.058 1.927-2.176 3.965-2.176 4.238 0 5.02 2.792 5.02 6.42v7.395h-4.183v-6.56c0-1.564-.03-3.574-2.178-3.574-2.18 0-2.514 1.7-2.514 3.46v6.668h-4.187V12.61z" fill="#FFF"></path></svg></span><span class="a2a_label">LinkedIn</span></a>
        <a href="https://www.slideshare.net/P_B_I" target="_blank"><img src="<?php echo THEME_URL; ?>public/img/slideshare.svg" alt="" class="a2a_svg"></a>
      </div>

      <div class="search--wrapper">
        <form id="js-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
          <input id="search-input" class="search-input" type="checkbox">
          <label for="search-input"><img src="<?php echo THEME_URL; ?>public/img/search-icon.png" alt="" class="header__navigation-search">
          </label>
          <input class="inner" name="s" type="text"></input>
        </form>
      </div>
      <a class="header__navigation-english" href="<?php echo esc_url( home_url( '/en' ) ); ?>">
      <h3>en</h3>
    </a>
    </div>
    <?php if(!is_search()) : ?>
    <div class="header__title <?php if(get_field('header_class')) { echo 'header__title--'. get_field('header_class'); }?>">
      <div class="header__title-wrapper">
        <h1 class="header__title-text">
          <?php
            if(is_post_type_archive('media')){
              echo 'PBI w mediach';
            } else if(is_post_type_archive('press')) {
              echo 'Wystąpienia Konferencyjne';
            } else if(is_post_type_archive('alerts')) {
              echo 'Komunikaty';
            } else if(is_archive() || is_singular('reports')) {
              echo 'Raporty i Analizy';
            } else if(is_singular('press')) {
              echo 'Wystąpienia Konferencyjne';
            } else if(is_page(247)) {
              echo 'Jak dołączyć?';
            } else if(is_404()) {
              echo 'Błąd 404';
            } else if(is_home()) {
              echo 'Aktualności';
            } else {
              the_title();
            }
          ?>
        </h1>
      </div>
    </div>
    <?php endif; ?>
  </header>
