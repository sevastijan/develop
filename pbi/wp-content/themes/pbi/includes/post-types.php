<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

  add_action('init','init_posttypes');

  function init_posttypes(){

      $reports = array(
          'labels' => array (
              'name' => 'Raporty',
              'sungular_name' => 'Raporty',
              'all_items' => 'Wszystkie',
              'add_new' => 'Dodaj nowy',
              'add_new_item' => 'Dodaj nowy',
              'edit_item' => 'Edytuj',
              'new_item' => 'Nowy',
              'view_item' => 'Zobacz',
              'search_items' => 'Szukaj',
              'not_found' => 'Nie znaleziono',
              'not_found_in_trash' => 'Nie znaleziono w koszu',
              'parent_item_colon' => ''
          ),
          'public' => true,
          'public_queryable' => true,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'raporty'),
          'capatility_type' => 'post',
          'hierarchical' => true,
          'menu_position' => 5,
          'supports' => array(
              'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields'
          ),
          'has_archive' => true,
      );
      register_post_type('reports', $reports);

      $lessons = array(
          'labels' => array (
              'name' => 'Lekcje',
              'sungular_name' => 'Lekcje',
              'all_items' => 'Wszystkie',
              'add_new' => 'Dodaj nowy',
              'add_new_item' => 'Dodaj nowy',
              'edit_item' => 'Edytuj',
              'new_item' => 'Nowy',
              'view_item' => 'Zobacz',
              'search_items' => 'Szukaj',
              'not_found' => 'Nie znaleziono',
              'not_found_in_trash' => 'Nie znaleziono w koszu',
              'parent_item_colon' => ''
          ),
          'public' => true,
          'public_queryable' => true,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'lekcja'),
          'capatility_type' => 'post',
          'hierarchical' => true,
          'menu_position' => 5,
          'supports' => array(
              'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields'
          ),
          'has_archive' => false,
      );
      register_post_type('lessons', $lessons);

      $press = array(
          'labels' => array (
              'name' => 'Prezentacje',
              'sungular_name' => 'Prezentacje',
              'all_items' => 'Wszystkie',
              'add_new' => 'Dodaj nowy',
              'add_new_item' => 'Dodaj nowy',
              'edit_item' => 'Edytuj',
              'new_item' => 'Nowy',
              'view_item' => 'Zobacz',
              'search_items' => 'Szukaj',
              'not_found' => 'Nie znaleziono',
              'not_found_in_trash' => 'Nie znaleziono w koszu',
              'parent_item_colon' => ''
          ),
          'public' => true,
          'public_queryable' => true,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'prezentacje'),
          'capatility_type' => 'post',
          'hierarchical' => true,
          'menu_position' => 5,
          'supports' => array(
              'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields'
          ),
          'has_archive' => true,
      );
      register_post_type('press', $press);

      $certificates = array(
          'labels' => array (
              'name' => 'Certyfikaty',
              'sungular_name' => 'Certyfikaty'
          ),
          'public' => false,
          'public_queryable' => true,
          'show_ui' => true,
          'menu_position' => 5,
          'supports' => array(
              'title'
          ),
          'has_archive' => false,
      );
      register_post_type('certs', $certificates);

      $faq = array(
          'labels' => array (
              'name' => 'FAQ',
              'sungular_name' => 'FAQ'
          ),
          'public' => false,
          'public_queryable' => true,
          'show_ui' => true,
          'menu_position' => 5,
          'supports' => array(
              'title'
          ),
          'has_archive' => false,
      );
      register_post_type('faq', $faq);

      $opinions = array(
          'labels' => array (
              'name' => 'Opinie',
              'sungular_name' => 'Opinie'
          ),
          'public' => false,
          'public_queryable' => true,
          'show_ui' => true,
          'menu_position' => 5,
          'supports' => array(
              'title'
          ),
          'has_archive' => false,
      );
      register_post_type('opinions', $opinions );

      $media = array(
          'labels' => array (
              'name' => 'Media',
              'sungular_name' => 'Media',
              'all_items' => 'Wszystkie',
              'add_new' => 'Dodaj nowy',
              'add_new_item' => 'Dodaj nowy',
              'edit_item' => 'Edytuj',
              'new_item' => 'Nowy',
              'view_item' => 'Zobacz',
              'search_items' => 'Szukaj',
              'not_found' => 'Nie znaleziono',
              'not_found_in_trash' => 'Nie znaleziono w koszu',
              'parent_item_colon' => ''
          ),
          'public' => true,
          'public_queryable' => true,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'media'),
          'capatility_type' => 'post',
          'hierarchical' => true,
          'menu_position' => 5,
          'supports' => array(
              'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields'
          ),
          'has_archive' => true,
      );
      register_post_type('media', $media);

      $alerts = array(
          'labels' => array (
              'name' => 'Komunikaty',
              'sungular_name' => 'Komunikaty',
              'all_items' => 'Wszystkie',
              'add_new' => 'Dodaj nowy',
              'add_new_item' => 'Dodaj nowy',
              'edit_item' => 'Edytuj',
              'new_item' => 'Nowy',
              'view_item' => 'Zobacz',
              'search_items' => 'Szukaj',
              'not_found' => 'Nie znaleziono',
              'not_found_in_trash' => 'Nie znaleziono w koszu',
              'parent_item_colon' => ''
          ),
          'public' => true,
          'public_queryable' => true,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'komunikaty'),
          'capatility_type' => 'post',
          'hierarchical' => true,
          'menu_position' => 5,
          'supports' => array(
              'title', 'editor', 'author', 'thumbnail', 'custom-fields'
          ),
          'has_archive' => true,
      );
      register_post_type('alerts', $alerts);
  }

  add_action('init','init_taxonomies');
  function init_taxonomies() {
  	register_taxonomy(
  		'type',
  		array('media'),
  		array(
  			'hierarchical' => true,
  			'show_ui' => true,
  			'update_count_callback' => '_update_post_term_count',
  			'query_var' => true,
  			'rewrite' => array('slug' => 'typ')
  			)
  	);
  }

?>
