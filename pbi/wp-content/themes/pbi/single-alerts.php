<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
  get_header('title'); the_post(); global $post; ?> 
  <main class="single-article">
    <div class="single-article--wrapper">
      <article class="single-article__main">
        <h3 class="single-article__title">
          <?php the_title(); ?>
        </h3>
        <div class="single-article__details">
          <span class="single-article__author"><?php the_author(); ?>/
          </span>
          <span class="single-article__date"><?php the_date(); ?>
          </span>
        </div>
        <div class="single-article__body">
          <?php the_content(); ?>
        </div>
      </article>
    </div>
  </main>

  <?php get_footer(); ?>
