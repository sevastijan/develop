<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
  ?>
  <section class="footer">
  <div class="footer__mobile">
    <h3 class="footer__heading">Kontakt</h3>
    <div class="footer__container">
      <div class="footer__content">
        <div class="footer__content-mobile">
          <div class="footer__content-mobile__text">
            <p class="footer__content-paragraph">Polskie Badania Internetu Sp. z o.o.</p>
            <p class="footer__content-paragraph">Al. Jerozolimskie 65/79, pok. 3.175</p>
            <p class="footer__content-paragraph">  00-697 Warszawa</p>
          </div>
          <div class="footer__content-mobile__text">
            <p class="footer__content-paragraph">tel. (48) 22 630 72 68</p>
            <p class="footer__content-paragraph">fax. (48) 22 630 72 67</p>
          </div>
          <div class="footer__content-mobile__text">
            <p class="footer__content-paragraph">biuro@pbi.org.pl</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer__tablet">
    <div class="footer__container-left">
      <h3 class="footer__navbox-title">PBI</h3>
      <p class="footer__about">
        <?php the_field('footer_about', 'option'); ?>
      </p>
    </div>
    <div class="footer__container-middle">
      <?php for($i = 1; $i <= 8; $i++) :
          $menu_name = 'footer_'. $i;
          $locations = get_nav_menu_locations();
          $menu_id = $locations[ $menu_name ];
          $menNam = wp_get_nav_menu_object($menu_id);
        ?>
          <div class="footer__navbox">
            <h3 class="footer__navbox-title"><?php echo $menNam->name;?></h3>
          <?php bem_menu($menu_name, 'footer'); ?>
        </div>
      <?php endfor; ?>
    </div>
    <div class="footer__container-right">
      <h3 class="footer__navbox-title">Kontakt</h3>
        <?php the_field('footer_contact_address', 'option'); ?>
    </div>
    <div style="clear:both;"></div>
  </div>
  </section>

  <?php if(is_page_template('template-academy.php') || is_page_template('template-contact.php') || is_page(440)) : ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5q5791ItFXDw7GcHJHI9Asz7UMVYHqng"></script>
    <script>
      function initialize() {
          var mapCanvas = document.getElementById('map');

          var mapOptions = {
          center: new google.maps.LatLng(52.228101, 21.004942),
          zoom: 17,
          styles:[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}] ,
          mapTypeId: google.maps.MapTypeId.ROADMAP
              }
          var map = new google.maps.Map(mapCanvas, mapOptions)
          var marker = new google.maps.Marker({
          map:map,
          title: 'Polskie Badania Internetu',
          position: new google.maps.LatLng(52.228101, 21.004942)
        });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  <?php endif; ?>

  <script src="<?php echo THEME_URL; ?>public/javascripts/underscore-min.js"></script>
  <script src="<?php echo THEME_URL; ?>public/javascripts/ajaxQuery.js"></script>
  <script src="<?php echo THEME_URL; ?>public/javascripts/app.js"></script>

  <?php wp_footer(); ?>
  </body>
  </html>
