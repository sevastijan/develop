��          T      �       �      �   {   �      E     U  �   X     �  U       e  �   u     �       �        �                                        <!-- OG: %s --> If you like <strong>OG</strong> please leave us a %s&#9733;&#9733;&#9733;&#9733;&#9733;%s rating. A huge thanks in advance! Marcin Pietrzak OG Very tiny Open Graph plugin - add featured image as facebook image. This plugin do not have any configuration - you can check how it works looking into page source. http://iworks.pl/ PO-Revision-Date: 2016-10-26 06:33:23+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.3.0-alpha
Language: pl
Project-Id-Version: Plugins - OG - Stable (latest release)
 <!-- OG: %s --> Jeżeli lubisz wtyczkę <strong>OG</strong>, proszę daj jej ocenę %s&#9733;&#9733;&#9733;&#9733;&#9733;%s. Z góry bardzo dziękuję! Marcin Pietrzak OG Bardzo mała wtyczka implementująca Open Grapha - Dodaje miniaturę wpisu jako obrazek dla FaceBooka. Wtyczka nie ma żadnej konfiguracji, aby sprawdzić czy działa, obejrzyj źródło strony. http://iworks.pl/ 