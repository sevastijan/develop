<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
  ?>
<?php get_header(); ?>
  <main>
    <div class="body-max__wrapper">
    <section class="news">
      <h1 class="news__heading">Aktualności</h1>
      <div class="news__wrapper-right">

        <?php
          $news = new WP_Query(array(
              'post_type' => 'post',
              'posts_per_page' => 1,
              'meta_key' => 'featured',
              'meta_value' => true
            ));
        ?>
        <?php while($news->have_posts()) : $news->the_post();
          $category_name = get_the_category();
        ?>

      <article class="news__tarticle">
        <div class="news__tarticle--wrapper">
          <img src="<?php the_post_thumbnail_url('full'); ?>" class="news__tarticle-img" />
        </div>
        <img src="<?php echo THEME_URL; ?>public/img/circles/promo-circle.png" alt="" class="news__tarticle-circle">
        <div class="news__tarticle-title">
          <div class="news__tarticle-title--wrapper">
          <h3 class="news__tarticle-title--inline">
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h3>
          </div>

          <div class="news__tarticle-content">
            <p class="news__tarticle-content__text">
              <?php the_excerpt_max_charlength(85); ?>
            </p>
            <div class="news__tarticle-details">
              <a href="<?php the_permalink(); ?>"><img src="<?php echo THEME_URL; ?>public/img/main-news-arrow.svg" alt="" class="news__tarticle-details__link"></a>
              <p class="news__tarticle-details__date"><time pubdate datetime="2009-10-10T19:10-08:00"><?php echo get_the_date(); ?></time></p>
            </div>
          </div>
        </div>
        <div class="news__tarticle-container">
          <div class="news__tarticle-bar">
            <h3 class="news__tarticle-bar__title"><?php echo $category_name[0]->name; ?></h3>
            <div class="news__tarticle-bar__share">
              <?php echo do_shortcode('[addtoany url="' . get_the_permalink() . '" title="Polskie Badania Internetu"]'); ?>
            </div>
          </div>
          </div>
      </article>

      <?php endwhile; wp_reset_query();?>

      <?php
        $news = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 1,
            'meta_key' => 'featured',
            'meta_value' => true
          ));
      ?>
      <?php while($news->have_posts()) : $news->the_post();
        $category_name = get_the_category();
      ?>

      <article class="news__article news__article--main">
        <div class="news__article-bar">
          <h3 class="news__article-bar__tag"><?php echo $category_name[0]->name; ?></h3>
          <div class="news__article-bar__share">
            <?php echo do_shortcode('[addtoany url="' . get_the_permalink() . '" title="Polskie Badania Internetu"]'); ?>
          </div>
        </div>
        <?php the_post_thumbnail('', ['class' => 'news__article-photo']); ?>
        <div class="news__article-content">
          <p class="news__article-date"><time pubdate datetime="2009-10-10T19:10-08:00"><?php echo get_the_date(); ?></time></p>
          <div class="news__tarticle-title--wrapper">
            <h3 class="news__tarticle-title--inline">
              <a href="<?php the_permalink(); ?>" class="link--black"><?php the_title(); ?></a>
            </h3>
          </div>
          <p class="news__article-text"><?php the_excerpt_max_charlength(170); ?></p>
        </div>
      </article>

      <?php endwhile; wp_reset_query();?>

      </div>
      <div class="news__wrapper-left">
      <article class="news__small">

        <?php
          $news = new WP_Query(array(
              'post_type' => 'post',
              'posts_per_page' => 4
            ));
          $index = 1;
        ?>
        <?php while($news->have_posts()) : $news->the_post();
          $category_name = get_the_category();
          $circles = array(
            '<img src="' . THEME_URL . 'public/img/circles/news-small-red-circle.png" alt="" class="news__small-content__circle-half">',
            '<img src="' . THEME_URL . 'public/img/circles/news-small-blue-circle.png" alt="" class="news__small-content__circle-half">',
            '<img src="' . THEME_URL . 'public/img/circles/news-small-yellow-circle.png" alt="" class="news__small-content__circle-half">'
          );
        ?>

          <div class="news__small-container news__small-container--50 <?php if($index % 2 == 0) {echo "news__small-container--toright";}?>">
            <div class="news__small-container--padding">
              <div <?php if($index % 2 == 0) {echo 'where="right"';}?> class="news__small-content news__small-content--photo">
                <?php echo $circles[array_rand($circles, 1)]; ?>
                <?php the_post_thumbnail('', ['class' => 'news__small-photo']); ?>
              </div>
              <div class="news__small-content news__small-content--text">
                <div class="news__small-bar">
                  <h5 class="news__small-bar__tag"><?php echo $category_name[0]->name; ?></h5>
                  <div class="news__small-bar__share">
                    <?php echo do_shortcode('[addtoany url="' . get_the_permalink() . '" title="Polskie Badania Internetu"]'); ?>
                  </div>
                </div>
                <h3 class="news__small-content__title">
                  <a href="<?php the_permalink(); ?>" class="link--black"><?php the_title(); ?></a>
                </h3>
                <p class="news__small-content__text"><?php the_excerpt_max_charlength(150); ?></p>
              </div>
            </div>
            <div class="news__small-container__details">
              <p class="news__small-container__date"><time pubdate datetime=""><?php the_date(); ?></time></p>
              <a href="<?php the_permalink(); ?>"><img src="<?php echo THEME_URL; ?>public/img/main-news-arrow.svg" alt="" class="news__small-container__link"></a>
            </div>
        </div>

      <?php $index++; endwhile; wp_reset_query();?>

      <a href="<?php echo esc_url( home_url( 'aktualnosci/' ) ); ?>"><div class="news__small-showall">Zobacz wszystkie</div></a>
    </div>
      </article>
      <div class="news__wrapper-aside">
        <div class="news__aside">
          <div class="news__aside-content">
            <div class="news__aside-wrapper">
              <img src="<?php echo THEME_URL; ?>public/img/circles/kolo-poznaj-rotated.png" alt="" class="news__aside-circle">
              <img src="<?php echo THEME_URL; ?>public/img/research-icon-small.png" alt="" class="news__aside-icon">
              <img src="<?php echo THEME_URL; ?>public/img/research-icon.png" alt="" class="news__aside-icon--desktop">
              <h3 class="news__aside-title"><?php the_field('home_module_title', 'option'); ?></h3>
              <div class="news__aside-text">
                <?php the_field('home_module_content', 'option'); ?>
              </div>
               <div class="news__aside-button">
                 <a class="news__aside-link" href="<?php the_field('home_module_url', 'option'); ?>">
                   <!-- <img src="<?php echo THEME_URL; ?>public/img/gemius-arrow-small.png" alt="" class="news__aside-button__arrow"> -->
                 </a>
               </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="materials">
      <div class="materials__left">
        <h3 class="materials__heading--desktop">Materiały</h3>
        <img src="<?php echo THEME_URL; ?>public/img/folders-small.svg" alt="" class="materials__icon--desktop">
      </div>
      <div class="materials__middle">
        <h3 class="materials__heading">Materiały</h3>
        <?php if(have_rows('home_materials', 'option')) :  while (have_rows('home_materials', 'option')) : the_row(); ?>
          <div class="materials__content">
            <p class="materials__title"><?php the_sub_field('materials_headline', 'option'); ?></p>
            <p class="materials__description"><a href="<?php the_sub_field('materials_url', 'option'); ?>" class="materials__description-link" ><?php the_sub_field('materials_text', 'option'); ?></a></p>
            <?php if(get_sub_field('materials_url', 'option')) : ?>
              <a href="<?php the_sub_field('materials_url', 'option'); ?>"><img src="<?php echo THEME_URL; ?>public/img/materials-arrow.svg" alt="" class="materials__arrow--big"></a>
              <a href="<?php the_sub_field('materials_url', 'option'); ?>"><img src="<?php echo THEME_URL; ?>public/img/arrow-small.png" alt="" class="materials__arrow"></a>
            <?php endif; ?>
          </div>
      <?php endwhile; endif; ?>
        <div class="materials__number">
          <h3 class="materials__number-title">Liczba z badania <br> Gemius/PBI </h3>
          <div class="materials__number-content materials__number-content--left">
            <h1 class="materials__number-big"><?php the_field('day_number', 'option'); ?></h3>
              <p class="materials__number-text materials__number-text--laptop"><?php the_field('day_paragraph', 'option'); ?></p>
          </div>
          <div class="materials__number-content materials__number-content--right">
            <p class="materials__number-text"><?php the_field('day_paragraph', 'option'); ?></p>
          </div>
        </div>
      </div>
      <div class="materials__right">
        <div class="materials__number--desktop">
          <h3 class="materials__number-title--desktop">Liczba z badania Gemius/PBI</h3>
          <h3 class="materials__number-big--desktop"><?php the_field('day_number', 'option'); ?></h3>
          <p class="materials__number-text--desktop"><?php the_field('day_paragraph', 'option'); ?></p>
        </div>
      </div>
    </section>
    <section class="help">
      <img src="<?php echo THEME_URL; ?>public/img/circles/big-circle.png" alt="" class="help__circle">
      <h3 class="help__title--desktop">Jak możemy pomóc ?</h3>
      <div class="help__wrapper">
      <div class="help__container">
        <div class="help__box help__box--1">
          <div class="help__box-wrapper">
            <h3 class="help__box-title">Jak możemy pomóc ?</3>
          </div>
        </div>
        <?php
          $index = 2;
          if(have_rows('home_help_icons', 'option')) : while (have_rows('home_help_icons', 'option')) : the_row();
          ?>
        <div class="help__box help__box--<?php echo $index; ?>">
          <div class="help__box-wrapper">
            <img src="<?php the_sub_field('icon_logo_small', 'option'); ?>" alt="" class="help__box-icon">
            <img src="<?php the_sub_field('icon_logo', 'option'); ?>" alt="" class="help__box-icon--desktop">
            <p class="help__box-text"><a href="<?php the_sub_field('icon_url', 'option'); ?>" class="help__box-link"><?php the_sub_field('icon_text', 'option'); ?></a></p>
            <a class="help__box-button" href="<?php the_sub_field('icon_url', 'option'); ?>"></a>
          </div>
        </div>
        <?php $index++; endwhile; endif; ?>
      </div>
      </div>
      <div class="help__boxhide"></div>
    </section>
    <section class="partners">
      <h3 class="partners__heading">Partnerzy</h3>
      <div class="partners__boxes">
          <?php if( have_rows('home_partners', 'option') ): while ( have_rows('home_partners', 'option') ) : the_row(); ?>
            <div class="partners__box">
              <img src="<?php the_sub_field('partner_logo', 'option'); ?>" alt="" class="partners__box-logo">
            </div>
          <?php endwhile; endif; ?>
    </section>
</div>
</main>
<?php get_footer(); ?>
