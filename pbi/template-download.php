<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Media download
 * @package WordPress
 *
 */

 get_header('title'); the_post(); ?>

 <main>
	 <section class="media">
		 <div class="toggle__nav">
       <input id="toggle__checkbox" type="checkbox">
       <label for="toggle__checkbox">
         <span>Menu Toggle</span>
       </label>
       <?php bem_menu('sub_media', 'toggle__list'); ?>
     </div>
		 <div class="media__circle">
			 <h1 class="media__circle-title">SZYBKI KONTAKT</h1>
			 <h3 class="media__circle-name">Anna Miotk</h3>
			 <h3 class="media__circle-email">a.miotk@pbi.org.pl</h3>
			 <h3 class="media__circle-mobile">+48 606 611 500</h3>
			 <h3 class="media__circle-text">Potrzebujesz danych z badania Gemius/PBI  na potrzeby artykułu,
				audycji lub badań naukowych? Chiałbyś zaprosić prelegenta z PBI na swoją konferencję lub poprosić PBI o patronat? Skontaktuj się ze mną.</h3>
		 </div>
		 </div>
		 <img src="<?php echo THEME_URL; ?>public/img/media-download-circle.png" alt="" class="media__big-circle">
		 <div class="aside-nav">
       <?php bem_menu('sub_media', 'aside-nav'); ?>
     </div>
		 <div class="download">
			 <div class="download--wrapper">
				 <div class="download__intro">
					 <h1 class="download__intro-title"><?php the_field('section_1_title'); ?></h1>
					 <p class="download__intro-text"><?php the_field('section_1_text'); ?></p>
						 <div class="download__buttons--wrapper">
             <?php if(get_field('section_1_download')) : ?>
  						 <div class="download__media">
  							 <a href="<?php the_field('section_1_download'); ?>">
                   <span class="download__media-link">Pobierz presspack PDF<span>
                  </a>
  						 </div>
             <?php endif; ?>
             <?php if(get_field('section_1_download_2')) : ?>
						 <div class="download__media">
							 <a href="<?php the_field('section_1_download_2'); ?>">
                 <span class="download__media-link">Pobierz logo(zip)<span>
                </a>
						 </div>
             <?php endif; ?>
						 </div>
					 </div>
					 <div class="download__fast">
						 <h1 class="download__fast-title">SZYBKI KONTAKT</h1>
						 <h3 class="download__fast-name">Anna Miotk</h3>
						 <h3 class="download__fast-email">a.miotk@pbi.org.pl</h3>
						 <h3 class="download__fast-mobile">+48 606 611 500</h3>
						 <h3 class="download__fast-text">Potrzebujesz danych z badania Gemius/PBI  na potrzeby artykułu,
							audycji lub badań naukowych? Chiałbyś zaprosić prelegenta z PBI na swoją konferencję lub poprosić PBI o patronat? Skontaktuj się ze mną.</h3>
					 </div>
					 <div class="download__rules">
						 <h1 class="download__rules-title"><?php the_field('section_2_title'); ?></h1>
						 <p class="download__rules-text">
               <?php the_field('section_2_text'); ?>
             </p>
							 <div class="download__rules-details">
                 <?php if(get_field('section_2_url')) : ?>
  								 <a href="<?php the_field('section_2_url'); ?>" class="download__rules-link">Otwórz w osobnej zakładce</a>
                 <?php endif; ?>
                 <?php if(get_field('section_2_download')) : ?>
  								 <div class="download__rules-details__button">
  									 <a href="<?php the_field('section_2_download'); ?>">
                       <span class="download__rules-details__link">Pobierz logo(zip)<span>
                     </a>
  								 </div>
                 <?php endif; ?>
							 </div>
					 </div>
					 <div class="download__experts">
						 <h1 class="download__experts-title">Nasi eksperci</h1>
						 <div class="download__experts--wrapper">
               <?php if( have_rows('eksperci') ):  while ( have_rows('eksperci') ) : the_row(); ?>
							 <div class="download__experts-box">
								 <div class="download__experts-box--wrapper download__experts-box--wrapper-first">
									 <div class="download__experts-image--wrapper">
								 <img src="<?php the_sub_field('photo'); ?>" alt="" class="download__experts-box__image">
							 </div>
								 <h3 class="download__experts-box__name"><?php the_sub_field('name'); ?><span class="download__experts-box__name--position"> - <?php the_sub_field('position'); ?> </span></h3>
								 <p class="download__experts-box__topics"><?php the_sub_field('specializations'); ?></p>
                 <?php if(get_sub_field('email')) : ?>
                   <p class="download__experts-box__email"><?php the_sub_field('email'); ?></p>
                 <?php endif; ?>
               </div>
							 <div class="download__experts-button--wrapper">
								 <div class="download__experts-box__button">
									 <a href="<?php the_sub_field('biogram'); ?>">
                     <span class="download__experts-box__button-link">Biogram</span>
                    </a>
								 </div>
								 <div class="download__experts-box__button">
                   <a href="<?php the_sub_field('photod'); ?>">
  									 <span class="download__experts-box__button-link">Zdjęcie</span>
                   </a>
								 </div>
							 </div>
							 </div>
             <?php endwhile; endif; ?>
						 </div>
					 </div>
			 </div>
		 </div>
	 </section>
 </main>

  <?php get_footer(); ?>
