<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
  get_header('title'); $tag = get_query_var('cat', ''); ?>
  <main class="news js-news-container" data-default-tag="<?php echo $tag; ?>">
    <div class="search">
      <div class="search--wrapper-news">
        <div class="search__input--wrapper">
          <input id="js__posts--input" class="search__field" placeholder="Wpisz szukaną frazę">
        </div>
        <ul class="search__categories">
          
          <li title="Pokaż wszystkie kategorie" data-category-id="" class="search__categories-item <?php if(!$tag) { echo 'search__categories-item--active'; } ?>">Pokaż wszystkie kategorie</li>
          <?php
            $categories = get_categories();
            foreach ($categories as $category) :
          ?>
            <li title="<?php echo $category->name; ?>" data-category-id="<?php echo $category->cat_ID; ?>" class="search__categories-item <?php if($tag == $category->cat_ID) { echo 'search__categories-item--active'; } ?>"><?php echo $category->name; ?></li>
          <?php
            endforeach;
          ?>
        </ul>
        <div class="search__details">
          <select id="js__posts--sort" class="search__select">
            <option value="desc">Sortuj wg daty</option>
            <option value="desc">Najnowsze</option>
            <option value="asc">Najstarsze</option>
          </select>
        </div>
      </div>
    </div>
    <div class="news__content">
      <div class="news__content--wrapper">
        <?php

        $postsPerPage = get_query_var('posts', 4);
        $tag = get_query_var('cat', '');
        $order = get_query_var('order', 'desc');
        $search = get_query_var('s');

        $posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => $postsPerPage,
            'cat' => $tag,
            'order' => $order,
            'orderby' => 'date',
            's' => $search
          ));

        $index = 0;
        if($posts->have_posts()) : while($posts->have_posts()) : $posts->the_post(); ?>
        <div class="article news__content-article">
          <div class="article__box-left" style="background-image: url(<?php the_post_thumbnail_url();?>)">
          </div>
          <div class="article__box-right">
            <div class="article__share">
              <h5 class="article__share-tag">
                <?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>
              </h5>
              <div class="article__share-link">
                  <?php echo do_shortcode('[addtoany url="' . get_the_permalink() . '" title="Polskie Badania Internetu"]'); ?>
              </div>
            </div>
            <div class="article__content">
              <h3 class="article__content-title">
                <a href="<?php the_permalink(); ?>" class="link--black"><?php the_title(); ?></a>
              </h3>
              <p class="article__content-text">
                <?php the_excerpt_max_charlength(120); ?>
              </p>
            </div>
            <div class="article__details">
              <p class="article__details-date"><?php the_date(); ?></p>
              <a href="<?php the_permalink(); ?>" class="article__details-arrow"></a>
            </div>
          </div>
        </div>
      <?php endwhile; else: ?>
          <h2>Brak postów</h2>
      <?php endif; ?>
      </div>
      <div class="news__loader">
        <a id="js__posts--loadMore" class="news__button" href="">Pokaż więcej</a>
      </div>
    </div>
  </main>
  <?php get_footer(); ?>
