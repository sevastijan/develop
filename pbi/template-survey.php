<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Badania rodzic
 * @package WordPress
 *
 */

 get_header('title'); the_post(); ?>

  <main>
    <div class="intro">
      <div class="intro__wrapper">
        <div class="intro__wrapper--inside">
          <?php $index = 0; if( have_rows('introbox') ): while ( have_rows('introbox') ) : the_row(); ?>
              <div class="intro__box" <?php if($index == 0) : ?>style="border: 10px solid #EEDC00" <?php endif; ?>>
                <img src="<?php the_sub_field('icon'); ?>" alt="" class="intro__box-icon">
                <div class="intro__box-title--container">
                  <a href="<?php the_sub_field('url'); ?>">
                    <h3 class="intro__box-title"><?php the_sub_field('title'); ?></h3>
                  </a>
                </div>
                <p class="intro__box-text"><?php the_sub_field('description'); ?></p>
              </div>
          <?php $index++; endwhile; endif;?>
        </div>
      </div>
    </div>
    <div class="research">
      <div class="research__left">
        <div class="research__title-container">
          <h1 class="research__title">Gemius</h1>
          <h1 class="research__title">PBI</h1>
        </div>
        <img src="<?php echo THEME_URL;?>public/img/circle-big-yellow.png" alt="" class="research__circle">
        <div class="research__left-top"></div>
        <div class="research__left-top"></div>
      </div>
      <div class="research__right">
        <div class="research__boxes-wrapper">
          <div class="research__box research__box--first">
            <h1 class="research__box-first__title">Gemius</h1>
            <h1 class="research__box-first__title">PBI</h1>
            <img src="<?php echo THEME_URL;?>public/img/research-yellow-circle.png" alt="" class="research__box-circle--yellow">
          </div>
            <?php
              $children = get_pages(array('child_of' => $post->ID, 'sort_column' => 'menu_order', 'exclude' => array(294, 297, 300)));
              for($i = 0; $i <= count($children) - 1; $i++) : ?>
            <div class="research__box">
              <div class="research__box-container">
                <div class="research__box-title">
                  <a href="<?php echo get_page_link($children[$i]->ID); ?>">
                    <h1 class="research__box-title--long"><?php echo $children[$i]->post_title; ?></h1>
                  </a>
                </div>
                <div class="research__box-text"><?php the_field('zajawka', $children[$i]->ID); ?></div>
                <a href="<?php echo get_page_link($children[$i]->ID); ?>">
                  <div class="research__box-button research__box-button--mobile">
                    <img src="<?php echo THEME_URL;?>public/img/arrow-small.png" alt="" class="research__box-button__arrow">
                  </div>
                </a>
                  <?php if(in_array($i, array(1, 4, 6, 7))) : ?>
                    <div circle="grey" class="research__box-details">
                      <img src="<?php echo THEME_URL;?>public/img/circle-small-grey.png" alt="" class="research__box-circle">
                  <?php else : ?>
                    <div class="research__box-details">
                      <img src="<?php echo THEME_URL;?>public/img/circle-small-white.png" alt="" class="research__box-circle">
                  <?php endif; ?>
                  <a href="<?php echo get_page_link($children[$i]->ID); ?>">
                    <div class="research__box-button">
                      <img src="<?php echo THEME_URL;?>public/img/arrow-small.png" alt="" class="research__box-button__arrow">
                    </div>
                  </a>
                </div>
              </div>
            </div>
           <?php endfor; ?>
        </div>
      </div>
    </div>
  </main>
  <?php get_footer(); ?>
