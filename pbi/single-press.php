<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 get_header('title'); the_post(); ?>

 <main>
	 <div class="single-presentation">
		 <h1 class="single-presentation__title"><?php the_title(); ?></h1>
		 <div class="single-presentation__aside">
			 <ul>
				 <?php if(get_field('author')) : ?><li class="single-presentation__aside-details">Autor: <span><?php the_field('author'); ?></span></li><?php endif; ?>
				 <li class="single-presentation__aside-details">Data dodania: <span><?php echo get_the_date(); ?></span></li>
				 <a href="<?php the_field('download_file'); ?>">
					 <div class="single-presentation__aside-download single-presentation__aside-download--desktop">
						 <span class="single-presentation__aside-link">Pobierz prezentację<span>
					 </div>
					</a>
			 </ul>
		 </div>
		 <div class="single-presentation__main">
			 <div class="single-presentation__main-paragraph">
				 <?php the_content(); ?>
			 </div>
		 </div>
		 <a href="<?php the_field('download_file'); ?>">
			 <div class="single-presentation__aside-download single-presentation__aside-download--mobile">
				 <span class="single-presentation__aside-link">Pobierz prezentację<span>
				</div>
			</a>
	 </div>
 </main>

  <?php get_footer(); ?>
