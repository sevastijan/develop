/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
$(document).ready(function(){
  var $searchSelectors = $('#js__search--category, #js__search--sort'),
      $searchCategory = $('#js__search--category'),
      $searchSort = $('#js__search--sort'),
      $searchInput = $('#js__search--input'),
      $postsPerPage = $('#js__search--loadMore'),
      $searchCounter = $('#js__search--counter'),
      $postsSort = $('#js__posts--sort'),
      $postsWrapper = $('.news__content--wrapper'),
      $mediaWrapper = $('.media-main__boxes--wrapper'),
      $pressWrapper = $('.presentation-content--wrapper'),
      $postsCategory = $('.news .search__categories-item'),
      $mediaCategory = $('.media .search__categories-item'),
      $mediaSort = $('#js__media--sort'),
      $pressSort = $('#js__select--presentation'),
      $loadmoreOpinions = $('#loadmore-opinions'),
      $loadmoreFaqs = $('#loadmore-faqs'),
      $opinionsWrapper = $('#opinions-container'),
      $faqsWrapper = $('#faqs-container'),
      searchCategoryVal = $('#js__search--category').val(),
      searchSortVal = $('#js__search--sort').val(),
      pressSortVal = $('#js__select--presentation').val(),
      searchInputVal = $searchInput.val(),
      postsPerPageVal = 8,
      opinionsPerPageVal = 2,
      requestReports = function() {
        searchInputVal = $searchInput.val();
        var typ;
        if(searchCategoryVal !== '') {
          typ = 'typ=' + searchCategoryVal + '&';
        } else {
          typ = '';
        }
        $.ajax({
          url: 'http://pbi.org.pl/raporty?' + typ + '&order=' + searchSortVal + '&keyword=' + searchInputVal + '&posts=' + postsPerPageVal,
          beforeSend: function() {
            $('.report__wrapper').css('opacity', '0.7');
          },
          success: function(el) {
            console.log(this.url);
            $('.report__wrapper').fadeOut(300, function(){
                $('.report__wrapper').html($(el).find('.report__wrapper').html()).fadeIn();
                $('.report__wrapper').css('opacity', '1');
            });
          }
        });
      },
      requestPosts = function(postsPerPageVal) {
        searchInputVal = $('#js__posts--input').val();
        searchCategoryVal = $('.js-news-container').data('default-tag');
        console.log('http://pbi.org.pl/aktualnosci?cat=' + searchCategoryVal + '&order=' + searchSortVal + '&s=' + searchInputVal + '&posts=' + postsPerPageVal);
        $.ajax({
          url: 'http://pbi.org.pl/aktualnosci?cat=' + searchCategoryVal + '&order=' + searchSortVal + '&s=' + searchInputVal + '&posts=' + postsPerPageVal,
          beforeSend: function() {
            $postsWrapper.css('opacity', '0.7');
          },
          success: function(el) {
            $postsWrapper.fadeOut(300, function(){
                $postsWrapper.html($(el).find('.news__content--wrapper').html()).fadeIn();
                $postsWrapper.css('opacity', '1');
            });
          }
        });
      },
      requestMedia = function() {
        var typ;
        searchSortVal = $mediaSort.val();
        if(searchCategoryVal !== '') {
          typ = 'typ=' + searchCategoryVal + '&';
        } else {
          typ = '';
        }
        $.ajax({
          url: 'http://pbi.org.pl/media?' + typ + 'order=' + searchSortVal,
          beforeSend: function() {
            $mediaWrapper.css('opacity', '0.7');
          },
          success: function(el) {
            console.log(this.url);
            $mediaWrapper.fadeOut(300, function(){
                $mediaWrapper.html($(el).find('.media-main__boxes--wrapper').html()).fadeIn();
                $mediaWrapper.css('opacity', '1');
            });
          }
        });
      },
      requestOpinions = function() {
        var posts;

        console.log($opinionsWrapper);
        posts = '?posts=' + opinionsPerPageVal;

        $.ajax({
          url: 'http://pbi.org.pl/szkolenia' + posts,
          beforeSend: function() {
            $opinionsWrapper.css('opacity', '0.7');
          },
          success: function(el) {
            var elements = $(el).find('#opinions-container').html();
            $opinionsWrapper.fadeOut(300, function(){
                $('#opinions-container').html(elements);
                $opinionsWrapper.css('opacity', '1');
            });
            $opinionsWrapper.fadeIn(400);
          }
        });
      },
      requestFaqs = function() {
        var posts;

        console.log($faqsWrapper);
        posts = '?posts=' + opinionsPerPageVal;

        $.ajax({
          url: 'http://pbi.org.pl/szkolenia' + posts,
          beforeSend: function() {
            $faqsWrapper.css('opacity', '0.7');
          },
          success: function(el) {
            var elements = $(el).find('#faqs-container').html();
            $faqsWrapper.fadeOut(300, function(){
                $('#faqs-container').html(elements);
                $faqsWrapper.css('opacity', '1');
            });
            $faqsWrapper.fadeIn(400);
          }
        });
      },
      requestPress = function() {
        $.ajax({
          url: 'http://pbi.org.pl/prezentacje?order=' + pressSortVal,
          beforeSend: function() {
            $pressWrapper.css('opacity', '0.7');
          },
          success: function(el) {
            console.log(this.url);
            $pressWrapper.fadeOut(300, function(){
                $pressWrapper.html($(el).find('.presentation-content--wrapper').html()).fadeIn();
                $pressWrapper.css('opacity', '1');
            });
          }
        });
      };

/**
 * Raporty i badania
 */

  $searchSelectors.change(function(e){
    searchCategoryVal = $searchCategory.val();
    searchSortVal = $searchSort.val();
    requestReports();
  });
  $postsPerPage.on('click', function(e){
    e.preventDefault();
    postsPerPageVal += 4;
    requestReports();
  });
  $searchInput.on('keyup', _.debounce(requestReports, 600));

/**
 * Aktualności
 */

  $postsSort.change(function(e){
    searchSortVal = $postsSort.val();
    requestPosts();
  });
  $postsCategory.on('click', function(){
    searchCategoryVal = $(this).attr('data-category-id');
    $postsCategory.removeClass('search__categories-item--active');
    $(this).addClass('search__categories-item--active');
    requestPosts();
  });
  $('#js__posts--loadMore').on('click', function(e){
    e.preventDefault();
    postsPerPageVal += 4;
    requestPosts(postsPerPageVal);
  });
  $('#js__posts--input').on('keyup', _.debounce(requestPosts, 600));

/**
 * Media
 */

  $mediaSort.change(function(e){
    searchSortVal = $mediaSort.val();
    requestMedia();
  });

  $mediaCategory.on('click', function(){
    searchCategoryVal = $(this).data('category-id');
    requestMedia();
  });

/**
 * Prezentacje
 */

  $pressSort.change(function(e){
    pressSortVal = $pressSort.val();
    requestPress();
  });

/**
 * Mobile search
 */

 var searchInput = $('.header__navigation-search'),
     textInput = $('.inner');

  searchInput.on('click',function(){
   if (textInput.value !== 0){
       $(textInput).submit(function(){
     });
   }
  });

 $loadmoreOpinions.on('click', function(){
   opinionsPerPageVal += 2;
   requestOpinions();
 });
 $loadmoreFaqs.on('click', function(){
   opinionsPerPageVal += 2;
   requestFaqs();
 });
  $('input[type="radio"]').on('click', function(){

    if($('input[value="Szkoleniem zamkniętym"]').is(':checked')) {
      $('.training__form-details__newform').show();
    } else {
      $('.training__form-details__newform').hide();
    }

  });

});
