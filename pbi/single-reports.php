<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 get_header('title'); the_post(); ?>
 <main class="single-report">
   <h1 class="single-report__title"><?php the_title(); ?></h1>
   <div class="single-report__aside">
     <ul>
       <li class="single-report__aside-details">Typ: <span><?php the_field('tag'); ?></span></li>
       <?php if(get_field('author')) : ?><li class="single-report__aside-details">Autor: <span><?php the_field('author'); ?></span></li><?php endif; ?>
       <li class="single-report__aside-details">Data dodania: <span><?php echo get_the_date(); ?></span></li>
       <li class="single-report__aside-details">Kategoria: <span><?php the_field('tag'); ?></span></li>
       <?php if(get_field('pages')) : ?><li class="single-report__aside-details">Liczba stron: <span><?php the_field('pages'); ?></span></li><?php endif; ?>
       <?php if(get_field('file')) : ?>
       <a href="<?php the_field('file'); ?>">
         <div class="single-report__aside-downl oad single-report__aside-download--desktop">
           <span class="single-report__aside-link">
             <?php if(get_field('tag') == 'raporty') : ?>
               Pobierz raport
             <?php else : ?>
               Pobierz analizę
             <?php endif; ?>
           <span>
          </div>
        </a>
        <?php endif; ?>
        <li class="single-report__aside-details" style="padding-top: 15px;">
          <!-- AddToAny BEGIN -->
          <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_google_plus"></a>
            <a class="a2a_button_pinterest"></a>
            <a class="a2a_button_email"></a>
            <a class="a2a_button_linkedin"></a>
            <a class="a2a_button_tumblr"></a>
          </div>
          <script async src="https://static.addtoany.com/menu/page.js"></script>
          <!-- AddToAny END -->
        </li>
     </ul>
   </div>
   <div class="single-report__main">
     <div class="single-report__main-paragraph">
       <?php the_content(); ?>
     </div>
   </div>
   <?php if(get_field('tag') == 'raporty') : ?>
     <a href="<?php the_field('file'); ?>"><div class="single-report__aside-download single-report__aside-download--mobile"> <span class="single-report__aside-link">Pobierz raport<span></div></a>
   <?php endif; ?>
 </main>
  <?php get_footer(); ?>
