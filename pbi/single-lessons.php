<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 get_header('title'); the_post(); ?>

 <main class="single-lesson">
 	<a href="<?php echo esc_url( home_url( '/akademia/lekcje' ) ); ?>" class="return return--lesson">Wróć</a>
 	<h3 class="single-lesson__title">Lekcja</h3>
 	<h3 class="single-lesson__number"><?php the_field('lesson_number'); ?></h3>
 	<h1 class="single-lesson__name"><?php the_title(); ?></h1>

 	<div class="single-lesson__content" style="padding-bottom: 55px;">
		<?php the_content(); ?>
		<!-- AddToAny BEGIN -->
		<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
			<a class="a2a_button_facebook"></a>
			<a class="a2a_button_twitter"></a>
			<a class="a2a_button_google_plus"></a>
			<a class="a2a_button_pinterest"></a>
			<a class="a2a_button_email"></a>
			<a class="a2a_button_linkedin"></a>
			<a class="a2a_button_tumblr"></a>
		</div>
		<script async src="https://static.addtoany.com/menu/page.js"></script>
      <!-- AddToAny END -->
	</main>
 	</div>
  <?php get_footer(); ?>