<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 get_header('title'); ?>

<main>
	<section class="presentation">
		<div class="presentation-content">
			<!-- <a href="<?php echo esc_url( home_url( '/akademia' ) ); ?>" class="return return--presentation">Wróć</a> -->
			<div class="presentation-content__search">
				<div class="presentation-content__title">Wystąpienia konferencyjne</div>
				<div>
					<select id="js__select--presentation">
						<option value="desc">Pokaż wszystkie</option>
						<option value="desc">Najnowsze</option>
						<option value="asc">Najstarsze</option>
					</select>
				</div>
			</div>
			<div class="presentation-content--wrapper">
				<?php
					$order = get_query_var('order', 'desc');
					$circles = array(
						'presentation-content__box-circle--top',
						'presentation-content__box-circle--left',
						'presentation-content__box-circle--right'
					);
					$press = new WP_Query(array(
						'post_type' => 'press',
						'posts_per_page' => -1,
						'order' => $order,
						'orderby' => 'date'
					));
					$index = 0;
					if($press->have_posts()) : while($press->have_posts()) : $press->the_post();
				?>
				<div class="presentation-content__box <?php echo $circles[array_rand($circles, 1)]; ?>">
					<h3 class="presentation-content__box-title">
            <a href="<?php the_permalink(); ?>" class="link--black"><?php the_title(); ?></a>
          </h3>
					<div class="presentation-content__box-details">
						<p class="presentation-content__box-author">
							<?php the_author(); ?>
						</p>
						<p class="presentation-content__box-date"><?php echo get_the_date(); ?><p>
						<a href="<?php the_permalink(); ?>" class="presentation-content__box-button"></a>
					</div>
				</div>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>
