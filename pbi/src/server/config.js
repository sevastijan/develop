import express from 'express'
import path from 'path'

module.exports = (app) => {
  //Serve Static Files
  app.use('../', express.static(path.join(process.cwd(), 'public')))
  app.set('views', process.cwd())
  app.set('view engine', 'ejs')
}
