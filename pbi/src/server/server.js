import express from 'express'

const app = express()

require('./config')(app)
require('./routes')(app)





app.listen(process.env.PORT, function () {
  console.log(`Example app listening on port ${process.env.PORT}`)
})
