<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * Template Name: Kontakt
 * @package WordPress
 *
 */

 get_header('title'); the_post();?>

  <main class="contact">
    <img src="<?php echo THEME_URL;?>public/img/kontakt/contact-intro-circle.png" alt="" class="contact__intro-circle">
    <div class="contact--wrapper">
        <h1 class="contact__title">Wprowadzenie</h1>
        <div class="contact__intro-text">
          <?php the_content(); ?>
        </div>
        <div class="contact__us">
          <h1 class="contact__us-title">Skontaktuj się z nami</h1>
          <div class="contact__us--wrapper">
          <?php if( have_rows('contact_persons') ): while ( have_rows('contact_persons') ) : the_row(); ?>
            <div class="contact__us-box">
              <div class="contact__us-box--wrapper">
                <img src="<?php the_sub_field('image'); ?>" alt="" class="contact__us-box__image">
                <div class="contact__us-box--details">
                  <h3 class="contact__us-box__name"><?php the_sub_field('name'); ?></h3>
                  <h4 class="contact__us-box__surname"><?php the_sub_field('surname'); ?></h4>
                  <h5 class="contact__us-box__area"><?php the_sub_field('department'); ?></h5>
                </div>
            </div>
              <div class="contact__us-box__text"><?php the_sub_field('description'); ?></div>
              <p class="contact__us-box__mail"><a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></p>
            </div>
          <?php endwhile; endif; ?>
        </div>
        <div class="contact__us-map">
          <div class="contact__us-map__title">Jak Dojechać</div>
          <div class="contact__us-contact">
            <h1 class="contact__us-contact__title">Kontakt</h1>
            <div class="contact__us-contact__information">
              <div class="contact__us-contact__paragraph">
                <?php the_field('kontakt'); ?>
              </div>
            </div>
            <h1 class="contact__us-contact__title">Dane Rejestrowe</h1>
            <div class="contact__us-contact__information">
              <div class="contact__us-contact__paragraph">
                <?php the_field('dane_rejestrowe'); ?>
              </div>
            </div>
          </div>
          <section id="map" class="map__img">
          </section>
          <div class="map">
            <div class="map__contact">
              <div class="map__contact-content">
                <div class="map__contact-left">
                  <h1 class="map__contact-title">Kontakt</h1>
                  <div class="map__contact-text">
                    <?php the_field('kontakt'); ?>
                  </div>
                </div>
                <div class="map__contact-right">
                  <h1 class="map__contact-title">Dane Rejestrowe</h1>
                  <div class="map__contact-text">
                    <?php the_field('dane_rejestrowe'); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
    </div>
  </main>
<?php get_footer(); ?>
