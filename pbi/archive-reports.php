<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 get_header('title'); ?>
  <main>

    <div class="search">
      <div class="search--wrapper-reports">
        <div class="search__input--wrapper">
          <input id="js__search--input" class="search__field" placeholder="Wpisz szukaną frazę"/>
        </div>
        <div class="search__details">
          <select id="js__search--category" class="search__select">
            <option value="">Wszystkie</option>
            <option value="raporty">Raporty</option>
            <option value="analizy">Analizy</option>
            <option value="infografiki">Infografiki</option>
          </select>
          <select id="js__search--sort" class="search__select">
            <option value="desc">Sortuj wg daty</option>
            <option value="desc">Najnowsze</option>
            <option value="asc">Najstarsze</option>
          </select>
        </div>
      </div>
    </div>
    <div class="report">
      <div class="report__wrapper">
        <?php
          $postsPerPage = get_query_var('posts', 8);
          $tag = get_query_var('typ', array('raporty', 'analizy', 'infografiki'));
          $order = get_query_var('order', 'desc');
          $search = get_query_var('keyword');

          $reports = new WP_Query(array(
              'post_type' => 'reports',
              'posts_per_page' => $postsPerPage,
              'meta_query' => array(
                  array(
                      'key' => 'tag',
                      'value' => $tag,
                      'compare' => 'IN'
                  )
              ),
              'order' => $order,
              'orderby' => 'date',
              's' => $search
            ));

          $index = 0;
          if($reports->have_posts()) : while($reports->have_posts()) : $reports->the_post();

        ?>
        <div class="report__box">
          <img src="<?php echo THEME_URL;?>public/img/circles/basic-circle-top.png" alt="" class="report__box-circle">
          <div class="report__box--wrapper">
            <div class="report__box-tag
              <?php if(get_field('tag') == 'analizy') : ?>report__box-tag--red<?php endif; ?>
              <?php if(get_field('tag') == 'infografiki') : ?>report__box-tag--blue<?php endif; ?>
            ">
              <h5 class="report__box-tag__name
                <?php if(get_field('tag') == 'analizy') : ?>report__box-tag__name--red<?php endif; ?>
                <?php if(get_field('tag') == 'infografiki') : ?>report__box-tag__name--blue<?php endif; ?>
              "><?php the_field('tag'); ?></h5>
            </div>
            <h3 class="report__box-title">
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h3>
            <p class="report__box-text">
              <?php the_excerpt_max_charlength(190); ?>
            </p>
          </div>
          <div class="report__box-details">
            <div class="report__box-details--left">
              <p class="report__box-details__author"><?php the_author(); ?></p>
              <p class="report__box-details__date"><?php echo get_the_date('F j, Y'); ?></p>
            </div>
            <div class="report__box-details--right">
              <a class="report__box-details__link" href="<?php the_permalink(); ?>"><img src="<?php echo THEME_URL;?>public/img/arrow-small-HD.png" alt="" class="report__box-details__icon"></a>
            </div>
          </div>
        </div>
        <?php $index++; endwhile; else: ?>
          <h2>Brak postów</h2>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        <input type="hidden" id="js__search--counter" value="<?php echo $index; ?>">
      </div>
      <div class="clearfix" style="clear: both;"></div>
      <div class="report__loader">
        <a id="js__search--loadMore" href=""><div class="report__button">Pokaż więcej</div></a>
      </div>
    </div>
  </main>
<?php get_footer(); ?>
