<?php
/**
 * Wordpress template created for "Pawelblonski.pl"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Single article
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>


<article class="article subpage">

    <div class="content-wrapper narrow single">
        <h1 class="typo typo_primary heading">Aktualności</h1>

        <h1 class="typo typo_primary title">
            <?php the_title(); ?>
        </h1>

        <div class="typo typo_text">
            <?php the_content(); ?>
        </div>


        <div class="listing-btn">
            <div id="js-BackToListing" class="typo typo_primary">
                <span>Wróć do listy artykułów.</span>
            </div>
        </div>


    </div>

</article>


<?php get_footer(); ?>
