<?php
/**
 * Wordpress template created for "Browarsady"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Posts listing
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>

<div class="posts-listing subpage">
    <div class="content-wrapper narrow">
        <h1 class="typo typo_primary heading">Aktualności</h1>
    
        <?php 
            $loop = new WP_Query( array('posts_per_page' => 6, 'paged' => $paged ) );
            if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <article class="posts-item">
                        <a class="posts-item-href" href="<?php the_permalink(); ?>">
                            <figure class="posts-item_img" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></figure>
                            <div class="posts-item_content">
                                <h2 class="typo typo_primary">
                                    <?php the_title(); ?>
                                </h2>
                                <div class="typo typo_text">
                                    <?php $content = get_the_excerpt(); echo mb_strimwidth($content, 0, 300);?>
                                    <span>Czytaj dalej...</span>
                                </div>
                            </div>
                        </a>
                    </article>
                <?php endwhile;
            endif;
            wp_reset_postdata();
        ?>

    </div>

</div>

<?php get_footer(); ?>
