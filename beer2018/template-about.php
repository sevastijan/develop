<?php
/**
 * Wordpress template created for "Browarsady"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: About us
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>

<div class="about subpage">
    <div class="content-wrapper narrow">
        <h1 class="typo typo_primary heading"><?php the_title(); ?></h1>
    
        <div class="typo typo_text">
            <?php the_content(); ?>
        </div>

    </div>

</div>

<?php get_footer(); ?>
