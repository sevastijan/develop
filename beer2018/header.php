<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset') ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php wp_title(); ?>
	</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri() . '/css/style.css'?>" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <header class="header">
        <nav class="nav">
            <div class="content-wrapper">

                <?php require(THEME_DIR.'/_includes/_nav-list.php'); ?>

                <figure class="nav-logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo PB_THEME_URL; ?>/images/brand-logo.png" alt="brand-logo" class="nav-logo_img">
                    </a>
                </figure>

                <?php require(THEME_DIR.'/_includes/_burger.php'); ?>

                <?php require(THEME_DIR.'/_includes/_social-icons.php'); ?>
                
            </div>
        </nav>
    </header>