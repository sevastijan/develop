<?php
/**
 * Wordpress template created for "Browarsady"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Gallery
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>

<div class="gallery subpage">
    <div class="content-wrapper">
        <h1 class="typo typo_primary heading">Galeria</h1>

        <?php echo do_shortcode('[supsystic-gallery id=1]') ?>

    </div>

</div>

<?php get_footer(); ?>
