// burger
$("#js-burger, #js-navListClose").click(function(){
    $("#js-navList").toggleClass("is-active");
    $("body").toggleClass("is-active");
});


// hide menu after click
$(".list_item").click(function(){
    $("body").removeClass("is-active");
    $("#js-menu-list").removeClass("is-active");
});


// slider
$('#js-mainSlider').slick({
    dots: false,
    infinite: true,
    slidesToShow: 1,
    arrows: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    nextArrow: '<i class="slider-arrows right"></i>',
    prevArrow: '<i class="slider-arrows left"></i>',
});


$("#js-BackToListing").click(function() {
    window.history.back();
});

// fixed navbar
var num = 60;


$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('.nav').addClass('reduced');
    } else {
        $('.nav').removeClass('reduced');
    }
});

