<section class="beers clearfix">
    <div class="content-wrapper">
        <h1 class="typo typo_primary heading">Nasze piwa</h1>
        <ul class="beers-list">


        <?php if ( have_rows('beers', 'options') ) : ?>

            <?php while( have_rows('beers', 'options') ) : the_row(); ?>

                    <li class="beers-list_item">
                        <img src="<?php the_sub_field('beers_bootle', 'options'); ?>" alt="beer" class="beers-list_item-img">
                        <div class="beers-list_item-info">
                            <?php if( get_sub_field('beers_name', 'options') ) : ?>
                                <p class="typo typo_text">
                                    Nazwa: <span><?php the_sub_field('beers_name', 'options'); ?></span>
                                </p>
                            <?php endif; ?>
                            <?php if( get_sub_field('beers_materials', 'options') ) : ?>
                                <p class="typo typo_text">
                                    Surowce: <span><?php the_sub_field('beers_materials', 'options'); ?></span>
                                </p>
                            <?php endif; ?>
                            <?php if( get_sub_field('beers_alcohol', 'options') ) : ?>
                                <p class="typo typo_text">
                                    Zawartość alkoholu: <span><?php the_sub_field('beers_alcohol', 'options'); ?></span>
                                </p>
                            <?php endif; ?>
                            <?php if( get_sub_field('beers_bitterness', 'options') ) : ?>
                                <p class="typo typo_text">
                                    Goryczka: <span><?php the_sub_field('beers_bitterness', 'options'); ?></span>
                                </p>
                            <?php endif; ?>
                            <?php if( get_sub_field('beers_sweetness', 'options') ) : ?>
                                <p class="typo typo_text">
                                    Słodycz: <span><?php the_sub_field('beers_sweetness', 'options'); ?></span>
                                </p>
                            <?php endif; ?>
                        </div>
                        <div class="beers-list_item-descr">
                            <div class="typo typo_text">
                                <?php the_sub_field('beers_descr', 'options'); ?>
                            </div>
                        </div>
                    </li>

            <?php endwhile; ?>

        <?php endif; ?>


        </ul>
    </div>
</section>