<section id="js-mainSlider" class="main-slider content-wrapper" >
    <?php if ( have_rows('main_slider', 'options') ) : ?>
        <?php while( have_rows('main_slider', 'options') ) : the_row(); ?>
            <div class="main-slider_item" style="background-image:url('<?php the_sub_field('main_slider-img', 'options'); ?>');">
                <h2 class="typo typo_primary">
                    <?php the_sub_field('main_slider-heading', 'options'); ?>
                </h2>
                <h3 class="typo typo_primary">
                    <?php the_sub_field('main_slider-descr', 'options'); ?>
                </h3>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</section>