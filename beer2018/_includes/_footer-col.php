<div class="content-wrapper">
    <div class="footer-boxes">
        <div class="footer-boxes_box">
            <?php echo do_shortcode( '[contact-form-7 id="66" title="Formularz kontaktowy"]' ); ?>
        </div>
        <div class="footer-boxes_box">
        <div class="text-wrapper">
            <div class="typo typo_text">
                Siedziba firmy:
                <br><br>
                ul. Logistyczna 21
                <br>
                62-080 Sady
                <br>
                Polska
                <br>
                NIP: 7811947731
                <br>
                Mail: <a href="mailto:biuro@browarsady.pl">biuro@browarsady.pl</a>
                <br>
                Tel: <a href="callto:662078316">662 078 316</a>
            </div>
        </div>
        </div>
        <div class="footer-boxes_box">
            <div class="footer-boxes_box-map">
<iframe src="https://snazzymaps.com/embed/89264" width="340px" height="340px" style="border:none;"></iframe>            </div>
        </div>
</div>
</div>