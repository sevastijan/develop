<!-- <ul id="js-navList" class="nav-list">
    <li class="nav-list_item">
        <a href="#" class="typo typo_primary">
            Nasze piwa
        </a>
    </li>
    <li class="nav-list_item">
        <a href="#" class="typo typo_primary">
            Aktualności
        </a>
    </li>
    <li class="nav-list_item">
        <a href="#" class="typo typo_primary">
            O nas
        </a>
    </li>
    <li class="nav-list_item">
        <a href="#" class="typo typo_primary">
            Galeria
        </a>
    </li>
    <li class="nav-list_item">
        <a href="#" class="typo typo_primary">
            Gdzie kupić
        </a>
    </li>
    <li class="nav-list_item">
        <a href="#" class="typo typo_primary">
            Kontakt
        </a>
    </li>
</ul> -->

<?php
wp_nav_menu( array(
    'theme_location' => 'primary',
    'menu_id' => 'js-navList',
    'menu_class' => 'nav-list',
    'container'=> 'ul'
));
?>