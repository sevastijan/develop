<figure class="footer_logo">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
    <img src="<?php echo PB_THEME_URL; ?>/images/footer-logo.png" alt="brand-logo" class="footer_logo-img">
    </a>
</figure>