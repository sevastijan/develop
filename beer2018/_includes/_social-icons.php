<div class="social-icons">
    <?php if ( have_rows('social', 'options') ) : ?>
        <?php while( have_rows('social', 'options') ) : the_row(); ?>    
            <a href="<?php the_sub_field('social_url', 'options'); ?>">
                <img src="<?php the_sub_field('social_img', 'options'); ?>" alt="social icon" class="social-icons_img">
            </a>
        <?php endwhile; ?>
    <?php endif; ?>
</div>