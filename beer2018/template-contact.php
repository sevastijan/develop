<?php
/**
 * Wordpress template created for "Browarsady"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Contact
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>

<div class="contact subpage">
    <div class="content-wrapper narrow">
        <h1 class="typo typo_primary heading">Kontakt</h1>
    
        <div class="typo typo_text">
            <?php the_content(); ?>
        </div>

    </div>

</div>

<?php get_footer(); ?>
