  <footer class="footer">
    



<?php 
    $classes = get_body_class(); 
    
    if (in_array('page-id-62',$classes)) {
      require(THEME_DIR.'/_includes/_footer-col.php');
    } else {
      require(THEME_DIR.'/_includes/_footer-logo.php');
    }

?>


    <div class="footer-bar">
      <div class="content-wrapper">
        <?php require(THEME_DIR.'/_includes/_social-icons.php'); ?>
        <p class="typo typo_text">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
            BrowarSady.pl 2018
          </a>
        </p>
        <p class="typo typo_text">
          Projekt: <a href="https://pawelblonski.pl/" target="_blank" title="Rysownik i ilustrator Paweł Błoński - Studio grafiki i ilustracji">PawelBlonski.pl</a>
        </p>
      </div>
    </div>


  </footer>
  
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . '/js/app.js'?>"></script>
    <?php wp_footer(); ?>
</body>
</html>