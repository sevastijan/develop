// burger
$("#js-burger, #js-navListClose").click(function(){
    $("#js-navList").toggleClass("is-active");
    $("body").toggleClass("is-active");
});


// hide menu after click
$(".list_item").click(function(){
    $("body").removeClass("is-active");
    $("#js-menu-list").removeClass("is-active");
});


// header slider
$('#js-headerSlider').slick({
    dots: false,
    infinite: true,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
    nextArrow: '<i class="slider-arrows right"></i>',
    prevArrow: '<i class="slider-arrows left"></i>',
});

// realizations slider
$("#js-realizationsSlider").slick({
  dots: false,
  infinite: true,
  slidesToShow: 4,
  centerMode: true,
  nextArrow: '<i class="slider-arrows right"></i>',
  prevArrow: '<i class="slider-arrows left"></i>',
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        centerMode: false
      }
    },
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        centerMode: true
      }
    },
    {
      breakpoint: 1550,
      settings: {
        slidesToShow: 3,
        centerMode: true
      }
    }
  ]
});

// collabo slider
$('#js-collaboSlider').slick({
    infinite: true,
    slidesToShow: 5,
    centerMode: true,
    nextArrow: '<i class="slider-arrows right"></i>',
    prevArrow: '<i class="slider-arrows left"></i>',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                centerMode: false
            }
        },
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                centerMode: true
            }
        },
        {
            breakpoint: 1550,
            settings: {
                slidesToShow: 4,
                centerMode: true
            }
        }
    ]
});

//go back function
$(".js-goBackBtn").click(function() {
    window.history.back();
});

//preloader
$(window).on('load', function () {
    $("#preloader").fadeOut();
    setTimeout(function() {
      $("#preloader").remove();
    }, 500);
});


// realization scroll
$('#realizationsBtn').on('click', function (event) {
    var target = $('#realizations');
    
    if (target.length) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top - 30
        }, 1000);
    }
});


// fixed navbar
var num = 60,
    show = true;


$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('.header-nav').addClass('reduced');
    } else {
        $('.header-nav').removeClass('reduced');
    }
});

// desktop navbar fixed
var num = 130,
    show = true;


$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('#js-navWrapper').addClass('desktop-fixed');
    } else {
        $('#js-navWrapper').removeClass('desktop-fixed');
    }
});
