<?php
/**
 * Wordpress template created for "Pawelblonski.pl"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Subpage listing
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>

<?php 
    $classes = get_body_class(); 
    if (in_array('page-id-87',$classes)) {
        the_post();
    }

    require(THEME_DIR.'/_includes/_subpage-listing.php');
    
    if (in_array('page-id-7',$classes)) {
        require(THEME_DIR.'/_includes/_realizations-listing.php');
    } elseif (in_array('page-id-37', $classes)) {
        require(THEME_DIR.'/_includes/_realizations-listing_tales.php');
    } elseif (in_array('page-id-87', $classes)) {
        // require(THEME_DIR.'/_includes/_www-offer.php');
        require(THEME_DIR.'/_includes/_realizations-listing_tales.php');
    } elseif (in_array('page-id-169', $classes)) {
        require(THEME_DIR.'/_includes/_realizations-listing_tales.php');
    } elseif (in_array('page-id-84', $classes)) {
        require(THEME_DIR.'/_includes/_realizations-listing_tales.php');
    } elseif (in_array('page-id-465', $classes)) {
        require(THEME_DIR.'/_includes/_realizations-listing_tales.php');
    }

?>

<?php get_footer(); ?>
