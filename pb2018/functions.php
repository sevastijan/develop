<?php

if(!defined('THEME_DIR')) {
define('THEME_DIR', get_theme_root(). '/' .get_template() . '/');
}

if(!defined('PB_THEME_URL')) {
define('PB_THEME_URL', WP_CONTENT_URL. '/themes/' .get_template() . '/');
}

  /**
  *  Register settings page
  *
  */
  if( function_exists('acf_add_options_page')) {
  	acf_add_options_page(array(
  		'page_title' 	=> 'Ustawienia',
        'post_id' => 'options',
  		'menu_title'	=> 'Ustawienia',
  		'menu_slug' 	=> 'module-settings',
  		'capability'	=> 'edit_posts',
  		'redirect'		=> false
  	));
  }

  register_nav_menus(array(
    'primary' => 'Main navigation'
  ));
  register_nav_menus(array(
    'secondary' => 'Footer col-0'
  ));
  register_nav_menus(array(
    'tertiary' => 'Footer col-1'
  ));
  register_nav_menus(array(
    'quaternary' => 'Footer col-2'
  ));
  add_theme_support( 'post-thumbnails' );

  function create_post_type() {
    register_post_type( 'tales',
      array(
        'labels' => array(
          'name' => __( 'Ilustracje' ),
          'singular_name' => __( 'Ilustracje' )
        ),
        'public' => true,
        'has_archive' => true,
	    	'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
      )
    );
  }
  add_action( 'init', 'create_post_type' );

  function create_post_type1() {
    register_post_type( 'branding',
      array(
        'labels' => array(
          'name' => __( 'Branding' ),
          'singular_name' => __( 'Branding' )
        ),
        'public' => true,
        'has_archive' => true,
	    	'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
      )
    );
  }
  add_action( 'init', 'create_post_type1' );

  function create_post_type2() {
    register_post_type( 'www',
      array(
        'labels' => array(
          'name' => __( 'Strony WWW' ),
          'singular_name' => __( 'Strony WWW' )
        ),
        'public' => true,
        'has_archive' => true,
	    	'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
      )
    );
  }
  add_action( 'init', 'create_post_type2' );

  function create_post_type3() {
    register_post_type( 'package',
      array(
        'labels' => array(
          'name' => __( 'Etykiety - opakowania' ),
          'singular_name' => __( 'Etykiety - opakowania' )
        ),
        'public' => true,
        'has_archive' => true,
	    	'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
      )
    );
  }
  add_action( 'init', 'create_post_type3' );

  function create_post_type4() {
    register_post_type( 'comics',
      array(
        'labels' => array(
          'name' => __( 'Komiksy' ),
          'singular_name' => __( 'Komiksy' )
        ),
        'public' => true,
        'has_archive' => true,
	    	'supports' => array( 'title', 'thumbnail')
      )
    );
  }
  add_action( 'init', 'create_post_type4' );

    function create_post_type6() {
    register_post_type( 'other_posts',
      array(
        'labels' => array(
          'name' => __( 'Pozostałe artykuły' ),
          'singular_name' => __( 'Pozostałe artykuły' )
        ),
        'public' => true,
        'has_archive' => true,
	    	'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
      )
    );
  }
  add_action( 'init', 'create_post_type6' );

    function create_post_type7() {
    register_post_type( 'copywriting',
      array(
        'labels' => array(
          'name' => __( 'Copywriting' ),
          'singular_name' => __( 'Copywriting' )
        ),
        'public' => true,
        'has_archive' => true,
	    	'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
      )
    );
  }
  add_action( 'init', 'create_post_type7' );

  function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a.*>)?\s*(<img.*\/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
  }

  add_filter('the_content', 'filter_ptags_on_images');

?>
