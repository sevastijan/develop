<?php
/**
 * Wordpress template created for "Pawelblonski.pl"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Coloring page
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>

<section class="subpage-heading no-realizations">
    <div class="content-wrapper narrow">
        <img src="https://pawelblonski.pl/wp-content/themes/pb2018/images/subpage-heading.png" alt="subpage-img" class="subpage-heading_img">
        <h1 class="typo typo_primary title">
            Kolorowanki
        </h1>
    </div>
</section>

<div class="content-wrapper narrow">
    <div class="typo typo_text subpage-listing_heading">
        <?php the_content(); ?>
    </div>
</div>

<section class="coloring">
    <div class="content-wrapper">
        <div class="coloring-items">
        
            <?php if ( have_rows('coloring', 'options') ) : ?>
            
                <?php while( have_rows('coloring', 'options') ) : the_row(); ?>

                <a href="<?php the_sub_field('url', 'options') ?>">
                    <figure class="coloring-items_item" style="background-image:url('<?php the_sub_field('img', 'options'); ?>');"></figure>
                </a>
            
                <?php endwhile; ?>
            
            <?php endif; ?>
        
        </div>
    </div>
</section>





<?php get_footer(); ?>
