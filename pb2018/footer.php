  <footer class="footer">

    <?php require(THEME_DIR.'/_includes/_footer-col.php'); ?>


    <?php require(THEME_DIR.'/_includes/_footer-sitemap.php'); ?>


    <?php require(THEME_DIR.'/_includes/_copyright-bar.php'); ?>

  </footer>
  
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . '/js/app.js'?>"></script>
    <?php wp_footer(); ?>
</body>
</html>