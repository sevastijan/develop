<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset') ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php wp_title(); ?>
	</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri() . '/css/style.css'?>" rel="stylesheet">
	<link rel="shortcut icon" href="<?php echo PB_THEME_URL; ?>images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo PB_THEME_URL; ?>images/favicon.ico" type="image/x-icon">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="preloader" class="preloader">
	<img class="preloader_img" src="<?php echo PB_THEME_URL; ?>/images/preloader.png" alt="preloader-img"/>
</div>


    <header class="header">
        <div class="content-wrapper">
            <section class="header-nav">
                <figure class="header-nav_logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php echo PB_THEME_URL; ?>/images/brand-logo.png" alt="" class="header-nav_logo-img">
                    </a>
                </figure>


				<?php require(THEME_DIR.'/_includes/_header-about.php'); ?>


				<?php require(THEME_DIR.'/_includes/_header-social.php'); ?>


                <?php require(THEME_DIR.'/_includes/_burger.php'); ?>
                

				<?php require(THEME_DIR.'/_includes/_nav-list.php'); ?>

            </section>
        </div>
    </header>