<?php
/**
 * Wordpress template created for "Pawelblonski.pl"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Contact
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>

<section class="subpage-heading no-realizations">
    <div class="content-wrapper narrow">
        <img src="https://pawelblonski.pl/wp-content/themes/pb2018/images/subpage-heading.png" alt="subpage-img" class="subpage-heading_img">
        <h1 class="typo typo_primary title">
            Kontakt
        </h1>
    </div>
</section>


<section class="contact">
    <div class="content-wrapper narrow">
        <figure class="contact-figure">
            <img src="<?php echo PB_THEME_URL; ?>/images/pb-head.png" alt="" class="contact-figure_img">
        </figure>
        <div class="contact_descr">
            <p class="typo typo_text">
                Kliknąłeś w zakładkę kontakt - to bardzo dobrze. Jeżeli masz dużo pytań, chcesz realizować swój projekt, ale nie bardzo wiesz, jak się do tego zabrać, to najlepiej zadzwoń. A ja doradzę, podpowiem - to nic nie kosztuje. Preferuję kontakt telefoniczny ze względu na to, że łatwiej jest się zrozumieć i zajmuje to zdecydowanie mniej czasu, a efekty rozmowy są precyzyjne i dokładne. Jeżeli jednak wolisz formę pisemną, to oczywiście nie mam nic przeciwko. Tylko postaraj się być konkretnym, ponieważ na ogólnikowo zadane pytanie będę mógł odpowiedzieć też w ogólny i mało precyzyjny sposób.
            </p>
        </div>
    </div>
</section>


<?php get_footer(); ?>
