<div class="content-wrapper">
    <div class="footer-boxes">
            <div class="footer-boxes_box">
        <div class="footer-boxes_box_heading">
            <img src="<?php echo PB_THEME_URL; ?>/images/brand-logo-small.png" alt="" class="footer-boxes_box_heading-img">
            <h4 class="typo typo_primary">
                Napisz wiadomość.
            </h4>
        </div>
            <?php echo do_shortcode( '[contact-form-7 id="5" title="Formularz kontaktowy"]' ); ?>
        </div>
        <div class="footer-boxes_box">
        <div class="footer-boxes_box_heading">
            <img src="<?php echo PB_THEME_URL; ?>/images/brand-logo-small.png" alt="" class="footer-boxes_box_heading-img">
            <h4 class="typo typo_primary">
                Dane teleadresowe.
            </h4>
        </div>
        <div class="text-wrapper">
            <p class="typo typo_text">
                Paweł Błoński -<br />
                Studio grafiki i ilustracji
            </p>
            <p class="typo typo_text">
                37-700 Przemyśl<br />
                ul. Bakończycka 7 /210<br />
                NIP:795 214 53 01
            </p>
            <p class="typo typo_text">
                tel. <a href="tel:606398351">606 398 351</a><br />
                mail: <a href="mailto:pracownia@pawelblonski.pl">pracownia@pawelblonski.pl</a> 
            </p> <br>
            <p class="typo typo_text">
                Godziny pracy: <br />
                Do poniedziałku do piątku <br />
                od 10:00 do 16:00
            </p>
        </div>
        </div>
        <div class="footer-boxes_box">
        <div class="footer-boxes_box_heading">
            <img src="<?php echo PB_THEME_URL; ?>/images/brand-logo-small.png" alt="" class="footer-boxes_box_heading-img">
            <h4 class="typo typo_primary">
                Kolorowanki do pobrania.
            </h4>
        </div>
            <ul class="footer-boxes_box_list">

                <?php if ( have_rows('coloring', 'options') ) :
                    $i = 0; 
                ?>
                
                    <?php while( have_rows('coloring', 'options') ) : the_row();
                        $i++;
                        if( $i > 4 )
                        {
                            break;
                        }
                    ?>

                        <li class="footer-boxes_box_list-item">
                            <a href="<?php the_field('c_url', 'options') ?>">
                                <img src="<?php the_sub_field('img', 'options'); ?>" alt="" class="footer-boxes_box_list-item-img">
                            </a>
                        </li> 
                
                    <?php endwhile; ?>
                
                <?php endif; ?>

                <!-- <li class="footer-boxes_box_list-item">
                    <a href="#">
                        <img src="<?php echo PB_THEME_URL; ?>/images/footer-colo.png" alt="" class="footer-boxes_box_list-item-img">
                    </a>
                </li>
                <li class="footer-boxes_box_list-item">
                    <a href="#">
                        <img src="<?php echo PB_THEME_URL; ?>/images/footer-colo.png" alt="" class="footer-boxes_box_list-item-img">
                    </a>
                </li>
                <li class="footer-boxes_box_list-item">
                    <a href="#">
                        <img src="<?php echo PB_THEME_URL; ?>/images/footer-colo.png" alt="" class="footer-boxes_box_list-item-img">
                    </a>
                </li>
                <li class="footer-boxes_box_list-item">
                    <a href="#">
                        <img src="<?php echo PB_THEME_URL; ?>/images/footer-colo.png" alt="" class="footer-boxes_box_list-item-img">
                    </a>
                </li> -->
            </ul>
        </div>
</div>
</div>