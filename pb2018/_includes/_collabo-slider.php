<section class="collabo-slider">
    <section class="page-heading">
        <img src="https://pawelblonski.pl/wp-content/themes/pb2018/images/brand-logo-small.png" alt="" class="page-heading_img">
        <h1 class="typo typo_primary">
            Dla nich pracowałem i są zadowoleni.
        </h1>
    </section>
    <div id="js-collaboSlider" class="header-slider-wrapper">
    <?php if ( have_rows('partners_slider', 'options') ) : ?>
        <?php while( have_rows('partners_slider', 'options') ) : the_row(); ?>
            <div class="header-slider-wrapper_item">
                <figure class="header-slider-wrapper_item-box">
                    <a href="<?php the_sub_field('partners_slider-url', 'options') ?>">
                        <img src="<?php the_sub_field('partners_slider-item', 'options'); ?>" alt="" class="header-slider-wrapper_item-box-img">
                    </a>
                </figure>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
    </div>
</section>