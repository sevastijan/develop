<section id="realizations" class="realizations-listing tales clearfix">
        <section class="page-heading">
            <img src="https://pawelblonski.pl/wp-content/themes/pb2018/images/brand-logo-small.png" alt="" class="page-heading_img">
            <h1 class="typo typo_primary">
                <?php the_field('realizations_heading'); ?>
            </h1>
        </section>
        <div class="content-wrapper">
            <?php
                $classes = get_body_class(); 
                if (in_array('page-id-37',$classes)) {
                    $post_type = 'tales';
                } elseif (in_array('page-id-87', $classes)) {
                    $post_type = 'www';
                } elseif (in_array('page-id-84', $classes)) {
                    $post_type = 'package';
                } elseif (in_array('page-id-169', $classes)) {
                    $post_type = 'branding';
                } elseif (in_array('page-id-465', $classes)) {
                    $post_type = 'copywriting';
                }
                $loop = new WP_Query( array( 'post_type' => $post_type, 'paged' => $paged ) );
                if ( $loop->have_posts() ) :
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <div class="realizations-listing-item">
                            <a href="<?php the_permalink(); ?>">
                                <figure class="realizations-listing-item_img" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></figure>
                                <div class="realizations-listing-item_descr">
                                    <h3 class="typo typo_secondary">
                                        <?php the_title(); ?>
                                    </h3>
                                    <div class="typo typo_text">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            </a>
                        </div>

                    <?php endwhile;
                endif;
                wp_reset_postdata();
            ?>

        </div>
    </section>