<div class="articles-more">
    <a
    <?php 
        if (in_array('post-template-default',$classes) or in_array('home',$classes)) {
            $btnHref = get_field('main_btn_more', 'options');
            $href = href; 
            $btnBack = "";
        } else {
            $btnHref = "";
            $btnBack = "js-goBackBtn";
        }
    ?>
    class="articles-more_href <?php echo $btnBack ?>" <?php echo $href ?>="<?php echo $btnHref ?>">
        <p <?php echo $btnBack ?> class="typo typo_secondary">
            <?php
                if (in_array('post-template-default',$classes) or in_array('home',$classes)) {
                    echo "Więcej artykułów";
                } else {
                    echo "Więcej realizacji";
                }
            ?>
        </p>
    </a> 
</div>