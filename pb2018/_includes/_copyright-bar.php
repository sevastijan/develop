<div class="copyright-bar">
    <div class="content-wrapper">
        <p class="typo typo_secondary">
           &copy; Paweł Błoński - Studio grafiki i ilustracji 2018 - PawelBlonski.pl
        </p>
        <p class="typo typo_secondary">
            <a href="<?php the_field('cookies_url', 'options'); ?>">
                Polityka plików cookie
            </a>
        </p>
        <p class="typo typo_secondary">
            projekt i relizacja: <a href="#">PawelBlonski.pl</a>
        </p>
    </div>
</div>