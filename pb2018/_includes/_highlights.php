<section class="highlights">
    <div class="content-wrapper">
        <div class="highlights_heading">
            <h2 class="typo typo_primary">
                Copywriting i scenariusze
            </h2>
            <div class="typo typo_text">
                <?php the_field('copy_scenario', 'options') ?>
            </div>
        </div>
        <div class="highlights-boxes">
        <?php if ( have_rows('copy_scenario-list', 'options') ) : ?>
            <?php while( have_rows('copy_scenario-list', 'options') ) : the_row(); ?>
                <div class="highlights-boxes_box">
                    <img src="<?php the_sub_field('copy_scenario-list-img', 'options'); ?>" alt="" class="highlights-boxes_box-img">
                    <a href="<?php the_sub_field('copy_scenario-list-subpage', 'options'); ?>">
                         <h4 class="typo typo_primary">
                            <?php the_sub_field('copy_scenario-list-heading', 'options'); ?>
                        </h4>
                    </a>
                    <div class="typo typo_text">
                        <?php the_sub_field('copy_scenario-list-descr', 'options'); ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </div>
</section>