<section class="header-slider">
    <div id="js-headerSlider" class="header-slider-wrapper">
        <?php if ( have_rows('main_slider', 'options') ) : ?>
            <?php while( have_rows('main_slider', 'options') ) : the_row(); ?>
            <figure class="header-slider-wrapper_item">
                <img src="<?php the_sub_field('main_slider-item', 'options'); ?>" alt="" class="header-slider-wrapper_item-img">
            </figure>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>