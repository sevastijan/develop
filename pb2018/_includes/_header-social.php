<div class="header-nav_social">
    <a href="https://www.facebook.com/przygodyrysownika/" target="_blank">
        <img src="<?php echo PB_THEME_URL; ?>/images/fb-logo.png" alt="" class="header-nav_social-img">
    </a>
    <a href="http://www.przygodyrysownika.pl/" target="_blank">
        <p class="typo typo_blog">
            BLOG
        </p>
    </a>
    <span class="typo typo_primary">tel. <a href="tel:606398351" class="typo typo_primary">606 398 351</a><br /> </span>
</div>