    <section class="www-offer">
        <div class="content-wrapper">

            <?php if ( have_rows('www_offer') ) : ?>
                <?php while( have_rows('www_offer') ) : the_row(); ?>
                <div class="www-offer-item">
                    <img src="<?php the_sub_field('www_offer-img'); ?>" alt="" class="www-offer-item_img">
                    <h3 class="typo typo_text www-offer_heading">
                        <?php the_sub_field('www_offer-header'); ?>
                    </h3>
                    <p class="typo typo_text www-offer_descr">
                        <?php the_sub_field('www_offer-descr'); ?>
                    </p>
                    <?php if ( have_rows('www_offer-list') ) : ?>
                        <?php while( have_rows('www_offer-list') ) : the_row(); ?>
                            <ul class="www-offer-item_list">
                                <li class="typo typo_text www-offer-item_list-item">
                                    <?php the_sub_field('www_offer-list-item'); ?>
                                </li>
                            </ul>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
            <?php endif; ?>

        </div>
    </section>