<div class="header-nav_about">
    <a href="<?php the_field('about_url', 'options'); ?>">
        <p class="typo typo_primary">
            czytaj więcej o mnie.
        </p>
        <div class="header-nav_about-img" style="background-image:url('<?php echo PB_THEME_URL; ?>/images/ja.jpg');"></div>
    </a>
    <p class="typo typo_primary">
        Cześć, nazywam się Paweł! Jestem zawodowym rysownikiem, ilustratorem
        i projektantem. W branży artystycznej działam już ponad 15 lat. <br />
        Co mogę dla ciebie zrobić ? 
    </p>
</div>