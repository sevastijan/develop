<section class="articles main">
    <div class="content-wrapper">
        <?php 
            $classes = get_body_class(); 
            $loop = new WP_Query( array('posts_per_page' => 3, 'paged' => $paged ) );
            if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <article class="articles-item">
                        <a class="articles-item-href" href="<?php the_permalink(); ?>">
                            <figure class="articles-item_img" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></figure>
                            <div class="articles-item_content">
                                <h2 class="typo typo_primary">
                                    <?php the_title(); ?>
                                </h2>
                                <div class="typo typo_text">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        </a>
                    </article>
                <?php endwhile;
            endif;
            wp_reset_postdata();

            require(THEME_DIR.'/_includes/_more-articles-btn.php');
        ?>
    </div>
</section>