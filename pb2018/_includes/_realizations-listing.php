<section id="realizations" class="realizations-listing clearfix">
        <section class="page-heading">
            <img src="https://pawelblonski.pl/wp-content/themes/pb2018/images/brand-logo-small.png" alt="" class="page-heading_img">
            <h1 class="typo typo_primary">
                Realizacje - Fragmenty komiksów.
            </h1>
        </section>
        <div class="content-wrapper">
            <?php
                $loop = new WP_Query( array( 'post_type' => 'comics', 'paged' => $paged ) );
                if ( $loop->have_posts() ) :
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <div class="realizations-listing-item">
                            <a href="<?php the_field('comics_link') ?>" target="_blank">
                                <figure class="realizations-listing-item_img" style="background-image:url('<?php the_post_thumbnail_url(); ?>');">
                                    <figcaption class="realizations-listing-item_img-heading typo typo_text">
                                        <span>
                                            <?php the_title(); ?>
                                        </span>
                                        <span>Kliknij i pobierz fragmet.</span>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>

                    <?php endwhile;
                endif;
                wp_reset_postdata();
            ?>
        </div>
    </section>