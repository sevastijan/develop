<nav id="js-navWrapper" class="header-nav_nav">  
    <div class="content-wrapper">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header-nav_nav-brand-logo">
            <img src="<?php echo PB_THEME_URL; ?>/images/nav-brand-logo2.png" alt="">
        </a>
        <?php
        wp_nav_menu( array(
            'theme_location' => 'primary',
            'menu_id' => 'js-navList',
            'menu_class' => 'nav-list',
            'container'=> 'ul'
        ));
        ?>
    </div>
</nav>