<?php get_header(); ?>


    <?php require(THEME_DIR.'/_includes/_header-slider.php'); ?>

    <?php require(THEME_DIR.'/_includes/_offer.php'); ?>

    <?php require(THEME_DIR.'/_includes/_bar.php'); ?>

    <?php require(THEME_DIR.'/_includes/_realizations-slider.php'); ?>

    <?php require(THEME_DIR.'/_includes/_highlights.php'); ?>

    <?php require(THEME_DIR.'/_includes/_article.php'); ?>

    <?php require(THEME_DIR.'/_includes/_collabo-slider.php'); ?>


<?php get_footer(); ?>