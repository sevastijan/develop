<?php
/**
 * Wordpress template created for "Pawelblonski.pl"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Listing artykułów
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>

<section class="subpage-heading no-realizations">
    <div class="content-wrapper narrow">
        <img src="https://pawelblonski.pl/wp-content/themes/pb2018/images/subpage-heading.png" alt="subpage-img" class="subpage-heading_img">
        <h1 class="typo typo_primary title">
            Artykuły
        </h1>
    </div>
</section>
<section class="articles">
    <div class="content-wrapper narrow">
        <?php 
            $classes = get_body_class(); 
            $loop = new WP_Query( array('paged' => $paged ) );
            if ( $loop->have_posts() ) :
                while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <article class="articles-item">
                        <a class="articles-item-href" href="<?php the_permalink(); ?>">
                            <figure class="articles-item_img" style="background-image:url('<?php the_post_thumbnail_url(); ?>');"></figure>
                            <div class="articles-item_content">
                                <h2 class="typo typo_primary">
                                    <?php the_title(); ?>
                                </h2>
                                <div class="typo typo_text">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                        </a>
                    </article>
                <?php endwhile;
            endif;
            wp_reset_postdata();
        ?>
    </div>
</section>


<?php get_footer(); ?>
