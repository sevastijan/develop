$(document).ready(function() {
  // slick carousel
  $('.slider').slick({
    dots: true,
    vertical: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 123500,
    slidesToShow: 1,
    slidesToScroll: 1,
    verticalSwiping: false,
  });
});
