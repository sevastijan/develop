<?php

if(!defined('THEME_DIR')) {
define('THEME_DIR', get_theme_root(). '/' .get_template() . '/');
}

if(!defined('THEME_URL')) {
define('THEME_URL', WP_CONTENT_URL. '/themes/' .get_template() . '/');
}

  /**
  *  Register settings page
  *
  */
  if( function_exists('acf_add_options_page')) {
  	acf_add_options_page(array(
  		'page_title' 	=> 'Ustawienia',
        'post_id' => 'options',
  		'menu_title'	=> 'Ustawienia',
  		'menu_slug' 	=> 'module-settings',
  		'capability'	=> 'edit_posts',
  		'redirect'		=> false
  	));
  }

  register_nav_menus(array(
    'primary' => 'Header nav - left'
  ));
  register_nav_menus(array(
    'secondary' => 'Header nav - right'
  ));
  add_theme_support( 'post-thumbnails' );

  /**
  *  Delete auto-created paragraphs by contact form 7
  *
  */
  add_filter('wpcf7_autop_or_not', '__return_false');

  /**
  *  Custom post type
  *
  */
  function create_post_type() {
  register_post_type( 'finalists',
	array(
	  'labels' => array(
		'name' => __( 'Finalistki' ),
		'singular_name' => __( 'Finalistki' )
	  ),
	  'public' => true,
	  'has_archive' => true,
	  'supports' => array( 'title', 'editor', 'thumbnail')
	)
  );
  }
  add_action( 'init', 'create_post_type' );
?>
