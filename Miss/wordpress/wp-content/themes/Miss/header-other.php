<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow|Vollkorn:400,400i,600,600i,700,900i" rel="stylesheet">
  	<link href="<?php echo get_stylesheet_directory_uri() . '/main.css'?>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

<header class="header">
	<nav id="js-nav" class="nav header_nav">
		<div class="container">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'js-nav-wrapper nav-wrapper menu-menu-left-container' ,'menu_class' => 'menu js-mobile-menu') ); ?>
			<figure>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo-href">
					<img src="<?php echo THEME_URL; ?>/images/brand-logo.png" alt="Miss Earth Poland" id="js-brand-logo" class="brand-logo">
				</a>
			</figure>
			<?php wp_nav_menu( array( 'theme_location' => 'secondary',  'container_class' => 'js-nav-wrapper nav-wrapper menu-menu-right-container' ,'menu_class' => 'menu js-mobile-menu') ); ?>
			<div id="js-burger" class="burger">
				<span class="burger_item"></span>
				<span class="burger_item"></span>
				<span class="burger_item"></span>
			</div>
		</div>
	</nav>
</header>

<section class="subpage-header">
	<div class="container">
		<div class="subpage-header_heading typo typo_heading-primary">
			<?php the_title(); ?>
			 <span class="typo typo_posts-counter">
				 <?php
					  $body_class = get_body_class();
					  if (in_array('page-template-template-news', $body_class)) {
						 $total = wp_count_posts()->publish;
						 echo $total;
					  }
				  ?>
			 </span>
		</div>
	</div>
</section>
