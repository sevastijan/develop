<?php require(THEME_DIR.'_includes/_modules/_instagram-gallery.php'); ?>

	<footer class="footer">

		<?php require_once(THEME_DIR.'_includes/_modules/_footer-content.php'); ?>

		<?php require_once(THEME_DIR.'_includes/_modules/_copyrights-bar.php'); ?>

	</footer>

	<script src="<?php echo get_template_directory_uri() . '/main.js'?>"></script>
	<?php wp_footer(); ?>
	</body>
</html>
