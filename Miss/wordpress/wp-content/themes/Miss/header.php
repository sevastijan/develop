<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow|Vollkorn:400,400i,600,600i,700,900i" rel="stylesheet">
  	<link href="<?php echo get_stylesheet_directory_uri() . '/main.css'?>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

<header class="header">
	<nav id="js-nav" class="nav header_nav">
		<div class="container">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'js-nav-wrapper nav-wrapper menu-menu-left-container' ,'menu_class' => 'menu js-mobile-menu') ); ?>
			<figure>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo-href">
					<img src="<?php echo THEME_URL; ?>/images/brand-logo.png" alt="Miss Earth Poland" id="js-brand-logo" class="brand-logo">
				</a>
			</figure>
			<?php wp_nav_menu( array( 'theme_location' => 'secondary',  'container_class' => 'js-nav-wrapper nav-wrapper menu-menu-right-container' ,'menu_class' => 'menu js-mobile-menu') ); ?>
			<div id="js-burger" class="burger">
				<span class="burger_item"></span>
				<span class="burger_item"></span>
				<span class="burger_item"></span>
			</div>
		</div>
	</nav>
</header>

<section class="hero">
	<div class="container">
		<iframe class="hero_live" src="https://livestream.com/accounts/19459665/events/8356977/player?width=960&height=540&enableInfoAndActivity=true&defaultDrawer=feed&autoPlay=true&mute=false" frameborder="0"></iframe>
		<div class="hero_heading">
			<?php the_field('header_content' ,'options'); ?>

			<?php if( have_rows('header_button', 'options') ): ?>
				<?php while ( have_rows('header_button', 'options') ) : the_row();?>
					<a href="<?php the_sub_field('url', 'options') ?>" class="btn btn_primary">
						<?php the_sub_field('text', 'options') ?>
					</a>
				<?php endwhile;?>
			<?php endif;?>
		</div>
		<div class="hero_social social">
			<?php require(THEME_DIR.'_includes/_modules/_social.php'); ?>
		</div>
		<div class="hero_scroll-to">
			<ul class="scroll-to_ul">
				<li class="scroll-to_ul-item">
					<a href="#">
						<p id="js-slide-to-about-us" class="typo typo_primary">
							Miss earth poland
						</p>
					</a>
				</li>
				<li class="scroll-to_ul-item">
					<a href="#">
						<p id="js-slide-to-find-us" class="typo typo_primary">
							Konkurs
						</p>
					</a>
				</li>
				<?php
					$the_query = new WP_Query( array(
						'post_type' => 'finalists',
						'post_status' => 'publish'
					));
				?>
				<?php if( $the_query->have_posts() ) { ?>
					<li class="scroll-to_ul-item">
						<a href="#">
							<p id="js-slide-to-finalists" class="typo typo_primary">
								finalistki
							</p>
						</a>
					</li>
				<?php }  ?>
				<li class="scroll-to_ul-item">
					<a href="#">
						<p id="js-slide-to-news" class="typo typo_primary">
							news
						</p>
					</a>
				</li>
				<li class="scroll-to_ul-item">
					<a href="#">
						<p id="js-slide-to-ig" class="typo typo_primary">
							instagram
						</p>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div id="js-slide-down" class="slide-down-arrow">
		<img src="<?php echo THEME_URL; ?>/images/slide-down-arrow.png" alt="arrow" class="slide-down-arrow_img">
	</div>
</section>
