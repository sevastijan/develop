<ul class="social_ul">
	<li class="social_ul-item">
		<a href="https://twitter.com/PolandMiss" class="social_ul-item-href">
			<span class="social_ul-item-href-img" style="background-image: url(<?php echo THEME_URL; ?>/images/icons/tw.png);"></span>
		</a>
	</li>
	<li class="social_ul-item">
		<a href="https://www.instagram.com/Missearthpolandofficial/" class="social_ul-item-href">
			<span class="social_ul-item-href-img" style="background-image: url(<?php echo THEME_URL; ?>/images/icons/ig.png);"></span>
		</a>
	</li>
	<li class="social_ul-item">
		<a href="https://www.facebook.com/MissEarthPoland/" class="social_ul-item-href">
			<span class="social_ul-item-href-img" style="background-image: url(<?php echo THEME_URL; ?>/images/icons/fb.png);"></span>
		</a>
	</li>
</ul>
