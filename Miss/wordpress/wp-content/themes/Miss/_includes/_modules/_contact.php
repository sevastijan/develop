<div class="contact clearfix">

	<?php if( have_rows('contact_information') ): ?>
		<?php while ( have_rows('contact_information') ) : the_row();?>
			<div class="contact_person">
				<div class="contact_person-img"  style="background-image: url('<?php the_sub_field('contact_img'); ?>')"></div>
				<h6 class="typo typo_name">
					<?php the_sub_field('contact_name'); ?>
				</h6>
				<p class="typo typo_prof">
					<?php the_sub_field('contact_role'); ?>
				</p>
			</div>
		<?php endwhile;?>
	<?php endif;?>

</div>
