<section class="subpage-content">
	<div class="container">
		<?php $body_class = get_body_class(); ?>
		<?php if (in_array('single-post', $body_class)) { ?>
			<h3 class="typo typo_heading-tertiary">
				Być może zainteresują Cię także
			</h3>
		<?php } elseif (in_array('home', $body_class)) { ?>
            <h3 class="typo typo_heading-tertiary home">
                piszą o nas
            </h3>
        <?php } ?>
		<section class="posts-list media clearfix">
			<?php
				$body_class = get_body_class();
				if (in_array('page-template-template-media', $body_class)) {
					$the_query = new WP_Query( array(
						'posts_per_page' => 12, 'post_type' => 'media'
					));
				} else {
					$the_query = new WP_Query( array(
						'posts_per_page' => 3, 'post_type' => 'media'
					));
				}
			?>

			<?php if ( $the_query->have_posts() ) : ?>
			  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<a href="<?php the_field('link'); ?>">
							<div class="posts-list_box">
								<?php if (has_post_thumbnail( $post->ID ) ): ?>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									<div class="posts-list_box-img" style="background-image:url('<?php echo $image[0]; ?>');"></div>
								<?php endif; ?>
								<h3 class="typo typo_heading-primary">
									<?php the_title(); ?>
								</h3>
								<p class="typo typo_date">
									<?php echo get_the_date(); ?>
								</p>
							</div>
						</a>
			  <?php endwhile; ?>
			  <?php wp_reset_postdata(); ?>
			<?php endif; ?>
			<?php $body_class = get_body_class(); ?>
			<?php if (in_array('home', $body_class)) { ?>
				<div class="btn-wrapper">
						<a href="/pisza-o-nas" class="btn btn_primary">
							Zobacz wszystkie
						</a>
				</div>
			<?php } ?>
		</section>
	</div>
</section>
