<section class="last-miss subpage-content ">
	<div class="container">
		<div class="last-miss-heading">
			<img class="last-miss-heading_img" src="<?php echo THEME_URL; ?>/images/last-miss-heading.png" alt="last-miss-logo">
		</div>
		<div class="last-miss-body">
			
		<?php if( have_rows('last_miss', 'options') ): ?>
				<?php while ( have_rows('last_miss', 'options') ) : the_row();?>
					
					<div class="last-miss-body-item">
						<div class="miss_img" style="background-image: url('<?php the_sub_field('img', 'options'); ?>')">
							<h3 class="typo typo_secondary">
								<?php the_sub_field('name', 'options'); ?>
							</h3>
							<img class="miss_logo" src="<?php the_sub_field('logo', 'options'); ?>" alt="Miss earth poland logo">
						</div>
					</div>
			
			<?php endwhile;?>
		<?php endif;?>

		</div>
	</div>
</section>



