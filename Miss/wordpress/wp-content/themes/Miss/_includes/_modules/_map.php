<section class="map">
	<iframe src="https://snazzymaps.com/embed/65883" width="100%" height="500px" style="border:none;"></iframe>
	<div class="map_description">
		<h4 class="typo typo_map-heading">
			Dane kontaktowe
		</h4>
		<?php if( have_rows('contact_information-brand') ): ?>
			<?php while ( have_rows('contact_information-brand') ) : the_row();?>
				<div class="typo typo_primary">
					<?php the_sub_field('contact_information-brand-content') ?>
				</div>
			<?php endwhile;?>
		<?php endif;?>
	</div>
</section>
