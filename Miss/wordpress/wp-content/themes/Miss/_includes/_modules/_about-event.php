<section class="about-event">
	<div class="container clearfix">
		<div class="about-event_heading">
			<div class="about-event_heading-img"></div>
			<h5 class="typo typo_heading-primary">
				MISS EARTH 2017
			</h5>
		</div>
		<div class="about-event_description">
			<h3 class="typo typo_heading-primary">
				<?php the_field('about-event_heading' ,'options') ?>
			</h3>
			<p class="typo typo_primary">
				<?php the_field('about-event_content' ,'options') ?>
			</p>
			<a href="http://www.missearth.tv" class="btn btn_primary">
				www.missearth.tv
			</a>
		</div>
	</div>
</section>
