<div id="js-ig" class="instagram-gallery">
	<h3 class="typo typo_heading-tertiary">
		#missearthpoland
	</h3>
	<p class="typo typo_primary">
		na Instagramie 	<img src="<?php echo THEME_URL; ?>/images/emoji.png" alt="Smile emoji">
	</p>
	<?php echo do_shortcode('[instagram-feed]') ?>
</div>
