<section id="js-about-us" class="about-us">
	<div class="container clearfix">
		<div class="about-us_heading">
			<p class="typo typo_primary">
				<?php the_field('about-us_heading' ,'options') ?>
			</p>
		</div>
		<div class="about-us_featured">
			<figure>
				<img src="<?php echo THEME_URL; ?>/images/miss-heading.png" alt="Miss Earth Poland" class="about-us_featured-heading-img">
			</figure>
			<div class="about-us_featured-img" style="background-image: url('<?php the_field('about-us_img' ,'options') ?>')">
				<p class="typo typo_heading-primary">
					<?php the_field('about-us_img-date' ,'options') ?>
				</p>
			</div>
		</div>
	</div>
</section>
