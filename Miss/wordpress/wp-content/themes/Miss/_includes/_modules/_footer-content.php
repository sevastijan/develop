<div class="footer_content container clearfix">
	<div class="footer_content-box">
		<figure>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<img src="<?php echo THEME_URL; ?>/images/footer-logo.png" alt="Miss Earth Poland" class="footer_content-box-img">
			</a>
		</figure>
		<div class="footer_content-box-social social">
			<?php require(THEME_DIR.'_includes/_modules/_social.php'); ?>
		</div>
	</div>
	<div class="footer_content-box">
		<p class="typo typo_text">
			<?php the_field('footer_content', 'options') ?>
		</p>
	</div>
</div>
