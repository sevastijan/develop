<section class="memories">
	<div class="container">
		<div class="memories-heading">
			<h2 class="typo typo_secondary">
				Wspomnienia z poprzednich finałów
			</h2>
		</div>
		<div class="memories-body">
			<div class="memories-body-item">
				<div class="memories-body-item_sub"></div>
				<div class="memories-body-item_sub">
					<h5 class="typo typo_primary">
						Finał Miss Earth Poland 2018
					</h5>
					<a href="" class="btn btn_primary">Zobacz więcej</a>
				</div>
			</div>
			<div class="memories-body-item">
				<div class="memories-body-item_sub">
					<a href="#"></a>
				</div>
				<div class="memories-body-item_sub">
					<a href="#"></a>
				</div>
				<div class="memories-body-item_sub">
					<a href="#"></a>
				</div>
				<div class="memories-body-item_sub">
					<a href="#"></a>
				</div>
			</div>
		</div>
	</div>
</section>
