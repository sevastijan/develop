	<section id="js-finalists" class="finalists">
		<div class="container">
			<div class="finalists_heading">
				<h1 class="typo typo_heading-primary">
					Finalistki
				</h1>
			</div>
			<div class="finalists_presentation">
				<div id="js-finalists-slider" class="finalists_presentation-slider">
					<?php if( have_rows('f_slider', 'options') ):
							while ( have_rows('f_slider', 'options') ) : the_row(); ?>
							<div class="finalists_presentation-slider-item">
								  <div class="finalists_presentation-slider-item-img" style="background-image: url('<?php the_sub_field('f_image', 'options') ?>')">
									  <a href="<?php the_sub_field('f_url', 'options'); ?>" class="finalists_presentation-slider-item-img-href">
										  <div class="finalists_presentation-slider-item-img-description">
											  <span class="name typo typo_heading-primary">
												  <?php the_sub_field('f_name', 'options'); ?>
											  </span>
											  <div class="name typo typo_heading-primary">
												  <?php the_sub_field('f_surname', 'options'); ?>
											  </div>
											  <span class="number-info typo typo_heading-primary">
												  Kandydatka nr
											  </span>
											  <span class="number typo typo_heading-primary">
												  <?php the_sub_field('f_number', 'options'); ?>
											  </span>
										  </div>
									  </a>
								  </div>
							  </div>
						<?php endwhile;
						endif; ?>
				</div>
			</div>
		</div>
	</section>

