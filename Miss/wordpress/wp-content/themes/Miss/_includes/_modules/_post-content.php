<section class="subpage-content ">
	<div class="container">
		<section class="post">
			<div class="post_boxes clearfix">
				<div class="post_boxes-box">
					<?php if (has_post_thumbnail( $post->ID ) ): ?>
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
						<div class="post_boxes-img" style="background-image:url('<?php echo $image[0]; ?>');"></div>
					<?php endif; ?>
					<div class="post_boxes-box-social">
						<?php require(THEME_DIR.'_includes/_modules/_social.php'); ?>
					</div>
				</div>
				<div class="post_boxes-box">
					<p class="typo typo_date">
						<?php echo get_the_date(); ?>
					</p>
					<h2>
						<?php the_title(); ?>
					</h2>
					<?php the_content(); ?>
				</div>
			</div>
		</section>
	</div>
</section>
