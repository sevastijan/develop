<section id="js-find-us" class="find-us">
	<div class="container">
		<div class="find-us_information">
			<h3 class="typo typo_heading-secondary cl-other heading">
				<!-- Najbliższy casting -->
				Finał
			</h3>
			<h6 class="typo typo_heading-secondary">
				<!-- <?php the_field('find-us_date' ,'options') ?> -->
				Miss Earth Poland
			</h6>
			<h3 class="typo typo_heading-secondary cl-other heading">
				miejsce
			</h3>
			<h6 class="typo typo_heading-secondary">
				<!-- <?php the_field('find-us_place' ,'options') ?> -->
				Teatr Capitol <br> 
				Warszawa
			</h6>
			<h3 class="typo typo_heading-secondary cl-other heading">
				<!-- GODZINY CASTINGU -->
				Data
			</h3>
			<h6 class="typo typo_heading-secondary">
				<!-- <?php the_field('find-us_time' ,'options') ?> -->
				20.00 <br>
				06.09.2018
			</h6>
			<a href="/zglos-sie-do-konkursu" class="btn btn_primary">
				Zgłoś się do konkursu
			</a>
		</div>
		<div class="find-us_place">
			<a href="<?php the_field('find-us_place-url' ,'options') ?>">
				<h5 class="typo typo_heading-secondary">
					Wskazówki dojazdu
				</h5>
			</a>
			<a href="<?php the_field('find-us_place-url' ,'options') ?>">
				<div class="find-us_place-img" style="background-image: url('<?php the_field('find-us_place-img' ,'options') ?>')"></div>
			</a>
		</div>
	</div>
</section>
