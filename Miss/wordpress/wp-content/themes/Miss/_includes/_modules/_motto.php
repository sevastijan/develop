<?php
	$the_query = new WP_Query( array(
		'post_type' => 'finalists',
		'post_status' => 'publish'
	));
?>
<?php if( $the_query->have_posts() ) { ?>
	<section class="motto">
		<div class="container">
			<div class="typo typo_primary">
				<?php the_field('motto_content', 'options') ?>
			</div>
		</div>
	</section>
<?php }  ?>
