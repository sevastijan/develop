<?php
/**
 * Wordpress template created for "Miss Earth Poland"
 *
 * Version 1.0
 * Date: 20.04.2018
 *Template Name: Form
 *
 * @package WordPress
 *
 */
?>

<?php require(THEME_DIR.'header-other.php'); ?>
<?php the_post(); ?>
	<section class="subpage-content">
		<div class="container">
			<?php the_content(); ?>
			<?php echo do_shortcode('[contact-form-7 id="144" title="ZGŁOŚ SIĘ DO KONKURSU - Dane"]') ?>
		</div>
	</section>
<?php get_footer(); ?>
