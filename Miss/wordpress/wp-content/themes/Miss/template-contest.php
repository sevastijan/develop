<?php
/**
 * Wordpress template created for "Miss Earth Poland"
 *
 * Version 1.0
 * Date: 20.04.2018
 *Template Name: Konkurs
 *
 * @package WordPress
 *
 */
?>

<?php require(THEME_DIR.'header-other.php'); ?>
<?php the_post(); ?>

	<section class="subpage-content typo">
		<div class="container">
			<?php the_content(); ?>
		</div>
	</section>

<?php get_footer(); ?>
