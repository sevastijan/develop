<?php
/**
 * Wordpress template created for "Miss Earth Poland"
 *
 * Version 1.0
 * Date: 20.04.2018
 *Template Name: Contact
 *
 * @package WordPress
 *
 */
?>

<?php require(THEME_DIR.'header-other.php'); ?>
<?php the_post(); ?>
	<section class="subpage-content">
		<div class="container">
			<?php the_content(); ?>
			
			<?php require(THEME_DIR.'_includes/_modules/_contact.php'); ?>

			<?php echo do_shortcode('[contact-form-7 id="96" title="Formularz kontaktowy"]') ?>

			<h4>Nasze dane</h4>
			<p>
				Organizator Konkursu <br>
        Queens Production sp. z o.o. <br>
      	<a href="mailto:biuro@missearthpoland.pl">
        	biuro@missearthpoland.pl
        </a>
			</p>

		</div>
	</section>

<?php get_footer(); ?>
