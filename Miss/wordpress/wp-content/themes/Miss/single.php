<?php
/**
 * Wordpress template created for "Miss Earth Poland"
 *
 * Version 1.0
 * Date: 20.04.2018
 *Template Name: Pojedynczy widok bloga tekstowego
 *
 * @package WordPress
 *
 */
?>

<?php require(THEME_DIR.'header-other.php'); ?>
<?php the_post(); ?>

	<?php require(THEME_DIR.'_includes/_modules/_post-content.php'); ?>

	<?php require(THEME_DIR.'_includes/_modules/_posts-list.php'); ?>

<?php get_footer(); ?>
