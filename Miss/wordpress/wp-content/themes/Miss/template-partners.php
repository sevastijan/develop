<?php
/**
 * Wordpress template created for "Miss Earth Poland"
 *
 * Version 1.0
 * Date: 20.04.2018
 *Template Name: Partners
 *
 * @package WordPress
 *
 */
?>
<?php require(THEME_DIR.'header-other.php'); ?>
	<section class="subpage-content">
		<div class="container">
			<?php the_content(); ?>
			<section class="partners clearfix">
				<?php if( have_rows('main_partner') ): ?>
					<h3 class="typo typo_heading-primary">Partner główny</h3>
					<div class="partners_boxes main-partner clearfix">
						<?php while ( have_rows('main_partner') ) : the_row();?>
							<div class="partners_boxes-box">
							<a href="<?php the_sub_field('url') ?>">
								<img src="<?php the_sub_field('logo') ?>" alt="Miss Earth Poland Partner" class="partners_boxes-box-img">
							</a>
							</div>
						<?php endwhile;?>
					</div>
				<?php endif;?>
				<?php if( have_rows('partners') ): ?>
					<h3 class="typo typo_heading-primary">Partnerzy</h3>
					<div class="partners_boxes clearfix">
						<?php while ( have_rows('partners') ) : the_row();?>
							<div class="partners_boxes-box">
							<a href="<?php the_sub_field('url') ?>">
								<img src="<?php the_sub_field('logo') ?>" alt="Miss Earth Poland Partner" class="partners_boxes-box-img">
							</a>
							</div>
						<?php endwhile;?>
					</div>
				<?php endif;?>
				<?php if( have_rows('media_partners') ): ?>
					<h3 class="typo typo_heading-primary">Patroni Medialni</h3>
					<div class="partners_boxes clearfix">
						<?php while ( have_rows('media_partners') ) : the_row();?>
							<div class="partners_boxes-box">
								<a href="<?php the_sub_field('url') ?>">
									<img src="<?php the_sub_field('logo') ?>" alt="Miss Earth Poland Partner" class="partners_boxes-box-img">
								</a>
							</div>
						<?php endwhile;?>
					</div>
				<?php endif;?>
			</section>
		</div>
	</section>
<?php get_footer(); ?>
