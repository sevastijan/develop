import $ from 'jquery';
import 'slick-carousel';

const burger = '#js-burger';
const body = 'body';
const nav = '#js-nav'

// mobile menu
const mobileNavbar = function() {
  const mobileMenu = $('.js-mobile-menu, .js-nav-wrapper');
  $(burger).click(function(){
    mobileMenu.toggleClass('showed');
    $(body).toggleClass('showed');
  });
}

// slick-carousel
const slick = function() {
	$('#js-finalists-slider').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		nextArrow: '<i class="finalists_presentation-slider-arrow right"></i>',
		prevArrow: '<i class="finalists_presentation-slider-arrow left"></i>',
		responsive: [
		  {
			breakpoint: 768,
			settings: {
			  slidesToShow: 1,
			  slidesToScroll: 1
			}
		  }
		]
	});
}

// fixed navbar
const fixedNavbar = function() {
  let num = 150;
  let show = true;
  let addReducedClass = '#js-burger, #js-nav, #js-brand-logo, .js-mobile-menu';

  $(window).bind('scroll', function () {
      if ($(window).scrollTop() > num) {
         $(addReducedClass).addClass('reduced');
      } else {
         $(addReducedClass).removeClass('reduced');
      }
  });
}

// slideTo
$("#js-slide-down").click(function() {
    $('html, body').animate({
        scrollTop: $("#js-about-us").offset().top - 95
    }, 1500);
});

$("#js-slide-to-about-us").click(function() {
    $('html, body').animate({
        scrollTop: $("#js-about-us").offset().top - 95
    }, 1500);
});
$("#js-slide-to-find-us").click(function() {
    $('html, body').animate({
        scrollTop: $("#js-find-us").offset().top - 95
    }, 1500);
});
$("#js-slide-to-finalists").click(function() {
    $('html, body').animate({
        scrollTop: $("#js-finalists").offset().top - 95
    }, 1500);
});
$("#js-slide-to-ig").click(function() {
    $('html, body').animate({
        scrollTop: $("#js-ig").offset().top - 95
    }, 1500);
});

export { slick, mobileNavbar, fixedNavbar };
