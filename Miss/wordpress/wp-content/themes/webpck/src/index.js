import style from "./_scss/main.scss";
import { slick } from "./_js/app.js";
import { mobileNavbar } from "./_js/app.js";
import { fixedNavbar } from "./_js/app.js";


slick();
mobileNavbar();
fixedNavbar();
