<?php
/**
 * Wordpress template created for "Pułapki w aptece"
 * Design author: Paweł Błoński
 * Theme author: Sebastian Ślęczka
 * If you have any questions feel free to ask us sebastians@interpages.pl
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 20.12.2016
 *
 * @package WordPress
 *
 */
  ?>
  <?php get_header(); ?>
    <main <?php if(is_home()) {echo 'class="is-home"'; } ?>>
        <div class="container">
            <div class="row">
                <div class="col-md-8 pr30">
                  <?php if(is_archive()): ?>
                  <a class="this-category" href="#">
                    <?php if(is_search()) {
                      echo 'Wyszukaj: ' . $_GET['s'];
                    } else {
                      single_cat_title();
                    }  ?>
                  </a>

                <?php endif;

                if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                      <article>
                          <a href="<?php the_permalink();?>">
                              <h3 class="title"><?php the_title(); ?></h3>
                          </a>
                          <?php if(has_post_thumbnail()) : ?>
                            <a class="article-thumbnail" href="<?php the_permalink();?>">
                                <figure>
                                    <?php the_post_thumbnail('post-thumbnail-254'); ?>
                                </figure>
                            </a>
                          <?php endif; ?>
                          <p class="excerpt">
                            <?php
                              $iampoem = get_field('jestem_wierszem');
                              if(get_field('jestem_wierszem') && in_array('poem', $iampoem))  {
                                the_excerpt();
                              } else {
                                the_excerpt_max_charlength(330);
                              }
                            ?>
                          </p>
                          <div class="meta">
                              <span class="luckiest-font">Komentarze: <?php comments_number('0', '1', '%') ?></span>
                              <a class="luckiest-font" href="<?php the_permalink();?>">Czytaj dalej</a>
                          </div>
                      </article>
                    <?php endwhile; else : ?>
                      <article>
                        <h3 class="title">Przepraszamy ale nie znaleźliśmy postów spełniających kryteria wyszukiwania.</h3>
                      </article>
                    <?php endif; ?>
                    <?php the_numeric_pagination(); ?>
                </div>
                <div class="col-md-4 pl30 b-aside">
                    <aside>
                        <?php get_sidebar(); ?>
                    </aside>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>
