<?php
/**
 * Wordpress template created for "Pułapki w aptece"
 * Design author: Paweł Błoński
 * Theme author: Sebastian Ślęczka
 * If you have any questions feel free to ask us sebastians@interpages.pl
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 20.12.2016
 *
 * @package WordPress
 *
 */
  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Noweklucze.pl</title>

      <link href="<?php echo THEME_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
      <link href="<?php echo THEME_URL; ?>assets/css/style.min.css" rel="stylesheet">
      <link href="<?php echo THEME_URL; ?>style.css" rel="stylesheet">
      <link href="<?php echo THEME_URL; ?>assets/nk_scss/style.css" rel="stylesheet">

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8&appId=1519239591719932";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 parallax <?php if(!is_home()) { echo 'parallax_slim'; } ?>">
          <div class="parallax-caption">
            <p>W tym gąszczu znajdziesz swoje miejsce</p>
            <p><span class="next-paragraph">ogólnopolski serwis nieruchomości - noweklucze - BLOG</span></p>
          </div>
        </div>
      </div>
    </div>
    <header class="fixed">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-2 brand">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo THEME_URL; ?>assets/images/logo_header.png" alt=""></a>
                </div>
                <div class="col-md-offset-7 col-md-3 add_ad">
                    <div class="wrapper">
                      <a href="#">
                        <span class="text-wrapper">
                          <span class="add">Dodaj </span>
                          <span class="add_2">ogłoszenie</span>
                        </span>
                      </a>
                    </div>
                </div>
            </div>
        </div>
    </header>
