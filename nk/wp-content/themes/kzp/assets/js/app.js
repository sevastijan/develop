$('.burger').click(function() {
    $('header nav').toggleClass('mobile-open', 500, 'easeOutSine');
});

var th = 150,
    ww = $(window).width();

$(document).ready(function(){
  if(!$('header').hasClass('parallax_slim')) {
    if(ww > 1921) {
      th = 915;
    } else if(ww > 640) {
      th = 555;
    } else {
      th = 0;
    }
  } else {
    th = 0;
  }
  if (th === 0) {
      $('header').addClass('fixed');
      $('main').addClass('fixed');
  }


});
$(window).resize(function(){
    ww = $(window).width();
    if(!$('header').hasClass('parallax_slim')) {
      if(ww > 1921) {
        th = 915;
      } else if(ww > 640) {
        th = 555 ;
      } else {
        th = 0;
      }
    } else {
      th = 0;
    }
});
