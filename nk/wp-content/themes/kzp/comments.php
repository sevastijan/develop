<?php
/**
 * Wordpress template created for "Pułapki w aptece"
 * Design author: Paweł Błoński
 * Theme author: Sebastian Ślęczka
 * If you have any questions feel free to ask us sebastians@interpages.pl
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 20.12.2016
 *
 * @package WordPress
 *
 */
  ?>
<?php if($comments) : ?>
	<ol class="comments">
    <?php
      $commentsParams = array( 'callback' => 'pwa_comments' );
      wp_list_comments($commentsParams); 
    ?>
	</ol>
<?php else : ?>
	<h3 class="comment title">Brak komentarzy</h3>
<?php endif; ?>
<div id="respond">
  <?php if(comments_open()) : comment_form(); else : ?>
  	<p>Dodawanie nowych komentarzy jest wyłączone</p>
  <?php endif; ?>
</div>
