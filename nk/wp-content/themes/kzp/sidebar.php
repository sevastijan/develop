<div class="module about">
    <figure>
        <img src="<?php the_field('about_photo','option'); ?>" alt="">
    </figure>
    <figcaption>
        <h3 class="luckiest-font"><?php the_field('about_headline','option'); ?></h3>
    </figcaption>
    <div class="about"><?php the_field('about_text','option'); ?></div>
</div>
  <?php if( have_rows('partner_logos', 'option') ): ?>
    <div class="module partner">
      <?php while ( have_rows('partner_logos', 'option') ) : the_row(); ?>
      <a href="<?php the_sub_field('link'); ?>">
        <img src="<?php the_sub_field('partner_logo'); ?>" alt="">
      </a>
      <?php endwhile; ?>
    </div>
  <?php endif; ?>

<div class="module newsletter">
    <h3 class="luckiest-font">Newsletter</h3>
    <div class="descr"><?php the_field('newsletter_text', 'option'); ?></div>
    <?php echo do_shortcode('[newsletter_form form="1"]'); ?>
</div>

<div class="module search">
  <form action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <fieldset>
      <input type="text" placeholder="Wyszukaj" name="s" />
      <button type="submit"><img src="<?php echo THEME_URL; ?>assets/images/search.png" alt=""></button>
    </fieldset>
  </form>
</div>

<?php
  $worth_posts = new WP_Query(array(
      'post_type' => 'post',
      'cat' => 91,
      'posts_per_page' => 4
    ));
?>

<?php if((get_field('worth_posts', 'option')) && ($worth_posts->have_posts())) : ?>
  <div class="module worth">
    <h3 class="luckiest-font after-polish-char">Warto przeczytać</h3>
    <div class="row">
    <?php while($worth_posts->have_posts()) : $worth_posts->the_post(); ?>
      <article class="col-sm-6 col-md-6">
          <a href="<?php the_permalink(); ?>">
            <figure>
                <?php the_post_thumbnail('post-thumbnail-665'); ?>
            </figure>
            <p class="title"><?php the_title(); ?></p>
          </a>
      </article>
    <?php endwhile; wp_reset_query(); ?>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
<?php endif; ?>
