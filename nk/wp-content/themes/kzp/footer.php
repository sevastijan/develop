<?php
/**
 * Wordpress template created for "Pułapki w aptece"
 * Design author: Paweł Błoński
 * Theme author: Sebastian Ślęczka
 * If you have any questions feel free to ask us sebastians@interpages.pl
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 20.12.2016
 *
 * @package WordPress
 *
 */
  ?>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-6 left">Copyright &copy; 2017 NoweKlucze.pl</div>
        <div class="col-md-offset-3 col-md-3 right">
          <a href="#">Projekt graficzny: Paweł Błoński</a>
        </div>
      </div>
    </div>
  </footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="<?php echo THEME_URL; ?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo THEME_URL; ?>assets/js/app.js"></script>
  <?php wp_footer(); ?>
</body>
</html>
