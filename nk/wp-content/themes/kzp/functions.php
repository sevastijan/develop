<?php
/**
 * Wordpress template created for "Pułapki w aptece"
 * Design author: Paweł Błoński
 * Theme author: Sebastian Ślęczka
 * If you have any questions feel free to ask us sebastians@interpages.pl
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 20.12.2016
 *
 * @package WordPress
 *
 */

 /**
 *  Define root theme url's
 *
 */
  if (!defined('THEME_DIR')) {
      define('THEME_DIR', get_theme_root().'/'.get_template().'/');
  }
  if (!defined('THEME_URL')) {
      define('THEME_URL', WP_CONTENT_URL.'/themes/'.get_template().'/');
  }

  /**
  *  Register menus
  *
  */
  register_nav_menus(array(
    'main_menu' => 'Menu główne'
  ));

  /**
  *  Custom post excerpt
  *
  */
  function the_excerpt_max_charlength($charlength) {
      echo cutText(get_the_excerpt(), $charlength);
  }
  function cutText($text, $maxLength) {
      $maxLength++;
      $return = '';
      if (mb_strlen($text) > $maxLength) {
          $subex = mb_substr($text, 0, $maxLength - 5);
          $exwords = explode(' ', $subex);
          $excut = -(mb_strlen($exwords[count($exwords) - 1]));
          if ($excut < 0) {
              $return = mb_substr($subex, 0, $excut);
          } else {
              $return = $subex;
          }
          $return .= '...';
      } else {
          $return = $text;
      }
      return $return;
  }

  /**
  *  Theme thumbnails
  *
  */
  add_theme_support('post-thumbnails', array('post'));
  add_image_size('post-thumbnail-254', 254, 190, true);

  /**
  *  Register settings page
  *
  */
  if( function_exists('acf_add_options_page')) {
  	acf_add_options_page(array(
  		'page_title' 	=> 'Ustawienia modułów',
  		'menu_title'	=> 'Ustawienia modułów',
  		'menu_slug' 	=> 'module-settings',
  		'capability'	=> 'edit_posts',
  		'redirect'		=> false
  	));
  }

  /**
  *  Custom numeric pagination
  *
  */
  function the_numeric_pagination() {

  	if( is_singular() )
  		return;

  	global $wp_query;

  	if( $wp_query->max_num_pages <= 1 )
  		return;

  	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  	$max   = intval( $wp_query->max_num_pages );

  	if ( $paged >= 1 )
  		$links[] = $paged;

  	if ( $paged >= 3 ) {
  		$links[] = $paged - 1;
  		$links[] = $paged - 2;
  	}

  	if ( ( $paged + 2 ) <= $max ) {
  		$links[] = $paged + 2;
  		$links[] = $paged + 1;
  	}

  	echo '<div class="navigation"><ul>' . "\n";

  	if ( get_previous_posts_link() )
  		printf( '<li>%s</li>' . "\n", get_previous_posts_link('<span class="label-prev"></span>') );

  	if ( ! in_array( 1, $links ) ) {
  		$class = 1 == $paged ? ' class="active"' : '';

  		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

  		if ( ! in_array( 2, $links ) )
  			echo '<li>…</li>';
  	}

  	sort( $links );
  	foreach ( (array) $links as $link ) {
  		$class = $paged == $link ? ' class="active"' : '';
  		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  	}

  	if ( ! in_array( $max, $links ) ) {
  		if ( ! in_array( $max - 1, $links ) )
  			echo '<li>…</li>' . "\n";

  		$class = $paged == $max ? ' class="active"' : '';
  		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  	}

  	if ( get_next_posts_link() )
  		printf( '<li>%s</li>' . "\n", get_next_posts_link('<span class="label-next"></span>') );

  	echo '</ul></div>' . "\n";
  }

  /**
  *  Custom numeric pagination
  *
  */
  function pwa_comments($comment, $args, $depth) {
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <li id="comment-<?php comment_ID(); ?>" <?php if($comment->comment_parent != 0) {echo 'class="comment-reply"';} ?>>
      <div class="avatar">
        <?php echo get_avatar( $comment, 66 ); ?>
      </div>
      <div class="meta">
        <span class="author"><?php comment_author_link(); ?></span>
        <cite><?php comment_date(); ?> o <?php comment_time(); ?></cite>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
        <p class="wait-for-approve">Twoj komentarz oczekuje na przegląd</p>
      <?php endif; ?>
      <?php comment_text(); ?>
      <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
    </li>

    <?php
    }

 ?>
