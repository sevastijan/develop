<?php
/**
 * Wordpress template created for "Pułapki w aptece"
 * Design author: Paweł Błoński
 * Theme author: Sebastian Ślęczka
 * If you have any questions feel free to ask us sebastians@interpages.pl
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 20.12.2016
 *
 * @package WordPress
 *
 */
  ?>
  <?php get_header(); the_post(); ?>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-8 pr30">

                    <?php $category = get_the_category($post->ID); if(is_single()) : ?>
                      <a class="this-category" href="<?php echo esc_url( get_category_link( $category[0]->term_id ) ) ?>"><?php echo esc_html( $category[0]->name ); ?></a>
                    <?php elseif(!is_page()) : ?>
                      <a class="this-category" href=""><?php single_cat_title(); ?></a>
                    <?php endif;
                          $iampoem = get_field('jestem_wierszem'); ?>
                    <article class="single <?php if(in_array('poem', $iampoem)) { echo 'iampoem'; } ?>">
                        <h3 class="title"><?php the_title(); ?></h3>
                        <figure>
                            <?php the_post_thumbnail('post-thumbnail-665'); ?>
                        </figure>
                        <?php the_content(); ?>
                    </article>
                    <?php if (comments_open()) :

                       comments_template();

                    endif; ?>
                </div>
                <div class="col-md-4 pl30">
                    <aside>
                      <?php get_sidebar();?>
                    </aside>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>
