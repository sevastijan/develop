<?php
/**
 * Wordpress template created for "Wild Dogs"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: About
 *
 * @package WordPress
 *
 */
?>
<?php require_once(PB_THEME_DIR . 'header-other.php'); ?>
<?php the_post(); ?>

<section class="news news_listing about section clearfix">
	<div class="heading-bar">
		<h2 class="m-typo m-typo_primary heading">
			O nas
		</h2>
		<p class="m-typo m-typo_text descr">
			<?php the_field('about_description', 'options') ?>
		</p>
		<div class="two-boxes clearfix">
			<div class="content-wrapper">
				<div class="box">
					<span class="team">Wild Dogs Team</span> jest grupą zrzeszającą osoby
					z zamiłowaniem do psów. <br>
					Zrzeszamy ludzi w każdym wieku, a także psy wszystkich ras. <br>
					Naszym celem jest wymiana doświadczeń,
					socjalizacja psów, budowanie wzajemnych relacji.
					<div class="sign-wrapper clearfix">
						<span class="boss">
							prezes stowarzyszenia
						</span>
						<span class="sign">
							<?php the_field('about_sign', 'options') ?>
						</span>
					</div>
				</div>
				<div class="box clearfix">
					<?php if( have_rows('about_sub-boxes', 'options') ): ?>
						<?php while ( have_rows('about_sub-boxes', 'options') ) : the_row();?>
							<div class="sub-box">
								<div class="image" style="background-image: url('<?php the_sub_field('image', 'options'); ?>')"></div>
								<h3>
									<?php the_sub_field('heading', 'options'); ?>
								</h3>
								<p>
									<?php the_sub_field('descr', 'options'); ?>
								</p>
							</div>
						<?php endwhile;?>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
	<div class="content-wrapper">
		<div class="post-content">
			<?php the_content(); ?>
			<div class="tab-nav-wrapper clearfix">
				<?php if( have_rows('about_tabs') ): $index = 0?>
					<?php while ( have_rows('about_tabs') ) : the_row();?>
						<p class="js-tab tab-nav js-tab-item-<?php echo $index ?>">
							<?php the_sub_field('tab_title'); ?>
						</p>
					<?php $index++; endwhile;?>
				<?php endif;?>
			</div>
			<?php if( have_rows('about_tabs') ): $index = 0?>
				<?php while ( have_rows('about_tabs') ) : the_row();?>
					<div class="js-tab tab-content js-tab-item-<?php echo $index ?>">
						<?php the_sub_field('tab_content'); ?>
					</div>
				<?php $index++; endwhile;?>
			<?php endif;?>

		</div>
	</div>
</section>

<div class="button-bar">
	<div class="news-button">
		<a href="https://goo.gl/forms/0XneEynZTFgQx9ah1" class="m-btn m-btn_primary m-typo m-typo_primary">Dołącz do nas</a>
	</div>
</div>

<?php get_footer(); ?>
