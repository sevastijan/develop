// // burger
$(".burger").click(function(){
    $("#menu-list").toggleClass("is-active");
    $("body").toggleClass("is-active");
});

// // hide menu after click
$(".list_item").click(function(){
    $("body").removeClass("is-active");
    $("#menu-list").removeClass("is-active");
});

// slick
$('.carousel-wrapper-primary').slick({
	arrows: false,
	dots: true,
	infinite: true,
	autoplay: true,
	autoplaySpeed: 5000,
});

$('.carousel-wrapper-secondary').slick({
	dots: false,
    infinite: true,
	arrows: true,
    speed: 300,
    slidesToShow: 4,
	nextArrow: '<i class="arrow"><span></span><span></span></i>',
	prevArrow: '<i class="arrow"><span></span><span></span></i>',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});

// contact textarea height
$("textarea").click( function() {
  $( this ).height(100);
  $( this ).blur( function() {
    if( $( this ).val() !== ""){
      $( this ).height(100);
    } else{
      $( this ).height(31);
    }
  });
});

// tabs
$('.js-tab-item-0').addClass('active');
$('.js-tab-item-0').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-0').addClass('active');
});
$('.js-tab-item-1').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-1').addClass('active');
});
$('.js-tab-item-2').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-2').addClass('active');
});
$('.js-tab-item-3').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-3').addClass('active');
});
$('.js-tab-item-4').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-4').addClass('active');
});
$('.js-tab-item-5').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-5').addClass('active');
});
$('.js-tab-item-6').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-6').addClass('active');
});
$('.js-tab-item-7').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-7').addClass('active');
});
$('.js-tab-item-8').click(function(){
	$('.js-tab').removeClass('active');
	$('.js-tab-item-8').addClass('active');
});

// sub-menu
$('#menu-list li > .sub-menu').parent().hover(function(){
  $(this).children('.sub-menu').slideToggle(200);
});
