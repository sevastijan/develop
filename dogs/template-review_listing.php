<?php
/**
 * Wordpress template created for "Wild Dogs"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Review listing
 *
 * @package WordPress
 *
 */
?>
<?php require_once(PB_THEME_DIR . 'header-other.php'); ?>

<section class="news news_listing clearfix">
	<div class="heading-bar">
		<h2 class="m-typo m-typo_primary heading">
			Recenzje
		</h2>
		<p class="m-typo m-typo_text descr">
			<?php the_field('review_description', 'options') ?>
		</p>
	</div>
	<div class="content-wrapper">
		<div class="boxes clearfix">
			<?php
				$the_query = new WP_Query( array(
					'post_type' => 'review',
					'posts_per_page' => -1
				));
			?>
			<?php if ( $the_query->have_posts() ) : ?>
			  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<a href="<?php the_permalink(); ?>">
							<div class="box">
								<?php if (has_post_thumbnail( $post->ID ) ): ?>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									<div class="img" style="background-image:url('<?php echo $image[0]; ?>');"></div>
								<?php endif; ?>
								<div class="text-wrapper">
									<div class="category m-typo m-typo_text">
										<?php $cat = get_the_category(); echo $cat[0]->cat_name; ?>
									</div>
									<h3 class="m-typo m-typo_secondary">
										<?php the_title(); ?>
									</h3>
									<div class="info">
										<p class="m-typo m-typo_text">autor: <span><?php the_author(); ?></span></p> /
										<p class="m-typo m-typo_text"><?php the_date(); ?></p>
									</div>
								</div>
							</div>
						</a>
			  <?php endwhile; ?>
			  <?php wp_reset_postdata(); ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<div class="button-bar">
	<div class="news-button">
		<a href="https://goo.gl/forms/0XneEynZTFgQx9ah1" class="m-btn m-btn_primary m-typo m-typo_primary">Dołącz do nas</a>
	</div>
</div>

<?php get_footer(); ?>
