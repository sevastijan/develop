<?php
/**
 * Wordpress template created for "Wild Dogs"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Post listing
 *
 * @package WordPress
 *
 */
?>
<?php require_once(PB_THEME_DIR . 'header-other.php'); ?>
<?php the_post(); ?>

<section class="news news_listing post clearfix">
	<div class="heading-bar small">
		<h2 class="m-typo m-typo_primary heading">
			<?php the_title(); ?>
		</h2>
		<p class="m-typo m-typo_text">
			<?php echo get_the_date(); ?>
		</p>
		-
		<p class="m-typo m-typo_text cat">
			<?php $cat = get_the_category(); echo $cat[0]->cat_name; ?>
		</p>
	</div>
	<div class="content-wrapper">
		<div class="boxes clearfix">
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
				<div class="img" style="background-image:url('<?php echo $image[0]; ?>');"></div>
			<?php endif; ?>
		</div>
		<div class="post-content">
			<?php the_content(); ?>
		</div>
	</div>
</section>

<?php
	$prev_post = get_previous_post();
	$next_post = get_next_post();
	if(!empty($next_post || $prev_post)): ?>
	<section class="post-navigation clearfix">
		<div class="content-wrapper">
			<div class="box">
				<?php
					if (!empty( $prev_post )): ?>
						<a href="<?php echo $prev_post->guid ?>" class="m-typo m-typo_text">Poprzedni</a>
						<i class="arrow slick-arrow"><span></span><span></span></i>
				<?php endif ?>
			</div>
			<div class="box">
				<?php
					if (!empty( $next_post )): ?>
				 		<a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="m-typo m-typo_text">Następny</a>
						<i class="arrow slick-arrow"><span></span><span></span></i>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php else :?>
<?php endif; ?>


<div class="button-bar">
	<div class="news-button">
		<a href="https://goo.gl/forms/0XneEynZTFgQx9ah1" class="m-btn m-btn_primary m-typo m-typo_primary">Dołącz do nas</a>
	</div>
</div>

<?php get_footer(); ?>
