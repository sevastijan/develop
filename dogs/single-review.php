<?php
/**
 * Wordpress template created for "Wild Dogs"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Review
 *
 * @package WordPress
 *
 */
?>
<?php require_once(PB_THEME_DIR . 'header-other.php'); ?>
<?php the_post(); ?>

<section class="news news_listing post review clearfix">
	<div class="heading-bar small">
		<h2 class="m-typo m-typo_primary heading">
			<?php the_title(); ?>
		</h2>
		<p class="m-typo m-typo_text">
			<?php echo get_the_date(); ?>
		</p>
		-
		<p class="m-typo m-typo_text cat">
			<?php $cat = get_the_category(); echo $cat[0]->cat_name; ?>
		</p>
	</div>
	<div class="content-wrapper">
		<div class="boxes clearfix">
			<?php if (has_post_thumbnail( $post->ID ) ): ?>
				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
				<div class="img" style="background-image:url('<?php echo $image[0]; ?>');"></div>
			<?php endif; ?>
		</div>
		<div class="post-content">
			<?php the_content(); ?>
		</div>
	</div>
</section>


<section class="review-table clearfix">
	<div class="content-wrapper">
		<div class="row clearfix">
			<div class="box">
				Nazwa
			</div>
			<div class="box">
				<?php the_field('review_title') ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Producent
			</div>
			<div class="box">
				<?php the_field('review_maker') ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Cena
			</div>
			<div class="box">
				<?php the_field('review_price') ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Opis
			</div>
			<div class="box descr">
				<?php the_field('review_descr') ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Plusy
			</div>
			<div class="box descr">
				<?php the_field('review_advantages') ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Minusy
			</div>
			<div class="box descr">
				<?php the_field('review_disadvantages') ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Galeria
			</div>
			<div class="box descr">
				<?php the_field('gallery_shortcode') ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Podsumowanie
			</div>
			<div class="box">
				<?php the_field('review_summary') ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Ocena
			</div>
			<div class="box clearfix">
				<?php
					$stars = get_field('review_stars');
					$max = 6;
				?>
				<?php for ($i = $max - $stars; $i < 6 ; $i++) { ?>
					<div class="star selected">
						<img src="<?php echo PB_THEME_URL; ?>images/dog-star.png" alt="star">
					</div>
				<?php } ?>
				<?php for ($i = $max - $stars - 1; $i > 0 ; $i--) { ?>
					<div class="star">
						<img src="<?php echo PB_THEME_URL; ?>images/dog-star.png" alt="star">
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="row clearfix">
			<div class="box">
				Recenzent
			</div>
			<div class="box">
				<?php the_field('review_author') ?>
			</div>
		</div>
	</div>
</section>


<?php
	$prev_post = get_previous_post();
	$next_post = get_next_post();
	if(!empty($next_post || $prev_post)): ?>
	<section class="post-navigation clearfix">
		<div class="content-wrapper">
			<div class="box">
				<?php
					if (!empty( $prev_post )): ?>
						<a href="<?php echo $prev_post->guid ?>" class="m-typo m-typo_text">Poprzedni</a>
						<i class="arrow slick-arrow"><span></span><span></span></i>
				<?php endif ?>
			</div>
			<div class="box">
				<?php
					if (!empty( $next_post )): ?>
				 		<a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="m-typo m-typo_text">Następny</a>
						<i class="arrow slick-arrow"><span></span><span></span></i>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php else :?>
<?php endif; ?>




<div class="button-bar">
	<div class="news-button">
		<a href="https://goo.gl/forms/0XneEynZTFgQx9ah1" class="m-btn m-btn_primary m-typo m-typo_primary">Dołącz do nas</a>
	</div>
</div>

<?php get_footer(); ?>
