<?php

if(!defined('PB_THEME_DIR')) {
define('PB_THEME_DIR', get_theme_root(). '/' .get_template() . '/');
}

if(!defined('PB_THEME_URL')) {
define('PB_THEME_URL', WP_CONTENT_URL. '/themes/' .get_template() . '/');
}

  /**
  *  Register settings page
  *
  */
  if( function_exists('acf_add_options_page')) {
  	acf_add_options_page(array(
  		'page_title' 	=> 'Ustawienia',
        'post_id' => 'options',
  		'menu_title'	=> 'Ustawienia',
  		'menu_slug' 	=> 'module-settings',
  		'capability'	=> 'edit_posts',
  		'redirect'		=> false
  	));
  }

  register_nav_menus(array(
    'primary' => 'Main navigation'
  ));
  add_theme_support( 'post-thumbnails' );

  function create_post_type() {
    register_post_type( 'sections',
      array(
        'labels' => array(
          'name' => __( 'Sekcje' ),
          'singular_name' => __( 'Sekcje' )
        ),
        'public' => true,
        'has_archive' => true,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
      )
    );
  }
  add_action( 'init', 'create_post_type' );

  function create_post_type2() {
	register_post_type( 'review',
	  array(
		'labels' => array(
		  'name' => __( 'Recenzje' ),
		  'singular_name' => __( 'Recenzje' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail')
	  )
	);
  }
  add_action( 'init', 'create_post_type2' );

  function create_post_type3() {
	register_post_type( 'events',
	  array(
		'labels' => array(
		  'name' => __( 'Wydarzenia' ),
		  'singular_name' => __( 'Wydarzenia' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail')
	  )
	);
  }
  add_action( 'init', 'create_post_type3' );
?>
