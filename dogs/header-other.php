<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,500,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
	<link href="<?php echo get_stylesheet_directory_uri() . '/css/style.css'?>" rel="stylesheet">
  <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
	  <section class="header header_other">
	  	<nav class="navbar">
	  	  <div class="content-wrapper">
	  		  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo">
	  			  <img src="<?php echo PB_THEME_URL; ?>images/brand-logo.png" alt="logo">
	  		  </a>
	        <?php
	          wp_nav_menu( array(
	            'theme_location' => 'primary',
	            'menu_id' => 'menu-list',
	            'menu_class' => 'list',
	            'container'=> false
	          ));
	        ?>
	  	    <div class="burger">
	  	      <span class="burger_item"></span>
	  	      <span class="burger_item"></span>
	  	      <span class="burger_item"></span>
	  	    </div>
	  	  </div>
	  	</nav>
		<div class="header-bg" style="background-image:url('<?php echo PB_THEME_URL; ?>images/header-other.jpg');">
		</div>
	  </section>
