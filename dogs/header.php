<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,500,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
	<link href="<?php echo get_stylesheet_directory_uri() . '/css/style.css'?>" rel="stylesheet">
  <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
	  <section class="header">
	  	<nav class="navbar">
	  	  <div class="content-wrapper">
	  		  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo">
	  			  <img src="<?php echo PB_THEME_URL; ?>images/brand-logo.png" alt="logo">
	  		  </a>
	        <?php
	          wp_nav_menu( array(
	            'theme_location' => 'primary',
	            'menu_id' => 'menu-list',
	            'menu_class' => 'list',
	            'container'=> false
	          ));
	        ?>
	  	    <div class="burger">
	  	      <span class="burger_item"></span>
	  	      <span class="burger_item"></span>
	  	      <span class="burger_item"></span>
	  	    </div>
	  	  </div>
	  	</nav>
	  	<div class="header-text">
	  		<div class="content-wrapper">
	  			<h1 class="m-typo m-typo_primary">
	          <?php the_field('header_heading', 'options') ?>
	  			</h1>
	  			<p class="m-typo m-typo_text">
	          <?php the_field('header_description', 'options') ?>
	  			</p>
	  		</div>
	  	</div>
	  	<div class="header-button">
	  		<a href="https://goo.gl/forms/0XneEynZTFgQx9ah1" class="m-btn m-btn_primary m-typo m-typo_primary __mPS2id _mPS2id-h">Dołącz do nas</a>
	  	</div>
	  	<div class="carousel-wrapper-primary">
	      <?php if( have_rows('header_carousel', 'options') ):?>
	        <?php while ( have_rows('header_carousel', 'options') ) : the_row();?>
	          <div class="box" style="background-image:url(<?php the_sub_field('img', 'options'); ?>);">
	          </div>
	        <?php endwhile;?>
	      <?php endif;?>
	  	</div>
	  </section>
