<footer id="js-footer" class="footer">
	<section class="contact">
		<div class="content-wrapper">
			<h2 class="m-typo m-typo_primary heading">
				Skontaktuj się z nami
			</h2>
			<div class="boxes clearfix">
				<div class="box">
					<p class="m-typo m-typo_primary">
						Osobiście
					</p>
					<p class="m-typo m-typo_text">
						<?php the_field('contact_city', 'options') ?>
						<br>
						<?php the_field('contact_street', 'options') ?>
					</p>
				</div>
				<div class="box">
					<p class="m-typo m-typo_primary">
						Mailowo
					</p>
					<p class="m-typo m-typo_text email">
						<?php the_field('contact_mail', 'options') ?>
					</p>
				</div>
				<div class="box">
					<p class="m-typo m-typo_primary">
						Telefonicznie
					</p>
					<p class="m-typo m-typo_text">
						+48 <?php the_field('contact_phone', 'options') ?>
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="form clearfix">
		<?php echo do_shortcode('[contact-form-7 id="43" title="Formularz 1"]') ?>
	</section>
	<section class="bottom-bar clearfix">
		<div class="content-wrapper">
			<p class="m-typo m-typo_text">
				Copyright © 2018 WildDogsTeam. All rights reserved
			</p>
			<p class="m-typo m-typo_text">
				Handcrafted by: Rednelo
			</p>
		</div>
	</section>
</footer>



  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri() . '/js/app.js'?>"></script>
  <?php wp_footer(); ?>
  </body>
</html>
