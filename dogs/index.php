<?php get_header(); ?>


<section class="categories">
	<div class="content-wrapper">
		<h2 class="m-typo m-typo_primary heading">
			Serdecznie witamy
		</h2>
		<p class="m-typo m-typo_text">
			<?php the_field('news_description', 'options') ?>
		</p>
		<div class="boxes clearfix">
		<?php
			$the_query = new WP_Query( array(
				'post_type' => 'sections',
				'posts_per_page' => 4
			));
		?>
		<?php if ( $the_query->have_posts() ) : ?>
		  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<a href="<?php the_permalink(); ?>">
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
						<div class="box" style="background-image:url('<?php echo $image[0]; ?>');">
							<h3 class="m-typo m-typo_primary">
								<?php the_title(); ?>
							</h3>
							<div class="m-typo m-typo_text">
								<?php the_excerpt(); ?>
							</div>
						</div>
					</a>
		  <?php endwhile; ?>
		  <?php wp_reset_postdata(); ?>
		<?php endif; ?>
		</div>
	</div>
</section>
<section class="news clearfix">
	<div class="content-wrapper">
		<h2 class="m-typo m-typo_primary heading">
			Aktualności
		</h2>
		<div class="boxes clearfix">

			<?php
				$the_query = new WP_Query( array(
					'posts_per_page' => 3
				));
			?>
			<?php if ( $the_query->have_posts() ) : ?>
			  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<a href="<?php the_permalink(); ?>">
							<div class="box">
								<?php if (has_post_thumbnail( $post->ID ) ): ?>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									<div class="img" style="background-image:url('<?php echo $image[0]; ?>');"></div>
								<?php endif; ?>
								<div class="text-wrapper">
									<div class="category m-typo m-typo_text">
										<?php $cat = get_the_category(); echo $cat[0]->cat_name; ?>
									</div>
									<h3 class="m-typo m-typo_secondary">
										<?php the_title(); ?>
									</h3>
									<div class="info">
										<p class="m-typo m-typo_text">autor: <span><?php the_author(); ?></span></p> /
										<p class="m-typo m-typo_text"><?php the_date(); ?></p>
									</div>
								</div>
							</div>
						</a>
			  <?php endwhile; ?>
			  <?php wp_reset_postdata(); ?>
			<?php endif; ?>

		</div>
		<div class="news-button">
			<a href="<?php the_field('news_listing', 'options') ?>" class="m-btn m-btn_primary m-typo m-typo_primary">Zobacz wszystkie aktualności</a>
		</div>
	</div>
</section>

<section id="js-partners" class="partners">
	<div class="content-wrapper">
		<h2 class="m-typo m-typo_primary heading">
			Partnerzy stowarzyszenia
		</h2>
		<div class="carousel-wrapper-secondary">
			<?php if( have_rows('partners_carousel', 'options') ):?>
				<?php while ( have_rows('partners_carousel', 'options') ) : the_row();?>
					<div class="box">
						<a href="<?php the_sub_field('partner_url', 'options') ?>" target="_blank">
							<img src="<?php the_sub_field('partner_logo', 'options') ?>" alt="partner-logo">
						</a>
					</div>
				<?php endwhile;?>
			<?php endif;?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
