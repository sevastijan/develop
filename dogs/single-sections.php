<?php
/**
 * Wordpress template created for "Wild Dogs"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Section
 *
 * @package WordPress
 *
 */
?>
<?php require_once(PB_THEME_DIR . 'header-other.php'); ?>
<?php the_post(); ?>

<section class="news news_listing post section clearfix">
	<div class="heading-bar section">
		<h2 class="m-typo m-typo_primary heading">
			<?php the_title(); ?>
		</h2>
		<div class="m-typo m-typo_text descr">
			<?php the_field('section_description') ?>
		</div>
	</div>
	<div class="content-wrapper">
		<div class="post-content">
			<div class="tab-nav-wrapper clearfix">
				<?php if( have_rows('section_tabs') ): $index = 0?>
					<?php while ( have_rows('section_tabs') ) : the_row();?>
						<p class="js-tab tab-nav js-tab-item-<?php echo $index ?>">
							<?php the_sub_field('tab_title'); ?>
						</p>
					<?php $index++; endwhile;?>
				<?php endif;?>
			</div>

			<?php if( have_rows('section_tabs') ): $index = 0?>
				<?php while ( have_rows('section_tabs') ) : the_row();?>
					<div class="js-tab tab-content js-tab-item-<?php echo $index ?>">
						<?php the_sub_field('tab_content'); ?>
					</div>
				<?php $index++; endwhile;?>
			<?php endif;?>


		</div>
	</div>
</section>

<div class="button-bar">
	<div class="news-button">
		<a href="https://goo.gl/forms/0XneEynZTFgQx9ah1" class="m-btn m-btn_primary m-typo m-typo_primary">Dołącz do nas</a>
	</div>
</div>

<?php get_footer(); ?>
