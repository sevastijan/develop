<?php
/**
 * Wordpress template created for "Stolarka Sanok"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Widok artykułu - Okna
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>



<article class="article category-listing">
    <div class="content-wrapper">
        <h2 class="typo typo_primary heading">
            Okna Hensfort
        </h2>
        <div class="category-listing-head">
            <p class="typo typo_text">	
                Poprzez przemyślaną politykę firmy, pełni pasji  i zaangażowania, konsekwentnie dążymy do efektywnego rozwoju zarówno naszego jak i naszych klientów, ponieważ klient i my to jeden organizm. Głęboko wierzymy, że taka filozofia jest gwarancją naszych wspólnych sukcesów w przyszłości.
            </p>
        </div>
        <div class="article-presentation category-listing-body">
            <div class="article-presentation_item category-listing-body_item">
                <img class="article-presentation_item-img category-listing-body_item-img" src="<?php the_post_thumbnail_url(); ?>" alt="image">
                <i class="arrow">
                    <p class="typo typo_secondary">
                        <?php the_title(); ?> 
                    </p>
                </i>
            </div>
            <?php if ( have_rows('window-single') ) : ?>
                <?php while( have_rows('window-single') ) : the_row(); ?>
                    <div class="article-presentation_item">
                        <img src=" <?php the_sub_field('window-single_img'); ?>" alt="" class="article-presentation_item-img">
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</article>

<article class="about-product">
    <div class="content-wrapper">
        <div class="typo typo_secondary">
            Najważniejsze cechy.
        </div>
        <?php the_content(); ?>
    </div>
</article>

<section class="article-navigation">
    <div class="content-wrapper">
        <?php $prev_post = get_previous_post(); ?>
            <a href="<?php echo $prev_post->guid ?>" class="article-navigation_item">
                <div class="article-navigation_item-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>images/article-arrow.png" alt="arrow" class="article-navigation_item-img">        
                </div>
            </a>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="article-navigation_item">
                <div>
                    <img src="<?php echo PB_THEME_URL; ?>images/article-icon0.png" alt="icon" class="article-navigation_item-img">        
                </div>
            </a>
        <?php $next_post = get_next_post(); ?>
            <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="article-navigation_item">
                <div class="article-navigation_item-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>images/article-arrow.png" alt="arrow" class="article-navigation_item-img">        
                </div>
            </a>
    </div>
</section>


<?php require_once(THEME_DIR.'/_includes/_bar.php'); ?>


<?php get_footer(); ?>










