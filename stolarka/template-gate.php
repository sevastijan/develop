<?php
/**
 * Wordpress template created for "Stolarka Sanok"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Bramy - Główna
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>


<?php require_once(THEME_DIR.'/_includes/_header-slider.php'); ?>


<section class="presentation gate">
   <div class="content-wrapper">
      <h2 class="typo typo_primary heading">
         Bramy
      </h2>
      <div class="presentation-head">
         <h3 class="typo typo_text">
            <?php the_field('gate_title'); ?>
         </h3>
         <div class="typo typo_text">	
            <?php the_field('gate_descr'); ?>
         </div>
      </div>
      <div class="presentation-body" style="margin-top: 50px;">
         <div class="boxes-wrapper small">
            <div class="box" style="background-image:url('<?php the_field('gate_main0') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <div class="typo typo_secondary">
                        Bramy <br> segmentowe
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                     <a href="<?php the_field('gate_main0-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="box" style="background-image:url('<?php the_field('gate_main1') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <div class="typo typo_secondary">
                        Bramy <br> uchylne
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                    <a href="<?php the_field('gate_main1-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="presentation-body">
         <div class="boxes-wrapper small gate">
            <div class="box" style="background-image:url('<?php the_field('gate_main0') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <div class="typo typo_secondary">
                        Bramy <br> roletowe
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                     <a href="<?php the_field('gate_main2-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="box" style="background-image:url('<?php the_field('gate_main1') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <div class="typo typo_secondary">
                        Bramy <br> rozwierne
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                    <a href="<?php the_field('gate_main3-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="box" style="background-image:url('<?php the_field('gate_main1') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <div class="typo typo_secondary">
                        Bramy <br> przemysłowe
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                    <a href="<?php the_field('gate_main4-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="presentation gate">
   <div class="content-wrapper">
      <h2 class="typo typo_primary heading">
         Automatyka do bram
      </h2>
      <div class="presentation-head">
         <h3 class="typo typo_text">
            <?php the_field('gate_title02'); ?>
         </h3>
         <div class="typo typo_text">	
            <?php the_field('gate_descr02'); ?>
         </div>
      </div>
      <div class="icons-slider">
        <?php if ( have_rows('gate_icons') ) : ?>

            <?php while( have_rows('gate_icons') ) : the_row(); ?>
          
              <div class="box">
                <a href="<?php the_sub_field('url') ?>">
                  <img src="<?php the_sub_field('icon'); ?>" alt="icon">
                  <h6 class="typo typo_primary heading">
                    <?php the_sub_field('text'); ?>
                  </h6>
                </a>
              </div>
          
            <?php endwhile; ?>

        <?php endif; ?>
        
        
      </div>
  </div>
</section>

<?php require_once(THEME_DIR.'/_includes/_presentation-contact.php'); ?>

<?php require_once(THEME_DIR.'/_includes/_partners.php'); ?>

<?php get_footer(); ?>


