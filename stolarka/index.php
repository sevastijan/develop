<?php get_header(); ?>

<section class="offer clearfix">
	<div class="content-wrapper">
		<h2 class="typo typo_primary heading">
			Sprawdź naszą ofertę
		</h2>
		<div class="boxes-wrapper">
		<?php if( have_rows('category_wrapper', 'options') ):?>
			<?php while ( have_rows('category_wrapper', 'options') ) : the_row();?>
				<div class="boxes-wrapper_box">
					<div class="img" style="background-image:url('<?php the_sub_field('category_img', 'options') ?>');">
						<div class="img_name typo typo_secondary">
							<?php the_sub_field('category_name', 'options') ?>
						</div>
						<a href="<?php the_sub_field('category_url', 'options') ?>">
							<i class="arrow">
								<img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
							</i>
						</a>
					</div>
					<div class="typo typo_text">
						<?php the_sub_field('category_descr', 'options') ?>
					</div>
				</div>
			<?php endwhile;?>
		<?php endif;?>
		</div>
	</div>
</section>


<section class="whyus clearfix">
	<div class="content-wrapper">
		<h2 class="typo typo_primary heading">
			Dlaczego warto nas wybrać
		</h2>
		<div class="box">
			<div class="box_img">
				<div class="subbox">
					<h6 class="typo typo_secondary">
						profesjonalizm
					</h6>
				</div>
				<div class="subbox">
					<h6 class="typo typo_secondary">
						najwyższa jakość
					</h6>
				</div>
				<div class="subbox">
					<h6 class="typo typo_secondary">
						montaż
					</h6>
				</div>
				<div class="subbox">
					<h6 class="typo typo_secondary">
						największy wybór
					</h6>
				</div>
				<div class="subbox">
					<h6 class="typo typo_secondary">
						doradztwo
					</h6>
				</div>
				<div class="subbox">
					<h6 class="typo typo_secondary">
						szybka realizacja
					</h6>
				</div>
			</div>
		</div>
	</div>
</section>

<?php require_once(THEME_DIR.'/_includes/_partners.php'); ?>

<section class="portfolio">
	<div class="content-wrapper">
		<h2 class="typo typo_primary heading">
			Nasze realizacje
		</h2>
		<div class="secondary-slider-wrapper">

		<?php if( have_rows('portfolio_wrapper', 'options') ):?>
			<?php while ( have_rows('portfolio_wrapper', 'options') ) : the_row();?>
				<div class="boxes-wrapper">
					<div class="box_img" style="background-image:url('<?php the_sub_field('portfolio_img', 'options') ?>');"></div>
					<a href="<?php the_sub_field('portfolio_url', 'options') ?>">
						<div class="box_name">
							<span><?php the_sub_field('portfolio_name', 'options') ?></span>
							<i class="arrow">
								<img src="<?php echo PB_THEME_URL; ?>/images/arrow.png" alt="arrow" class="arrow_img">
							</i>
						</div>
					</a>
				</div>
			<?php endwhile;?>
		<?php endif;?>

		</div>
	</div>
</section>


<?php get_footer(); ?>
