<footer class="footer">
	<section class="map">
		<div class="content-wrapper">
			<h2 class="typo typo_primary heading">
				Masz pytanie?
			</h2>
			<p class="typo typo_secondary">
				Zapraszamy do salonu sprzedaży lub napisz wiadomość, skontaktujemy się z Tobą.
			</p>
			<iframe src="https://snazzymaps.com/embed/76851" width="100%" height="400px" style="border:none;"></iframe>
		</div>
	</section>
	<section class="contact">
		<div class="content-wrapper">
			<div class="boxes clearfix">
				<div class="box">
					<p class="typo typo_primary">
						Osobiście
					</p>
					<p class="typo typo_text">
						Słowackiego 18 <br>
						38-500 Sanok
					</p>
				</div>
				<div class="box">
					<p class="typo typo_primary">
						Mailowo
					</p>
					<p class="typo typo_text">
						sanok@prostolarka.pl
					</p>
				</div>
				<div class="box">
					<p class="typo typo_primary">
						Telefonicznie
					</p>
					<p class="typo typo_text">
						+48 666 737 199
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="form clearfix">
		<?php echo do_shortcode('[contact-form-7 id="30" title="Formularz 1"]')?>
	</section>

	<?php require_once(THEME_DIR.'/_includes/_icons-category.php'); ?>

	<section class="bottom-bar">
		<div class="content-wrapper">
			<div class="box clearfix">
				<p class="typo typo_text">
					Copyright &copy; 2018 Prostolarka Sanok.. All Rights Reserved
				</p>
				<p class="typo typo_text">
					Website handcrafted by: Rednelo
				</p>
			</div>
		</div>
	</section>
</footer>



  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri() . '/js/app.js'?>"></script>
    <?php wp_footer(); ?>
  </body>
</html>
