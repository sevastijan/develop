<?php
/**
 * Wordpress template created for "Stolarka Sanok"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Okna - Główna
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>


<?php require_once(THEME_DIR.'/_includes/_header-slider.php'); ?>


<section class="presentation">
   <div class="content-wrapper">
      <h2 class="typo typo_primary heading">
         Okna PCV
      </h2>
      <div class="presentation-head">
         <h3 class="typo typo_text">
            <?php the_field('pcv_title'); ?>
         </h3>
         <div class="typo typo_text">	
            <?php the_field('pcv_descr'); ?>
         </div>
      </div>
      <div class="presentation-body">
         <div class="boxes-wrapper large">
            <div class="box" style="background-image:url('<?php the_field('windows-main_pcv') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <img src="<?php echo PB_THEME_URL; ?>images/win-icon.png" alt="icon" class="icon">
                     <div class="typo typo_secondary">
                        Okna <span>ProEnenrgy</span>
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                     <a href="<?php the_field('windows-main_pcv-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <div class="boxes-wrapper small">
            <div class="box" style="background-image:url('<?php the_field('windows-main_schuco') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <img src="<?php echo PB_THEME_URL; ?>images/Layer-17.png" alt="icon" class="icon">
                     <div class="typo typo_secondary">
                        okna schüco
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                     <a href="<?php the_field('windows-main_schuco-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="box" style="background-image:url('<?php the_field('windows-main_hensfort') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <img src="<?php echo PB_THEME_URL; ?>images/hensfort-logo.png" alt="icon" class="icon">
                     <div class="typo typo_secondary">
                        okna hensfort
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                    <a href="<?php the_field('windows-main_hensfort-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


<section class="presentation">
   <div class="content-wrapper">
      <h2 class="typo typo_primary heading">
         Okna drewniane
      </h2>
      <div class="presentation-head">
         <h3 class="typo typo_text">
            <?php the_field('wood_title'); ?>
         </h3>
         <div class="typo typo_text">	
            <?php the_field('wood_descr'); ?>
         </div>
      </div>
      <div class="presentation-body">
         <div class="boxes-wrapper large">
            <div class="box" style="background-image:url('<?php the_field('windows-main_wood') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <img src="<?php echo PB_THEME_URL; ?>images/win-icon.png" alt="icon" class="icon">
                     <div class="typo typo_secondary">
                        Seria  <span>Okien Drewnianych</span>
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                     <a href="<?php the_field('windows-main_wood-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>



<?php require_once(THEME_DIR.'/_includes/_partners.php'); ?>


<?php get_footer(); ?>


