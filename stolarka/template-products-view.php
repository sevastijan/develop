<?php
/**
 * Wordpress template created for "Stolarka Sanok"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Okna - widok produktów
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>



<section class="category-listing">
    <div class="content-wrapper">
        <h2 class="typo typo_primary heading">
            <?php the_title(); ?>
        </h2>
        <div class="category-listing-head">
            <div class="typo typo_text">	
                <?php the_field('excerpt'); ?>
            </div>
        </div>
        <div class="category-listing-body">

            <?php
                
                $classes = get_body_class(); 
                if (in_array('page-id-51',$classes)) {
                    $post_type = 'windows_pcv';
                } elseif (in_array('page-id-121', $classes)) {
                    $post_type = 'windows_schuco';
                } elseif (in_array('page-id-134', $classes)) {
                    $post_type = 'windows_wood';
                } elseif (in_array('page-id-132', $classes)) {
                    $post_type = 'windows_hensfort';
                }
            

                $loop = new WP_Query( array( 'post_type' => $post_type, 'paged' => $paged ) );
                if ( $loop->have_posts() ) :
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <div class="category-listing-body_item">
                            <a href="<?php the_permalink(); ?>">
                                <img class="category-listing-body_item-img" src="<?php the_post_thumbnail_url(); ?>" alt="image">
                                <i class="arrow">
                                    <p class="typo typo_secondary">
                                        <?php the_title(); ?>
                                    </p>
                                    <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                                </i>
                            </a>
                        </div>

                    <?php endwhile;
                endif;
                wp_reset_postdata();
            ?>

        </div>

    </div>    
</section>

<?php require_once(THEME_DIR.'/_includes/_bar.php'); ?>

<section class="presentation">
   <div class="content-wrapper">
      <h2 class="typo typo_primary heading">
         Pozostałe okna pcv
      </h2>
      <div class="presentation-body">
         <div class="boxes-wrapper small">
            <div class="box" style="background-image:url('<?php the_field('windows-listing_large', 'options') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <img src="<?php echo PB_THEME_URL; ?>images/Layer-17.png" alt="icon" class="icon">
                     <div class="typo typo_secondary">
                        okna schüco
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                     <a href="#">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="box" style="background-image:url('<?php the_field('windows-listing_large', 'options') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <img src="<?php echo PB_THEME_URL; ?>images/hensfort-logo.png" alt="icon" class="icon">
                     <div class="typo typo_secondary">
                        okna hensfort
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                     <a href="#">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


<?php require_once(THEME_DIR.'/_includes/_partners.php'); ?>



<?php get_footer(); ?>
