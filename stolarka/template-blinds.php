<?php
/**
 * Wordpress template created for "Stolarka Sanok"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Rolety - Główna
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>


<?php require_once(THEME_DIR.'/_includes/_header-slider.php'); ?>


<section class="presentation">
   <div class="content-wrapper">
      <h2 class="typo typo_primary heading">
         Rolety
      </h2>
      <div class="presentation-head">
         <h3 class="typo typo_text">
            <?php the_field('blinds_title'); ?>
         </h3>
         <div class="typo typo_text">	
            <?php the_field('blinds_descr'); ?>
         </div>
      </div>
      <div class="presentation-body" style="margin-top: 50px;">

         <div class="boxes-wrapper small">
            <div class="box" style="background-image:url('<?php the_field('blinds_main0') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <div class="typo typo_secondary">
                        zewnętrzne
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                     <a href="<?php the_field('blinds_main0-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
            <div class="box" style="background-image:url('<?php the_field('blinds_main1') ?>');">
               <div class="sub-boxes-wrapper">
                  <div class="sub-boxes-wrapper_box">
                     <div class="typo typo_secondary">
                        wewnętrzne
                     </div>
                  </div>
                  <div class="sub-boxes-wrapper_box">
                    <a href="<?php the_field('blinds_main1-url') ?>">
                        <i class="arrow">
                           <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                        </i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<?php require_once(THEME_DIR.'/_includes/_partners.php'); ?>

<?php require_once(THEME_DIR.'/_includes/_presentation-contact.php'); ?>


<?php get_footer(); ?>


