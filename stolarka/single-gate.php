<?php
/**
 * Wordpress template created for "Stolarka Sanok"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Widok artykułu - Bramy
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>



<article class="article category-listing">
    <div class="content-wrapper">
        <h2 class="typo typo_primary heading">
            <?php the_title(); ?> 
        </h2>
        <div class="category-listing-head">
            <div class="typo typo_text">	
                <?php the_content(); ?> 
            </div>
        </div>
    </div>
</article>

<section class="gate-heading">
    <div class="content-wrapper">
        <div class="gate-heading_img" style="background-image:url('<?php the_field('heading_img') ?>');"></div>
        <div class="gate-heading_name">
            <?php the_title(); ?> 
        </div>
    </div>
</section>


<section class="tabs">
    <div class="content-wrapper">
        <ul class="tabs-nav-ul">
            <li id="js-tab-item-0" class="tabs-nav-ul_item">Charakterystyka</li>
            <li id="js-tab-item-1" class="tabs-nav-ul_item">Dodatki</li>
            <li id="js-tab-item-2" class="tabs-nav-ul_item">Zdjęcia</li>
            <li id="js-tab-item-3" class="tabs-nav-ul_item">Dane techniczne</li>
            <li id="js-tab-item-4" class="tabs-nav-ul_item">Kolorystyka</li>
        </ul>

        <div class="tabs-content">
            <div id="js-tab-0" class="tab tabs-tg-dl">
                <span class="heading">Charakterystyka modelu</span>
                <br>
                <div class="typo typo_text">
                    <?php the_field('t0_content'); ?>
                </div>
            </div>
            <div id="js-tab-1" class="tab tabs-tg-dl">
                <span class="heading">Dodatki</span>
                <br>
                <?php the_field('t1_content'); ?>
            </div>
            <div id="js-tab-2" class="tab tabs-tg-dl">
                <span class="heading">Zdjęcia</span>
                <br>
                <?php the_field('t2_content'); ?>
            </div>
            <div id="js-tab-3" class="tab tabs-tg-dl">
                <span class="heading">Dane techniczne</span>
                <br>
                <?php the_field('t3_content'); ?>            
            </div>
            <div id="js-tab-4" class="tab tabs-tg-dl">
                <span class="heading">Kolorystyka</span>
                <br>
                <?php the_field('t4_content'); ?>
            </div>
        </div>
    </div>
</section>


<section class="article-navigation">
    <div class="content-wrapper">
        <?php $prev_post = get_previous_post(); ?>
            <a href="<?php echo $prev_post->guid ?>" class="article-navigation_item">
                <div class="article-navigation_item-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>images/article-arrow.png" alt="arrow" class="article-navigation_item-img">        
                </div>
            </a>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="article-navigation_item">
                <div>
                    <img src="<?php echo PB_THEME_URL; ?>images/article-icon0.png" alt="icon" class="article-navigation_item-img">        
                </div>
            </a>
        <?php $next_post = get_next_post(); ?>
            <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="article-navigation_item">
                <div class="article-navigation_item-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>images/article-arrow.png" alt="arrow" class="article-navigation_item-img">        
                </div>
            </a>
    </div>
</section>


<?php require_once(THEME_DIR.'/_includes/_bar.php'); ?>

<?php get_footer(); ?>










