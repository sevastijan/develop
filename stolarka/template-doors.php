<?php
/**
 * Wordpress template created for "Stolarka Sanok"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Drzwi - Główna
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>

<section class="presentation doors">
   <div class="content-wrapper">
      <h2 class="typo typo_primary heading">
         Drzwi zewnętrzne Stalowe
      </h2>
      
      <div class="presentation-head">
         <h3 class="typo typo_text">
            <?php the_field('doors_title'); ?>
         </h3>
         <div class="typo typo_text">	
            <?php the_field('doors_descr'); ?>
        </div>
      </div>
    </div>
    <div class="content-wrapper">
        <div class="presentation-body">
            <div class="two-boxes">
                <?php if ( have_rows('doors_pdf') ) : ?>
                    <?php while( have_rows('doors_pdf') ) : the_row(); ?>
                        <a href="<?php the_sub_field('doors_pdf-url'); ?>">
                            <div class="two-boxes-item">
                                <div class="two-boxes-item_img" style="background-image:url('<?php the_sub_field('doors_pdf-img'); ?>');"></div>
                                <i class="arrow">
                                    <p class="typo typo_secondary">
                                        <?php the_sub_field('doors_pdf-title'); ?> 
                                    </p>
                                    <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                                </i>
                            </div>
                        </a>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

</section>

<?php require_once(THEME_DIR.'/_includes/_bar.php'); ?>


<?php require_once(THEME_DIR.'/_includes/_presentation-contact.php'); ?>






<?php get_footer(); ?>
