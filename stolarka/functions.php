<?php

if(!defined('THEME_DIR')) {
define('THEME_DIR', get_theme_root(). '/' .get_template() . '/');
}

if(!defined('PB_THEME_URL')) {
define('PB_THEME_URL', WP_CONTENT_URL. '/themes/' .get_template() . '/');
}

  /**
  *  Register settings page
  *
  */
  if( function_exists('acf_add_options_page')) {
  	acf_add_options_page(array(
  		'page_title' 	=> 'Ustawienia',
        'post_id' => 'options',
  		'menu_title'	=> 'Ustawienia',
  		'menu_slug' 	=> 'module-settings',
  		'capability'	=> 'edit_posts',
  		'redirect'		=> false
  	));
  }

  register_nav_menus(array(
    'primary' => 'Main navigation'
  ));
  add_theme_support( 'post-thumbnails' );


  function create_post_type() {
	register_post_type( 'windows_pcv',
	  array(
		'labels' => array(
		  'name' => __( 'Okna PCV' ),
		  'singular_name' => __( 'Okna PCV' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
	add_action( 'init', 'create_post_type' );
	
  function create_post_type1() {
	register_post_type( 'windows_schuco',
	  array(
		'labels' => array(
		  'name' => __( 'Okna Schuco' ),
		  'singular_name' => __( 'Okna Schuco' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
	add_action( 'init', 'create_post_type1' );
	
  function create_post_type2() {
	register_post_type( 'windows_hensfort',
	  array(
		'labels' => array(
		  'name' => __( 'Okna Hensfrot' ),
		  'singular_name' => __( 'Okna Hensfrot' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
  add_action( 'init', 'create_post_type2' );
	
  function create_post_type3() {
	register_post_type( 'windows_wood',
	  array(
		'labels' => array(
		  'name' => __( 'Okna Drewniane' ),
		  'singular_name' => __( 'Okna Drewniane' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
	add_action( 'init', 'create_post_type3' );
	
  function create_post_type4() {
	register_post_type( 'blinds_out',
	  array(
		'labels' => array(
		  'name' => __( 'Rolety zewnętrzne' ),
		  'singular_name' => __( 'Rolety zewnętrzne' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
  add_action( 'init', 'create_post_type4' );
	
  function create_post_type5() {
	register_post_type( 'blinds_in',
	  array(
		'labels' => array(
		  'name' => __( 'Rolety wewnętrzne' ),
		  'singular_name' => __( 'Rolety wewnętrzne' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
	add_action( 'init', 'create_post_type5' );
		
  function create_post_type6() {
	register_post_type( 'gate',
	  array(
		'labels' => array(
		  'name' => __( 'Bramy segmentowe' ),
		  'singular_name' => __( 'Bramy segmentowe' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
	add_action( 'init', 'create_post_type6' );
	
  function create_post_type7() {
	register_post_type( 'gate_1',
	  array(
		'labels' => array(
		  'name' => __( 'Bramy uchylne' ),
		  'singular_name' => __( 'Bramy uchylne' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
  add_action( 'init', 'create_post_type7' );
  function create_post_type8() {
	register_post_type( 'gate_2',
	  array(
		'labels' => array(
		  'name' => __( 'Bramy roletowe' ),
		  'singular_name' => __( 'Bramy roletowe' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
  add_action( 'init', 'create_post_type8' );
  function create_post_type9() {
	register_post_type( 'gate_3',
	  array(
		'labels' => array(
		  'name' => __( 'Bramy rozwierne' ),
		  'singular_name' => __( 'Bramy rozwierne' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
	add_action( 'init', 'create_post_type9' );

  function create_post_type10() {
	register_post_type( 'gate_4',
	  array(
		'labels' => array(
		  'name' => __( 'Bramy przemysłowe' ),
		  'singular_name' => __( 'Bramy przemysłowe' )
		),
		'public' => true,
		'has_archive' => true,
		'taxonomies' => array('category'),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail',)
	  )
	);
  }
  add_action( 'init', 'create_post_type10' );
?>
