<section class="icons-category">
	<div class="content-wrapper">
		<div class="icons-category-wrapper">

            <?php if ( have_rows('footer_icons', 'options') ) : ?>
            
                <?php while( have_rows('footer_icons', 'options') ) : the_row(); ?>
            
                    
                    <div class="box">
                        <a href="<?php the_sub_field('footer_icons-link', 'options'); ?>">
                            <img src="<?php the_sub_field('footer_icons-img', 'options'); ?>" alt="category-icon">
                        </a>
                    </div>
            
                <?php endwhile; ?>
            
            <?php endif; ?>
            


		</div>
	</div>
</section>