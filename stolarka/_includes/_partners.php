<?php 
	$classes = get_body_class(); 
	if (in_array('page-id-161',$classes)) {
		$carousel = 'maker_carousel';
		$options = '';
	} else {
		$carousel = 'partners_carousel';
		$options = 'options';
	}
if( have_rows($carousel, $options) ):?>
<section class="partners">
	<div class="content-wrapper">
		<h2 class="typo typo_primary heading">
			<?php 
				if (in_array('page-id-161',$classes)) {
					echo 'Nasi producenci';
				} else {
					echo 'Partnerzy';
				}
			?>
		</h2>
		<div class="carousel-wrapper-secondary">
			<?php while ( have_rows($carousel, $options) ) : the_row();?>
				<div class="box">
					<img src="<?php the_sub_field('partner_logo', 'options') ?>" alt="partner-logo">
				</div>
			<?php endwhile;?>
		</div>
	</div>
</section>
<?php endif;?>