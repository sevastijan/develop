<section class="presentation-contact">
    <div class="content-wrapper">
        <div class="presentation-contact-box">
            <div class="typo typo_text">
                <?php the_field('contact_bar', 'options'); ?>
            </div>
        </div>
        <div class="presentation-contact-box">
            <h6 class="typo typo_secondary">
                Nowa  seria <span>SEtto</span>
            </h6>
        </div>
    </div>
</section>