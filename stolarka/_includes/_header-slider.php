<section class="header-slider">
	<div class="content-wrapper">
		<div class="primary-slider-wrapper">

        <?php 
            $classes = get_body_class(); 
            if (in_array('home',$classes)) {
                $mainSlider = 'slider_wrapper';
            } elseif (in_array('page-id-44',$classes)) {
                $mainSlider = 'slider_wrapper-windows';
			} elseif (in_array('page-id-62',$classes)) {
                $mainSlider = 'slider_wrapper-doors';
            } elseif (in_array('page-id-161',$classes)) {
                $mainSlider = 'slider_wrapper-blinds';
            } elseif(in_array('page-id-233', $classes)) {
				$mainSlider = 'slider_wrapper-blinds';
            }
        ?>

			<?php if( have_rows($mainSlider, 'options') ):?>
				<?php while ( have_rows($mainSlider, 'options') ) : the_row();?>
					<div class="box">
						<div class="box-heading clearfix">
							<h2 class="typo typo_primary box-heading_title">
								<?php the_sub_field('slider_main', 'options') ?>
							</h2>
							<div class="box-heading_img" style="background-image:url('<?php the_sub_field('slider_img', 'options') ?>');">
							</div>
						</div>
						<div class="box-sub clearfix">
							<span class="pagingInfo"></span>
							<div class="box-sub_info typo typo_text">
								<?php the_sub_field('slider_descr', 'options') ?>
							</div>
							<a class="arrow-wrapper" href="<?php the_sub_field('slider_url', 'options') ?>">
								<i class="arrow">
									<img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
								</i>
							</a>
						</div>
					</div>
				<?php endwhile;?>
			<?php endif;?>
		</div>
	</div>
</section>

