<?php
/**
 * Wordpress template created for "Stolarka Sanok"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Rolety - widok produktów
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>



<section class="category-listing">
    <div class="content-wrapper">
        <h2 class="typo typo_primary heading">
            <?php the_title(); ?>
        </h2>
        <div class="category-listing-head">
            <div class="typo typo_text">	
                <?php the_field('excerpt'); ?>
            </div>
        </div>
        <div class="category-listing-body">

            <?php 
                $classes = get_body_class(); 
                if (in_array('page-id-204',$classes)) {
                    $post_type = 'blinds_out';
                } elseif (in_array('page-id-210', $classes)) {
                    $post_type = 'blinds_in';
                } elseif (in_array('page-id-405' , $classes)) {
                    $post_type = 'gate';
                } elseif (in_array('page-id-413', $classes)) {
                    $post_type = 'gate_1';
                } elseif (in_array('page-id-415', $classes)) {
                    $post_type = 'gate_2';
                } elseif (in_array('page-id-417', $classes)) {
                    $post_type = 'gate_3';
                } elseif (in_array('page-id-419', $classes)) {
                    $post_type = 'gate_4';
                }
                $loop = new WP_Query( array( 'post_type' => $post_type, 'paged' => $paged ) );
                if ( $loop->have_posts() ) :
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <div class="category-listing-body_item">
                            <a href="<?php the_permalink(); ?>">
                                <img class="category-listing-body_item-img" src="<?php the_post_thumbnail_url(); ?>" alt="image">
                                <i class="arrow">
                                    <p class="typo typo_secondary">
                                        <?php the_title(); ?>
                                    </p>
                                    <img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow" class="arrow_img">
                                </i>
                            </a>
                        </div>

                    <?php endwhile;
                endif;
                wp_reset_postdata();
            ?>

        </div>

    </div>    
</section>

<?php require_once(THEME_DIR.'/_includes/_bar.php'); ?>

<?php require_once(THEME_DIR.'/_includes/_presentation-contact.php'); ?>

<?php get_footer(); ?>
