// // burger
$(".burger").click(function(){
    $("#menu-list").toggleClass("is-active");
    $("body").toggleClass("is-active");
});

// // hide menu after click
$(".list_item").click(function(){
    $("body").removeClass("is-active");
    $("#menu-list").removeClass("is-active");
});
   var $status = $('.pagingInfo');
   var $primarySlickElement = $('.primary-slider-wrapper');
   var $secondarySlickElement = $('.secondary-slider-wrapper');
   $primarySlickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
	   //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
	   var i = (currentSlide ? currentSlide : 0) + 1;
	   $status.html('<span>' + i + '</span>' + '<span>/</span>' + '<span>' + slick.slideCount + '</span>');
   });
   $primarySlickElement.slick({
	   	arrows: false,
	   	dots: false,
	   	infinite: true,
	   	autoplay: true,
	   	autoplaySpeed: 3000,
		fade: true,
		cssEase: 'linear'
   });
   $secondarySlickElement.slick({
	   	arrows: false,
	   	dots: false,
	   	infinite: true,
	   	autoplay: true,
	   	autoplaySpeed: 3000,
		fade: true,
		cssEase: 'linear'
   });

$('.carousel-wrapper-secondary').slick({
	dots: false,
    infinite: true,
	arrows: true,
    speed: 300,
    slidesToShow: 4,
	nextArrow: '<i class="arrow"><span></span><span></span></i>',
	prevArrow: '<i class="arrow"><span></span><span></span></i>',
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});

// contact textarea height
$("textarea").click( function() {
  $( this ).height(100);
  $( this ).blur( function() {
    if( $( this ).val() !== ""){
      $( this ).height(100);
    } else{
      $( this ).height(31);
    }
  });
});

// tabs
$('.tabs-tg-dl').fadeOut();
$('#js-tab-0').fadeIn()
$("#js-tab-item-0").addClass("active");
$('#js-tab-item-0').click(function(){

  $(".tabs-nav-ul_item").removeClass("active");
  $("#js-tab-item-0").addClass("active");

  $('.tabs-tg-dl').fadeOut();
	setTimeout(() => {
    $('#js-tab-0').fadeIn();
  }, 500);
});
$('#js-tab-item-1').click(function(){
    
  $(".tabs-nav-ul_item").removeClass("active");
  $("#js-tab-item-1").addClass("active");

  $(".tabs-tg-dl").fadeOut();
  setTimeout(() => {
    $('#js-tab-1').fadeIn();
  }, 500);
});
$('#js-tab-item-2').click(function () {

  $(".tabs-nav-ul_item").removeClass("active");
  $("#js-tab-item-2").addClass("active");

  $(".tabs-tg-dl").fadeOut();
  setTimeout(() => {
    $('#js-tab-2').fadeIn();
  }, 500);
});
$("#js-tab-item-3").click(function() {
  $(".tabs-nav-ul_item").removeClass("active");
  $("#js-tab-item-3").addClass("active");

  $(".tabs-tg-dl").fadeOut();
  setTimeout(() => {
    $("#js-tab-3").fadeIn();
  }, 500);
});
$("#js-tab-item-4").click(function() {
  $(".tabs-nav-ul_item").removeClass("active");
  $("#js-tab-item-4").addClass("active");

  $(".tabs-tg-dl").fadeOut();
  setTimeout(() => {
    $("#js-tab-4").fadeIn();
  }, 500);
});


// sub-menu
$('#menu-list li > .sub-menu').parent().hover(function(){
  $(this).children('.sub-menu').slideToggle(200);
});
