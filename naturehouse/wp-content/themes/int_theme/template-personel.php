<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: Personel
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); ?>



<div class="b-container">

  <?php require_once(THEME_DIR . '_includes/_modules/_breadcrumbs.php'); ?>


  <section class="b-article-text">
  <div class="b-article-text_title">
    <?php if(get_the_ID() == 76 ) : ?>
      <a href="<?php echo esc_url( home_url( '/#js-pos' ) ); ?>" class="m-btn m-btn_tertiary">Umów się na wizytę</a>
    <?php else : ?>
      <a href="<?php echo esc_url( home_url( '/#js-contact' ) ); ?>" class="m-btn m-btn_tertiary">Umów się na wizytę</a>
    <?php endif; ?>
    <h1 class="m-typo m-typo_primary">
      <?php the_title(); ?>
    </h1>
  </div>


  <?php if( have_rows('personel', 'options') ): while ( have_rows('personel', 'options') ) : the_row(); ?>

          <div class="b-personel">
          	<div class="b-personel_image">
          		<img src="<?php the_sub_field('photo'); ?>">
          	</div>          	
          	<div class="b-personel_descr">
          		<?php the_sub_field('opis'); ?>
          	</div>
          </div>

    <?php endwhile; endif; ?>
  <?php if(get_field('mosaic_activity')) : ?>
    <?php require_once(THEME_DIR . '_includes/_modules/_mosaic.php'); ?>
  <?php endif; ?>  
</section>
<?php if(get_field('testimonials_activity')) : ?>
	<?php require_once(THEME_DIR . '_includes/_modules/_testimonials.php'); ?>
<?php endif; ?>



  <?php require_once(THEME_DIR . '_includes/_modules/_nav-arrows.php'); ?>


  <?php require_once(THEME_DIR . '_includes/_modules/_knowledge-zone.php'); ?>


</div>



<?php get_footer(); ?>
