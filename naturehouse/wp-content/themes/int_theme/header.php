<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="Jako jedyni oferujemy kuracje z suplementami oraz oparte tylko na diecie. Poznaj nową jakość odchudzania!">
  <title><?php bloginfo('name'); ?> - <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
	<meta property="og:title" content="<?php bloginfo('name'); ?>" />
	<meta property="og:description" content="<?php bloginfo('name'); ?> - <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>" />
	<meta property="og:image" content="<?php echo THEME_URL; ?>assets/images/fb-og-img.png" />

  <link href="https://fonts.googleapis.com/css?family=Lekton:400,700|Raleway:400,700|Roboto+Condensed:300,400|Roboto:300,400,500" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>assets/css/style.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>style.css">
  <link rel="shortcut icon" type="image/png" href="<?php echo THEME_URL; ?>assets/images/favicon-16x16.png">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="m-lines">
  <div class="m-lines_single"></div>
  <div class="m-lines_single"></div>
  <div class="m-lines_single"></div>
  <div class="m-lines_single"></div>
  <div class="m-lines_single"></div>
</div>
<?php require_once(THEME_DIR . '_includes/_blocks/_header.php'); ?>
