<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: Article
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); ?>



<div class="b-container b-knowledgeZone">

  <?php require_once(THEME_DIR . '_includes/_modules/_breadcrumbs.php'); 

  $plans = new WP_Query(array(
    'post_type' => 'post',
    'posts_per_page' => -1
  )); ?>

 <?php if($plans->have_posts()) : ?>
 <section class="b-mosaic clearfix">
  <div class="mosaic-boxes-wrapper">
    <?php while($plans->have_posts()) : $plans->the_post(); ?>
    <div class="box" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
      <h3 class="m-typo m-typo_primary">
        <?php the_title(); ?>
      </h3>
      <p class="m-typo m-typo_secondary">
        <?php the_excerpt_max_charlength(160); ?>
      </p>
      <a href="<?php the_permalink(); ?>" class="m-btn m-btn_primary">Czytaj więcej</a>
    </div>
     <?php endwhile; ?>
  </div>
</section>
  <?php endif; ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_knowledge-zone.php'); ?>


</div>



<?php get_footer(); ?>
