<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: Search
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); ?>



<div class="b-container">

  <?php require_once(THEME_DIR . '_includes/_modules/_breadcrumbs.php'); ?>


  <section class="b-four-boxes">
  <?php $loop = 0; ?>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <div class="boxes-row-wrapper">
    <div class="box">
	  <?php if ($loop == 0) : ?>
	      <div class="b-four-boxes_title">
	        <h1 class="m-typo m-typo_primary">
	          Nasze <br> Lokalizacje
	        </h1>
	      </div>
	  <?php endif; ?>
      <h1 class="m-typo m-typo_primary">
        <?php the_title(); ?>
      </h1>
      <a href="#" class="m-btn m-btn_tertiary">facebook</a>
      <div class="text-wrapper">
        <p class="m-typo m-typo_secondary">
          Poniedziałek<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
          Wtorek<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
          Środa<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
          Czwartek<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
           Piątek<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
          Sobota<span>9-13</span>
        </p>
      </div>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_primary">
        ul.Mickiewicza 21c <br> 38-500 Sanok
      </h1>
      <h1 class="m-typo m-typo_secondary">
        +48 13 46 789
      </h1>
      <h1 class="m-typo m-typo_secondary">
        sanok@projekt-zdrowie.info
      </h1>
      <div class="btn-wrapper">
        <p href="#" class="m-btn m-btn_tertiary js-fourBoxes_btn">Umów się na wizytę</p>
      </div>
    </div>
    <div class="box">
      <!--  przenieść dodawanie bg z css'a -->
    </div>
    <div class="box">
      <!--  przenieść dodawanie bg z css'a -->
    </div>
    <div class="box_contact">
      <h1 class="m-typo m-typo_primary">
        Zostaw nam swój numer telefonu, a my skontaktujemy się z Tobą aby umówić datę spotkania.
      </h1>
      <?php echo do_shortcode('[contact-form-7 id="90" title="Contact form 1"]'); ?>
      <p class="close">
      </p>
    </div>
  </div>

  <?php $loop++; endwhile; ?>
</section>
<?php endif; ?>


</div>



<?php get_footer(); ?>
