<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: Price
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); ?>

<div class="b-container">

  <?php require_once(THEME_DIR . '_includes/_modules/_breadcrumbs.php'); ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_article-text.php'); ?>

  <img src="<?php echo THEME_URL; ?>/assets/images/jakdzialamyinfo.jpg" alt="jak-dzialamy" class="price-template-img">

  <?php require_once(THEME_DIR . '_includes/_modules/_sidebar-product.php'); ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_benefits.php'); ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_price-three-boxes.php'); ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_nav-arrows.php'); ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_pos-search.php'); ?>

</div>

<?php get_footer(); ?>
