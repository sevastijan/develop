<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); ?>


	<?php require_once(THEME_DIR . '_includes/_modules/_features.php'); ?>



	<?php require_once(THEME_DIR . '_includes/_modules/_pos-search.php'); ?>



	<?php require_once(THEME_DIR . '_includes/_modules/_testimonials.php'); ?>



	<?php require_once(THEME_DIR . '_includes/_modules/_feature-products.php'); ?>



  <?php require_once(THEME_DIR . '_includes/_modules/_knowledge-zone.php'); ?>




<?php get_footer(); ?>
