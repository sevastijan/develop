<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php require_once(THEME_DIR . '_includes/_blocks/_footer.php'); ?>
<div class="b-container b-footer-copyrights">
    all right reserved - projektzdrowie.info &copy; 2017
</div>
<?php require_once(THEME_DIR.'_includes/_modules/_mobile.php'); ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script type="text/javascript" src="<?php echo THEME_URL; ?>/assets/js/app/app.js"></script>
<?php wp_footer(); ?>
<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    location = '<?php echo esc_url( home_url( '/dziekujemy' ) ); ?>';
}, false );
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 831382630;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/831382630/?guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>
