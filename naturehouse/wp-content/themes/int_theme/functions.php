<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
/**
*  Define root theme url's
*
*/
if (!defined('THEME_DIR')) {
    define('THEME_DIR', get_theme_root().'/'.get_template().'/');
}
if (!defined('THEME_URL')) {
    define('THEME_URL', WP_CONTENT_URL.'/themes/'.get_template().'/');
}

/**
*  Register menus
*
*/
register_nav_menus(array(
  'main_menu' => 'Main menu',
  'footer_first' => 'Footer first menu',
  'footer_second' => 'Footer second menu'
));

/**
*  Register option
*
*/
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'APP Settings',
		'menu_title'	=> 'APP Settings',
		'menu_slug' 	=> 'app-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

/**
*  Custom queries
*
*/
function custom_query_vars_filter($vars) {
  $vars[] = 'cpt';

  return $vars;
}
add_filter('query_vars', 'custom_query_vars_filter');

add_filter( 'acf/rest_api/field_settings/show_in_rest', '__return_true' );



add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );

function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
    $my_attr = 'email-728';

    if ( isset( $atts[$my_attr] ) ) {
        $out[$my_attr] = $atts[$my_attr];
    }

    return $out;
}

if( function_exists('acf_set_options_page_capability') ) {
    acf_set_options_page_capability( 'edit_posts' );
}

/**
*  Theme thumbnails
*
*/
add_theme_support('post-thumbnails', array('post', 'trainers', 'packages', 'tips', 'camps',));

/**
*  Import custom post types
*
*/
require_once(THEME_DIR.'/_includes/_functions/_custom-post-types.php');

/**
*  Custom post excerpt
*
*/
require_once(THEME_DIR.'/_includes/_functions/_the-excerpt.php');

/**
*  Custom walker
*
*/
require_once(THEME_DIR.'/_includes/_functions/_custom-walker.php');
