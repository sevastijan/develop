<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: POS View
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php get_header(); ?>
<div class="b-posHome">
<section id="js-contact" class="b-container b-four-boxes">
  <div class="boxes-row-wrapper">
    <div class="box">
      <div class="b-four-boxes_title">
        <h1 class="m-typo m-typo_primary">
          Przyjdź osobiście <br> lub umów się na wizytę
        </h1>
      </div>
      <h1 class="m-typo m-typo_primary">
        <?php the_field('name', 'options');?>
      </h1>
      <!--<a href="#" class="m-btn m-btn_tertiary">facebook</a>-->
      <div class="text-wrapper">
        <div class="m-typo m-typo_secondary">
          Poniedziałek<span><?php the_field('pn', 'options');?></span>
        </div>
        <div class="m-typo m-typo_secondary">
          Wtorek<span><?php the_field('wt', 'options');?></span>
        </div>
        <div class="m-typo m-typo_secondary">
          Środa<span><?php the_field('sr', 'options');?></span>
        </div>
        <div class="m-typo m-typo_secondary">
          Czwartek<span><?php the_field('czw', 'options');?></span>
        </div>
        <div class="m-typo m-typo_secondary">
           Piątek<span><?php the_field('pt', 'options');?></span>
        </div>
        <div class="m-typo m-typo_secondary">
          Sobota<span><?php the_field('sb', 'options');?></span>
        </div>
      </div>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_primary">
        <?php the_field('adres', 'options');?>
      </h1>
      <a class="phone" href="tel://+48<? the_field('telefon', 'options'); ?>">
        <h1 class="m-typo m-typo_secondary">
          <?php the_field('telefon', 'options');?>
        </h1>
      </a>
      <h1 class="m-typo m-typo_secondary">
        <?php the_field('email', 'options');?>
      </h1>
      <div class="btn-wrapper">
        <p href="#" class="m-btn m-btn_tertiary js-fourBoxes_btn">Umów się na wizytę</p>
      </div>
    </div>
    <?php $photo1 = get_field('photo_1', 'options'); ?>
    <?php $photo2 = get_field('photo_2', 'options'); ?>



    <a href="<?php echo esc_url( home_url( '/personel' ) ); ?>">
     <div class="box" style="background-image: url(<?php echo $photo1['url']; ?>">
     </div>
    </a>
    <a href="<?php the_field('fb', 'options'); ?>" target="_blank" class="js-MapLink">
      <div class="box" style="background-image: url(<?php echo $photo2['url']; ?>">
      </div>
    </a>
    <div class="box_contact">
      <h1 class="m-typo m-typo_primary">
        Zostaw nam swój numer telefonu, a my skontaktujemy się z Tobą aby umówić datę spotkania.
      </h1>
      <?php echo do_shortcode('[contact-form-7 id="90" title="Formularz kontaktowy"]'); ?>
      <p class="close">
      </p>
    </div>
  </div>

</section>

  <div class="b-container">

    <?php require_once(THEME_DIR . '_includes/_modules/_two-boxes.php'); ?>

  </div>
	<?php //require_once(THEME_DIR . '_includes/_modules/_features.php'); ?>

	<?php require_once(THEME_DIR . '_includes/_modules/_pos-search.php'); ?>

	<?php require_once(THEME_DIR . '_includes/_modules/_testimonials.php'); ?>

	<?php require_once(THEME_DIR . '_includes/_modules/_feature-products.php'); ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_knowledge-zone.php'); ?>
</div>

<?php get_footer(); ?>
