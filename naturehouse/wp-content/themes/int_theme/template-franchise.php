<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: Franchise
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); ?>



<div class="b-container">


  <?php require_once(THEME_DIR . '_includes/_modules/_franchise-text.php'); ?>


</div>



<?php get_footer(); ?>
