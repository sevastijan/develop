<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: POS View
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

t
<?php get_header(); ?>
<div class="b-posHome">
<section class="b-container b-hero">
	<div class="b-hero-wrapper_heading">
    <div class="b-hero-heading">
      <div class="heading-box">
        <a href="#">
          <h3 class="m-typo m-typo_primary heading-slide">
            <span>Sprawdzone Metody <br> Dietetyków</span>
          </h3>
        </a>
      </div>
      <div class="heading-box">
        <a href="#">
          <h3 class="m-typo m-typo_primary heading-slide">
            <span>Nowa jakość <br> odchudzania</span>
          </h3>
        </a>
      </div>
      <div class="heading-box">
        <a href="#">
          <h3 class="m-typo m-typo_primary heading-slide">
            <span>Chudnij razem <br> z nami!</span>
          </h3>
        </a>
      </div>
    </div>
	</div>
  <div class="b-hero-wrapper_about">
    <div class="box-about">
      <div class="box-about_title">
        <h3 class="m-typo m-typo_primary">
          Projekt - Zdrowie <br> oto jest....
        </h3>
      </div>
      <div class="box-about_text">
        <p class="m-typo m-typo_secondary">
          <?php the_field('excerpt', 'option'); ?>
        </p>
        <a href="#" class="m-btn m-btn_primary">Zobacz całą naszą ofertę</a>
      </div>
    </div>
  </div>
</section>
<section class="b-container b-four-boxes">
  <div class="boxes-row-wrapper">
    <div class="box">
      <div class="b-four-boxes_title">
        <h1 class="m-typo m-typo_primary">
          Przyjdź osobiście <br> lub umów się na wizytę
        </h1>
      </div>
      <h1 class="m-typo m-typo_primary">
        Sanok
    </h1>
      <a href="#" class="m-btn m-btn_tertiary">facebook</a>
      <div class="text-wrapper">
        <p class="m-typo m-typo_secondary">
          Poniedziałek<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
          Wtorek<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
          Środa<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
          Czwartek<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
           Piątek<span>9-17</span>
        </p>
        <p class="m-typo m-typo_secondary">
          Sobota<span>9-13</span>
        </p>
      </div>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_primary">
        ul.Mickiewicza 21c <br> 38-500 Sanok
      </h1>
      <h1 class="m-typo m-typo_secondary">
        +48 13 46 789
      </h1>
      <h1 class="m-typo m-typo_secondary">
        sanok@projekt-zdrowie.info
      </h1>
      <div class="btn-wrapper">
        <p href="#" class="m-btn m-btn_tertiary js-fourBoxes_btn">Umów się na wizytę</p>
      </div>
    </div>
    <div class="box">
      <!--  przenieść dodawanie bg z css'a -->
    </div>
    <div class="box">
      <!--  przenieść dodawanie bg z css'a -->
    </div>
    <div class="box_contact">
      <h1 class="m-typo m-typo_primary">
        Zostaw nam swój numer telefonu, a my skontaktujemy się z Tobą aby umówić datę spotkania.
      </h1>
      <?php echo do_shortcode('[contact-form-7 id="90" title="Formularz kontaktowy"]'); ?>
    </div>
  </div>

</section>

<div class="b-container b-posts_title">
	  <h1 class="m-typo m-typo_primary">
	    Oferta<br>
	    Metodologia
	  </h1>
	</div>
	<?php require_once(THEME_DIR . '_includes/_modules/_features.php'); ?>



	<?php require_once(THEME_DIR . '_includes/_modules/_pos-search.php'); ?>



	<?php require_once(THEME_DIR . '_includes/_modules/_testimonials.php'); ?>



	<?php require_once(THEME_DIR . '_includes/_modules/_feature-products.php'); ?>



  <?php require_once(THEME_DIR . '_includes/_modules/_knowledge-zone.php'); ?>
</div>

<?php get_footer(); ?>
