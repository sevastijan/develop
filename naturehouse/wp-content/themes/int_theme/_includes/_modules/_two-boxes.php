<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>


<div class="b-two-boxes">




  <?php if(get_the_ID() !=  83) : ?>
  <section class="b-features b-two-boxes_box">
    <div class="b-posts_title">
      <h1 class="m-typo m-typo_primary">
        Aktualności
      </h1>
    </div>
    <div id="js-offer" class="b-posts_boxes">
      <?php
        $posList = new WP_Query(array(
          'post_type' => 'post',
          'posts_per_page' => 2
        ));
      ?>
    <div class="b-features_boxes">
     <?php if($posList->have_posts()) : while($posList->have_posts()) : $posList->the_post(); ?>
      <div class="m-features-box" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
        <div class="m-features-layer">
            <h6 class="m-typo m-typo_primary">
              <?php the_title(); ?>
            </h6>
            <p class="m-typo m-typo_secondary">
              <?php the_excerpt_max_charlength(190); ?>
            </p>
              <a href="<?php the_permalink(); ?>" class="m-btn m-btn_primary">Czytaj więcej</a>
        </div>
      </div>
    <?php $i++; endwhile; endif; ?>
	<a href="/aktualnosci" target="_blank" class="m-btn m-btn_primary more">Zobacz więcej</a>
    </div>
  </section>
  <?php endif; ?>




  <?php if(get_the_ID() !=  83) : ?>
      <section class="b-posts b-two-boxes_box">
  	<div class="b-posts_title">
  	  <h1 class="m-typo m-typo_primary">
  	    Strefa <br> Wiedzy
  	  </h1>
  	</div>
  	<div id="js-offerMother" class="b-posts_boxes is-loading">
  		<section class="b-four-boxes">
  		 <?php $children = get_pages(array('child_of' => 83, 'sort_column' => 'menu_order')); ?>
  			 <?php for($i = 0; $i <= 1; $i++) : ?>
  			<div class="m-posts-box">
  				<div class="m-posts-layer">
  					<h6 id="js-titleM<?php echo $i; ?>" class="m-typo m-typo_primary">
  						<?php if(count($children) > 3) : ?>
  							<?php echo $children[$i]->post_title; ?>
  						<?php endif; ?>
  					</h6>
  					<?php if(count($children) > 3) : ?>
  					<?php endif; ?>
  					<a id="js-hrefM<?php echo $i; ?>" href="<?php if(count($children) > 3) : ?><?php echo get_page_link($children[$i]->ID); ?><?php endif; ?>" class="m-btn m-btn_primary">Czytaj więcej</a>
  					<img class="m-posts-layer_icon" src="<?php echo THEME_URL; ?>/assets/images/features-icon<?php echo $i; ?>.png" alt="">
  				</div>
  			</div>
  		<?php endfor; ?>
  		</section>
		<a href="http://projektzdrowie.info/strefa-wiedzy/" target="_blank" class="m-btn m-btn_primary more">Zobacz więcej</a>
  	</div>
  </section>
  <?php endif; ?>
</div>
