<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
//TODO Change hardcoded post id for dynamic variable
?>

<?php if(get_the_ID() !=  52) : ?>
<section class="b-container b-posts">
	<div class="b-posts_title">
	  <h1 class="m-typo m-typo_primary">
	    Komu<br>pomagamy
	  </h1>
	</div>
	<div id="js-offer" class="b-posts_boxes">
		<section class="b-four-boxes">
		 <?php $children = get_pages(array('child_of' => 52, 'sort_column' => 'menu_order')); ?>
			 <?php for($i = 0; $i <= 3; $i++) : ?>
			<div class="m-posts-box">
				<div class="m-posts-layer">
					<h6 id="js-title<?php echo $i; ?>" class="m-typo m-typo_primary">
						<?php if(count($children) > 3) : ?>
							<?php echo $children[$i]->post_title; ?>
						<?php endif; ?>
					</h6>
					<?php if(count($children) > 3) : ?>
					<p class="m-typo m-typo_secondary">
						<?php the_field('zajawka', $children[$i]->ID); ?>
					</p>
					<?php endif; ?>
					<a id="js-href<?php echo $i; ?>" href="<?php if(count($children) > 3) : ?><?php echo get_page_link($children[$i]->ID); ?><?php endif; ?>" class="m-btn m-btn_primary">Czytaj więcej</a>
					<img class="m-posts-layer_icon" src="<?php echo THEME_URL; ?>/assets/images/features-icon<?php echo $i; ?>.png" alt="">
				</div>
			</div>
		<?php endfor; ?>

		</section>
	</div>
</section>
<?php endif; ?>
