<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<section id="js-pos" class="b-container b-pos">
	<div class="b-pos_title">
	  <h1 class="m-typo m-typo_primary">
	    Sprawdź  najbliższą lokalizację <br> Umów się na wizytę w swoim mieście
	  </h1>
	</div>
	<div class="m-pos_select-wrapper">
    <form method="get" action="http://projektzdrowie.info">
    <select id="js-regio" class="m-pos_select-box m-typo m-typo_secondary">
      <option disabled selected class="m-pos_select-item m-typo m-typo">wybierz województwo</option>
      <option value="podkarpackie" class="m-pos_select-item">Podkarpackie</option>
      <option value="malopolskie" class="m-pos_select-item">Małopolskie</option>
      <option value="mazowieckie" class="m-pos_select-item">Mazowieckie</option>
      <option value="slaskie" class="m-pos_select-item">Śląskie</option>
      <option value="lodzkie" class="m-pos_select-item">Łódzkie</option>
      <option value="pomorskie" class="m-pos_select-item">Pomorskie</option>
      <option value="wielkopolskie" class="m-pos_select-item">Wielkopolskie</option>
      <option value="kujawskie" class="m-pos_select-item">Kujawsko-Pomorskie</option>
      <option value="lubelskie" class="m-pos_select-item">Lubelskie</option>
      <option value="zachodniopomorskie" class="m-pos_select-item">Zachodniopomorskie</option>
    </select>
    <select id="js-city" name="s" class="m-pos_select-box m-typo m-typo_secondary">
      <option disabled selected class="m-pos_select-item">wybierz miasto</option>
    </select>
    <input type="hidden" value="pos" name="cpt">
    <button type="submit" class="m-btn m-btn_tertiary">Szukaj</button>
    </form>
	</div>
</section>
