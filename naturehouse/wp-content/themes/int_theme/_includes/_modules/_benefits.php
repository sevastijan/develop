<section class="b-benefits">
  <div class="b-benefits_title">
	  <h1 class="m-typo m-typo_primary">
	    Co zyskujesz
	  </h1>
	</div>
  <div class="m-benefits-box-wrapper">
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        możliwość uzupełnienia terapii <br> żywieniowej naturalną suplementacją <br>dopasowaną do etapu kuracji
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        stałe wsparcie oraz monitorowanie <br> efektów
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        profesjonalną opiekę <br> wykwalifikowanego dietetyka
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        praktyczną wiedzę na temat zdrowego <br> odżywiania
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        możliwość korzystania z produktów <br> sezonowych, ciekawe, proste i szybkie <br> pomysły na posiłki
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        praktyczną wiedzę na temat zdrowego <br> odżywiania
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        możliwość uzupełnienia terapii <br> żywieniowej naturalną suplementacją <br>dopasowaną do etapu kuracji
      </h1>
    </div>
  </div>
</section>
