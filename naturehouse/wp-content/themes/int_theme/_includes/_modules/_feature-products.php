<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php $active = false; ?>
<?php if($active == true) : ?>
<section class="b-container b-product">
	<div class="b-product_title">
	  <h1 class="m-typo m-typo_primary">
	    Nasze <br> produkty
	  </h1>
	</div>
	<div class="b-box_wrapper">
	  <div class="b-box">
	    <img src="<?php echo THEME_URL; ?>/assets/images/product.png" alt="product-image" class="product-img">
	  </div>
    <div class="two-boxes">
      <div class="b-box">
        <img src="<?php echo THEME_URL; ?>/assets/images/product-small.png" alt="product-image" class="product-img">
        <h6 class="m-typo m-typo_secondary">
          suplement diety
        </h6>
          <a href="#" class="m-btn m-btn_primary">Ujędrnianie</a>
      </div>
      <div class="b-box">
        <img src="<?php echo THEME_URL; ?>/assets/images/product-small.png" alt="product-image" class="product-img">
        <h6 class="m-typo m-typo_secondary">
          suplement diety
        </h6>
          <a href="#" class="m-btn m-btn_primary">Ujędrnianie</a>
      </div>
    </div>
	</div>
</section>
<?php endif; ?>