<section class="b-sidebar-product">
  <div class="box">
    <h1 class="m-typo m-typo_primary">Nasze produkty</h1>
    <img src="<?php echo THEME_URL; ?>/assets/images/product-small.png" alt="product-image" class="product-img">
      <a href="#" class="m-btn m-btn_primary">Zobacz całą naszą ofertę</a>
  </div>
</section>
