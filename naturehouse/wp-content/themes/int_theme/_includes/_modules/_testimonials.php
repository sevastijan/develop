<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<section class="b-container m-testimonials">
	<div id="js-testimonials_slider" class="m-testimonials_wrapper">
	<?php $posList = new WP_Query(array(
		  'post_type' => 'testimonials',
		));
	?>
	<?php if($posList->have_posts()) : while($posList->have_posts()) : $posList->the_post(); ?>

	  <div class="m-testimonials-item">
	    <div class="m-testimonials-itemDescribe">
	      <p class="m-typo m-typo_secondary">
	          <?php the_excerpt_max_charlength(400); ?>
	      </p>
	    </div>
	    <div class="m-testimonials-itemAuthor">
	      <h4 class="m-typo m-typo_primary">
         <?php the_field('imie_i_nazwisko')?> <br>
          <span><?php the_field('opis')?> </span>
	      </h4>
	    </div>
	  </div>
	<?php endwhile; endif; ?>
	</div>
  <div id="js-testimonials_navi" class="m-testimonials_navi"></div>
</section>
