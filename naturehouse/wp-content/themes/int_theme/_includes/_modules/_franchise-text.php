<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<section class="b-article-text b-article-text_franchise">
  <div class="b-article-text_title">

    <h1 class="m-typo m-typo_primary">
      <?php the_title(); ?>
    </h1>
  </div>
  <?php the_content(); ?>

<?php require_once(THEME_DIR . '_includes/_modules/_franchise-info.php'); ?>

	<p>&nbsp;</p>

  <h5>
  	Nasz doradca ds. franczyzy umówi się z Tobą na spotkanie w celu udzielenia <br>
	kompleksowych informacji o zasadach współpracy. <br>
	W sieci Projektem Zdrowie wykorzystasz pełnię swoich możliwości!
  </h5>

</section>
<img src="<?php echo THEME_URL; ?>/assets/images/franchise-bottom-img.png" class="franchise-bottom-img" alt="">
