<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<div class="m-menu m-menu_footer">
  <div class="b-columns-3 clearfix">
    <h3 class="m-typo m-typo_primary js-collapseTrigger">Mapa strony</h3>
    <div class="b-collapse js-collapse">
      <div class="b-columns-2 footer-menu">
        <?php custom_menu('footer_first','m-menu'); ?>
      </div>
      <div class="b-columns-2 footer-menu">
        <?php //custom_menu('footer_second','m-menu'); ?>
      </div>
    </div>
  </div>
</div>
