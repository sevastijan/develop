<section class="b-container b-knowledge-zone">
<?php if(get_field('knowledge_activity') || is_null(get_field('knowledge_activity')))  : ?>
	<?php require_once(THEME_DIR . '_includes/_modules/_posts.php'); ?>
<?php endif; ?>

<?php require_once(THEME_DIR . '_includes/_modules/_partners.php'); ?>

</section>
