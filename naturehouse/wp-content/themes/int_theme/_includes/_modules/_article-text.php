<section class="b-article-text">
  <div class="b-article-text_title">

  <?php $body_classes = get_body_class();?>
    <?php  if(in_array('page-template-default', $body_classes)) : ?>
      <a href="<?php echo esc_url( home_url( '/#js-pos' ) ); ?>" class="m-btn m-btn_tertiary">Umów się na wizytę</a>
    <?php else : ?>
      <a href="<?php echo esc_url( home_url( '/#js-contact' ) ); ?>" class="m-btn m-btn_tertiary">Umów się na wizytę</a>
  <?php endif; ?>
    
    <h1 class="m-typo m-typo_primary">
      <?php the_title(); ?>
    </h1>
  </div>
  <?php the_content(); ?>
  <?php if(get_field('mosaic_activity')) : ?>
    <?php require_once(THEME_DIR . '_includes/_modules/_mosaic.php'); ?>
  <?php endif; ?>
</section>
<?php if(get_field('testimonials_activity')) : ?>
	<?php require_once(THEME_DIR . '_includes/_modules/_testimonials.php'); ?>
<?php endif; ?>
