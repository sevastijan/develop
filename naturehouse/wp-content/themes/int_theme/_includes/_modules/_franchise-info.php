<section class="b-franchise-info">
  <div class="boxes-wrapper">
	  <div class="box">
		  <p>
			  Jak przystąpić do sieci <br> projekt zdrowie?
		  </p>
	  </div>
	  <div class="box">
		  <p>
			  Wyslij zgłoszenie na adres: <br>
			  <span>
			  	<a href="mailto:biuro@projektzdrowie.info">biuro@projektzdrowie.info</a>
			  </span>
		  </p>
		  <p>
			  lub zadzwoń: <br>
			  <span>
			  	tel. <a href="callto:+4842 237 30 30‬">42 237 30 30</a>
			  </span>
		  </p>
	  </div>
  </div>
</section>
