<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>


<section class="b-mosaic b-mosaic_contact">
  <div class="mosaic-boxes-wrapper">
    <div class="box">
      <h3 class="m-typo m-typo_primary">
        Adres siedziby:
      </h3>
      <p class="m-typo m-typo_secondary">
        Polskie Centra Dietetyczne Sp. z o.o. <br>
        Łódzka 57 <br>
        93-050 Konstantynów Łódzki
      </p>
    </div>
    <div class="box">
      <h3 class="m-typo m-typo_primary">
        Kontakt Centrala
      </h3>
      <p class="m-typo m-typo_secondary">
        tel: <a style="font-size: 18px;" href="callto:+4842 237 30 30">42 237 30 30‬</a> <br>
        e-mail: <a style="font-size: 18px;" href="mailto:biuro@projektzdrowie.info">biuro@projektzdrowie.info</a>
      </p>
    </div>
    <div class="box">
      <h3 class="m-typo m-typo_primary">
        DYREKTOR
      </h3>
      <p class="m-typo m-typo_secondary">
        Andrzej Gładysz<br>
       e-mail: <a style="font-size: 18px;" href="mailto:agladysz.pcd@gmail.com">agladysz.pcd@gmail.com</a>
      </p>
    </div>
    <div class="box">
      <h3 class="m-typo m-typo_primary">
        DZIAŁ FRANCZYZY
      </h3>
      <p class="m-typo m-typo_secondary">
        Region Polska północna <br>
        Artur Kwiatkowski <br>
        e-mail: <a style="font-size: 18px;" href="mailto:kwiatkowski.pcd@gmail.com">kwiatkowski.pcd@gmail.com</a> <br>
        tel: <a style="font-size: 18px;" href="callto:+48730 602 740">730 602 740</a>
      </p>
    </div>
    <div class="box">
      <h3 class="m-typo m-typo_primary">
      </h3>
      <p class="m-typo m-typo_secondary">
        Region Polska centralna <br>
        Marcin Leszczyński <br>
        e-mail: <a style="font-size: 18px;" href="mailto:mleszczynski.pcd@gmail.com">mleszczynski.pcd@gmail.com</a> <br>
        tel: <a style="font-size: 18px;" href="callto:+48605 199 464">605 199 464</a>
      </p>
    </div>
    <div class="box">
      <h3 class="m-typo m-typo_primary">
      </h3>
      <p class="m-typo m-typo_secondary">
        Region Polska południowa <br>
        Rafał Miller <br>
        e-mail: <a style="font-size: 18px;" href="mailto:rmiller.pcd@gmail.com">rmiller.pcd@gmail.com</a> <br>
        tel: <a style="font-size: 18px;" href="callto:+48692 009 333">692 009 333</a>
      </p>
    </div>
    <div class="box">
      <h3 class="m-typo m-typo_primary">
        DZIAŁ DIETETYKI I SZKOLEŃ
      </h3>
      <p class="m-typo m-typo_secondary">
        Joanna Sobczak <br>
        e-mail: <a style="font-size: 18px;" href="mailto:jsobczak.pcd@gmail.com">jsobczak.pcd@gmail.com</a> <br>
        tel: <a style="font-size: 18px;" href="callto:+48797 667 883">797 667 883</a>
      </p>
    </div>
  </div>
</section>
