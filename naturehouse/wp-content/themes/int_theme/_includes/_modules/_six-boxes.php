<section class="b-six-boxes">
  <div class="b-six-boxes_title">
	  <h1 class="m-typo m-typo_primary">
	    Cennik
	  </h1>
	</div>
  <div class="box_row">
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        Pakiet podstawowy:
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        wizyta u dietetyka, analiza składu ciała, tygodniowy plan żywieniowy
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        już od 79 zł
      </h1>
    </div>
  </div>
  <div class="box_row">
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        Pakiet rozszerzony:
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        wizyta u dietetyka, analiza składu ciała,  <br> tygodniowy plan żywieniowy, naturalna suplementacja
      </h1>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_secondary">
        już od 106 zł
      </h1>
    </div>
  </div>
</section>
