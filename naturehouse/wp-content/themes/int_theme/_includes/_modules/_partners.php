<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<section class="b-container b-partners">
  <div class="b-partners_title">
    <div class="m-partners-title-box">
      <h1 class="m-typo m-typo_secondary">
        Nasi partnerzy
      </h1>
    </div>
  </div>
	<div id="js-partners_slider" class="b-partners_wrapper">
    <div class="m-partners-box">
      <a href="http://www.euroimmundna.pl/" target="_blank">
        <img src="<?php echo THEME_URL; ?>/assets/images/eucomuna.png" alt="partner-logo" class="m-partners-box_logo">
      </a>
    </div>
    <div class="m-partners-box">
      <a href="https://www.pep.pl/" target="_blank">
        <img src="<?php echo THEME_URL; ?>/assets/images/polskieplatnosci.png" alt="partner-logo" class="m-partners-box_logo">
      </a>
    </div>
    <div class="m-partners-box">
      <a href="http://tylkosprobuj.com/" target="_blank">
        <img src="<?php echo THEME_URL; ?>/assets/images/tylkosproboj.png" alt="partner-logo" class="m-partners-box_logo">
      </a>
    </div>
	</div>
  <div id="js-partners_navi" class="m-partners_navi"></div>
</section>
