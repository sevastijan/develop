<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<div class="m-newsletter">
	<div class="b-columns-3">
		<h3 class="m-typo m-typo_primary js-collapseTrigger">Newsletter</h3>
		<div class="b-collapse js-collapse">
			<div class="m-typo m-typo_tertiary">
				Chcesz być na bieżąco? <br/> Zapisz się do mojego neweslettera </div>
			<?php echo do_shortcode('[newsletter_form form="1"]'); ?>
		</div>
	</div>
</div>
