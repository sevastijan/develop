<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>


<section class="b-mosaic">
  <div class="mosaic-boxes-wrapper">
    <?php $children = get_pages(array('child_of' => get_the_ID(), 'sort_column' => 'menu_order'));
          for($i = 0; $i <= count($children) - 1; $i++) : ?>
    <div class="box" style="background-image: url('<?php the_field('thumbnail', $children[$i]->ID); ?>')">
      <h3 class="m-typo m-typo_primary">
        <?php echo $children[$i]->post_title; ?>
      </h3>
      <p class="m-typo m-typo_secondary">
        <?php the_field('zajawka', $children[$i]->ID); ?>
      </p>
      <a href="<?php echo get_page_link($children[$i]->ID); ?>" class="m-btn m-btn_primary">Czytaj więcej</a>
    </div>
     <?php endfor; ?>
  </div>
</section>
