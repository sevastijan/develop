<section class="b-nav-arrows">
  <div class="arrows-wrapper">
    <?php $prev_post = get_previous_post(); if (!empty( $prev_post )): ?>
    	<a href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>" class="m-btn m-btn_tertiary">Poprzedni</a>
    <?php endif ?>
    <?php $next_post = get_next_post(); if (!empty( $next_post )): ?>
    	<a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>" class="m-btn m-btn_tertiary">Następny</a>
    <?php endif; ?>
  </div>
</section>