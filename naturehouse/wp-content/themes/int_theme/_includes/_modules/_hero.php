<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<section class="b-container b-hero">
	<div class="b-hero-wrapper_heading">
    <div class="b-hero-heading">
      <div class="heading-box">
        <a href="http://projektzdrowie.info/jak-dzialamy/">
          <h3 class="m-typo m-typo_primary heading-slide">
            <span>Sprawdzone Metody <br> Dietetyków</span>
          </h3>
        </a>
      </div>
      <div class="heading-box">
        <a href="/o-nas/">
          <h3 class="m-typo m-typo_primary heading-slide">
            <span>Nowa jakość <br> odchudzania</span>
          </h3>
        </a>
      </div>
      <div class="heading-box">
        <a href="http://projektzdrowie.info/komu-pomagamy/">
          <h3 class="m-typo m-typo_primary heading-slide">
            <span>Mały krok<br>do dużej zmiany</span>
          </h3>
        </a>
      </div>
    </div>
	</div>
  <div class="b-hero-wrapper_about">
    <div class="box-about">
      <div class="box-about_title">
        <h3 class="m-typo m-typo_primary">
          Projekt - Zdrowie <br> oto jest....
        </h3>
      </div>
      <div class="box-about_text">
        <p class="m-typo m-typo_secondary">
          <?php the_field('excerpt', 'option'); ?>
        </p>
        <a href="http://projektzdrowie.info/komu-pomagamy" class="m-btn m-btn_primary">Zobacz całą naszą ofertę</a>
      </div>
    </div>
  </div>
</section>
