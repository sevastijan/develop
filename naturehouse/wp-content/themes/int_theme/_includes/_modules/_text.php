<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
  <div class="m-text">
    <div class="b-columns-3">
      <h3 class="m-typo m-typo_primary js-collapseTrigger">Dostępność</h3>
      <div class="b-collapse js-collapse">
        <div class="m-typo m-typo_tertiary">
          <?php the_field('footer_last_column_text', 'options'); ?>
        </div>
      </div>
    </div>
  </div>
