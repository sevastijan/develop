<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php

$postsPerPage = get_query_var('cpt');
$search = get_query_var('s');

if($postsPerPage == 'pos')  :

$posList = new WP_Query(array(
  'post_type' => 'pos',
  's' => $search
));
$index = 0;

?>
<section class="b-four-boxes">
  <?php if($posList->have_posts()) : while($posList->have_posts()) : $posList->the_post(); ?>
  <div style="display: none !important;" class="js-api-url" data-url="<?php the_field('pos_url');?>"></div>
  <div class="boxes-row-wrapper">
    <div class="box">
      <div class="b-four-boxes_title">
        <h1 class="m-typo m-typo_primary">
          Nasze <br> Lokalizacje
        </h1>
      </div>
      <h1 class="m-typo m-typo_primary js-title">

      </h1>
      <?php if(get_field('pos_url')) : ?>
        <a href="http://<?php the_field('pos_url');?>"  class="m-btn m-btn_tertiary">Strona www</a>
      <?php endif; ?>
      <!-- <a href="#" class="m-btn m-btn_tertiary">Facebook</a> -->
      <div class="text-wrapper">
        <p class="m-typo m-typo_secondary ">
          Poniedziałek<span class="js-pn"></span>
        </p>
        <p class="m-typo m-typo_secondary ">
          Wtorek<span class="js-wt"></span>
        </p>
        <p class="m-typo m-typo_secondary ">
          Środa<span class="js-sr"></span>
        </p>
        <p class="m-typo m-typo_secondary ">
          Czwartek<span class="js-czw"></span>
        </p>
        <p class="m-typo m-typo_secondary">
           Piątek<span class="js-pt"></span>
        </p>
        <p class="m-typo m-typo_secondary ">
          Sobota<span class="js-sb"></span>
        </p>
      </div>
    </div>
    <div class="box">
      <h1 class="m-typo m-typo_primary js-address">
      </h1>
      <a class="js-tel-href phone" href="">
        <h1 class="m-typo m-typo_secondary js-tel">
        </h1>
      </a>
      <h1 class="m-typo m-typo_secondary js-mail">
      </h1>
      <div class="btn-wrapper">
        <p href="#" class="m-btn m-btn_tertiary js-fourBoxes_btn">Umów się na wizytę</p>
      </div>
    </div>
    <a href="http://<?php the_field('pos_url');?>/personel">
      <div class="box js-Stuff">
        <div class="btn-wrapper personel">
          <a href="http://<?php the_field('pos_url');?>/personel"  class="m-btn m-btn_tertiary m-btn_personel">Nasz personel</a>
        </div>
      </div>
    </a>
    <a href="#" class="js-MapLink">
      <div class="box js-Map">
        <div class="btn-wrapper personel">
          <a href="#"  class="m-btn m-btn_tertiary m-btn_personel js-MapLink">Zobacz na mapie</a>
        </div>
      </div>
    </a>
    <div class="box_contact">
      <h1 class="m-typo m-typo_primary">
        Zostaw nam swój numer telefonu, a my skontaktujemy się z Tobą aby umówić datę spotkania.
      </h1>
      <?php echo do_shortcode('[contact-form-7 id="167" title="Contact form 1" email-728="ss3303708@gmail.com"]'); ?>
      <p class="close">
      </p>
    </div>
  </div>

  <?php endwhile; endif; ?>
</section>
<?php endif; ?>
