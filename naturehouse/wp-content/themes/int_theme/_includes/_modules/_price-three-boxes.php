<section class="b-price-three-boxes">
  <div class="b-price-three-boxes_title">
    <h1 class="m-typo m-typo_primary">
      Cennik
    </h1>
  </div>
  <div class="box">
    <h6 class="m-typo m-typo_primary">
      Pakiet podstawowy
    </h6>
    <ul class="list">
      <li class="list-item m-typo m-typo_secondary">
        - wizyta u dietetyka
      </li>
      <li class="list-item m-typo m-typo_secondary">
        - analiza składu ciała
      </li>
      <li class="list-item m-typo m-typo_secondary">
        - tygodniowy plan żywieniowy
      </li>
    </ul>
    <div class="price-wrapper">
      już od <span class="price">79</span> <span class="price shadow">79</span>zł
    </div>
  </div>
  <div class="box">
    <h6 class="m-typo m-typo_primary">
      PAKIET rozszerzony
    </h6>
    <ul class="list">
      <li class="list-item m-typo m-typo_secondary">
        - wizyta u dietetyka
      </li>
      <li class="list-item m-typo m-typo_secondary">
        - analiza składu ciała
      </li>
      <li class="list-item m-typo m-typo_secondary">
        - tygodniowy plan żywieniowy
      </li>
      <li class="list-item m-typo m-typo_secondary">
        - naturalna suplementacja
      </li>
    </ul>
    <div class="price-wrapper">
      już od <span class="price">106</span> <span class="price shadow">106</span>zł
    </div>
  </div>
  <div class="box">
    <h6 class="m-typo m-typo_primary">
      pakiet dwutygodniowy
    </h6>
    <ul class="list">
      <li class="list-item m-typo m-typo_secondary">
        - wizyta u dietetyka
      </li>
      <li class="list-item m-typo m-typo_secondary">
        - analiza składu ciała
      </li>
      <li class="list-item m-typo m-typo_secondary">
        - dwutygodniowy plan żywieniowy
      </li>
    </ul>
    <div class="price-wrapper">
      już od <span class="price">139</span> <span class="price shadow">139</span>zł
    </div>
  </div>
</section>
