<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
$posList = new WP_Query(array(
  'post_type' => 'post',
  'posts_per_page' => 4
));

?>
  <section class="b-container b-features">
    <div class="b-features_boxes">
     <?php $i = 0; if($posList->have_posts()) : while($posList->have_posts()) : $posList->the_post(); ?>
      <div class="m-features-box" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
        <div class="m-features-layer">
            <h6 class="m-typo m-typo_primary">
              <?php the_title(); ?>
            </h6>
            <p class="m-typo m-typo_secondary">
              <?php the_excerpt_max_charlength(190); ?>
            </p>
            <?php if($i == 3) : ?>
              <a href="http://projektzdrowie.info/strefa-wiedzy" class="m-btn m-btn_primary">Zobacz całą naszą ofertę</a>
            <?php else : ?>
              <a href="<?php the_permalink(); ?>" class="m-btn m-btn_primary">Czytaj więcej</a>
            <?php endif; ?>
        </div>
      </div>
    <?php $i++; endwhile; endif; ?>
    </div>
  </section>
