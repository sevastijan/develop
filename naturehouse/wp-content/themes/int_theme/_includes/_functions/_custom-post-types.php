<?php
/**
 * Wordpress template created for "Projekt Zdrowie"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php

  add_action('init','custom_post_types');
  function custom_post_types(){
    
    /**
    *  Register pos 
    *
    */
    $pos = array(
      'labels' => array (
        'name' => 'POS',
        'sungular_name' => 'POS',
        'all_items' => 'Wszystkie',
        'add_new' => 'Dodaj',
        'add_new_item' => 'Dodaj',
        'edit_item' => 'Edytuj',
        'new_item' => 'Nowy',
        'view_item' => 'Zobacz',
        'search_items' => 'Szukaj',
        'not_found' => 'Nie znaleziono',
        'not_found_in_trash' => 'Nie znaleziono w koszu',
        'parent_item_colon' => ''
      ),
      'public' => true,
      'has_archive' => true,
      'public_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'pos'),
      'capatility_type' => 'post',
      'capabilities' => array(
        'edit_post'          => 'update_core',
        'read_post'          => 'update_core',
        'delete_post'        => 'update_core',
        'edit_posts'         => 'update_core',
        'edit_others_posts'  => 'update_core',
        'delete_posts'       => 'update_core',
        'publish_posts'      => 'update_core',
        'read_private_posts' => 'update_core'
      ),
      'hierarchical' => true,
      'menu_position' => 5,
      'supports' => array(
          'title', 'editor', 'author', 'thumbnail'
      ),
    );
    register_post_type('pos', $pos);
    
    /**
    *  Register testimonials 
    *
    */
    $testimonials = array(
      'labels' => array (
          'name' => 'Opinie',
          'sungular_name' => 'Opinie',
          'all_items' => 'Wszystkie',
          'add_new' => 'Dodaj',
          'add_new_item' => 'Dodaj',
          'edit_item' => 'Edytuj',
          'new_item' => 'Nowy',
          'view_item' => 'Zobacz',
          'search_items' => 'Szukaj',
          'not_found' => 'Nie znaleziono',
          'not_found_in_trash' => 'Nie znaleziono w koszu',
          'parent_item_colon' => ''
      ),
      'public' => true,
      'public_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'testimonials'),
      'capatility_type' => 'post',
      'hierarchical' => true,
      'menu_position' => 5,
      'supports' => array(
          'title', 'editor', 'author', 'thumbnail'
      ),
    );
    register_post_type('testimonials', $testimonials);


  }

  add_action('init','init_taxonomies');
  function init_taxonomies() {
    register_taxonomy(
      'state',
      array('pos'),
      array(
        'hierarchical' => true,
        'labels' => array(
          'name' => 'Województwo',
          'add_new_item' => 'Dodaj'
        ),
        'show_ui' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'state'),
        'show_in_rest' => true,
        'query_var' => true,
        'rest_base'  => 'state'
        )
    );
  }
?>
