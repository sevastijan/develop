<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
  <header class="b-container b-hero_header clearfix">
    <div class="b-hero_nav-wrapper">
      <div class="m-logo">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
          <img src="<?php echo THEME_URL; ?>/assets/images/brand-logo.png" class="logo-img" alt="">
        </a>
      </div>
      <div class="nav-icon">
        <a href="#js-pos">
          <img src="<?php echo THEME_URL; ?>/assets/images/calendar.png" alt="contact-icon" class="contact-icon">
          <p class="m-typo m-typo_text">
            Umów się na wizytę
          </p>
        </a>
      </div>
      <?php custom_menu('main_menu','m-menu'); ?>
      <div class="m-burger js-burger">
        <span class="m-burger_item"></span>
        <span class="m-burger_item"></span>
        <span class="m-burger_item"></span>
      </div>
    </div>
  </header>

<?php if(is_front_page()) : ?>

	<?php require_once(THEME_DIR . '_includes/_modules/_hero.php'); ?>

<?php endif; ?>
