<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>


<footer class="b-container b-footer clearfix">
	<?php require_once(THEME_DIR . '_includes/_modules/_menu-footer.php'); ?>
	<?php require_once(THEME_DIR . '_includes/_modules/_newsletter.php'); ?>
	<?php require_once(THEME_DIR . '_includes/_modules/_text.php'); ?>
</footer>