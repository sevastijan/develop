<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: Article
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); ?>



<div class="b-container">

  <?php require_once(THEME_DIR . '_includes/_modules/_breadcrumbs.php'); ?>


  <?php require_once(THEME_DIR . '_includes/_modules/_article-text.php'); ?>

<?php if(get_post_type(get_the_ID()) == 'post') : ?>
  <?php require_once(THEME_DIR . '_includes/_modules/_nav-arrows.php'); ?>
<?php endif; ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_knowledge-zone.php'); ?>


</div>



<?php get_footer(); ?>
