<?php
/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 29.09.2017
 *
 * Template Name: Search
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); the_post();?>



<div class="b-container">

  <?php require_once(THEME_DIR . '_includes/_modules/_breadcrumbs.php'); ?>

  <?php require_once(THEME_DIR . '_includes/_modules/_four-boxes.php'); ?>


</div>



<?php get_footer(); ?>
