/**
 * Wordpress template created for "Nature House"
 *
 * Version 1.0
 * Date: 26.09.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */



 $(document).ready(function(){

   //TODO: Spearate slider init function

   var $partnersSlider;

   $partnersSlider = $('#js-partners_slider');

   $partnersSlider.slick({
     infinite: true,
     slidesToShow: 3,
     slidesToScroll: 3,
     autoplay: false,
     appendArrows: '#js-partners_navi',
     prevArrow: '<div class="m-partners_prev"></div>',
     nextArrow: '<div class="m-partners_next"></div>',
     responsive: [
        {
          breakpoint: 1350,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 720,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
   });


   var $fourBoxesButton,
          $contactBox,
          $closeBtn;

   $fourBoxesButton = $('.js-fourBoxes_btn');
   $contactBox = $('.box_contact');
   $closeBtn = $('.close');

   $fourBoxesButton.click(function() {
     $contactBox.addClass('active');
   });

   $closeBtn.click(function() {
     $contactBox.removeClass('active');
   });




   var $headingWrapper;

   $headingWrapper = $('.heading-slide');

   $(function(){
     $headingWrapper.addClass('slide')
   });


   var $mobileBurger,
          $body;

   $mobileBurger = $('.js-burger');
   $body = $('body');

   $mobileBurger.click(function() {
     $body.toggleClass('is-open');
   });


   var $testimonialsSlider;

   $testimonialsSlider = $('#js-testimonials_slider');

   $testimonialsSlider.slick({
     infinite: true,
     slidesToShow: 1,
     slidesToScroll: 1,
     autoplay: true,
     vertical: true,
     autoplaySpeed: 8000,
     appendArrows: '#js-testimonials_navi',
     prevArrow: '<div class="m-testimonials_prev"></div>',
     nextArrow: '<div class="m-testimonials_next"></div>',
     responsive: [
        {
          breakpoint: 1024,
          settings: {
            vertical: false
          }
        }
      ]
   });

   var $collapseTrigger,
       $collapseContainer,
       $this,
       isOpenClass;

   $collapseTrigger = $('.js-collapseTrigger');
   $collapseContainer = $('.js-collapse');

   isOpenClass = 'is-open';

   $collapseTrigger.on('click', function(){
     var $this,
         $nextElement;

     $this = $(this);
     $nextElement = $($this.context.nextElementSibling);

     if(!$nextElement.hasClass(isOpenClass)) {
       $collapseContainer.removeClass(isOpenClass);
     }
   });

   // FIXED-MENU
   var num = 1,
       show = true;
   $(window).bind('scroll', function () {
       if ($(window).scrollTop() > num) {
          $('.b-hero_nav-wrapper, .m-menu, .m-logo, .m-burger, .nav-icon').addClass('reduced');
       } else {
          $('.b-hero_nav-wrapper, .m-menu, .m-logo, .m-burger, .nav-icon').removeClass('reduced');
       }
   });


    var $jsCity = $('#js-city');
    var $jsRegio = $('#js-regio');


$jsRegio.change(function(){
  $jsCity.html('<option disabled selected class="m-pos_select-item">wybierz miasto</option>');
   if($('#js-regio').val() === 'podkarpackie') {
      $jsCity.append('<option value="Sanok">Sanok</option><option value="Leżajsk">Leżajsk</option><option value="Rzeszów">Rzeszów</option><option value="Krosno">Krosno</option><option value="Jarosław">Jarosław</option><option value="Przemyśl">Przemyśl</option><option value="Mielec">Mielec</option><option value="Dębica">Dębica</option><option value="Ropczyce">Ropczyce</option>')
   } else if($('#js-regio').val() === 'slaskie') {
      $jsCity.append('<option value="Katowice">Katowice</option><option value="Cieszyn">Cieszyn</option>')
   } else if($('#js-regio').val() === 'mazowieckie') {
      $jsCity.append('<option value="Płock">Płock</option><option value="Pruszków">Pruszków</option><option value="Grodzisk Mazowiecki">Grodzisk Mazowiecki</option><option value="Sochaczew">Sochaczew</option><option value="Ostrów Mazowiecka">Ostrów Mazowiecka</option>')
   } else if($('#js-regio').val() === 'malopolskie') {
      $jsCity.append('<option value="Chrzanów">Chrzanów</option><option value="Nowy Sącz">Nowy Sącz</option><option value="Myślenice">Myślenice</option><option value="Limanowa">Limanowa</option><option value="Krynica Zdrój">Krynica Zdrój</option><option value="Wadowice">Wadowice</option><option value="Rabka Zdrój">Rabka Zdrój</option>')
   } else if($('#js-regio').val() === 'wielkopolskie') {
      $jsCity.append('<option value="Kalisz">Kalisz</option><option value="Konin">Konin</option>');
   } else if($('#js-regio').val() === 'pomorskie') {
       $jsCity.append('<option value="Wejherowo">Wejherowo</option><option value="Starogard Gdański">Starogard Gdański</option><option value="Pruszcz Gdański">Pruszcz Gdański</option>');
   } else if($('#js-regio').val() === 'kujawskie') {
      $jsCity.append('<option value="Lipno">Lipno</option><option value="Bydgoszcz Wyżyny">Bydgoszcz Wyżyny</option><option value="Bydgoszcz Gdańska">Bydgoszcz Gdańska</option><option value="Toruń">Toruń</option><option value="Grudziądz">Grudziądz</option>');
  } else if($('#js-regio').val() === 'lodzkie') {
      $jsCity.append('<option value="Łowicz">Łowicz</option>');
  } else if($('#js-regio').val() === 'lubelskie') {
      $jsCity.append('<option value="Świdnik">Świdnik</option><option value="Biała Podlaska">Biała Podlaska</option><option value="Lubartów">Lubartów</option><option value="Łęczna">Łęczna</option>');
  } else if($('#js-regio').val() === 'zachodniopomorskie') {
      $jsCity.append('<option value="Szczecin Jagiellońska">Szczecin Jagiellońska</option><option value="Szczecin Kazimierska">Szczecin Kazimierska</option>');
  }
})

  //TODO: separate burger function

  var $burger,
      $mobile;

  $burger = $('.js-burger');
  $mobile = $('#js-mobile');

  $burger.on('click', function(){
    $mobile.toggleClass('is-open');
  });

  if($(window).width() < 1024) {
    $('.m-menu_url').on('click', function(){
      $mobile.toggleClass('is-open');
    });
  }

  $( window ).resize(function() {
    if($(window).width() > 1024) {
      $mobile.removeClass('is-open');
    }
});
  //TODO: separate burger function

  var $collapseTrigger,
      $collapseContainer,
      $this,
      isOpenClass;

  $collapseTrigger = $('.js-collapseTrigger');
  $collapseContainer = $('.js-collapse');

  isOpenClass = 'is-open';

  $collapseTrigger.on('click', function(){
    var $this,
        $nextElement;

    $this = $(this);
    $nextElement = $($this.context.nextElementSibling);

    if(!$nextElement.hasClass(isOpenClass)) {
      $collapseContainer.removeClass(isOpenClass);
    }
    $nextElement.toggleClass(isOpenClass);
  });

  //TODO: SPEARATE AJAX FUNC
  var url = $('.js-api-url').data('url');

  if(url != undefined) {
    $.ajax({
      url: 'https://' + url + '/wp-json/acf/v3/options/options',
    }).done(function(response) {
      var data = response.acf;
      $('.js-title').html(data.name);
      $('.js-pn').html(data.pn);
      $('.js-wt').html(data.wt);
      $('.js-sr').html(data.sr);
      $('.js-czw').html(data.czw);
      $('.js-pt').html(data.pt);
      $('.js-sb').html(data.sb);
      $('.js-tel').html(data.telefon);
      $(".js-tel-href").attr('href', 'tel://+48' + data.telefon);
      $('.js-mail').html(data.email);
      $('.js-MailInput').val(data.email);
      $('.js-address').html(data.adres);
      $('.js-Stuff').css('background-image', 'url(' + data.photo_1.url + ')');
      $('.js-Map').css('background-image', 'url(' + data.photo_2.url + ')');
      $('.js-MapLink').attr('href', data.fb);
    });
  }

  $.ajax({
      url: 'https://projektzdrowie.info/wp-json/wp/v2/pages?parent=52&per_page=100&order=asc',
    }).done(function(response) {
      $('#js-title0').html(response[0].title.rendered);
      $('#js-title1').html(response[1].title.rendered);
      $('#js-title2').html(response[2].title.rendered);
      $('#js-title3').html(response[3].title.rendered);
      $('#js-href0').attr('href', response[0].link);
      $('#js-href1').attr('href', response[1].link);
      $('#js-href2').attr('href', response[2].link);
      $('#js-href3').attr('href', response[3].link);
    });

    if(typeof $('#js-offerMother') !== 'undefined') {
      $.ajax({
        url: 'https://projektzdrowie.info/wp-json/wp/v2/posts',
      }).done(function(response) {
        $('#js-offerMother').find('#js-titleM0').html(response[0].title.rendered);
        $('#js-offerMother').find('#js-titleM1').html(response[1].title.rendered);
        $('#js-offerMother').find('#js-hrefM0').attr('href', response[0].link);
        $('#js-offerMother').find('#js-hrefM1').attr('href', response[1].link);
        $('#js-offerMother').removeClass('is-loading');
      });
    }



 });
