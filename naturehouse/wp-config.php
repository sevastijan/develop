<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'fi1618_zdrowi');

/** MySQL database username */
define('DB_USER', 'fi1618_zdrowi');

/** MySQL database password */
define('DB_PASSWORD', 'u2VIe36F');

/** MySQL hostname */
define('DB_HOST', 'fi1618.pro');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'j<R6(!~zw>IloJj7^+3.o~j(yU,_VJRqTi_%lyW8_f!yS[A|6?ydW<,22A*wGLUS');
define('SECURE_AUTH_KEY',  '83pMv~g>_ o:at+JIQVJL]`KNe`1Q[`r/i!gw)o8_; L%eDf`8C_!wC4{nA6!3,0');
define('LOGGED_IN_KEY',    'yTa3qGC8o0i>LUsh.%g*6zd:MT*DI;r;#JWwySm5&UFH+V;A+j&J#lMg8cVQdc7Q');
define('NONCE_KEY',        '5Mq1UwI0NRq$l)5:>PY8&sff,=?QDPTiR767#VY~AT[D`s}+B?vJTSfd,{DP,cKb');
define('AUTH_SALT',        '=TN@Ror+/QWn:7c%<t;4z7d?3O _LUPTbQmL4|)ygpw$:;gzb9GTBnxY8}yS>aTs');
define('SECURE_AUTH_SALT', '}[h?o{mU?+,uv]`ETSru3!Y3 ~i9@fPDKWG$DN4v)Y|fXl!LTtm7:7nKhr)[f&r>');
define('LOGGED_IN_SALT',   'YnOQgWKmvCu0<r{^H?M@E}h#>SC?BlTurvtoqUe:oA&lU^dba4&$l~k3NJEojoj2');
define('NONCE_SALT',       'n?wHQ~1>~NacgWbw/S)6xnuEz:o}J).|=gM%#^;eef{h%t80YURr`_6%]hU8#Q5D');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tvvc4c4t3_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define( 'WP_ALLOW_MULTISITE', true );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
