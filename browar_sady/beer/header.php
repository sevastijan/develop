<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri() . '/css/style.min.css'?>" rel="stylesheet">
	  <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
	<header class="header">
		<div class="content-wrapper">
			<figure>
				<img class="header_img" src="<?php echo PB_THEME_URL; ?>/images/logo-top.png" alt="Browar Sady logo">
			</figure>
			<h1 class="typo typo_primary">
				Przedstawia:
			</h1>
			<div class="typo typo_text">
				<?php the_field('header_content', 'options') ?>
			</div>
		</div>
		<figure class="header-arrow">
			<img class="header-arrow_img" src="<?php echo PB_THEME_URL; ?>/images/arrow.png" alt="Arrow">
		</figure>
	</header>
