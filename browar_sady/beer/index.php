<?php get_header(); ?>

<?php if( have_rows('beers_modules', 'options') ):?>
	<?php while ( have_rows('beers_modules', 'options') ) : the_row();?>
		<section class="beer-module">
			<div class="content-wrapper">
				<div class="beer-module-presentation">
					<figure class="beer-module-presentation_name">
						<img class="beer-module-presentation_name-img" src="<?php the_sub_field('name', 'options') ?>" alt="beer name">
					</figure>
					<figure class="beer-module-presentation_bottle">
						<img class="beer-module-presentation_bottle-img" src="<?php the_sub_field('bottle', 'options') ?>" alt="beer bottle">
					</figure>
				</div>
				<div class="beer-module-descr">
					<div class="typo typo_text">
						<?php the_sub_field('descr', 'options') ?>
					</div>
				</div>
			</div>
			<div class="beer-module-ilustration" style="background-image:url('<?php the_sub_field('ilustration', 'options') ?>');">
				<div class="content-wrapper">
					<figure id="js-bottle-<?php the_row_index(); ?>" class="bottle-scene">
						<img id="img" data-depth=".35" class="beer-module-ilustration_bottle" src="<?php the_sub_field('bottle', 'options') ?>" alt="beer bottle">
					</figure>
				</div>
			</div>
		</section>
	<?php endwhile;?>
<?php endif;?>

<?php get_footer(); ?>
