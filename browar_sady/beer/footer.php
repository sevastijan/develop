
	<footer class="footer">
		<div class="content-wrapper">
			<div class="typo typo_text">
				<?php the_field('footer_content', 'options') ?>
			</div>
			<?php if( have_rows('footer_bottles', 'options') ):?>
				<ul class="footer-bottles">
					<?php while ( have_rows('footer_bottles', 'options') ) : the_row();?>
						<li>
							<figure class="footer-bottles_item">
								<img class="footer-bottles_item-img" src="<?php the_sub_field('bottle_img', 'options') ?>" alt="bottle">
							</figure>
						</li>
					<?php endwhile;?>
				</ul>
			<?php endif;?>
			<div class="footer-brand-info">
				<img class="footer-brand-info_logo" src="<?php echo PB_THEME_URL; ?>/images/logo-bottom.png" alt="Browar Sady logo">
				<p class="typo typo_text">
					Wyprodukowano przez: <br>
					Browar Sady sp. z o.o. sp. k. <br>
					ul. Logistyczna 21, 62-080 Sady. <br>
					<a href="www.browarsady.pl">www.browarsady.pl</a>
				</p>
			</div>
			<p class="typo typo_text author">
				Projekt i realizacjia <a href="https://pawelblonski.pl/" target="_blank" title="Rysownik i ilustrator Paweł Błoński - Studio grafiki i ilustracji">PawelBlonski.pl</a>
			</p>
		</div>
	</footer>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
  <script>
  <?php if( have_rows('beers_modules', 'options') ):?>
  		<?php while ( have_rows('beers_modules', 'options') ) : the_row();?>
  			const bootle_<?php the_row_index(); ?> = document.getElementById('js-bottle-<?php the_row_index(); ?>');
  			const parallaxInstance_<?php the_row_index(); ?> = new Parallax(bootle_<?php the_row_index(); ?>);
  		<?php endwhile;?>
  <?php endif;?>
  </script>
  </body>
</html>
