<?php
/**
 * Wordpress template created for "Otwarte Historie"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
 ?>
 <!doctype html>
 <html lang="pl">
 <head>
   <!-- Meta -->
   <meta charset="utf-8">
   <title>Wydawnictwo Otwarte Historie</title>
   <meta name="OH" content="OH">
   <meta name="Otwarte Historie" content="Otwarte Historie">
   <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
   <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

   <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/static/stylesheets/normalize.css">
   <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/static/stylesheets/main.css">
	 <?php if(is_page_template('template-about.php')) : ?>
		 	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/static/stylesheets/about.css">
	 <?php endif; ?>
	 <?php if(is_page_template('template-contact.php')) : ?>
		 	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/static/stylesheets/contact.css">
	 <?php endif; ?>
	 <?php if(is_page_template('template-buy.php')) : ?>
		 	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/static/stylesheets/buy.css">
	 <?php endif; ?>
	 <?php if(is_singular('books')) : ?>
		 	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/static/stylesheets/book.css">
	 <?php endif; ?>
 	 <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/style.css">

   <style>
   <?php
      $colors = get_field('tag', 'options');
      foreach ($colors as $color) : ?>
      .books__box-tag[data-new-taxonomy="<?php echo $color['tag']->term_id; ?>"]::before {
        background-color: <?php echo $color['kolor']; ?>
      }
      <?php endforeach; ?>
   </style>
	 <?php wp_head(); ?>

 </head>
 <body>
   <!-- Navigation -->
 <nav class="nav">
   <div class="nav__logo--wrapper">
     <a href="<?php echo esc_url(home_url( '/' )); ?>"><img src="<?php echo THEME_URL; ?>/static/img/logo.png" alt="" class="nav__logo"></a>
   </div>
   <ul>
     <li class="nav__item"><a href="<?php echo esc_url(home_url( '/' )); ?>" class="nav__link <?php if(  is_home() ) {echo 'nav__link--active';} ?> "><div class="nav__item--border"></div>Strona Główna</a></li>
     <li class="nav__item"><a href="<?php echo esc_url(home_url( '/o-nas' )); ?>" class="nav__link <?php if(  is_page(4) ) {echo 'nav__link--active';} ?>">O nas</a></li>
     <li class="nav__item"><a href="<?php echo esc_url(home_url( '/kontakt' )); ?>" class="nav__link <?php if(  is_page(6) ) {echo 'nav__link--active';} ?>">Kontakt</a></li>
     <li class="nav__item"><div class="nav__item--inner"><a href="<?php echo esc_url( home_url( '/dla-autorow' ) ); ?>" class="nav__link <?php if(  is_page(8) ) {echo 'nav__link--active';} ?>">Dla Autorów</a></div></li>
   </ul>
   <form action="?php echo esc_url(home_url( '/' )); ?>" class="search--wrapper">
   <input id="search-input" class="search-input" type="checkbox">
   <label for="search-input">
     <img src="<?php echo THEME_URL; ?>/static/img/search-icon--black.png" alt="" class="header__navigation-search">
   </label>
   <input class="inner" placeholder="wpisz tytuł lub autora" type="text" name="s"></input>
 </form>
 </nav>
 <div class="mobile">
 <div class="mobile--wrapper">
 <img src="<?php echo THEME_URL; ?>/static/img/mobile-logo.svg" alt="" class="mobile__logo">
 <div class="nav-toggle"><span></span></div>
   </div>
   <nav class="nav-mobile">
     <div>
     <ul class="nav-mobile__list">
       <div class="nav-mobile__list--wrapper">
       <li class="nav-mobile__item"><a class="nav-mobile__link nav-mobile__link--background" href="<?php echo esc_url(home_url( '/' )); ?>">Strona Główna</a></li>
       <li class="nav-mobile__item"><a class="nav-mobile__link nav-mobile__link--background" href="<?php echo esc_url(home_url( '/o-nas' )); ?>">O nas</a></li>
       <li class="nav-mobile__item"><a class="nav-mobile__link nav-mobile__link--background" href="<?php echo esc_url(home_url( '/kontakt' )); ?>">Kontakt</a></li>
       <li class="nav-mobile__item"><a class="nav-mobile__link nav-mobile__link--background" href="<?php echo esc_url(home_url( '/dla-autorow' )); ?>">Dla Autorów</a></li>
     </div>
      </ul>
      <ul class="nav-mobile__aside">
        <div class="nav-mobile__list-aside--wrapper">
          <h1 class="nav-mobile__aside-title"> Katalog Wydawniczy </h1>
        <li class="nav-mobile__aside-item"><a class="nav-mobile__aside-link nav-mobile__link--background" href="">Nowości</a></li>
        <li class="nav-mobile__aside-item"><a class="nav-mobile__aside-link nav-mobile__link--background" href="">monografie</a></li>
        <li class="nav-mobile__aside-item"><a class="nav-mobile__aside-link nav-mobile__link--background" href="">prace zbiorowe</a></li>
        <li class="nav-mobile__aside-item"><a class="nav-mobile__aside-link nav-mobile__link--background" href="">źródła</a></li>
        <li class="nav-mobile__aside-item"><a class="nav-mobile__aside-link nav-mobile__link--background" href="">czasopisma</a></li>
       </ul>
     </div>
   </nav>
 </div>
 </div>
