<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

  add_action('init','init_posttypes');

  function init_posttypes(){

      $books = array(
          'labels' => array (
              'name' => 'Ksiązki',
              'sungular_name' => 'Książki',
          ),
          'public' => true,
          'public_queryable' => true,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'books'),
          'capatility_type' => 'post',
          'hierarchical' => true,
          'menu_position' => 5,
          'supports' => array(
              'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields'
          ),
          'has_archive' => true,
          'show_in_rest' => true,
          'rest_base' => 'books',
      );
      register_post_type('books', $books);
  }

  add_action('init','init_taxonomies');
  function init_taxonomies() {
  	register_taxonomy(
  		'bookAuthor',
  		array('books'),
  		array(
  			'hierarchical' => true,
        'labels' => array(
          'name' => 'Autor',
          'add_new_item' => 'Dodaj autora'
        ),
  			'show_ui' => true,
  			'update_count_callback' => '_update_post_term_count',
  			'query_var' => true,
  			'rewrite' => array('slug' => 'bookAuthor'),
        'show_in_rest' => true,
        'query_var' => true,
        'rest_base'  => 'bookAuthor'
  			)
  	);
    register_taxonomy(
      'type',
      array('books'),
      array(
        'hierarchical' => true,
        'labels' => array(
          'name' => 'Typ',
          'add_new_item' => 'Dodaj typ'
        ),
        'show_ui' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array('slug' => 'typ'),
        'show_in_rest' => true,
        'rest_base'  => 'type'
        )
    );
  }

?>
