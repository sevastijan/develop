<?php
/**
 * Wordpress template created for "Pbi: Polskie Badania Internetu"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */

 /**
 *  Define root theme url's
 *
 */
 if (!defined('THEME_DIR')) {
		 define('THEME_DIR', get_theme_root().'/'.get_template());
 }
 if (!defined('THEME_URL')) {
		 define('THEME_URL', WP_CONTENT_URL.'/themes/'.get_template());
 }

 /**
 *  Import custom post types
 *
 */
 require_once(THEME_DIR.'/inc/mod-post-types.php');

 /**
 *  Thumbnail support
 *
 */
 add_theme_support('post-thumbnails', array('books'));

 /**
 *  Thumbnail sizes
 *
 */
 add_image_size('thumbnail-book-241', 241, 325);

 /**
 *  Thumbnail into REST API
 *
 */
 function get_thumbnail_url($post) {
     if(has_post_thumbnail($post['id'])) {
         $imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $post['id'] ), 'full' );
         $imgURL = $imgArray[0];

         return $imgURL;
     } else {
         return false;
     }
 }
 function insert_thumbnail_url() {
      register_rest_field( 'books',
                           'featured_image',
                            array(
                              'get_callback'    => 'get_thumbnail_url',
                              'update_callback' => null,
                              'schema'          => null,
                              )
                          );
      }
 add_action('rest_api_init', 'insert_thumbnail_url');

 /**
 *  Custom post excerpt
 *
 */
 function the_excerpt_max_charlength($charlength) {
     echo cutText(get_the_excerpt(), $charlength);
 }
 function cutText($text, $maxLength) {
     $maxLength++;
     $return = '';
     if (mb_strlen($text) > $maxLength) {
         $subex = mb_substr($text, 0, $maxLength - 5);
         $exwords = explode(' ', $subex);
         $excut = -(mb_strlen($exwords[count($exwords) - 1]));
         if ($excut < 0) {
             $return = mb_substr($subex, 0, $excut);
         } else {
             $return = $subex;
         }
         $return .= '...';
     } else {
         $return = $text;
     }
     return $return;
 }

 function custom_excerpt_length( $length ) {
        return 2000;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

 ?>
