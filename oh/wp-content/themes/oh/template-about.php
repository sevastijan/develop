<?php
/**
 * Wordpress template created for "Otwarte Historie"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 * Template Name: O nas
 *
 */
 ?>
 <?php get_header(); ?>
 <main class="main">
   <nav class="aside-nav">
     <?php $sideBookTypes = get_categories('taxonomy=type&type=book'); ?>
     <ul>
       <p class="aside-nav__title"> Katalog Wydawniczy </p>
       <?php foreach ($sideBookTypes as $sideBookType) : ?>
       <li class="aside-nav__item"><a href="<?php echo esc_url(home_url('?taxoType='. $sideBookType->term_id .'')); ?>" class="aside-nav__link"><?php echo $sideBookType->name; ?></a></li>
       <?php endforeach; ?>
       <li class="aside-nav__item"><a href="<?php echo esc_url(home_url()); ?>" class="aside-nav__link">Wszystkie</a></li>
     </ul>
   </nav>
   <div class="content">
     <div class="about">
       <div class="about__nav">
         <ul>
           <?php if( have_rows('section') ): while ( have_rows('section') ) : the_row(); ?>
             <li class="about__nav-item"><a href="#<?php the_sub_field('id'); ?>"><?php the_sub_field('title'); ?></a></li>
           <?php endwhile; endif;?>
         </ul>
       </div>

       <div class="about-content">
         <?php if( have_rows('section') ): while ( have_rows('section') ) : the_row(); ?>
           <h3 id="<?php the_sub_field('id'); ?>" class="about-content__title"><?php the_sub_field('title'); ?></h3>
           <div class="about-content__text"><?php the_sub_field('describe'); ?></div>
         <?php endwhile; endif;?>
       </p>
     </div>
   </div>
	 <?php get_footer(); ?>
