<?php
/**
 * Wordpress template created for "Otwarte Historie"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
 ?>

<?php get_header(); the_post(); ?>
<main class="main">
  <nav class="aside-nav">
    <?php $sideBookTypes = get_categories('taxonomy=type&type=book'); ?>
    <ul>
      <p class="aside-nav__title"> Katalog Wydawniczy </p>
      <?php foreach ($sideBookTypes as $sideBookType) : ?>
      <li class="aside-nav__item"><a href="<?php echo esc_url(home_url('?taxoType='. $sideBookType->term_id .'')); ?>" class="aside-nav__link"><?php echo $sideBookType->name; ?></a></li>
      <?php endforeach; ?>
      <li class="aside-nav__item"><a href="<?php echo esc_url(home_url()); ?>" class="aside-nav__link">Wszystkie</a></li>
    </ul>
  </nav>
  <div class="content">
    <div class="book">
      <h5 class="book__tag book__tag--mobile">
        <?php $types = get_the_terms( get_the_id(), 'type' ); ?>
        <span>
          <?php
            if(is_array($types)) {
              foreach($types as $type) {
                  echo $type->name;
              }
            }
          ?>
        </span>
      </h5>
      <div class="book-left">
  			<?php if(has_post_thumbnail()) : the_post_thumbnail('thumbnail-book-241', ['class' => 'book__image']); else :?>
  					<img src="<?php echo THEME_URL; ?>/static/img/book-default.png" alt="" class="book__image">
  			<?php	endif; ?>
      </div>
      <div class="book-right">
        <h5 class="book__tag">
          <span>
            <?php
              if(is_array($types)) {
                foreach($types as $type) {
                    echo $type->name;
                }
              }
            ?>
          </span>
        </h5>
        <h1 class="book__title"><?php the_title(); ?></h1>
          <?php $bookAuthors = get_the_terms( get_the_id(), 'bookAuthor' ); ?>
  	      <p class="book__author">
            <?php
              if(is_array($bookAuthors)) {
                foreach($bookAuthors as $bookAuthor) {
                    echo $bookAuthor->name;
                }
              }
            ?>
          </p>
        <div class="book-details">
          <?php
            $params = '?bookID=' . $post->ID . '&bookTitle=' . get_the_title() . '&taxType=' . $types[0]->name . '&taxAuth=' . $bookAuthors[0]->name . '';
          ?>

  				<?php if( have_rows('sources') ): while ( have_rows('sources') ) : the_row();


          ?>

  				<div class="book-details__row <?php if(get_sub_field('active') != TRUE) { echo 'book-details__row--disabled'; } ?>">
  					<p class="book-details__type"><?php the_sub_field('format'); ?></p>
  					<p class="book-details__isbn"><?php the_sub_field('isbn'); ?></p>
  					<p class="book-details__price"><?php the_sub_field('price'); ?></p>
  					<p class="book-details__buy <?php if(get_sub_field('active') != TRUE) { echo 'book-details__buy--inactive'; } ?>">
            <?php if(get_sub_field('active') != TRUE) : ?>
              <a href="#"><?php the_sub_field('label'); ?></a>
            <?php elseif(get_sub_field('linkTekst')) : ?>
              <a href="<?php the_sub_field('linkTekst') ?>"><?php the_sub_field('label'); ?></a>
            <?php else : ?>
              <a href="<?php echo esc_url(home_url( '/kup' . $params . '' )); ?>"><?php the_sub_field('label'); ?></a>
            <?php endif; ?>
            </p>
  				</div>



  			<?php endwhile; endif; ?>
        </div>
        <div class="book__read">Przeczytaj</div>
        <a class="book__share">Share</a>
      </div>
      <div class="book-tabs">
        <div class="tabs">
  				 <?php if(get_field('description')) : ?>
  	         <div class="tab">
  	             <input type="radio" id="tab-1" name="tab-group-1" checked>
  	             <label for="tab-1">opis</label>
  	             <div class="tab__content">
  								 <?php the_field('description'); ?>
  							 </div>
  	         </div>
  				 <?php endif; ?>
  				 <?php if(get_field('content')) : ?>
           <div class="tab">
               <input type="radio" id="tab-2" name="tab-group-1">
               <label for="tab-2">zawartość</label>
               <div class="tab__content">
                	<?php the_field('content'); ?>
  						  </div>
           </div>
  				 <?php endif; ?>
  				 <?php if(get_field('authors')) : ?>
  	          <div class="tab">
  	             <input type="radio" id="tab-3" name="tab-group-1">
  	             <label for="tab-3">Autorzy</label>
  	             <div class="tab__content">
  	                 <?php the_field('authors'); ?>
  								</div>
  	         </div>
  				 <?php endif; ?>
  				 <?php if(get_field('copyrights')) : ?>
  	         <div class="tab">
  	            <input type="radio" id="tab-4" name="tab-group-1">
  	            <label for="tab-4">Prawa Autorskie</label>
  	            <div class="tab__content">
  	                <?php the_field('copyrights'); ?>
  	            </div>
  	        </div>
  				<?php endif; ?>
          <div class="tab">
             <input type="radio" id="tab-5" name="tab-group-1">
             <label for="tab-5">Komentarze</label>
             <div class="tab__content">
              Disqus
             </div>
         </div>
        </div>
        <div class="tabs-mobile">
  				<?php if(get_field('description')) : ?>
          <div class="tabs-mobile__1">
            <h1 class="tabs-mobile__title">opis</h1>
            <p class="tabs-mobile__text"><?php the_field('description'); ?></p>
          </div>
  			<?php endif; ?>
  			<?php if(get_field('content')) : ?>
          <div class="tabs-mobile__2">
            <h1 class="tabs-mobile__title">Zawartość</h1>
            <p class="tabs-mobile__text"><?php the_field('content'); ?></p>
          </div>
  			<?php endif; ?>
  			<?php if(get_field('authors')) : ?>
          <div class="tabs-mobile__3">
            <h1 class="tabs-mobile__title">Autorzy</h1>
            <p class="tabs-mobile__text"><?php the_field('authors'); ?></p>
          </div>
  			<?php endif; ?>
  			<?php if(get_field('copyrights')) : ?>
          <div class="tabs-mobile__4">
            <h1 class="tabs-mobile__title">Prawa autorskie</h1>
            <p class="tabs-mobile__text"><?php the_field('copyrights'); ?></p>
          </div>
  			<?php endif; ?>
          <div class="tabs-mobile__5">
            <h1 class="tabs-mobile__title">Komentarze</h1>
            <p class="tabs-mobile__text">
  					</p>
          </div>
        </div>
      </div>
    </div>
  </div>
	<?php get_footer(); ?>
