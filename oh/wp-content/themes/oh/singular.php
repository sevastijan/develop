<?php
/**
 * Wordpress template created for "Otwarte Historie"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 *
 */
 ?>
 <?php get_header(); the_post() ?>
<main class="main">
  <nav class="aside-nav">
    <?php $sideBookTypes = get_categories('taxonomy=type&type=book'); ?>
    <ul>
      <p class="aside-nav__title"> Katalog Wydawniczy </p>
      <?php foreach ($sideBookTypes as $sideBookType) : ?>
      <li class="aside-nav__item"><a href="<?php echo esc_url(home_url('?taxoType='. $sideBookType->term_id .'')); ?>" class="aside-nav__link"><?php echo $sideBookType->name; ?></a></li>
      <?php endforeach; ?>
      <li class="aside-nav__item"><a href="<?php echo esc_url(home_url()); ?>" class="aside-nav__link">Wszystkie</a></li>
    </ul>
  </nav>
  <div class="content">
		<h1 class="contact-content__title"><?php the_title(); ?></h1>
    <?php the_content(); ?>
  </div>
	<?php get_footer(); ?>
