<?php
/**
 * Wordpress template created for "Otwarte Historie"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
 ?>

 <?php get_header(); ?>
 <main id="js-books__boxes" class="main" <?php if(isset($_GET['taxoType'])) : ?>data-display-taxo-type="<?php echo $_GET['taxoType']; ?>" <?php endif; ?>>
   <nav class="aside-nav">
     <ul>
       <p class="aside-nav__title"> Katalog Wydawniczy </p>
       <li v-for="taxonomy in bookTaxonomies" class="aside-nav__item">
         <a v-on:click="getBooksFromTaxonomy" :data-taxonomy="'type'" :data-new-taxonomy="taxonomy.id" :class="'aside-nav__link aside-taxonomy_' + taxonomy.id">{{taxonomy.name}}</a>
       </li>
       <li class="aside-nav__item">
         <a v-on:click="getBooksFromTaxonomy" :data-taxonomy="1" :data-new-taxonomy="1" class="aside-nav__link">wszystkie</a>
       </li>
     </ul>
   </nav>
   <?php
     $slider = new WP_Query(array(
       'post_type' => 'post',
       'posts_per_page' => -1,
     ));

     if($slider->have_posts() && !isset($_GET['s'])) : ?>
   <div id="js-slider" class="content">
    <?php while($slider->have_posts()) : $slider->the_post(); ?>
     <div class="news">
       <h5 class="news__tag news__tag--mobile">wydarzenia</h5>
       <div class="news__left">
         <img src="<?php echo THEME_URL; ?>/static/img/news-img.png" alt="" class="news__image">
       </div>
       <div class="news__right">
         <h5 class="news__tag">
           <?php
              $category = get_the_category();
              echo $category[0]->cat_name;
            ?>
         </h5>
         <div class="news__title--wrapper">
           <h1 class="news__title"><?php the_title(); ?></h1>
         </div>
         <p class="news__text" data-short="<?php the_excerpt_max_charlength(630); ?>" data-original="<?php the_excerpt_max_charlength(1700); ?>"><?php the_excerpt_max_charlength(630); ?></p>
         <div class="news__button--tablet"><a class="js-load-more-content" data-status="hide" href="<?php the_permalink(); ?>">Więcej</a></div>
         <div class="news__button--wrapper">
           <div class="news__button"><a class="js-load-more-content" data-status="hide" href="<?php the_permalink(); ?>">Więcej</a></div>
         </div>
       </div>
     </div>
     <?php endwhile; ?>
   </div>
   <?php else : ?>
     <div class="content">
       <h1 class="news__title">Wyniki wyszukiwania dla: <?php echo $_GET['s']; ?></h1>
     </div>
   <?php endif; ?>
   <div class="books">
     <div class="books__boxes">
       <div class="oh-loader" v-if="isLoading">
          <img src="<?php echo THEME_URL; ?>/static/img/loader.gif" alt="">
       </div>
       <div v-for="book in books" class="books__box">
           <img v-if="book.featured_image" :src="book.featured_image" alt="" class="books__box-image">
           <img v-else src="<?php echo THEME_URL; ?>/static/img/book-default.png" alt="" class="books__box-image">
           <div class="books__box-content--wrapper">
         <div class="books__box-title--wrapper">
           <h3 class="books__box-title"><a :href="book.link">{{book.title.rendered}}</a> </h3>
         </div>
         <div v-for="pure_taxonomy in book.pure_taxonomies.bookAuthor" class="books__box-author--wrapper">
           <div class="books__box-author">
             <a v-on:click="getBooksFromTaxonomy" :data-taxonomy="'bookAuthor'" :data-new-taxonomy="pure_taxonomy.term_id">{{pure_taxonomy.name}}</a>
           </div>
         </div>
           <div v-for="pure_taxonomy in book.pure_taxonomies.type" class="books__box-tag" :data-new-taxonomy="pure_taxonomy.term_id">
             <a v-on:click="getBooksFromTaxonomy" :data-taxonomy="'type'" :data-new-taxonomy="pure_taxonomy.term_id">{{pure_taxonomy.name}}</a>
           </div>
         </div>
       </div>
     </div>
   </div>
   <div class="pages">
     <ul>
       <li class="pages__arrow pages__arrow--left"><a v-on:click="getNewPage" :data-new-page="prevPage"></a></li>

       <li v-for="i in pages" class="pages__number ">
         <a v-if="currentPage == i" v-on:click="getNewPage" :data-new-page="i" class="pages__link pages__link--active">{{i}}</a>
         <a v-else v-on:click="getNewPage" :data-new-page="i" class="pages__link">{{i}}</a>
       </li>

       <li class="pages__arrow pages__arrow--right"><a v-on:click="getNewPage" :data-new-page="nextPage"></a></li>
     </ul>
   </div>
	 <?php get_footer(); ?>
