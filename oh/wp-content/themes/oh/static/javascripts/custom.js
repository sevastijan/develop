$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });


  /**
   *
   * SHOW MORE CONTENT SLIDER
   *
   */

   var showMoreContentSlider,
       $loadMoreContent;


   showMoreContentSlider  = function(status) {
     var $paragraph;

     $paragraph = $('.news__text');

     if(status === 'show') {
       $paragraph.substring(0,9999);
     } else {
       $paragraph.substring(0,100);
     }
   }

   $loadMoreContent = $('.js-load-more-content');

   $loadMoreContent.on('click', function(e){
     var status;

     e.preventDefault();
     status = $loadMoreContent.data('status');

     if(status === 'hide') {
       showMoreContentSlider('show');
       $loadMoreContent.data('status', 'show');
     } else {
       showMoreContentSlider();
       $loadMoreContent.data('status', 'hide');
     }
   });
});
