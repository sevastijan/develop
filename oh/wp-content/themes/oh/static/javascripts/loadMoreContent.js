$(document).ready(function(){

  /**
   *
   * SHOW MORE CONTENT SLIDER
   *
   */

   var showMoreContentSlider,
       $loadMoreContent;


   showMoreContentSlider  = function(status, $paragraph) {
     var newParagraph,
         oldParagraph;

     oldParagraph = $paragraph.data('original');
     shortParagraph = $paragraph.data('short');

     if(status === 'show') {
       newParagraph = oldParagraph;
     } else {
        newParagraph = shortParagraph;
     }

     $paragraph.text(newParagraph);
   }


   $loadMoreContent = $('.js-load-more-content');

   $loadMoreContent.on('click', function(e){
     var status,
         $paragraph;

     e.preventDefault();

    //  $paragraph = $(this).parent().parent().parent().find('.news__text');
     $paragraph = $('.news__text');
     status = $loadMoreContent.data('status');

     if(status === 'hide') {
       showMoreContentSlider('show', $paragraph);
       $loadMoreContent.data('status', 'show');

       $loadMoreContent.text('Zwiń');

       $('.slick-track').css('min-height', 630);
       $('.news__button--wrapper').css({'bottom': 'auto', 'margin-top': 50});
     } else {
       showMoreContentSlider('hide', $paragraph);
       $loadMoreContent.data('status', 'hide');
       $loadMoreContent.text('Więcej');

       $('.slick-track').css('min-height', 0);
       $('.news__button--wrapper').css('bottom', 0);
     }
   });
});
