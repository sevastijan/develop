var getBooks,
		getTaxonomies,
		books;


var app = new Vue({
  el: '#js-books__boxes',
  data: function(){
		return {
			bookTaxonomies: null,
			books: null,
			pages: null,
			currentPage: 1,
			nextPage: null,
			prevPage: null,
			isLoading: false,
			isActiveTaxonomy: null
		}
	},
	watch: {
    bookTaxonomies: function () {
      this.currentTaxonomy();
    }
  },
  methods: {
		initSlider: function() {
			$('#js-slider').slick({
		    infinite: false,
		    dots: true,
				customPaging: function(){
					return '<a></a>'
				},
		    speed: 3000,
				arrows: false,
		    autoplay: true
		  });
		},
		loadSlideContent: function() {
			$('.news__button a').on('click', function(e){
				 e.preventDefault();
			});
		},
		getNewPage: function(e) {
			var newPage;

			if(typeof e !== 'undefined') {
				newPage = e.srcElement.attributes['data-new-page'].value;
			} else {
				newPage = 1;
			}
			if(newPage == 0 || this.currentPage == this.pages && newPage == this.pages) {
				return;
			}

			this.currentPage = parseInt(newPage);
			this.getBooks(newPage);
		},

		getBooksTaxonomies: function() {
			var self;

			self = this;

			urlDomain = 'http://dev.oh.dkonto.pl/';
			restApi = 'wp-json/wp/v2/';
			urlParams = 'type';
			urlRequest = urlDomain + restApi + urlParams;

			axios({
					method: 'get',
					url: urlRequest
				})
				.then(function(response) {
						self.bookTaxonomies = response.data;
						setTimeout(function(){
                self.currentTaxonomy();
            }, 1);

				});

		},
		getBooksFromTaxonomy: function(e) {
			var newTaxonomy,
					taxonomy,
					target,
					targetParent;

			newTaxonomy = parseInt(e.srcElement.attributes['data-new-taxonomy'].value);
			taxonomy = e.srcElement.attributes['data-taxonomy'].value;
			target = e.target;
			targetParent = target.offsetParent;

			$('.aside-nav__item').removeClass('aside-nav__item--active');
			$(targetParent).addClass('aside-nav__item--active');

			$('.aside-nav__link').removeClass('aside-nav__link--active');
			$(target).addClass('aside-nav__link--active');


			// window.location.href = window.location.href.split("?")[0];

			$(this.$el).removeAttr('data-display-taxo-type');

			this.getBooks(1, newTaxonomy, taxonomy);
		},
		currentTaxonomy: function() {
			if(typeof this.$el.attributes['data-display-taxo-type'] != 'undefined') {
				var loadBookType,
						$taxoLink;

				loadBookType = parseInt(this.$el.attributes['data-display-taxo-type'].value);
				$taxoLink = $('.aside-taxonomy_'+ loadBookType);

				$taxoLink.addClass('aside-nav__link--active');
				$taxoLink.parent().addClass('aside-nav__item--active')

			}
		},
		getBooks: function(newPage, newTaxonomy, taxonomy) {

			var self,
					urlRequest,
					urlDomain,
					urlParams,
			 		restApi,
					booksRequest,
					currentPage,
					getNewRequest,
					loadBookType,
					nextPage,
					prevPage,
					currentPage,
					totalPages;

			self = this;

			if(newPage != undefined) {
				getNewRequest = newPage;
			} else {
				getNewRequest = 1;
			}
			if(typeof this.$el.attributes['data-display-taxo-type'] != 'undefined') {
				loadBookType = parseInt(this.$el.attributes['data-display-taxo-type'].value);
			}



			urlDomain = 'http://dev.oh.dkonto.pl/';
			restApi = 'wp-json/wp/v2/';

		 	if(typeof loadBookType === 'number') {
				urlParams = 'books?per_page=8&page=' + getNewRequest + '&type=' + loadBookType;
			} else if(typeof newTaxonomy == 'undefined' && typeof taxonomy == 'undefined') {
				urlParams = 'books?per_page=8&page=' + getNewRequest;
			} else {
				urlParams = 'books?per_page=8&page=' + getNewRequest + '&' + taxonomy + '=' + newTaxonomy;
			}

			urlRequest = urlDomain + restApi + urlParams;


			$('.books__box').fadeOut();
			$('.pages').fadeOut();
			this.isLoading = true;

			axios({
					method: 'get',
					url: urlRequest
				})
				.then(function(response) {
						self.books = response.data;
						self.pages = parseInt(response.headers['x-wp-totalpages']);
						self.isLoading = false;
						$('.books__box').fadeIn();
						$('.pages').fadeIn();
				});

				totalPages = this.pages;
				currentPage = this.currentPage;

				if(currentPage === 1) {
					this.prevPage = currentPage - 1;
					$('.pages__arrow--left').addClass('inactive');
				} else {
					$('.pages__arrow--left').removeClass('inactive');
					this.prevPage = currentPage - 1;
				}

				if(currentPage === totalPages) {
					this.nextPage = totalPages;
					$('.pages__arrow--right').addClass('inactive');
				} else {
					$('.pages__arrow--right').removeClass('inactive');
					this.nextPage = currentPage + 1;
				}

		}
  },
  mounted: function() {
		this.getBooksTaxonomies();
		this.getBooks();
		this.initSlider();
		this.loadSlideContent();
  }
});
