<?php
/**
 * Wordpress template created for "Otwarte Historie"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 * Template Name: Kontakt
 *
 */
 ?>
 <?php get_header(); ?>
<main class="main">
  <nav class="aside-nav">
    <?php $sideBookTypes = get_categories('taxonomy=type&type=book'); ?>
    <ul>
      <p class="aside-nav__title"> Katalog Wydawniczy </p>
      <?php foreach ($sideBookTypes as $sideBookType) : ?>
      <li class="aside-nav__item"><a href="<?php echo esc_url(home_url('?taxoType='. $sideBookType->term_id .'')); ?>" class="aside-nav__link"><?php echo $sideBookType->name; ?></a></li>
      <?php endforeach; ?>
      <li class="aside-nav__item"><a href="<?php echo esc_url(home_url()); ?>" class="aside-nav__link">Wszystkie</a></li>
    </ul>
  </nav>
  <div class="content">
    <div class="contact-content">
      <h1 class="contact-content__title">FUNDACJA NAUKOWA "OTWARTE HISTORIE"</h1>
      <p class="contact-content__paragraph">Krakowskie Przedmieście 26/28<br>
      00-927 Warszawa</p>
      <p class="contact-content__paragraph">Numer KRS 0000607184</p>
      <p class="contact-content__paragraph">NIP 525-265-15-09</p>
      <p class="contact-content__paragraph">REGON 36397575000000</p>
      <p class="contact-content__paragraph">Krakowskie Przedmieście 26/28<br>
      00-927 Warszawa</p>
    </div>
    <section id="map" class="map__img">
    </section>
  </div>
	<?php get_footer(); ?>
