<?php
/**
 * Wordpress template created for "Otwarte Historie"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 */
 ?>
 <div class="footer-main">
	 <div class= "footer">
		 <a href="https://pl.wikipedia.org/wiki/Otwarty_dost%C4%99p"><img src="<?php echo THEME_URL; ?>/static/img/open-access.png" alt="" class="footer__logo-access"></img></a>
		 <a href="https://creativecommons.org/"><img src="<?php echo THEME_URL; ?>/static/img/creative-commons.png" alt="" class="footer__logo-creative"></img></a>
	 </div>
	 <div class="footer-details">
		 <p class="footer-details__copyright">copyright otwarte historie 2016</p>
		 <p class="footer-details__cookies">polityka cookies</p>
		 <p class="footer-details__design"> designed by logotomia</p>
	 </div>
 </div>

</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
 <?php if(is_page_template('template-contact.php')) : ?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5q5791ItFXDw7GcHJHI9Asz7UMVYHqng"></script>
		<script>



    function initialize() {
			  var mapCanvas = document.getElementById('map');

			  var mapOptions = {
				  center: new google.maps.LatLng(52.239927, 21.017306),
				  zoom: 17,
				  styles:[
				    {
				        "featureType": "all",
				        "elementType": "labels.text.fill",
				        "stylers": [
				            {
				                "saturation": 36
				            },
				            {
				                "color": "#333333"
				            },
				            {
				                "lightness": 40
				            }
				        ]
				    },
				    {
				        "featureType": "all",
				        "elementType": "labels.text.stroke",
				        "stylers": [
				            {
				                "visibility": "on"
				            },
				            {
				                "color": "#ffffff"
				            },
				            {
				                "lightness": 16
				            }
				        ]
				    },
				    {
				        "featureType": "all",
				        "elementType": "labels.icon",
				        "stylers": [
				            {
				                "visibility": "off"
				            }
				        ]
				    },
				    {
				        "featureType": "administrative",
				        "elementType": "geometry.fill",
				        "stylers": [
				            {
				                "color": "#fefefe"
				            },
				            {
				                "lightness": 20
				            }
				        ]
				    },
				    {
				        "featureType": "administrative",
				        "elementType": "geometry.stroke",
				        "stylers": [
				            {
				                "color": "#ffffff"
				            },
				            {
				                "lightness": 17
				            },
				            {
				                "weight": 1.2
				            }
				        ]
				    },
				    {
				        "featureType": "administrative",
				        "elementType": "labels.text",
				        "stylers": [
				            {
				                "hue": "#ff0000"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "color": "#f5f5f5"
				            },
				            {
				                "lightness": 20
				            }
				        ]
				    },
				    {
				        "featureType": "landscape",
				        "elementType": "geometry.fill",
				        "stylers": [
				            {
				                "color": "#cdcdcd"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape",
				        "elementType": "geometry.stroke",
				        "stylers": [
				            {
				                "hue": "#ff0000"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape.man_made",
				        "elementType": "geometry.fill",
				        "stylers": [
				            {
				                "color": "#d8d8d8"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape.natural.landcover",
				        "elementType": "geometry.fill",
				        "stylers": [
				            {
				                "color": "#949494"
				            }
				        ]
				    },
				    {
				        "featureType": "landscape.natural.landcover",
				        "elementType": "geometry.stroke",
				        "stylers": [
				            {
				                "hue": "#ff0000"
				            }
				        ]
				    },
				    {
				        "featureType": "poi",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "color": "#f5f5f5"
				            },
				            {
				                "lightness": 21
				            }
				        ]
				    },
				    {
				        "featureType": "poi",
				        "elementType": "geometry.fill",
				        "stylers": [
				            {
				                "color": "#757575"
				            }
				        ]
				    },
				    {
				        "featureType": "poi.park",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "color": "#dedede"
				            },
				            {
				                "lightness": 21
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "geometry.fill",
				        "stylers": [
				            {
				                "hue": "#ff0000"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "geometry.stroke",
				        "stylers": [
				            {
				                "hue": "#ff0000"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "labels.text.fill",
				        "stylers": [
				            {
				                "color": "#0b0a0a"
				            }
				        ]
				    },
				    {
				        "featureType": "road",
				        "elementType": "labels.icon",
				        "stylers": [
				            {
				                "color": "#303030"
				            }
				        ]
				    },
				    {
				        "featureType": "road.highway",
				        "elementType": "geometry.fill",
				        "stylers": [
				            {
				                "color": "#ffffff"
				            },
				            {
				                "lightness": 17
				            }
				        ]
				    },
				    {
				        "featureType": "road.highway",
				        "elementType": "geometry.stroke",
				        "stylers": [
				            {
				                "color": "#ffffff"
				            },
				            {
				                "lightness": 29
				            },
				            {
				                "weight": 0.2
				            }
				        ]
				    },
				    {
				        "featureType": "road.arterial",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "color": "#ffffff"
				            },
				            {
				                "lightness": 18
				            }
				        ]
				    },
				    {
				        "featureType": "road.local",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "color": "#ffffff"
				            },
				            {
				                "lightness": 16
				            }
				        ]
				    },
				    {
				        "featureType": "transit",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "color": "#f2f2f2"
				            },
				            {
				                "lightness": 19
				            }
				        ]
				    },
				    {
				        "featureType": "transit",
				        "elementType": "geometry.stroke",
				        "stylers": [
				            {
				                "color": "#ffffff"
				            }
				        ]
				    },
				    {
				        "featureType": "water",
				        "elementType": "geometry",
				        "stylers": [
				            {
				                "color": "#e9e9e9"
				            },
				            {
				                "lightness": 17
				            }
				        ]
				    },
				    {
				        "featureType": "water",
				        "elementType": "geometry.fill",
				        "stylers": [
				            {
				                "color": "#808080"
				            }
				        ]
				    }
					] ,
				  mapTypeId: google.maps.MapTypeId.ROADMAP
				      }
				  var map = new google.maps.Map(mapCanvas, mapOptions)
				  var marker = new google.maps.Marker({
				  map:map,
				  title: 'Otwarte Historie',
				  position: new google.maps.LatLng(52.239927, 21.017306)
				});
	    }
	    google.maps.event.addDomListener(window, 'load', initialize)
	  </script>
 <?php endif; ?>
 <script src="<?php echo THEME_URL; ?>/static/javascripts/app.js"></script>
 <?php if(is_page_template('template-about.php')) : ?>
	 <script src="<?php echo THEME_URL; ?>/static/javascripts/custom.js"></script>
 <?php endif; ?>
 <script src="<?php echo THEME_URL; ?>/static/javascripts/loadMoreContent.js"></script>
 <script src="<?php echo THEME_URL; ?>/static/javascripts/appVue.js"></script>
 <?php wp_footer(); ?>
</body>
</html>
