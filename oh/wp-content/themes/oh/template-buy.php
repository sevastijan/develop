<?php
/**
 * Wordpress template created for "Otwarte Historie"
 * Code author: Jacek Jacek Grzegorczyk
 * Theme author: Sebastian Ślęczka
 *
 * The set of all functions available on the theme
 *
 * Version 0.0.1
 * Date: 12.01.2017
 *
 * @package WordPress
 *
 * Template Name: Kup
 *
 */
 ?>
 <?php get_header(); ?>
<main class="main">
  <!-- Side Navigation -->
	<nav class="aside-nav">
    <?php $sideBookTypes = get_categories('taxonomy=type&type=book'); ?>
    <ul>
      <p class="aside-nav__title"> Katalog Wydawniczy </p>
      <?php foreach ($sideBookTypes as $sideBookType) : ?>
      <li class="aside-nav__item"><a href="<?php echo esc_url(home_url('?taxoType='. $sideBookType->term_id .'')); ?>" class="aside-nav__link"><?php echo $sideBookType->name; ?></a></li>
      <?php endforeach; ?>
      <li class="aside-nav__item"><a href="<?php echo esc_url(home_url()); ?>" class="aside-nav__link">Wszystkie</a></li>
    </ul>
  </nav>
  <div class="content">
    <div class="buy-content">
      <h1 class="buy-content__tag"><?php echo $_GET['taxType']; ?></h1>
      <h5 class="buy-content__title"><?php echo $_GET['bookTitle']; ?></h5>
      <h3 class="buy-content__author"><?php echo $_GET['taxAuth']; ?></h3>
      <p class="buy-content__text"><?php the_field('description', $_GET['bookID']); ?></p>
      <div class="buy-content__form">
				<?php echo do_shortcode('[contact-form-7 id="46" title="Formularz 1"]'); ?>
			</div>
    </div>
  </div>
	<?php get_footer(); ?>
