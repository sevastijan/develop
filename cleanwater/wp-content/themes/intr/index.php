<?php
/**
 * Wordpress INTR THEME framework
 *
 * Version 1.0
 * Date: 06.11.2017
 *
 * @author Sebastian Ślęczka @ me[at]sevastijan[dot]com
 * @package WordPress
 * @subpackage Timber for INTR THEME
 *
 */

$context = Timber::get_context();
$context['posts'] = new Timber\PostQuery();
$templates = array( 'index.twig' );
if ( is_home() ) {
	array_unshift( $templates, 'home.twig' );
}
Timber::render( $templates, $context );

?>
