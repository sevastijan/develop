<?php
/**
 * Wordpress INTR THEME framework
 *
 * Version 1.0
 * Date: 06.11.2017
 *
 * @author Sebastian Ślęczka @ me[at]sevastijan[dot]com
 * @package WordPress
 * @subpackage Timber for INTR THEME 
 *
 */

Timber::$dirname = array('templates/views', 'templates/views');

class StarterSite extends TimberSite {
	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}
	function register_post_types() {
		//this is where you can register custom post types
	}
	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}
	function add_to_context( $context ) {
		$context['foo'] = WP_CONTENT_URL.'/themes/'.get_template().'/';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['menu'] = new TimberMenu('main_menu');
		$context['site'] = $this;
		return $context;
	}
	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		return $twig;
	}
}
new StarterSite();

/**
*  Register menus
*
*/
register_nav_menus(array(
  'main_menu' => 'Main menu',
  'footer_menu' => 'Footer menu'
));