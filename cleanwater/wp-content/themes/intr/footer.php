<?php
/**
 * Wordpress INTR THEME framework
 *
 * Version 1.0
 * Date: 06.11.2017
 *
 * @author Sebastian Ślęczka @ me[at]sevastijan[dot]com
 * @package WordPress
 * @subpackage Timber for INTR THEME
 *
 */


$timberContext = $GLOBALS['timberContext'];
if ( ! isset( $timberContext ) ) {
	throw new \Exception( 'Timber context not set in footer.' );
}
$timberContext['content'] = ob_get_contents();

ob_end_clean();

$templates = array( 'page-plugin.twig' );
Timber::render( $templates, $timberContext );

?>
<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://unpkg.com/vue@2.1.10/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="<?php echo THEME_URL; ?>/static/js/_modules/search.js"></script>
<script src="<?php echo THEME_URL; ?>/static/js/app.js"></script>
<?php wp_footer(); ?>
</body>
</html>
