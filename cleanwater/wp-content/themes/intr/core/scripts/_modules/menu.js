const menu = function() {
	const $menuTrigger = $('.js-burger');
	const $menuList = $('.js-menu_list');
	const $menuParent = $('.js-parent');

	if($(window).width() < 1024) {
		$menuTrigger.on('click', function(){
			$(this).toggleClass('is-active');
			$menuList.toggleClass('is-active');
		});
		$menuParent.on('click', function(e){
			e.preventDefault();

			if($(this).parent().find('.js-children_list').hasClass('is-active')) {
				$(this).parent().find('.js-children_list').removeClass('is-active');
				$(this).parent().removeClass('is-active');
			} else {
				$menuParent.parent().removeClass('is-active');
				$menuParent.parent().find('.js-children_list').removeClass('is-active');

				$(this).parent().find('.js-children_list').addClass('is-active');
				$(this).parent().addClass('is-active');
			}
		});

	}


}