const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: [
        './core/scripts/app.js',
        './app/scripts/app.js',
        './core/styles/style.scss'
    ],
    output: {
        path: `${__dirname}/static/js`,
        filename: 'scripts-[name].js'
    },
    watch: true,
    mode: "development",
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [["env", {
                            targets: {
                                browsers: ['> 1%']
                            }
                        }]]
                    }
                }
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            context: path.resolve(__dirname, "src/"),
                            outputPath: '../images/',
                            publicPath: '../',
                            useRelativePaths: true
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        // Specify output file name and path
        new ExtractTextPlugin({
            filename: '../styles/style.min.css'
        })
    ]
}