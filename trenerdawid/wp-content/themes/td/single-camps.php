<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Widok artykułu
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<div class="b-container subpage_fit-camp">
<div class="m-two-boxes heading fit-camp">
  <div class="boxes-wrapper">
    <div class="box">
      <div class="slider-wrapper">
        <div class="slider slider-single">
		  <?php if( have_rows('images_wrapper') ):?>
		      <?php while( have_rows('images_wrapper') ) : the_row(); ?>
				  <div class="slider-image" style="background-image: url('<?php the_sub_field('image'); ?>');"></div>
		      <?php endwhile;?>
		 <?php  endif;?>
        </div>
        <div class="slider slider-nav">
		  <?php if( have_rows('images_wrapper') ):?>
		      <?php while( have_rows('images_wrapper') ) : the_row(); ?>
				  <div class="slider-image" style="background-image: url('<?php the_sub_field('image'); ?>');"></div>
		      <?php endwhile;?>
		 <?php  endif;?>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="m-typo m-typo_text">
		  <?php the_field('fitcamp_description'); ?>
      </div>
    </div>
  </div>
</div>
<div class="m-tab-nav">
  <div class="wrapper">
    <ul class="list">
      <li class="js-campBtn-1 list_item m-typo m-typo_text show">
        Zakwaterowanie na wyspie
      </li>
      <li class="js-campBtn-2 list_item m-typo m-typo_text">
        Program wyjazdu
      </li>
      <li class="js-campBtn-3 list_item m-typo m-typo_text">
        Wycieczki fakultatywne
      </li>
      <li class="js-campBtn-4 list_item m-typo m-typo_text">
        Kwiestie organizacyjne
      </li>
      <li class="js-campBtn-5 list_item m-typo m-typo_text">
        Koszt wycieczki
      </li>
      <li class="js-campBtn-6 list_item m-typo m-typo_text">
        Podsumowanie
      </li>
    </ul>
  </div>
</div>
<div class="m-fit-tabs">
  <div class="wrapper">
	<p class="js-campBtn-1 m-typo m-typo_text mobile-btn show">
		Zakwaterowanie na wyspie
	</p>
    <div class="tab js-tab-1 show">
      <?php the_field('tab1_content'); ?>
    </div>
	<p class="js-campBtn-2 m-typo m-typo_text mobile-btn">
		Program wyjazdu
	</p>
    <div class="tab js-tab-2">
      <?php the_field('tab2_plan'); ?>
    </div>
	<p class="js-campBtn-3 m-typo m-typo_text mobile-btn">
		Wycieczki fakultatywne
	</p>
    <div class="tab js-tab-3">
      <?php the_field('tab3_optional'); ?>
    </div>
	<p class="js-campBtn-4 m-typo m-typo_text mobile-btn">
		Kwiestie organizacyjne
	</p>
    <div class="tab js-tab-4">
      <?php the_field('tab4_organization'); ?>
    </div>
	<p class="js-campBtn-5 m-typo m-typo_text mobile-btn">
		Koszt wycieczki
	</p>
    <div class="tab js-tab-5">


      <?php if( have_rows('tab5_table') ):?>
      <div class="m-table js-priceTable ticket">
        <div class="wrapper">
      	  <?php while( have_rows('tab5_table') ) : the_row();?>
      		  <ul class="table-list clearfix">
      		  	<li class="table-list_item m-typo m-typo_text">
      		  		Wylot <br>
      				<span><?php the_sub_field('wylot'); ?></span>
      		  	</li>
      		  	<li class="table-list_item m-typo m-typo_text">
      		  		Powrót<br>
      				<span><?php the_sub_field('powrot'); ?></span>
      		  	</li>
      		  	<li class="table-list_item m-typo m-typo_text">
      		  		Pobyt <br>
      				<span><?php the_sub_field('pobyt'); ?></span>
      		  	</li>
      		  	<li class="table-list_item m-typo m-typo_text">
      		  		Wyżywienie <br>
      				<span><?php the_sub_field('wyzywienie'); ?></span>
      		  	</li>
      		  	<li class="table-list_item m-typo m-typo_text">
      		  		Zakwaterowanie <br>
      				<span><?php the_sub_field('zakwaterowanie'); ?></span>
      		  	</li>
      		  	<li class="table-list_item m-typo m-typo_text">
      		  		Płatne w Polsce <br>
      				<span><?php the_sub_field('cena'); ?></span>
      		  	</li>
      		  	<li class="table-list_item m-typo m-typo_text">
      		  		Płatne na wyjeździe <br>
      				<span><?php the_sub_field('koszt_wycieczek'); ?></span>
      		  	</li>
      		  	<li class="table-list_item m-typo m-typo_text">
                <th class="m-typo m-typo_text"><a class="m-typo m-typo_text js-ask_form" href="<?php the_sub_field('link'); ?>">Zapisz <br> się </a></th>
      			</li>
      		  </ul>
      		<?php endwhile;?>
      <div class="wrapper">
      <table class="table w100">
      <tbody>
      <tr class="table_item bg-dbl">
      <th class="m-typo m-typo_text" colspan="5" style="background-color: #00345c; !important;">Ceny za szkolenia podane są w kwocie netto, w trakcie opłacania zamówienia automatycznie doliczony zostanie podatek VAT.</th>
      </tr>
      </tbody>
      </table>
      </div>
        </div>
      </div>


      <?php endif; ?>


      <?php the_field('tab5_cost'); ?>
    </div>
	<p class="js-campBtn-6 m-typo m-typo_text mobile-btn">
		Podsumowanie
	</p>
    <div class="tab js-tab-6">
      <?php the_field('tab6_summary'); ?>
    </div>
  </div>


  <?php if( have_rows('camp_table_wrapper', 'options') ):?>
  <div class="m-table js-priceTable ticket ticket_camp-info" style="border-bottom: 2px solid #b5b8ba; margin-bottom: 0; padding-bottom: 60px;">
    <h1 class="m-typo m-typo_secondary small-heading">Sprawdź wszystkie wyjazdy</h1>
    <div class="wrapper">
  	  <?php while( have_rows('camp_table_wrapper', 'options') ) : the_row(); ?>
  		  <ul class="table-list clearfix">
			<li class="table-list_item m-typo m-typo_text">
				Miejsce <br>
				<span><?php the_sub_field('miejsce'); ?></span>
			</li>
  		  	<li class="table-list_item m-typo m-typo_text">
  		  		Wylot <br>
  				<span><?php the_sub_field('wylot', 'options'); ?></span>
  		  	</li>
  		  	<li class="table-list_item m-typo m-typo_text">
  		  		Powrót<br>
  				<span><?php the_sub_field('powrot', 'options'); ?></span>
  		  	</li>
  		  	<li class="table-list_item m-typo m-typo_text">
  		  		Pobyt <br>
  				<span><?php the_sub_field('pobyt', 'options'); ?></span>
  		  	</li>
  		  	<li class="table-list_item m-typo m-typo_text">
  		  		Wyżywienie <br>
  				<span><?php the_sub_field('wyzywienie', 'options'); ?></span>
  		  	</li>
  		  	<li class="table-list_item m-typo m-typo_text">
  		  		Zakwaterowanie <br>
  				<span><?php the_sub_field('zakwaterowanie', 'options'); ?></span>
  		  	</li>
  		  	<li class="table-list_item m-typo m-typo_text">
  				<th class="m-typo m-typo_text">
					<a class="m-typo m-typo_text js-ask_form" href="<?php the_sub_field('link', 'options'); ?>">Wybierz »</a>
				</th>
  			</li>
  		  </ul>
  		<?php endwhile;?>
    </div>
  </div>
<?php endif; ?>

  <div class="cta-wrapper">
  <div class="cta_quaternary">
  <div class="box">
  <h1 class="m-typo m-typo_primary">Chętnie odpowiem na Twoje pytania, jeżeli cokolwiek jest niejasne</h1>
  <a class="m-btn m-btn_primary" href="/kontakt">Napisz do mnie</a>
  </div>
  </div>
  </div>
</div>
</div>
<?php get_footer(); ?>
