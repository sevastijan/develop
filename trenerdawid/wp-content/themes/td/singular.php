<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php 
	get_header(); the_post(); ?>
  <div class="b-container <?php if(is_singular() && !is_front_page()) { echo 'is-single'; } ?>">
    <?php the_content(); ?>
  </div>
<?php get_footer(); ?>
