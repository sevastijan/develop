<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title><?php bloginfo('name'); ?> » <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&amp;subset=latin-ext">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>assets/css/style.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>style.css">


  <?php
    /**
    *  Temporary vars
    *
    */

    $activateTopBar = true;
    $phoneNumber = '+&#052;&#056;&#055;&#050;&#056;&#053;&#050;&#050;&#055;&#050;&#056;';
    $emailAdress = '&#105;&#110;&#102;&#111;&#064;&#100;&#097;&#119;&#105;&#100;&#119;&#111;&#122;&#110;&#105;&#097;&#107;&#111;&#119;&#115;&#107;&#105;&#046;&#112;&#108;';
    if(is_page(59)) : ?>
      <script typse="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <?php endif; ?>
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.10&appId=1519239591719932';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <?php if($activateTopBar) : ?>
    <header class="b-header">
      <section class="b-container b-topbar clearfix">
        <div class="b-columns-4">
          <a href="tel://<?php echo $phoneNumber; ?>" class="m-typo m-typo_url is-phone"><?php echo $phoneNumber; ?></a>
        </div>
        <div class="b-columns-4">
          <a href="mail://<?php echo $emailAdress; ?>" class="m-typo m-typo_url is-email"><?php echo $emailAdress; ?></a>
        </div>
        <div class="b-columns-4">
          <a href="https://dawidwozniakowski.pl/kontakt/formularz/" class="m-typo m-typo_url is-form">Formularz konsultacyjny</a>
        </div>
        <div class="b-columns-4">
          <ul class="m-social">
            <?php //pll_the_languages(array('display_names_as'=>'slug'));?>
            <li class="m-social_item"><a href="https://www.instagram.com/dawidwozniakowski/"><img src="<?php echo THEME_URL; ?>assets/images/icons/in.png" alt="IN"></a></li>
            <!-- <li class="m-social_item"><a href="#"><img src="<?php echo THEME_URL; ?>assets/images/icons/yt.png" alt="YT"></a></li> -->
            <!-- <li class="m-social_item"><a href="#"><img src="<?php echo THEME_URL; ?>assets/images/icons/tw.png" alt="TW"></a></li> -->
            <li class="m-social_item"><a href="https://www.facebook.com/DawidWozniakowskiFanpage/"><img src="<?php echo THEME_URL; ?>assets/images/icons/fb.png" alt="FB"></a></li>
			<li class="m-social_item"><a href="https://dawidwozniakowski.pl/koszyk/"><img src="<?php echo THEME_URL; ?>assets/images/icons/cart.png" alt="cart"></a></li>
          </ul>
        </div>
      </section>
    </header>
  <?php endif; ?>
  <?php require_once(THEME_DIR.'_includes/_modules/_hero.php'); ?>
