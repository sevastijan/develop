<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Rekomendacje
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); the_post(); ?>

<div class="b-container subpage_recommendation">

  <?php $testimonial = new WP_Query(array(
    'post_type' => 'testimonials',
    'posts_per_page' => -1
  )); ?>

  <div class="m-two-boxes recommendations">
    <?php while($testimonial->have_posts()) : $testimonial->the_post(); ?>
      <div class="row <?php if(!get_field('photo')) : ?>other-type<?php endif; ?>">
        <div class="boxes-wrapper">
          <?php if(get_field('photo')) : ?>
            <div class="box">
              <div class="box_img" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/recom-bg.jpg);"></div>
            </div>
          <?php endif; ?>
          <div class="box">
            <div class="m-typo m-typo_text">
              <?php the_content(); ?>
              <?php if(get_field('author')) : ?>
                <span>~<?php the_field('author'); ?></span>
              <?php endif; ?>            
              <?php if(get_field('metamorphosis')) : ?>
                <a href="<?php the_field('metamorphosis'); ?>" class="m-btn m-btn_primary">Moja metamorfoza</a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
  </div>
  <?php the_content(); ?>
</div>



<?php get_footer(); ?>
