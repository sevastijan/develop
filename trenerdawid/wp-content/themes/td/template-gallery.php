<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Galeria
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>

<br>
<br>
<div class="b-container subpage_gallery">
  <?php the_content(); ?>
</div>
<br>
<br>





<?php get_footer(); ?>
