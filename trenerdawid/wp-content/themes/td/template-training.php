<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Szkolenia
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<div class="b-container subpage_training">

  <?php the_content(); 

  $camps = new WP_Query(array(
      'post_type' => 'training',
      'post_status' => 'publish',
      'posts_per_page' => 3
    ));
  if($camps->have_posts()) : ?>
  <div class="subpage_vacation">
   <div class="m-blog-posts">
     <div class="posts-wrapper">
      <?php  while($camps->have_posts()) : $camps->the_post(); ?>
     <div class="post">
         <a href="<?php the_permalink(); ?>">
           <div class="post_img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
         </a>
         <a href="<?php the_permalink(); ?>">
           <h1 class="m-typo m-typo_primary">
             <?php the_title(); ?>
           </h1>
          </a>
          <p class="m-typo m-typo_text">
            <?php the_excerpt_max_charlength(115); ?>            
           </p>
       </div>
      <?php endwhile; ?>
     </div>
  </div>
  </div>
  <?php endif; ?>

</div>

<?php get_footer(); ?>
