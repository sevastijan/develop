<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Page template
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<style>
	.subpage_editor::after {
		content: url(<?php the_post_thumbnail_url(); ?>) !important;
		top: -250px !important;
	}
</style>

<div class="b-container subpage_editor">

  <?php the_content(); ?>

</div>

<?php get_footer(); ?>
