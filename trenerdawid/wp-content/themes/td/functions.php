<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
/**
*  Define root theme url's
*
*/
if (!defined('THEME_DIR')) {
    define('THEME_DIR', get_theme_root().'/'.get_template().'/');
}
if (!defined('THEME_URL')) {
    define('THEME_URL', WP_CONTENT_URL.'/themes/'.get_template().'/');
}

/**
*  Register menus
*
*/
register_nav_menus(array(
  'main_menu' => 'Main menu',
  'footer_first' => 'Footer first menu',
  'footer_second' => 'Footer second menu'
));

/**
*  Theme thumbnails
*
*/
add_theme_support('post-thumbnails', array('post', 'trainers', 'packages', 'tips', 'camps'));

/**
*  Allow SVG Icons
*
*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
*  Register settings page
*
*/
if( function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title' 	=> 'Ustawienia',
		'menu_title'	=> 'Ustawienia',
		'menu_slug' 	=> 'module-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function max_title_length( $title ) {
	$max = 45;
	if( strlen( $title ) > $max ) {
		return substr( $title, 0, $max ). " &hellip;";
	} else {
		return $title;
	}
}


function SearchFilter($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}

add_filter('pre_get_posts','SearchFilter');

  /**
  *  Custom numeric pagination
  *
  */
  function the_numeric_pagination() {

  	if( is_singular() )
  		return;

  	global $wp_query;

  	if( $wp_query->max_num_pages <= 1 )
  		return;

  	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  	$max   = intval( $wp_query->max_num_pages );

  	if ( $paged >= 1 )
  		$links[] = $paged;

  	if ( $paged >= 3 ) {
  		$links[] = $paged - 1;
  		$links[] = $paged - 2;
  	}

  	if ( ( $paged + 2 ) <= $max ) {
  		$links[] = $paged + 2;
  		$links[] = $paged + 1;
  	}

  	echo '<div class="pagination-box"><ul class="pagination-ul">' . "\n";

  	if ( get_previous_posts_link() )
  		printf( '<li class="pagination-li">%s</li>' . "\n", get_previous_posts_link('<span class="label-prev arrow">Wstecz</span>') );

  	if ( ! in_array( 1, $links ) ) {
  		$class = 1 == $paged ? 'active' : '';

  		printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

  		if ( ! in_array( 2, $links ) )
  			echo '<li class="pagination-li">…</li>';
  	}

  	sort( $links );
  	foreach ( (array) $links as $link ) {
  		$class = $paged == $link ? 'active' : '';
  		printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  	}

  	if ( ! in_array( $max, $links ) ) {
  		if ( ! in_array( $max - 1, $links ) )
  			echo '<li class="pagination-li">…</li>' . "\n";

  		$class = $paged == $max ? 'active' : '';
  		printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  	}

  	if ( get_next_posts_link() )
  		printf( '<li class="pagination-li">%s</li>' . "\n", get_next_posts_link('<span class="label-next arrow">Kolejne</span>') );

  	echo '</ul></div>' . "\n";
  }

/**
*  Get tags
*
*/
    function wpb_tags() {
    $wpbtags =  get_tags();
    $string = '';
    foreach ($wpbtags as $tag) {
    $string .= '<span><a href="'. get_tag_link($tag->term_id) .'">'. $tag->name . '</a></span>' ."," . "\n"   ;
    }
    return $string;
    }
    add_shortcode('wpbtags' , 'wpb_tags' );

/**
*   WooCommerce Checkout Fields Hook
*
*/

add_action('init','prevent_access_to_product_page');
function prevent_access_to_product_page(){
    if ( is_product() ) {
        wp_redirect( site_url() );//will redirect to home page
    }
}

/**
*  Import custom post types
*
*/
require_once(THEME_DIR.'/_includes/_functions/_custom-post-types.php');

/**
*  Custom post excerpt
*
*/
require_once(THEME_DIR.'/_includes/_functions/_the-excerpt.php');

/**
*  Custom walker
*
*/
require_once(THEME_DIR.'/_includes/_functions/_custom-walker.php');

/**
*  Import VC shortcodes
*
*/
require_once(THEME_DIR.'/_includes/_functions/_vc-shortcodes.php');

/**
*  Import user profile custom fields function
*
*/
require_once(THEME_DIR.'/_includes/_functions/_user-profile-fields.php');

/**
*  Import user profile custom fields function
*
*/
require_once(THEME_DIR.'/_includes/_functions/_display-product-link.php');

add_filter( 'gform_file_permission', 'set_file_permission', 10, 2 );
function set_file_permission( $permission, $path ) {
    // Note the octal value with the leading zero. Don't return a string.
    return 0755;
}


?>
