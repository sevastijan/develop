<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Catering dietetyczny
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<div class="b-container subpage_catering">

  <?php the_content(); ?>

  <div class="row" style="float: none;">
    <div class="m-ten-boxes catering">
      <h1 class="m-typo m-typo_primary">Pakiety posiłków</h1>
    </div>
  </div>

  <?php if( have_rows('plan') ):  while ( have_rows('plan') ) : the_row(); ?>
    <div class="row">
      <div class="m-ten-boxes catering">
        <div class="row">
          <h3 class="m-typo m-typo_secondary">
            <?php the_sub_field('name'); ?>
          </h3>
          <p class="m-typo m-typo_text">
            <?php the_sub_field('Opis'); ?>
          </p>
          <ul class="list">
            <h3 class="m-typo m-typo_secondary">
              Przykładowe menu:
            </h3>
            <li class="list_item m-typo m-typo_text">
              Śniadanie:
            </li>
            <li class="list_item m-typo m-typo_text">
              <?php the_sub_field('sniadanie'); ?>
            </li>
            <li class="list_item m-typo m-typo_text">
              II Śniadanie:
            </li>
            <li class="list_item m-typo m-typo_text">
              <?php the_sub_field('sniadanie_second'); ?>
            </li>
            <li class="list_item m-typo m-typo_text">
              Obiad:
            </li>
            <li class="list_item m-typo m-typo_text">
              <?php the_sub_field('obiad'); ?>
            </li>
            <li class="list_item m-typo m-typo_text">
              Podwieczorek:
            </li>
            <li class="list_item m-typo m-typo_text">
              <?php the_sub_field('podwieczorek'); ?>
            </li>
            <li class="list_item m-typo m-typo_text">
              Kolacja:
            </li>
            <li class="list_item m-typo m-typo_text">
              <?php the_sub_field('kolacja'); ?>
            </li>
          </ul>
        </div>
      </div>
        <div class="post-wrapper">
          <?php if( have_rows('pakiety') ):  while ( have_rows('pakiety') ) : the_row(); ?>
          <div class="post">
            <div class="post_img"  style="background-image: url(<?php the_sub_field('miniatura'); ?>);"></div>
            <h1 class="m-typo m-typo_primary">
              <?php the_sub_field('tytul'); ?>
            </h1>
            <a href="#" class="js-ask_form m-btn m-btn_primary">Sprawdź ofertę</a>
          </div>
        <?php endwhile; endif; ?>
      </div>
    </div>
  <?php endwhile; endif; ?>
</div>
<br>
<br>





<?php get_footer(); ?>
