<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Konsultacje
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>

<div class="b-container subpage_consultation">

<div class="cta-wrapper">
  <div class="cta_quinary">
    <h1 class="m-typo m-typo_primary">
      W jaki sposób chcesz ze mną pracować?
    </h1>
    <p class="m-typo m-typo_text">
      Wybierz sposób współpracy, a następnie kliknij na przycisk "Sprawdź ofertę"
    </p>
    <div class="boxes-wrapper">
      <div class="box">
        <h6 class="m-typo m-typo_secondary">
          Warszawa
        </h6>
        <h3 class="m-typo m-typo_primary">
          Osobiście
        </h3>
        <p class="m-typo m-typo_text">
          Spotkaj się ze mną na żywo aby czerpać 100% korzyści z konsultacji.
        </p>
        <p id="consultationBtnOffline" class="m-btn m-btn_primary">Sprawdź ofertę</p>
      </div>
      <div class="box">
        <h6 class="m-typo m-typo_secondary">
          Online
        </h6>
        <h3 class="m-typo m-typo_primary">
          Zdalnie
        </h3>
        <p class="m-typo m-typo_text">
          Jeżeli spotkanie nie wchodzi w grę, istnieje możliwość współpracy na odległość.
        </p>
        <p id="consultationBtnOnline" class="m-btn m-btn_tertiary">Sprawdź ofertę</p>
      </div>
    </div>
  </div>
</div>

<div class="subboxes-wrapper">

    <?php if( have_rows('packages_locally') ): while ( have_rows('packages_locally') ) : the_row(); ?>
    <div class="subbox js-subBoxOffline">
      <h3 class="m-typo m-typo_primary">
        <?php the_sub_field('name'); ?>
      </h3>
      <p 
        class="m-typo m-typo_text js-toggleContent" 
        data-expanded="false" 
        data-short="" 
        data-full-text="<?php the_sub_field('descr_long'); ?>"
      >
        <?php the_sub_field('descr_short'); ?>
      </p>
      <?php if(get_sub_field('descr_long')) : ?>
        <p class="read-more js-readMoreButton">Rozwiń więcej</p>
      <?php endif; ?>
      <?php $consultationsIDS = get_sub_field('consultations'); ?>
      <?php if(count($consultationsIDS) > 1) : ?>
          <div class="price-wrapper">
        <?php endif; ?>
      
      <?php if( have_rows('consultations') ): while ( have_rows('consultations') ) : the_row(); ?>
        <?php 
          $productID = get_sub_field('productID'); 
          $product = wc_get_product( $productID );
        ?>
        <div class="price">
          <h6 class="m-typo m-typo_secondary">
            <?php the_sub_field('title');?> <br>
            <span><?php echo $product->get_price(); ?> zł</span>
          </h6>
          <a href="/koszyk/?add-to-cart=<?php echo $productID; ?>" class="m-btn m-btn_primary t-dni">Wybieram</a>
        </div>
      <?php endwhile; endif; ?>
      <?php if(count($consultationsIDS) > 1) : ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endwhile; endif; ?>

  <?php if( have_rows('packages_online') ): while ( have_rows('packages_online') ) : the_row(); ?>
    <div class="subbox js-subBoxOnline">
      <h3 class="m-typo m-typo_primary">
        <?php the_sub_field('name'); ?>
      </h3>
      <p 
        class="m-typo m-typo_text js-toggleContent" 
        data-expanded="false" 
        data-short="" 
        data-full-text="<?php the_sub_field('descr_long'); ?>"
      >
        <?php the_sub_field('descr_short'); ?>
      </p>
      <?php if(get_sub_field('descr_long')) : ?>
        <p class="read-more js-readMoreButton">Rozwiń więcej</p>
      <?php endif; ?>
      <?php $consultationsIDS = get_sub_field('consultations'); ?>
      <?php if(count($consultationsIDS) > 1) : ?>
          <div class="price-wrapper">
        <?php endif; ?>
      
      <?php if( have_rows('consultations') ): while ( have_rows('consultations') ) : the_row(); ?>
        <?php 
          $productID = get_sub_field('productID'); 
          $product = wc_get_product( $productID );
        ?>
        <div class="price">
          <h6 class="m-typo m-typo_secondary">
            <?php the_sub_field('title');?> <br>
            <span><?php echo $product->get_price(); ?> zł</span>
          </h6>
          <a href="/koszyk/?add-to-cart=<?php echo $productID; ?>" class="m-btn m-btn_primary t-dni">Wybieram</a>
        </div>
      <?php endwhile; endif; ?>
      <?php if(count($consultationsIDS) > 1) : ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endwhile; endif; ?>

</div>

</div>

<?php get_footer(); ?>
