<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

    <?php require_once(THEME_DIR.'_includes/_modules/_footer-columns.php'); ?>
    <div class="b-container b-footer-copyrights">
        all right reserved - Dawid Woźniakowski &copy; 2017
    </div>
    <?php require_once(THEME_DIR.'_includes/_modules/_mobile.php'); ?>
    <?php if(!is_page(2043)) : ?>
        <?php if(!is_page_template('template-page_template.php') && !is_page(2043)) : ?>
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
        <?php endif; ?>
    <?php endif; ?>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo THEME_URL; ?>/assets/js/app/app.js"></script>
    <?php wp_footer(); ?>
  </body>
</html>
