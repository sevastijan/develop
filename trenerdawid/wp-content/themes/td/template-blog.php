<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Blog
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>




<!-- <div class="b-container <?php if(is_singular() && !is_front_page()) { echo 'is-single'; } ?>">
  <?php the_content(); ?>
</div> -->
<br>
<br>
<div class="b-container subpage_blog">
   <div class="m-three-boxes">
     <div class="boxes-wrapper">
      <?php
        $index = 0;
        $posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => -1
        )); ?>
        <?php while ( $posts->have_posts() ) : $posts->the_post();
              $category_detail = get_the_category($post->ID); ?>
        <?php
          if($index in_array(0, 1, 2)) :
            if($index == 2) {
              $excerptLength = 106;
            } else {
              $excerptLength = 440;
            }
        ?>
         <div class="subbox">
           <div class="box_img"  style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
           <h1 class="m-typo m-typo_primary">
             <?php the_title(); ?>
           </h1>
           <p class="m-typo m-typo_text">
             <?php the_excerpt($excerptLength); ?>
           </p>
           <div class="date">
            <?php echo $category_detail[0]->name; ?> | <?php echo get_the_date('F j, Y'); ?>
           </div>
         </div>
        <?php endif; ?>
      <?php if($index == 2) : ?>
           </div>
         </div>
         <div class="cta-wrapper">
           <div class="cta_senary">
             <div class="box">
               <h1 class="m-typo m-typo_primary">
                 Przykładowy nagłówek <br> zachęcający do wyjazdów
               </h1>
             </div>
           </div>
         </div>
         <div class="m-blog-posts">
           <div class="posts-wrapper">
       <?php endif; ?>

         <div class="post">
           <div class="post_img"  style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
           <h1 class="m-typo m-typo_primary">
             <?php the_title(); ?>
           </h1>
           <p class="m-typo m-typo_text">
             <?php the_excerpt(106); ?>
           </p>
           <div class="date">
             <?php echo $category_detail[0]->name; ?> | <?php echo get_the_date('F j, Y'); ?>
           </div>
         </div>

        <?php if($index == 7) : ?>
           <div class="post post_graphic">
             <div class="post_img"  style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-post-img.jpg);"></div>
           </div>
        <?php endif; ?>
      <?php endwhile; ?>
     </div>
   </div>

   <div class="m-blog-info">
     <p class="m-typo m-typo_text">
       <?php the_field('blog_describe'); ?>
     </p>
   </div>
</div>

</div>





<?php get_footer(); ?>
