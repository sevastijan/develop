<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Personel - Martyna
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>

<br>  <br>
<div class="b-container subpage_personel martyna">

<p class="m-typo m-typo_text">
  Nazywam się Martyna Wójcik i jestem certyfikowanym Trenerem Personalnym. Ukończyłam studia na kierunku Dietetyka, Wychowanie Fizyczne oraz Kosmetologia. Pracuję również jako Instruktor Fitness, jednak głównie zajmuję się indywidualną opieką trenerską i dietetyczną swoich podopiecznych.
  <br><br>
	Większość trenerów, mówiąc o sobie, zaczyna słowami: „ze sportem byłem związany od dziecka”. W moim przypadku było zupełnie inaczej - nie lubiłam żadnej aktywności fizycznej, unikałam lekcji wychowania fizycznego i starałam się organizować zwolnienia na pozorowane, rozmaite dolegliwości, aby tylko nie uczestniczyć w lekcjach. Dlatego też, jak nikt inny rozumiem osoby, które chcą zmienić swoją sylwetkę, a nie koniecznie przekonały się jeszcze do aktywności fizycznej. Odkąd pamiętam fascynowałam się za to fizjologią człowieka. W planach były takie studia jak medycyna, psychologia, kognitywistyka, ostatecznie podjęłam decyzje – zostanę kosmetologiem.
  <br><br>
	Studia bardzo szybko zweryfikowały, że praca w tym zawodzie nie jest dla mnie. Jako że jestem osobą obowiązkową postanowiłam je ukończyć, a będąc na 3 roku podjęłam naukę na drugim kierunku. Skąd pomysł na kierunek Wychowanie Fizyczne? Chciałam od podszewki poznać funkcjonowanie ludzkiego organizmu z uwzględnieniem własnej aktywności fizycznej. Okazało się, że to był strzał w dziesiątkę! Fizjologia wysiłku fizycznego, biochemia, biomechanika, profilaktyka cardio-ortoped-inter to były przedmioty, z których poszerzałam swój zakres wiedzy znacznie poza program. Stopniowo sport wdrażałam również w swoje życie, a na swoją specjalizację wybrałam lekkoatletykę. Uznałam, że każda dyscyplina sportowa posiada jej elementy w swej specyfice. W końcu, mając świadomość funkcjonowania ludzkiego ciała, pokochałam sport, szczególnie trening siłowy i funkcjonalny.
  <br><br>
	Wiedząc również, że istotniejsza dla zdrowia jest profilaktyka niż leczenie, zainteresowałam się tematyką odżywiania. Wtrakcie studiów Wychowania Fizycznego zapragnęłam zostać dietetykiem. Przez lata łączyłam studia dzienne, zaoczne i pracę, międzyczasie zostałam trenerem personalnym i sędzią kettlebell federacji Girievoy Sport.
  <br><br>
	Od tej pory wiedziałam już czym chcę się zajmować. Wiedza zdobyta na studiach pozwala mi na holistyczne podejście do człowieka, rozumiem jak działa aparat ruchu i jak zoptymalizować trening dla konkretnej osoby, by przyniósł oczekiwane efekty. Nieustannie poszerzam swoją wiedzę, biorąc udział w konferencjach naukowych i szkoleniach z zakresu dietetyki i fitness oraz czytając najnowsze publikacje medyczne. Stosując sprawdzone metody Dawida, gwarantuję równie spektakularne efekty.
  <br><br>
	Jeśli chcesz poprawić jakość swojego życia, zmienić sylwetkę, poczuć się lepiej we własnej skórze, napisz do mnie. Możesz liczyć na pełen profesjonalizm i wsparcie.
</p>
<br><br>
<br class="visible-desktop"><br class="visible-desktop">
<br class="visible-desktop"><br class="visible-desktop">


<div class="cta-wrapper">
  <div class="cta_secondary">
    <h3 class="m-typo m-typo_primary">
      <span>Zdobądź się na pierwszy krok.</span> Później będzie tylko łatwiej!
    </h3>
    <a href="/personel-cennik-martyna/" class="m-btn m-btn_primary">Zobacz cennik</a>
  </div>
</div>

</div>
<br>
<br>





<?php get_footer(); ?>
