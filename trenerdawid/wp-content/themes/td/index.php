 <?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Blog
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php get_header('white'); ?>

<br>
<br>
<div class="page-template-template-personal">
<div class="b-container subpage_blog">
  <div class="m-top-bar clearfix">
    <div class="boxes-wrapper">
      <div class="box">
        <form class="searchbox" action="<?php echo home_url( '/' ); ?>">
          <input class="search m-typo m-typo_text" type="text" name="s" value="<?php if(isset($_GET['s'])) { echo $_GET['s']; } ?>" placeholder="Wyszukaj..">
          <input class="submit" type="submit" value="">
        </form>
        <!-- <form class="select" action="">
          <select class="category-select m-typo m-typo_text js-sortPosts">
            <option disabled="" selected="" class="category-select_item m-typo_text">Sortuj</option>
            <option class="category-select_item m-typo_text">Od najnowszego</option>
            <option class="category-select_item m-typo_text">Od Najstarszego</option>
          </select>
        </form> -->
      </div>
      <?php
        $categoryList = get_categories();
        $categories = get_the_category();
      ?>
      <?php if(!is_search()) : ?>
      <div class="box">
        <ul class="category-list">
          <li class="category-list_item <?php if(!is_category()) { echo 'active'; } ?>">
            <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
              Wszystkie
            </a>
          </li>
          <?php for($i = 1; $i <= count($categoryList) - 1; $i++) : ?>
            <li class="category-list_item <?php if(($categoryList[$i]->term_id == $categories[0]->cat_ID) && is_category()) { echo 'active';} ?>">
              <a href="<?php echo esc_url(get_category_link($categoryList[$i]->cat_ID)); ?>">
                <?php echo $categoryList[$i]->name;?>
              </a>
            </li>
          <?php endfor; ?>
        </ul>
      </div>
      <?php endif; ?>
    </div>
  </div>
  <?php if(!is_search()) : ?>
   <div class="m-three-boxes">
     <div class="boxes-wrapper">
     <?php
        endif;
        $index = 0;
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        while ( have_posts() ) : the_post(); $category_detail = get_the_category($post->ID);
          if(in_array($index, array(0, 1, 2)) && $paged == 1 && !is_search() ) :
            if($index == 2) {
	          $excerptLength = 440;
            } else {
              $excerptLength = 160;
            }
        ?>
         <div class="subbox">
            <a href="<?php the_permalink(); ?>">
           		<div class="box_img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
    		    </a>
		    	<a href="<?php the_permalink(); ?>">
           <h1 class="m-typo m-typo_primary">
            	<?php the_title(); ?>
           </h1>
           </a>
           <div class="m-typo m-typo_text">
              <?php if (has_excerpt()) {
                the_excerpt();
              } else {
                the_excerpt_max_charlength($excerptLength);
              } ?>
           </div>
           <div class="date">
            <?php echo $category_detail[0]->name; ?> | <?php echo get_the_date('F j, Y'); ?>
           </div>
         </div>
      <?php endif; ?>
      <?php if($index == 3 && $paged == 1 && !is_search()) : ?>
           </div>
           </div>
         </div>
         <div class="cta-wrapper">
           <div class="cta_senary">
             <div class="box">
               <h1 class="m-typo m-typo_primary">
                 Ezgotyczne wakacje, codzienne treningi,<br>
                 autorskie wycieczki, <br>własny kucharz i wiele więcej...
                 <div class="clearfix pt20"> <a href="/oferta/fit-wczasy/" class="m-btn m-btn_secondary">Zobacz ofertę fit wczasów!</a> </div>

               </h1>

             </div>
           </div>
         </div>
         <div class="b-container subpage_blog">
         <div class="m-blog-posts">
           <div class="posts-wrapper">
       <?php elseif( ($index == 0 && $paged != 1) || (is_search() && $index == 0) ) : ?>
       		<div class="m-blog-posts">
           <div class="posts-wrapper">
       <?php endif; ?>
       <?php if(!(in_array($index, array(0, 1, 2)) && $paged == 1) || is_search()) : ?>
        <div class="post">
         	<a href="<?php the_permalink(); ?>">
    				<div class="post_img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
    			</a>
    			<a href="<?php the_permalink(); ?>">
    				<h1 class="m-typo m-typo_primary">
    					<?php the_title(); ?>
    				</h1>
           </a>
           <p class="m-typo m-typo_text">
             <?php the_excerpt_max_charlength(135); ?>
           </p>
           <div class="date">
             <?php echo $category_detail[0]->name; ?> | <?php echo get_the_date('F j, Y'); ?>
           </div>
        </div>
        <?php endif; ?>

        <?php if($index == 6 && !is_search()) : ?>
           <div class="post post_graphic">
            <a href="/oferta/">
             <div class="post_img"  style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-post-img.jpg);"></div>
            </a>
           </div>
        <?php endif; ?>

      <?php $index++; endwhile; ?>
     </div>
   </div>

  <?php the_numeric_pagination(); ?>
  <div class="m-ten-boxes">
    <h1 class="m-typo m-typo_primary">Tagi:</h1>
    <div class="tags">
         <?php echo do_shortcode('[wpbtags]'); ?>
    </div>
  </div>
  <br>
  <br>
   <div class="m-blog-info">
     <div class="m-typo m-typo_text">
       <?php the_field('blog_describe', 'options'); ?>
     </div>
   </div>

</div>
<?php get_footer(); ?>
