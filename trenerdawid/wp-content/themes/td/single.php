<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Pojedynczy widok bloga tekstowego
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<?php
  if(get_field('is-recipe')) {
    require_once(THEME_DIR.'/_includes/_modules/_blog-text-recipe.php');
  } else {
    require_once(THEME_DIR.'/_includes/_modules/_blog-text.php');
  }

  ?>

<?php get_footer(); ?>
