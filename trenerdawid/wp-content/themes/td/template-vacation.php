<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Fit wczasy
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<div class="b-container subpage_vacation">

  <?php the_content(); ?>

    <div class="m-ten-boxes">
    <h1 class="m-typo m-typo_primary">Zobacz aktualną ofertę</h1>
   </div>
   <div class="m-blog-posts">
     <div class="posts-wrapper">
     <div class="post">
         <a href="/bali">
           <div class="post_img" style="background-image: url('https://dawidwozniakowski.pl/wp-content/uploads/2017/09/Bali.jpg');"></div>
         </a>
         <a href="/bali">
           <h1 class="m-typo m-typo_primary">
             Bali
           </h1>
          </a>
          <p class="m-typo m-typo_text">
            Przeżyj przygodę życia na Fit Camp Bali podczas długiego weekendu majowego!
           </p>
       </div>
     </div>
  </div>
</div>
<?php get_footer(); ?>
