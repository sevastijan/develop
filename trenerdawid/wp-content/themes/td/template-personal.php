<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Treningi Personalne
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white') ?>


<div class="b-container subpage_personal">

  <?php the_content(); ?>

</div>

<?php get_footer(); ?>
