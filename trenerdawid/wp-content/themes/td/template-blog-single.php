<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Pojedynczy widok bloga
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>

<br>  <br>

<div class="b-container subpage_blog-single">

<div class="top-bar">
  <div class="wrapper">
    <p class="m-typo m-typo_text">
      Kompletne plany <br> treningowe
    </p>
    <p class="m-typo m-typo_text">
      już od <span>145</span> pln
    </p>
  </div>
</div>

<div class="m-two-boxes heading">
  <div class="boxes-wrapper">
    <div class="box">
      <div class="slider-wrapper">
        <div class="slider slider-single">
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
        </div>
        <div class="slider slider-nav">
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
        </div>
      </div>
    </div>
    <div class="box">
      <p class="m-typo m-typo_text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ac pulvinar lacus. Nulla facilisi. Donec pharetra urna mollis neque egestas congue. Duis tempus lectus at rutrum mattis. Curabitur aliquam nisi felis, nec porttitor nisl facilisis finibus. Fusce porta dolor at neque eleifend, non finibus nunc venenatis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam eleifend tempor turpis, sed lacinia ex consectetur ac. Vivamus eu placerat ligula. Integer ac erat condimentum, aliquet sem consequat, mollis sapien. Quisque placerat, orci et accumsan consectetur, quam odio convallis turpis, sed condimentum ex nulla et nunc.
      </p>
      <div class="rate">
        <p class="m-typo m-typo_text">
          Poziom trudności
        </p>
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star checked"></span>
        <span class="star "></span>
        <span class="star "></span>
      </div>
      <div class="m-tags">
        <p class="m-typo m-typo_text">
          Tagi: <span><a href="#">pulpety</a>, <a href="#">fit</a>, <a href="#">danie na parze</a></span>
        </p>
      </div>
      <a href="#" class="m-btn m-btn_primary">
        Wydrukuj przepis
      </a>
    </div>
  </div>
</div>


<br>
<br>
<br>

<div class="m-two-boxes">
  <div class="boxes-wrapper">
    <div class="box">
      <h6 class="m-typo m-typo_secondary">
        Składniki odżywcze
      </h6>
      <table class="table">
        <tr class="table_item">
          <th class="m-typo m-typo_text">Białko</th>
          <th class="m-typo m-typo_text">110g</th>
        </tr>
        <tr class="table_item">
          <th class="m-typo m-typo_text">Tłuszcz</th>
          <th class="m-typo m-typo_text">28g</th>
        </tr>
        <tr class="table_item">
          <th class="m-typo m-typo_text">Węglowodany</th>
          <th class="m-typo m-typo_text">13g</th>
        </tr>
        <tr class="table_item">
          <th class="m-typo m-typo_text">Kalorie</th>
          <th class="m-typo m-typo_text">740kcal</th>
        </tr>
      </table>
      <h6 class="m-typo m-typo_secondary">
        Składniki
      </h6>
      <ul class="list">
        <input type="checkbox" id="component1">
        <label for="component1" class="list_item m-typo m-typo_text">kilka małych korniszonów</label>
        <input type="checkbox" id="component2">
        <label for="component2" class="list_item m-typo m-typo_text">1 cebula</label>
        <input type="checkbox" id="component3">
        <label for="component3" class="list_item m-typo m-typo_text">500 gram wołowiny (udziec / polędwica)</label>
        <input type="checkbox" id="component4">
        <label for="component4" class="list_item m-typo m-typo_text">1 całe jajko</label>
        <input type="checkbox" id="component5">
        <label for="component5" class="list_item m-typo m-typo_text">1 łyżeczka majeranku</label>
        <input type="checkbox" id="component6">
        <label for="component6" class="list_item m-typo m-typo_text">pół łyżeczki pieprzu</label>
        <input type="checkbox" id="component7">
        <label for="component7" class="list_item m-typo m-typo_text">szczypta soli</label>
        <input type="checkbox" id="component8">
        <label for="component8" class="list_item m-typo m-typo_text">1 łyżka otrąb owsianych lub żytnich</label>
      </ul>
    </div>
    <div class="box">
      <h6 class="m-typo m-typo_secondary">
        Sposób przygotowania
      </h6>
      <ul class="list">
        <li class="list_item m-typo m-typo_text">
          Cebulę oraz korniszony drobno kroimy.
        </li>
        <li class="list_item m-typo m-typo_text">
          Do zmielonego mięsa wołowego dodajemy jajko, przyprawy, otręby
          owsiane oraz pokrojone wcześniej korniszony z cebulą.
          Całość dokładnie mieszamy.
        </li>
        <li class="list_item m-typo m-typo_text">
          Formujemy kuleczki (najlepiej lekko mokrymi dłońmi).
        </li>
        <li class="list_item m-typo m-typo_text">
            Wstawiamy do parowaru (można też włożyć do durszlaka zawieszonego
            na garnku) i gotujemy na parze przez około pół godziny.
        </li>
        <li class="list_item m-typo m-typo_text">
          Pulpeciki genialnie komponują się z sosem na bazie jogurtu naturalnego
          np. tzatziki lub pietruszkowym.
        </li>
      </ul>
    </div>
  </div>
</div>

<div class="m-bar">
  <div class="boxes-wrapper">
    <div class="box prev">
      <a href="#">
        <img class="box_arrow" src="<?php echo THEME_URL; ?>assets/images/bar-arrow.png" alt="arrow">
        <p class="m-typo m-typo_text">
          Poprzedni wpis <br> <span>Fit klops wołowy z jajkiem</span>
        </p>
      </a>
    </div>
    <div class="box next">
      <a href="#">
        <img class="box_arrow" src="<?php echo THEME_URL; ?>assets/images/bar-arrow.png" alt="arrow">
        <p class="m-typo m-typo_text">
          Następny wpis <br> <span>3 porady na potężne tricepsy</span>
        </p>
      </a>
    </div>
  </div>
</div>

<div class="m-blog-posts">
  <h1 class="m-typo m-typo_secondary">
    Zobacz także inne wpisy z mojego bloga
  </h1>
  <div class="posts-wrapper">
      <div class="post">
       	<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<div class="post_img" style="background-image: url(http://dev.td.dkonto.pl/wp-content/uploads/2016/02/bananowe-ciasteczka-jaglane-1000x667.jpg);"></div>
    		</a>
    		<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<h1 class="m-typo m-typo_primary">
    				Bananowe ciasteczka jaglane z orzechami
          </h1>
         </a>
         <p class="m-typo m-typo_text">
           Te bananowe ciasteczka to niewyobrażalnie smaczna i zdrowa przekąska. Idealnie nadają się na ...
         </p>
         <div class="date">
           Przepisy | Luty 10, 2016
         </div>
      </div>
      <div class="post">
       	<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<div class="post_img" style="background-image: url(http://dev.td.dkonto.pl/wp-content/uploads/2016/02/bananowe-ciasteczka-jaglane-1000x667.jpg);"></div>
    		</a>
    		<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<h1 class="m-typo m-typo_primary">
    				Bananowe ciasteczka jaglane z orzechami
          </h1>
         </a>
         <p class="m-typo m-typo_text">
           Te bananowe ciasteczka to niewyobrażalnie smaczna i zdrowa przekąska. Idealnie nadają się na ...
         </p>
         <div class="date">
           Przepisy | Luty 10, 2016
         </div>
      </div>
      <div class="post">
       	<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<div class="post_img" style="background-image: url(http://dev.td.dkonto.pl/wp-content/uploads/2016/02/bananowe-ciasteczka-jaglane-1000x667.jpg);"></div>
    		</a>
    		<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<h1 class="m-typo m-typo_primary">
    				Bananowe ciasteczka jaglane z orzechami
          </h1>
         </a>
         <p class="m-typo m-typo_text">
           Te bananowe ciasteczka to niewyobrażalnie smaczna i zdrowa przekąska. Idealnie nadają się na ...
         </p>
         <div class="date">
           Przepisy | Luty 10, 2016
         </div>
      </div>
    </div>
  </div>





</div>
<br>
<br>





<?php get_footer(); ?>
