<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: CTA
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header(); the_post();?>



<?php the_content(); ?>

</div>
<?php get_footer(); ?>
