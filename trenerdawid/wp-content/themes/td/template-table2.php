<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Tabela nr2
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>

<div class="b-container subpage_table-secondary">

  <div class="product-boxes">
    <div class="product-boxes_box">
      <div class="content-wrapper">
        <div class="top">
          <p>Pakiet</p>
          <p>
            <span>5</span> spotkań
          </p>
        </div>
        <div class="bottom">
          <p>
            <span>279</span><span>zł</span><span>/spotkanie</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-boxes_box">
      <div class="content-wrapper">
        <div class="top">
          <p>Pakiet</p>
          <p>
            <span>5</span> spotkań
          </p>
        </div>
        <div class="bottom">
          <p>
            <span>279</span><span>zł</span><span>/spotkanie</span>
          </p>
        </div>
      </div>
    </div>
    <div class="product-boxes_box">
      <div class="content-wrapper">
        <div class="top">
          <p>Pakiet</p>
          <p>
            <span>5</span> spotkań
          </p>
        </div>
        <div class="bottom">
          <p>
            <span>279</span><span>zł</span><span>/spotkanie</span>
          </p>
        </div>
      </div>
    </div>
  </div>

<div class="m-two-boxes">
  <div class="box-shadow">
    <div class="boxes-wrapper">
      <div class="box">
        <div class="top">
          <h3 class="m-typo m-typo_primary">
            Pakiet standard
          </h3>
          <p class="m-typo m-typo_text">
            Suplementacja, trening, dieta
          </p>
        </div>
        <div class="body">
          <div class="row-box">
            <div class="img"></div>
            <p class="m-typo m-typo_text">
              Indywidualne podejście
            </p>
          </div>
          <div class="row-box">
            <div class="img"></div>
            <p class="m-typo m-typo_text">
              Uwzględnienie kontuzji i problemów z uk. Ruchu
            </p>
          </div>
          <div class="row-box">
            <div class="img"></div>
            <p class="m-typo m-typo_text">
              Regularna analiza postępów
            </p>
          </div>
        </div>
      </div>
      <div class="box">
        <div class="top">
          <h3 class="m-typo m-typo_primary">
            Pakiet plus
          </h3>
          <p class="m-typo m-typo_text">
            Suplementacja, trening, dieta
          </p>
        </div>
        <div class="body">
          <div class="row-box">
            <div class="img"></div>
            <p class="m-typo m-typo_text">
              Indywidualne podejści2e
            </p>
          </div>
          <div class="row-box">
            <div class="img active"></div>
            <p class="m-typo m-typo_text">
              Uwzględnienie kontuzji i problemów z uk. Ruchu
            </p>
          </div>
          <div class="row-box">
            <div class="img"></div>
            <p class="m-typo m-typo_text">
              Regularna analiza postępów
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="boxes-wrapper">
      <h3 class="m-typo m-typo_primary">
        Co wybierzesz?
      </h3>
      <div class="box">
        <div class="wrapper">
          <p class="m-typo m-typo_text">
            Najczęściej wybierany
          </p>
          <p class="m-typo m-typo_text">
            Pakiet All in One <br> <span>249</span> PLN + VAT
          </p>
          <a href="#" class="m-btn m-btn_primary">Zamawiam</a>
        </div>
      </div>
      <div class="box">
        <div class="wrapper">
          <p class="m-typo m-typo_text">
            Pakiet All in One <br> <span>499</span> PLN + VAT
          </p>
          <a href="#" class="m-btn m-btn_primary">Zamawiam</a>
        </div>
      </div>
    </div>
    <div class="boxes-wrapper">
      <h3 class="m-typo m-typo_primary">
        Pakiet dieta
      </h3>
      <div class="box">
        <div class="wrapper">
          <p class="m-typo m-typo_text">
            Dieta standard <br> <span>149</span> PLN + VAT
          </p>
          <a href="#" class="m-btn m-btn_primary">Zamawiam</a>
        </div>
      </div>
      <div class="box">
        <div class="wrapper">
          <p class="m-typo m-typo_text">
            Dieta plus <br> <span>299</span> PLN + VAT
          </p>
          <a href="#" class="m-btn m-btn_primary">Zamawiam</a>
        </div>
      </div>
    </div>
    <div class="boxes-wrapper">
      <h3 class="m-typo m-typo_primary">
        Pakiet trening
      </h3>
      <div class="box">
        <div class="wrapper">
          <p class="m-typo m-typo_text">
            Trening standard <br> <span>99</span> PLN + VAT
          </p>
          <a href="#" class="m-btn m-btn_primary">Zamawiam</a>
        </div>
      </div>
      <div class="box">
        <div class="wrapper">
          <p class="m-typo m-typo_text">
            Trening plus <br> <span>199</span> PLN + VAT
          </p>
          <a href="#" class="m-btn m-btn_primary">Zamawiam</a>
        </div>
      </div>
    </div>
    <div class="boxes-wrapper">
      <h3 class="m-typo m-typo_primary">
        Pakiet suplementacja
      </h3>
      <div class="box">
        <div class="wrapper">
          <p class="m-typo m-typo_text">
            Suplementacja standard <br> <span>49</span> PLN + VAT
          </p>
          <a href="#" class="m-btn m-btn_primary">Zamawiam</a>
        </div>
      </div>
      <div class="box">
        <div class="wrapper">
          <p class="m-typo m-typo_text">
            Suplementacja plus <br> <span>99</span> PLN + VAT
          </p>
          <a href="#" class="m-btn m-btn_primary">Zamawiam</a>
        </div>
      </div>
    </div>
  </div>
</div>

</div>






<?php get_footer(); ?>
