<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Personel - Martyna
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); the_post(); ?>

<div class="page-template-template-personel">

  <?php the_content(); ?>

</div>




<?php get_footer(); ?>