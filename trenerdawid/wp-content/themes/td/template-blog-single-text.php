<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Pojedynczy widok bloga tekstowego
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>

<br>  <br>
<div class="b-container subpage_blog-single-text">

<div class="m-two-boxes">
  <div class="boxes-wrapper">
    <div class="box">
      <div class="image-wrapper">
        <div class="image-wrapper_img"  style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-text-bg.png);"></div>
        <div class="date">
          Październik 29, 2017
        </div>
      </div>
    </div>
    <div class="box">
      <div class="author">
        <div class="author_img" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-avatar.png);"></div>
        <h6 class="m-typo m-typo_text">
          Dawid Woźniakowski
        </h6>
      </div>
      <div class="category">
        <h6 class="m-typo m-typo_text">
          <span>Kategoria:</span> Trening
        </h6>
      </div>
    </div>
  </div>
</div>

<div class="m-ten-boxes">
  <p class="m-typo m-typo_text">
    Aby ręce prezentowały  się okazale trzeba pracować nie tylko nad bicepsem ale i nad tricepsem. Oto porady, które przenoszą trening na zupełnie nowy poziom!
  </p>
  <h1 class="m-typo m-typo_secondary small-heading">
    1. Dobierz ćwiczenia tak aby angażować mięsień w różnych pozycjach.
  </h1>
  <p class="m-typo m-typo_text">
    Aby maksymalnie zaangażować triceps wykonuj ćwiczenia w których ułożenie łokcia względem tułowia (a dokładniej barku) jest różne. Przykładowo – kickback ze sztangielką (z łokciem maksymalnie cofniętym w stronę pleców), prostowanie przedramion z linką wyciągu górnego (łokcie są ułożone równolegle do tułowia) oraz francuskie wyciskanie sztangi do czoła / skullcrasher (łokcie są wysunięte mocno do przodu względem tułowia).
  </p>
  <h1 class="m-typo m-typo_secondary small-heading">
    2. Ćwicz w pełnym zakresie ruchu (ang. AROM, Active Range of Motion).
  </h1>
  <p class="m-typo m-typo_text">
    W celu szybkiego wzmocnienia/powiększenia mięśnia, zawsze ćwicz w pełnym zakresie ruchu. W przypadku tricepsa oznacza to świadome napięcie tricepsa w fazie koncentrycznej (przy maksymalnie wyprostowanym łokciu) oraz napięcie jego antagonisty – bicepsa w końcówce fazy ekscentrycznej (rozciągania tricepsa, czyli opuszczania ciężaru w dół). Jeżeli ciężar którym ćwiczysz uniemożliwia Ci pracę w pełnym zakresie ruchu to zdecydowanie należy go zmniejszyć. Napięcie bicepsa podczas opuszczania ciężaru spowoduje mocne rozciągnięcie tricepsa (a to właśnie podczas fazy koncentrycznej / rozciągania mięśnia – powstaje najwięcej mikrourazów włókien mięśniowych).
  </p>
  <h1 class="m-typo m-typo_secondary small-heading">
    3. Pilnuj ciągłości wykonywanego ruchu w trakcie każdej serii.
  </h1>
  <p class="m-typo m-typo_text">
    Chodzi o to, aby w trakcie serii mięsień nawet przez chwilę nie odpoczywał. Nie ma nic złego w spięciu mięśnia w fazie koncentrycznej – jednak unikaj przerw w fazie ekscentrycznej (podczas opuszczania ciężaru). W przypadku tricepsa – w końcowej fazie opuszczania ciężaru mocno napnij biceps (zgodnie z poradą numer 2) i od razu zacznij wykonywać kolejne powtórzenie, nie zatrzymując się na dole nawet na sekundę.
  </p>
</div>

<br><br><br>

<div class="m-tags">
  <p class="m-typo m-typo_text">
    Tagi: <span><a href="#">pulpety</a>, <a href="#">fit</a>, <a href="#">danie na parze</a></span>
  </p>
</div>

<br><br><br>

<div class="m-bar">
  <div class="boxes-wrapper">
    <div class="box prev">
      <a href="#">
        <img class="box_arrow" src="<?php echo THEME_URL; ?>assets/images/bar-arrow.png" alt="arrow">
        <p class="m-typo m-typo_text">
          Poprzedni wpis <br> <span>Fit klops wołowy z jajkiem</span>
        </p>
      </a>
    </div>
    <div class="box next">
      <a href="#">
        <img class="box_arrow" src="<?php echo THEME_URL; ?>assets/images/bar-arrow.png" alt="arrow">
        <p class="m-typo m-typo_text">
          Następny wpis <br> <span>3 porady na potężne tricepsy</span>
        </p>
      </a>
    </div>
  </div>
</div>

<div class="m-blog-posts">
  <h1 class="m-typo m-typo_secondary">
    Zobacz także inne wpisy z mojego bloga
  </h1>
  <div class="posts-wrapper">
      <div class="post">
       	<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<div class="post_img" style="background-image: url(http://dev.td.dkonto.pl/wp-content/uploads/2016/02/bananowe-ciasteczka-jaglane-1000x667.jpg);"></div>
    		</a>
    		<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<h1 class="m-typo m-typo_primary">
    				Bananowe ciasteczka jaglane z orzechami
          </h1>
         </a>
         <p class="m-typo m-typo_text">
           Te bananowe ciasteczka to niewyobrażalnie smaczna i zdrowa przekąska. Idealnie nadają się na ...
         </p>
         <div class="date">
           Przepisy | Luty 10, 2016
         </div>
      </div>
      <div class="post">
       	<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<div class="post_img" style="background-image: url(http://dev.td.dkonto.pl/wp-content/uploads/2016/02/bananowe-ciasteczka-jaglane-1000x667.jpg);"></div>
    		</a>
    		<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<h1 class="m-typo m-typo_primary">
    				Bananowe ciasteczka jaglane z orzechami
          </h1>
         </a>
         <p class="m-typo m-typo_text">
           Te bananowe ciasteczka to niewyobrażalnie smaczna i zdrowa przekąska. Idealnie nadają się na ...
         </p>
         <div class="date">
           Przepisy | Luty 10, 2016
         </div>
      </div>
      <div class="post">
       	<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<div class="post_img" style="background-image: url(http://dev.td.dkonto.pl/wp-content/uploads/2016/02/bananowe-ciasteczka-jaglane-1000x667.jpg);"></div>
    		</a>
    		<a href="http://dev.td.dkonto.pl/bananowe-ciasteczka-jaglane-z-orzechami/">
    			<h1 class="m-typo m-typo_primary">
    				Bananowe ciasteczka jaglane z orzechami
          </h1>
         </a>
         <p class="m-typo m-typo_text">
           Te bananowe ciasteczka to niewyobrażalnie smaczna i zdrowa przekąska. Idealnie nadają się na ...
         </p>
         <div class="date">
           Przepisy | Luty 10, 2016
         </div>
      </div>
    </div>
  </div>





</div>
<br>
<br>





<?php get_footer(); ?>
