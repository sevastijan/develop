<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Personel cennik - Martyna
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>


<div class="b-container subpage_personel-price martyna">

  <?php the_content(); ?>

</div>

<?php get_footer(); ?>