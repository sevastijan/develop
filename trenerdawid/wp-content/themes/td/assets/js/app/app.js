/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */



(function($){

  $(document).ready(function(){

    //TODO: separate burger function

    var $burger,
        $mobile;

    $burger = $('.js-burger');
    $mobile = $('#js-mobile');
    $body = $('body');

    $burger.on('click', function(){
      $mobile.toggleClass('is-open');
      $body.toggleClass('is-open');
    });

    //TODO: separate burger function

    var $collapseTrigger,
        $collapseContainer,
        $this,
        isOpenClass;

    $collapseTrigger = $('.js-collapseTrigger');
    $collapseContainer = $('.js-collapse');

    isOpenClass = 'is-open';

    $collapseTrigger.on('click', function(){
      var $this,
          $nextElement;

      $this = $(this);
      $nextElement = $($this.context.nextElementSibling);

      if(!$nextElement.hasClass(isOpenClass)) {
        $collapseContainer.removeClass(isOpenClass);
      }
      $nextElement.toggleClass(isOpenClass);
    });

    //TODO: Spearate slider init function

    var $slider;

    $slider = $('#js-testimonials_slider');

    $slider.slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      appendArrows: '#js-testimonials_navi',
      prevArrow: '<div class="m-testimonials_prev"></div>',
      nextArrow: '<div class="m-testimonials_next"></div>'
    });

    var $slider2;

    $slider2 = $('.js-categories');

    $slider2.slick({
      infinite: true,
      slidesToShow: 5,
      slidesToScroll: 1,
      infinite: true,
      autoplay: false,
      prevArrow: '<div class="m-testimonials_prev"></div>',
      nextArrow: '<div class="m-testimonials_next"></div>',
      responsive: [
        {
          breakpoint: 1500,
          settings: {
            slidesToShow: 4
          }
        },{
          breakpoint: 1370,
          settings: {
            slidesToShow: 4
          }
        },{
          breakpoint: 1024,
          settings: {
            slidesToShow: 3
          }
        },{
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },{
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });


    var togglePackageTab = function(elementToHide, elementToShow) {
    	$(elementToHide).removeClass('is-active');
      $(elementToShow).addClass('is-active');
    }

    $("#consultationBtnOffline").click(function() {
    	togglePackageTab('.js-subBoxOnline', '.js-subBoxOffline');
    });
    $("#consultationBtnOnline").click(function() {
    	togglePackageTab('.js-subBoxOffline', '.js-subBoxOnline');
    });

    $("#consultationBtnOffline, #consultationBtnOnline").click(function() {
        $('html, body').animate({
            scrollTop: $(".subboxes-wrapper").offset().top - 150
        }, 2000);
    });

    var $readMoreButton = $('.js-readMoreButton');

    $readMoreButton.on('click', function(){
      var $parent,
          fullText,
          shortText,
          showMoreTrans,
          showLessTrans;

      $parent = $(this).parent().find('.js-toggleContent');
      showMoreTrans = 'Rozwiń więcej';
      showLessTrans = 'Zwiń';

      if($parent.attr('data-expanded') === 'true') {
        $parent.attr('data-expanded', 'false');
        $(this).html(showMoreTrans);
        shortText = $parent.attr('data-short');
        $parent.html(shortText);
      } else {
        $parent.attr('data-short', $parent.html());
        $parent.attr('data-expanded', 'true');
        $(this).html(showLessTrans);
        fullText = $parent.data('fullText');
        $parent.html(fullText);
      }

    });


  // camp tabs

  var primaryBtn = $('.js-campBtn-1'),
      secondaryBtn = $('.js-campBtn-2'),
      tertiaryBtn = $('.js-campBtn-3'),
      quaternaryBtn = $('.js-campBtn-4'),
      quinaryBtn = $('.js-campBtn-5'),
      senaryBtn = $('.js-campBtn-6'),
      priceBtn = $('.js-priceBtn'),
      priceTable = $('.js-priceTable');

  primaryBtn.click(function(){
    $('.js-tab-2, .js-tab-3, .js-tab-4, .js-tab-5, .js-tab-6, .js-campBtn-2, .js-campBtn-3, .js-campBtn-4, .js-campBtn-5, .js-campBtn-6').removeClass('show');
    $('.js-tab-1, .js-campBtn-1').addClass('show');
  })
  secondaryBtn.click(function(){
    $('.js-tab-1, .js-tab-3, .js-tab-4, .js-tab-5, .js-tab-6, .js-campBtn-1, .js-campBtn-3, .js-campBtn-4, .js-campBtn-5, .js-campBtn-6').removeClass('show');
    $('.js-tab-2, .js-campBtn-2').addClass('show');
  })
  tertiaryBtn.click(function(){
    $('.js-tab-1, .js-tab-2, .js-tab-4, .js-tab-5, .js-tab-6, .js-campBtn-2, .js-campBtn-1, .js-campBtn-4, .js-campBtn-5, .js-campBtn-6').removeClass('show');
    $('.js-tab-3, .js-campBtn-3').addClass('show');
  })
  quaternaryBtn.click(function(){
    $('.js-tab-1, .js-tab-2, .js-tab-3, .js-tab-5, .js-tab-6, .js-campBtn-2, .js-campBtn-3, .js-campBtn-1, .js-campBtn-5, .js-campBtn-6').removeClass('show');
    $('.js-tab-4, .js-campBtn-4').addClass('show');
  })
  quinaryBtn.click(function(){
    $('.js-tab-1, .js-tab-2, .js-tab-3, .js-tab-4, .js-tab-6, .js-campBtn-2, .js-campBtn-3, .js-campBtn-4, .js-campBtn-1, .js-campBtn-6').removeClass('show');
    $('.js-tab-5, .js-campBtn-5').addClass('show');
  })
  senaryBtn.click(function(){
    $('.js-tab-1, .js-tab-2, .js-tab-3, .js-tab-4, .js-tab-5, .js-campBtn-2, .js-campBtn-3, .js-campBtn-4, .js-campBtn-5, .js-campBtn-1').removeClass('show');
    $('.js-tab-6, .js-campBtn-6').addClass('show');
  })
  priceBtn.click(function() {
	  $('html, body').animate({
		  scrollTop: priceTable.offset().top - 110
	  }, 1000);
  });




  // blog-single slider
  $('.slider-single').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   fade: false,
   adaptiveHeight: true,
   infinite: false,
   useTransform: true,
   speed: 400,
   cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
  });

  $('.slider-nav')
   .on('init', function(event, slick) {
     $('.slider-nav .slick-slide.slick-current').addClass('is-active');
   })
   .slick({
     slidesToShow: 7,
     slidesToScroll: 7,
     dots: false,
    arrows: false,
     focusOnSelect: false,
     infinite: false,
     responsive: [{
       breakpoint: 1024,
       settings: {
         slidesToShow: 5,
         slidesToScroll: 5,
       }
     }, {
       breakpoint: 640,
       settings: {
         slidesToShow: 4,
         slidesToScroll: 4,
       }
     }, {
       breakpoint: 420,
       settings: {
         slidesToShow: 3,
         slidesToScroll: 3,
     }
     }]
   });

  $('.slider-single').on('afterChange', function(event, slick, currentSlide) {
   $('.slider-nav').slick('slickGoTo', currentSlide);
   var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
   $('.slider-nav .slick-slide.is-active').removeClass('is-active');
   $(currrentNavSlideElem).addClass('is-active');
  });

  $('.slider-nav').on('click', '.slick-slide', function(event) {
   event.preventDefault();
   var goToSingleSlide = $(this).data('slick-index');

   $('.slider-single').slick('slickGoTo', goToSingleSlide);
  });

  $('.js-faq-wrapper').click(function(){
    var container = $(this).find(".js-question");
    var answer = $(this).find(".js-answer");

    answer.slideToggle(200);

    $(this).toggleClass("active");
    answer.toggleClass("active");

  });

  // fixed navbar
  var num = 150,
      show = true;


  $(window).bind('scroll', function () {
      if ($(window).scrollTop() > num) {
         $('.b-hero_header, .b-hero_teaser, .m-logo, .m-burger, .m-menu, .m-mobile, .b-topbar').addClass('reduced');
      } else {
         $('.b-hero_header, .b-hero_teaser, .m-logo, .m-burger, .m-menu, .m-mobile, .b-topbar').removeClass('reduced');
      }
  });



    //TODO: Separate tab init function

    var $tabs;

    $tabs = $('#tabs');

    $tabs.tabs({
      active: true
    });

    $(window).on('resize', function(){
      var $this;

      $this = $(this);

      if($this.width() >= 1024) {
        $mobile.removeClass('is-open');
      }
    });

    var $inputClass = $('.medium');

    $inputClass.on('change', function() {
    	if($(this).val()) {
        $( this ).parent().parent().find('.gfield_label').addClass('focused');
      }else{
        $( this ).parent().parent().find('.gfield_label').removeClass('focused');
      }
    });

    $.each($inputClass, function(){
      if($inputClass.val().length > 2) {
        $( this ).parent().parent().find('.gfield_label').addClass('focused');
      }
    });






  $("#js-mobile").find(".m-menu_item").click(function(){
    if ($(this).find(".m-menu_sub-menu").hasClass("is-active")){
      $(".m-menu_sub-menu").removeClass("is-active");
      $(".m-menu_url").removeClass("is-active");
    } else {
      $(".m-menu_sub-menu").removeClass("is-active");
      $(".m-menu_url").removeClass("is-active");
      $(this).find(".m-menu_sub-menu").addClass("is-active");
      $(this).find(".m-menu_url").addClass("is-active");
    }
  });

  $('label[for="billing_kupuje_jako_firma_33161"]').on('click', function(){
      $('p#billing_company_field').toggle();
      $('p#billing_numer_nip_70949_field').toggle();
  });

  });

})(jQuery);
