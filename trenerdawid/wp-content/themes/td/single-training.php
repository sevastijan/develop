<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Widok artykułu
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<div class="b-container subpage_fit-camp subpage_training-template">
<div class="m-two-boxes heading fit-camp">
  <div class="boxes-wrapper">
    <div class="box">
      <div class="slider-wrapper">
        <div class="slider slider-single">
	 		<div class="slider-image" style="background-image: url('  <?php the_field('image'); ?>');"></div>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="m-typo m-typo_secondary">
		  <?php the_field('title'); ?>
      </div>
      <div class="m-typo m-typo_text">
		  Moduł<br>
		  <span><?php the_field('module'); ?></span>
      </div>
      <div class="m-typo m-typo_text">
		  Data<br>
		  <span><?php the_field('date'); ?></span>
      </div>
      <div class="m-typo m-typo_text">
		  Prowadzący<br>
		  <span><?php the_field('who'); ?></span>
      </div>
      <div class="m-typo m-typo_text">
		  Czas trwania<br>
		  <span><?php the_field('time'); ?></span>
      </div>
      <div class="m-typo m-typo_text">
		  Miasto<br>
		  <span><?php the_field('place'); ?></span>
      </div>
      <div class="m-typo m-typo_text">
		  Ilość miejsc<br>
		  <span><?php the_field('free_places'); ?></span>
      </div>
    </div>
  </div>
</div>

<div class="text-wrapper m-typo m-typo_text">
	<?php the_field('training_short_description'); ?>
</div>

<div class="m-tab-nav">
	<div class="wrapper">
		<ul class="list clearfix">
			<li class="js-campBtn-1 list_item m-typo m-typo_text show">
				Opis
			</li>
			<li class="js-campBtn-2 list_item m-typo m-typo_text">
				Plan szkolenia
			</li>
			<li class="js-campBtn-3 list_item m-typo m-typo_text">
				Prowadzący
			</li>
			<li class="js-campBtn-4 list_item m-typo m-typo_text">
				Miejsce
			</li>
			<li class="js-campBtn-5 list_item m-typo m-typo_text">
				Organizacja
			</li>
		</ul>
	</div>
</div>

<div class="m-fit-tabs">
  <div class="wrapper">
	<p class="js-campBtn-1 m-typo m-typo_text mobile-btn show">
		Opis
	</p>
    <div class="tab js-tab-1 show m-typo m-typo_text">
      <?php the_field('tab1_content'); ?>
    </div>
	<p class="js-campBtn-2 m-typo m-typo_text mobile-btn">
		Plan szkolenia
	</p>
    <div class="tab js-tab-2 m-typo m-typo_text">
      <?php the_field('tab2_plan'); ?>
    </div>
	<p class="js-campBtn-3 m-typo m-typo_text mobile-btn">
		Prowadzący
	</p>
    <div class="tab js-tab-3 m-typo m-typo_text">
      <?php the_field('tab3_who'); ?>
    </div>
	<p class="js-campBtn-4 m-typo m-typo_text mobile-btn">
		Miejsce
	</p>
    <div class="tab js-tab-4 m-typo m-typo_text">
      <?php the_field('tab4_place'); ?>
    </div>
	<p class="js-campBtn-5 m-typo m-typo_text mobile-btn">
		Organizacja
	</p>
    <div class="tab js-tab-5 m-typo m-typo_text">
	 	<?php the_field('tab5_organization'); ?>
    </div>
  </div>

<div class="m-two-boxes payment_info">
	<div class="boxes-wrapper">
		<div class="box">
			<div class="m-typo m-typo_text">
				<?php the_field('training_payment_info', 'options'); ?>
			</div>
		</div>
		<div class="box">
			<a href="#" class="m-btn m-btn_primary js-ask_form">Zadaj pytanie</a>
			<a href="#" class="m-btn m-btn_tertiary"><span>Udostępnij</span></a>
			<p class="m-typo m-typo_text">
				<a href="#" class="js-ask_form">Zapytaj się o szkolenie w Twoim mieście</a>
			</p>
		</div>
	</div>
</div>


<?php if( have_rows('training_ticket_table_wrapper') ):?>
<div class="m-table js-priceTable ticket" style="border-bottom: 2px solid #b5b8ba; margin-bottom: 0; padding-bottom: 60px;">
  <h1 class="m-typo m-typo_secondary small-heading">Bilety</h1>
  <div class="wrapper">
	  <?php while( have_rows('training_ticket_table_wrapper') ) : the_row(); $product = wc_get_product( get_sub_field('link') ); ?>
		  <ul class="table-list clearfix">
		  	<li class="table-list_item m-typo m-typo_text">
		  		Typ biletu <br>
				<span><?php the_sub_field('type'); ?></span>
		  	</li>
		  	<li class="table-list_item m-typo m-typo_text">
		  		Cena netto<br>
				<span><?php echo $product->get_price(); ?> zł</span>
		  	</li>
		  	<li class="table-list_item m-typo m-typo_text">
		  		Moduł <br>
				<span><?php the_sub_field('module'); ?></span>
		  	</li>
		  	<li class="table-list_item m-typo m-typo_text">
		  		Miejsce <br>
				<span><?php the_sub_field('place'); ?></span>
		  	</li>
		  	<li class="table-list_item m-typo m-typo_text">
		  		Data <br>
				<span><?php the_sub_field('date'); ?></span>
		  	</li>
		  	<li class="table-list_item m-typo m-typo_text">
		  		Ilość miejsc <br>
				<span><?php the_sub_field('free_places'); ?></span>
		  	</li>
		  	<li class="table-list_item m-typo m-typo_text">
		  		Długość <br>
				<span><?php the_sub_field('time'); ?></span>
		  	</li>
		  	<li class="table-list_item m-typo m-typo_text">
			<?php
				$product = wc_get_product( get_sub_field('link', 'options') );
				if( $product->get_stock_quantity() > 0 ) :
			?>
				<th class="m-typo m-typo_text"><a class="m-typo m-typo_text" href="/koszyk/?add-to-cart=<?php the_sub_field('link', 'options') ?>">Kup</a></th>
			<?php else: ?>
				<th class="m-typo m-typo_text"><a style="opacity: 0.5;" class="m-typo m-typo_text" href="#">Kup</a></th>
			<?php endif; ?>
			</li>
		  </ul>
		<?php endwhile;?>
<div class="wrapper">
<table class="table w100">
<tbody>
<tr class="table_item bg-dbl">
<th class="m-typo m-typo_text" colspan="5" style="background-color: #00345c; !important;">Ceny za szkolenia podane są w kwocie netto, w trakcie opłacania zamówienia automatycznie doliczony zostanie podatek VAT.</th>
</tr>
</tbody>
</table>
</div>
  </div>
</div>
<?php endif; ?>

<?php if( have_rows('training_table_wrapper', 'options') ):?>
<div class="m-table js-priceTable ticket ticket_training-info">
  <h1 class="m-typo m-typo_secondary small-heading">Zobacz wszystkie szkolenia</h1>
  <div class="wrapper">
	<?php while( have_rows('training_table_wrapper', 'options') ) : the_row(); ?>
		<ul class="table-list clearfix">
		  <li class="table-list_item m-typo m-typo_text">
			  Nazwa <br>
			  <span><?php the_sub_field('title', 'options'); ?></span>
		  </li>
		  <li class="table-list_item m-typo m-typo_text">
			  Moduł<br>
			  <span><?php the_sub_field('module', 'options'); ?></span>
		  </li>
		  <li class="table-list_item m-typo m-typo_text">
			  Data <br>
			  <span><?php the_sub_field('date', 'options'); ?></span>
		  </li>
		  <li class="table-list_item m-typo m-typo_text">
			  Miasto <br>
			  <span><?php the_sub_field('place', 'options'); ?></span>
		  </li>
		  <li class="table-list_item m-typo m-typo_text">
			  Czas trwania <br>
			  <span><?php the_sub_field('time', 'options'); ?></span>
		  </li>
		  <li class="table-list_item m-typo m-typo_text">
			  Ilość miejsc <br>
			  <span><?php the_sub_field('free_places', 'options'); ?></span>
		  </li>
		  <li class="table-list_item m-typo m-typo_text">
			  <th class="m-typo m-typo_text">
				  <a class="m-typo m-typo_text js-ask_form" href="<?php the_sub_field('link', 'options'); ?>">Wybierz »</a>
			  </th>
		  </li>
		</ul>
	  <?php endwhile;?>
  </div>
</div>
<?php endif; ?>

</div>
</div>
<?php get_footer(); ?>
