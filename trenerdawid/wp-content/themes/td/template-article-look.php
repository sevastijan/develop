<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Widok artykułu
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<style>
	.subpage_article-post::after {
		content: url(<?php the_post_thumbnail_url(); ?>) !important;
		top: -250px !important;		
	}
</style>

<div class="b-container subpage_article-post">

	<?php the_content(); ?>

	<?php if( have_rows('related_posts') ): ?>
		<div class="m-blog-posts">
			<div class="posts-wrapper">
			<?php while ( have_rows('related_posts') ) : the_row(); $relatedPost = get_sub_field('post'); ?>
				<div class="post">
					<a href="<?php echo get_permalink($relatedPost->ID); ?>">
						<div class="post_img" style="background-image: url(<?php echo get_the_post_thumbnail_url($relatedPost->ID); ?>);"></div>
					</a>
					<a href="<?php echo get_permalink($relatedPost->ID); ?>">
						<h1 class="m-typo m-typo_primary">
							<?php echo $relatedPost->post_title; ?>
						</h1>
					</a>
					<p class="m-typo m-typo_text">
						<?php echo $relatedPost->post_content; ?>         
					</p>
					<div class="date">
						Wylot: <?php the_field('flight', $relatedPost->ID); ?>
					</div>
				</div>
			<?php endwhile; ?>
			</div>
		</div>
	<?php endif;?>

</div>
<?php get_footer(); ?>