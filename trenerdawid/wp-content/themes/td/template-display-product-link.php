<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 * Template Name: Podziękowanie z przyciskiem
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php get_header('white'); ?>

<style>
	.subpage_article-post::after {
		content: url(<?php the_post_thumbnail_url(); ?>) !important;
		top: -250px !important;		
	}
</style>

<div class="b-container subpage_article-post">

	<?php the_content(); ?>
	<div class="b-container_buttons">
		<?php displayProductLink(); ?>
	</div>
</div>
<?php get_footer(); ?>
