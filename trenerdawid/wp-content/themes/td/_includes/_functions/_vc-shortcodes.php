<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
  /**
  *  Import metamorphosis module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-about.php');

  /**
  *  Import blog module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-blog.php');

  /**
  *  Import testimonial module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-testimonials.php');

  /**
  *  Import team/trainers module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-team.php');

  /**
  *  Import offer tabs module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-offer.php');

  /**
  *  Import features module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-features.php');

  /**
  *  Import two columns module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-two-columns.php');

  /**
  *  Import plans module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-plans.php');

  /**
  *  Import plans module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-tips.php'); 

  /**
  *  Import plans module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-four-columns.php');
  
  /**
  *  Import plans module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-camps.php');
  
  /**
  *  Import sidebar module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_box-single.php');
  
  /**
  *  Import banners module
  *
  */
  require_once(THEME_DIR.'_includes/_modules/_boxes-banners.php');

?>
