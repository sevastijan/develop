<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php

  add_action('init','custom_post_types');
  function custom_post_types(){

    /**
    *  Register trainers
    *  Import in module
    *
    */
    $trainers = array(
      'labels' => array (
          'name' => 'Trenerzy',
          'sungular_name' => 'Trenerzy',
          'all_items' => 'Wszystkie',
          'add_new' => 'Dodaj',
          'add_new_item' => 'Dodaj',
          'edit_item' => 'Edytuj',
          'new_item' => 'Nowy',
          'view_item' => 'Zobacz',
          'search_items' => 'Szukaj',
          'not_found' => 'Nie znaleziono',
          'not_found_in_trash' => 'Nie znaleziono w koszu',
          'parent_item_colon' => ''
      ),
      'public' => true,
      'public_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'trainers'),
      'capatility_type' => 'post',
      'hierarchical' => true,
      'menu_position' => 5,
      'supports' => array(
          'title', 'editor', 'author', 'thumbnail'
      ),
    );
    register_post_type('trainers', $trainers);

    /**
    *  Register plans
    *  Import in module
    *
    */
    $packages = array(
      'labels' => array (
          'name' => 'Pakiety',
          'sungular_name' => 'Pakiety',
          'all_items' => 'Wszystkie',
          'add_new' => 'Dodaj',
          'add_new_item' => 'Dodaj',
          'edit_item' => 'Edytuj',
          'new_item' => 'Nowy',
          'view_item' => 'Zobacz',
          'search_items' => 'Szukaj',
          'not_found' => 'Nie znaleziono',
          'not_found_in_trash' => 'Nie znaleziono w koszu',
          'parent_item_colon' => ''
      ),
      'public' => true,
      'public_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'packages'),
      'capatility_type' => 'post',
      'hierarchical' => true,
      'menu_position' => 5,
      'supports' => array(
          'title', 'editor', 'author', 'thumbnail'
      ),
    );
    register_post_type('packages', $packages);


    /**
    *  Register testimonials
    *  Import in module
    *
    */
    $testimonials = array(
      'labels' => array (
          'name' => 'Opinie',
          'sungular_name' => 'Opinie',
          'all_items' => 'Wszystkie',
          'add_new' => 'Dodaj',
          'add_new_item' => 'Dodaj',
          'edit_item' => 'Edytuj',
          'new_item' => 'Nowy',
          'view_item' => 'Zobacz',
          'search_items' => 'Szukaj',
          'not_found' => 'Nie znaleziono',
          'not_found_in_trash' => 'Nie znaleziono w koszu',
          'parent_item_colon' => ''
      ),
      'public' => true,
      'public_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'testimonials'),
      'capatility_type' => 'post',
      'hierarchical' => true,
      'menu_position' => 5,
      'supports' => array(
          'title', 'editor', 'author', 'thumbnail'
      ),
    );
    register_post_type('testimonials', $testimonials);

    /**
    *  Fit camps
    *  Import in module
    *
    */
    $camps = array(
      'labels' => array (
          'name' => 'Wyjazdy',
          'sungular_name' => 'Wyjazdy',
          'all_items' => 'Wszystkie',
          'add_new' => 'Dodaj',
          'add_new_item' => 'Dodaj',
          'edit_item' => 'Edytuj',
          'new_item' => 'Nowy',
          'view_item' => 'Zobacz',
          'search_items' => 'Szukaj',
          'not_found' => 'Nie znaleziono',
          'not_found_in_trash' => 'Nie znaleziono w koszu',
          'parent_item_colon' => ''
      ),
      'public' => true,
      'public_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'camps'),
      'capatility_type' => 'post',
      'hierarchical' => true,
      'menu_position' => 5,
      'supports' => array(
          'title', 'editor', 'author', 'thumbnail'
      ),
    );
    register_post_type('camps', $camps);

    /**
    *  Register tips
    *  Import in module
    *
    */
    $tips = array(
      'labels' => array (
          'name' => 'Porady',
          'sungular_name' => 'Porady',
          'all_items' => 'Wszystkie',
          'add_new' => 'Dodaj',
          'add_new_item' => 'Dodaj',
          'edit_item' => 'Edytuj',
          'new_item' => 'Nowy',
          'view_item' => 'Zobacz',
          'search_items' => 'Szukaj',
          'not_found' => 'Nie znaleziono',
          'not_found_in_trash' => 'Nie znaleziono w koszu',
          'parent_item_colon' => ''
      ),
      'public' => true,
      'public_queryable' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'tips'),
      'capatility_type' => 'post',
      'hierarchical' => true,
      'menu_position' => 5,
      'supports' => array(
          'title', 'editor', 'author', 'thumbnail'
      ),
    );
    register_post_type('tips', $tips);

    /**
    *  Register training
    *  Import in module
    *
    */
		$training = array(
	      'labels' => array (
	          'name' => 'Szkolenia',
	          'sungular_name' => 'Szkolenia',
	          'all_items' => 'Wszystkie',
	          'add_new' => 'Dodaj',
	          'add_new_item' => 'Dodaj',
	          'edit_item' => 'Edytuj',
	          'new_item' => 'Nowy',
	          'view_item' => 'Zobacz',
	          'search_items' => 'Szukaj',
	          'not_found' => 'Nie znaleziono',
	          'not_found_in_trash' => 'Nie znaleziono w koszu',
	          'parent_item_colon' => ''
	      ),
	      'public' => true,
	      'public_queryable' => true,
	      'show_ui' => true,
	      'query_var' => true,
	      'rewrite' => array('slug' => 'szkolenia'),
	      'capatility_type' => 'post',
	      'hierarchical' => true,
	      'menu_position' => 5,
	      'supports' => array(
	          'title', 'editor', 'author', 'thumbnail'
	      ),
	    );
	    register_post_type('training', $training);
  }
?>
