<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */

function displayProductLink() {

	echo '<a class="m-btn m-btn_secondary form-btn clearfix t-dni" style="border: 2px solid" href="/">Wróć na stronę</a>';

	$trainer = $_GET['trainer'];
	$type = $_GET['type'];
	$duration = $_GET['duration'];
	$freq = $_GET['freq'];
	$answers = array(
		'dawidwozniakowski' => array(
			'regular_trainings' => array(
				'1' => 1735,
				'3' => 1787,
				'6' => 1789,
				'12' => 1790
			),
			'consultaiton_online' => array(
				'1' => array(
					'diet' => 1807,
					'trainings' => 1812,
					'aio' => 1813,
				),
				'3' => array(
					'diet' => 1808,
					'trainings' => 1811,
					'aio' => 1814,
				),
				'5' => array(
					'diet' => 1809,
					'trainings' => 1810,
					'aio' => 1815,
				),
			), 
			'consultation_live' => array(
				'1' => array(
					'diet' => 1797,
					'trainings' => 1801,
					'aio' => 1804,
				),
				'3' => array(
					'diet' => 1798,
					'trainings' => 1802,
					'aio' => 1805,
				),
				'5' => array(
					'diet' => 1799,
					'trainings' => 1803,
					'aio' => 1806,
				)
			),
		),		
		'martynawojcik' => array(
			'regular_trainings' => array(
				'1' => 1992,
				'3' => 2182,
				'6' => 2183,
				'12' => 2184
			),
			'consultaiton_online' => array(
				'1' => array(
					'diet' => 2004,
					'trainings' => 2005,
					'aio' => 2006,
				),
				'3' => array(
					'diet' => 2185,
					'trainings' => 2195,
					'aio' => 2194,
				),
				'5' => array(
					'diet' => 2187,
					'trainings' => 2196,
					'aio' => 2193,
				),
			), 
			'consultation_live' => array(
				'1' => array(
					'diet' => 2000,
					'trainings' => 2001,
					'aio' => 2003,
				),
				'3' => array(
					'diet' => 2185,
					'trainings' => 2190,
					'aio' => 2191,
				),
				'5' => array(
					'diet' => 2187,
					'trainings' => 2189,
					'aio' => 2192,
				)
			),
		),
	);

	foreach ( $answers as $answer => $trainerID ) {
		if ($answer == $trainer) {

			foreach ( $trainerID as $answer => $typeID ) {

				if($type == 'regular_trainings') {
					echo '<a class="m-btn m-btn_primary form-btn clearfix t-dni" href="/koszyk/?add-to-cart=' . $typeID[$duration] . '&quantity=' . $freq . '">Opłać pakiet</a>';
					return;
				}

				if ($answer == $type) {

					foreach ( $typeID as $answer => $durationID ) {
						if ($answer == $duration) {

							foreach ( $durationID as $answer => $freqID ) {
								if ($answer == $freq) {

									 echo '<a class="m-btn m-btn_primary form-btn clearfix fw t-dni" href="/koszyk/?add-to-cart=' . $freqID . '">Opłać pakiet</a>';

								}
							}
							 
						}
					}
	 
				} 
			}

		}
	}

}

?>