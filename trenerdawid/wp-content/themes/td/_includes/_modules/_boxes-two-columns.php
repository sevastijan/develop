<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_blog_shortcode' );
function vc_boxes_columns_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_columns'] = array(
     'name' =>'[VC] Two Columns',
     'base' => 'vc_two_columns',
     'category' => 'Content',
     'description' => 'Display Columns',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_columns', 'vc_boxes_columns_render' );
function vc_boxes_columns_render($atts) {
  $atts = shortcode_atts(
          array(
            'url_1' => "",
            'url_2' => ""
          ), $atts, 'vc_boxes_columns');
  $output = '
    <section class="b-container b-two-columns clearfix">
      <div class="b-boxes">
        <div class="b-box_solo">
          <div class="b-box_wrapper">
            <h2 class="m-typo m-typo_primary">
              Poznaj historie
              <br>mojej metamorfozy
            </h2>
            <a href="' . $atts['url_1'] . '" class="m-btn m-btn_tertiary">' . pll__('td.hero.see-more') . '</a>
          </div>
        </div>
        <div class="b-box_solo">
          <div class="b-box_wrapper">
            <h2 class="m-typo m-typo_secondary">
              Wiesz czym jest<br>dieta ketogeniczna?
            </h2>
              <a href="' . $atts['url_2'] . '" class="m-btn m-btn_primary">' . pll__('td.hero.see-more') . '</a>
          </div>
        </div>
      </div>
    </section>'; 
    echo $output;
} ?>
