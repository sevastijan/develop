<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_blog_shortcode' );
function vc_boxes_testimonial_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_testimonial'] = array(
     'name' =>'[VC] Testimonials',
     'base' => 'vc_testimonials',
     'category' => 'Content',
     'description' => 'Display testimonials',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_testimonial', 'vc_boxes_testimonial_render' );
function vc_boxes_testimonial_render() {
  global $post;

  $testimonial = new WP_Query(array(
    'post_type' => 'testimonials',
    'posts_per_page' => -1
  ));
  if($testimonial->have_posts()) :
    $output = '
      <div class="b-container m-testimonials clearfix">
        <a href="o-mnie/rekomendacje/">
          <div class="m-testimonials_describe">
            <h3 class="m-typo m-typo_primary">
               ' . pll__('td.testimonial.describe') . '
            </h3>
          </div>
        </a>
        <div id="js-testimonials_slider" class="m-testimonials_slider">';
    while($testimonial->have_posts()) : $testimonial->the_post();
      $output .=' <div class="m-testimonials_item">';
           if(has_post_thumbnail()) :
             $output .='<div class="m-testimonials_itemPhoto">
              ' . get_the_post_thumbnail() . '
            </div>';
          endif;
          if(!has_post_thumbnail()) : 
              $output .='<div class="m-testimonials_itemDescribe is-noPhoto">' ;
          else:
              $output .='<div class="m-testimonials_itemDescribe">';
          endif;
          $output .= get_the_excerpt_max_charlength(1000) . '<div class="author">' . get_field('author') . '</div></div></div>';
    endwhile;
        $output .='</div>
        <div id="js-testimonials_navi" class="m-testimonials_navi"></div>
      </div>';
      echo $output;
    endif;
} ?>
