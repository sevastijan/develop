  <?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *Template Name: Pojedynczy widok bloga
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<?php require_once(THEME_DIR . 'header-white.php'); ?>

<br>  <br>
<div class="b-container subpage_blog-single">

<div class="m-two-boxes heading">
  <div class="boxes-wrapper">
    <div class="box">
      <div class="slider-wrapper">
        <div class="slider slider-single">
          <div class="slider-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
          </div>
        </div>
        <!-- <div class="slider slider-nav">
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
          <div class="slider-image" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-single-large-slider.png);">
          </div>
        </div> -->
      </div>
    </div>
    <div class="box">
      <div class="m-typo m-typo_text">
        <?php
          the_content();
        ?>
      </div>
      <div class="rate">
        <p class="m-typo m-typo_text">
          Poziom trudności
        </p>
        <?php 

          $rate = get_field('rate');
          $scale = 5;

          $totalRate = $scale - $rate;
          for($i = 1; $i <= $rate; $i++) {
            echo '<span class="star checked"></span>';
          }        
          for($i = 1; $i <= $totalRate; $i++) {
            echo '<span class="star"></span>';
          }
        ?>
      </div>
      <div class="m-tags">
        <p class="m-typo m-typo_text">
          Tagi: <span><?php the_tags(''); ?></span>
        </p>
      </div>
      <?php if(get_field('print')) : ?>
           <a href="<?php the_field('print'); ?>" class="m-btn m-btn_primary">
             Wydrukuj przepis
           </a>
        <?php endif; ?>
    </div>
  </div>
</div>


<br>
<br>
<br>

<div class="m-two-boxes">
  <div class="boxes-wrapper">
    <div class="box">
      <h6 class="m-typo m-typo_secondary">
        Składniki odżywcze
      </h6>
      <table class="table">
        <tr class="table_item">
          <th class="m-typo m-typo_text">Białko</th>
          <th class="m-typo m-typo_text">
            <?php the_field('protein'); ?>
          </th>
        </tr>
        <tr class="table_item">
          <th class="m-typo m-typo_text">Tłuszcz</th>
          <th class="m-typo m-typo_text">
            <?php the_field('fat'); ?>
          </th>
        </tr>
        <tr class="table_item">
          <th class="m-typo m-typo_text">Węglowodany</th>
          <th class="m-typo m-typo_text">
            <?php the_field('carbohydrates'); ?>
          </th>
        </tr>
        <tr class="table_item">
          <th class="m-typo m-typo_text">Kalorie</th>
          <th class="m-typo m-typo_text">
            <?php the_field('calories'); ?>
          </th>
        </tr>
      </table>
      <h6 class="m-typo m-typo_secondary">
        Składniki
      </h6>

      <?php if( have_rows('ingredients') ): ?>
        <ul class="list">
        <?php $index = 0; while( have_rows('ingredients') ): the_row();
          ?>
          <li>
            <input type="checkbox" id="component-<?php echo $index; ?>">
            <label for="component-<?php echo $index; ?>" class="list_item m-typo m-typo_text"><?php the_sub_field('text'); ?></label>
          </li>
        <?php $index++; endwhile; ?>
        </ul>
      <?php endif; ?>


    </div>
    <div class="box">
      <h6 class="m-typo m-typo_secondary">
        Sposób przygotowania
      </h6>

      <?php if( have_rows('preparation') ): ?>
      	<ul class="list">
      	<?php while( have_rows('preparation') ): the_row();
      		?>
          <li class="list_item m-typo m-typo_text">
            <?php the_sub_field('text'); ?>
          </li>
      	<?php endwhile; ?>
      	</ul>
      <?php endif; ?>

    </div>
  </div>
</div>

<div class="m-bar">
  <div class="boxes-wrapper">
    <div class="box prev">
			<?php
			$prev_post = get_previous_post();
			if (!empty( $prev_post )): ?>
      <a href="<?php echo $prev_post->guid ?>">
        <img class="box_arrow" src="<?php echo THEME_URL; ?>assets/images/bar-arrow.png" alt="arrow">
        <p class="m-typo m-typo_text">
          Poprzedni wpis <br> <span><?php echo $prev_post->post_title ?></span>
        </p>
      </a>
			<?php endif ?>
    </div>
    <div class="box next">
			<?php
			$next_post = get_next_post();
			if (!empty( $next_post )): ?>
	      <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
	        <img class="box_arrow" src="<?php echo THEME_URL; ?>assets/images/bar-arrow.png" alt="arrow">
	        <p class="m-typo m-typo_text">
	          Następny wpis <br> <span><?php echo esc_attr( $next_post->post_title ); ?></span>
	        </p>
	      </a>
			<?php endif; ?>
    </div>
  </div>
</div>
<?php if (comments_open()) : ?>
  <div class="b-container mb50">
    <div id="disqus_thread"></div>
    <script>
        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
         */
        
        var disqus_config = function () {
            this.page.url = 'http://dawidwozniakowski.pl';  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = <?php global $post; echo $post->ID; ?> // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        
        (function() {  // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            
            s.src = '//http-dawidwozniakowski-pl.disqus.com/embed.js';
            
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
  </div>
<?php endif; ?>
<div class="m-blog-posts">
  <h1 class="m-typo m-typo_secondary">
    Zobacz także inne wpisy z mojego bloga
  </h1>
  <div class="posts-wrapper">

		<?php
		$blog = new WP_Query(array(
		    'post_type' => 'post',
		    'posts_per_page' => 3
		  ));
		 ?>
		 <?php while($blog->have_posts()) : $blog->the_post();?>
      <div class="post">
       	<a href="<?php the_permalink(); ?>">
    			<div class="post_img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
    		</a>
    		<a href="<?php the_permalink(); ?> ">
    			<h1 class="m-typo m-typo_primary">
    				<?php the_title(); ?>
          </h1>
         </a>
         <p class="m-typo m-typo_text">
					 <?php the_excerpt_max_charlength(150); ?>
         </p>
         <div class="date">
           <?php echo get_the_date('F j, Y'); ?>
         </div>
      </div>
		<?php endwhile;?>

    </div>
  </div>

</div>
<br>
<br>