<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_tips_shortcode' );
function vc_boxes_tips_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_tips'] = array(
     'name' =>'[VC] Tips boxes',
     'base' => 'vc_tips',
     'category' => 'Content',
     'description' => 'Display tips',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_tips', 'vc_boxes_tips_render' );
function vc_boxes_tips_render() {

  global $post;

  $tips = new WP_Query(array(
    'post_type' => 'tips',
    'posts_per_page' => 6
  ));

  if($tips->have_posts()) :

    $output = '<section class="b-container b-tips clearfix">';
    $index = 0;

    while($tips->have_posts()) : $tips->the_post();

      if($index == 0) {
        $output .= '<div class="b-tips-cols">
            <div class="b-tip_col-1 b-tip-col">
          <ul class="b-tip-boxes">';
      } 
      $output .= '<li class="b-tip-box">
          <a href="' . get_field('link')  . '">
            <img class="m-tip-icon" src="' . get_the_post_thumbnail_url() . '" alt="">
          </a>
          <div class="m-tip-text">
            <a href="' . get_field('link') . '">
              <h6 class="m-typo m-typo_heading">' . get_the_title() . '</h6>
            </a>
            <p class="m-typo m-typo_paragraph">
              ' . get_the_excerpt_max_charlength(90) . '
            </p>
          </div>
        </li>';

      if($index == 2) {
        $output .='</ul>
            </div>
          <div class="b-tip_col-2 b-tip-col">
          <ul class="b-tip-boxes">';
      }
      if($index == 5) {
        $output .='</ul></div>';
      }
      
      $index++;

    endwhile;
    
    $output .= '<a href="/oferta" class="m-btn m-btn_primary">Sprawdź ofertę</a></section>';
    
    echo $output;
  
  endif;

} ?>