<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_camps_shortcode' );
function vc_boxes_camps_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_camps'] = array(
     'name' =>'[VC] Blog Boxes',
     'base' => 'vc_say_hello',
     'category' => 'Content',
     'description' => 'Display last 4 posts from blog',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_camps', 'vc_boxes_camps_render' );
function vc_boxes_camps_render($atts) {
    $atts = shortcode_atts(
          array(
            'id' => ""
          ), $atts, 'vc_boxes_camps');

  global $post;

  if(!empty($atts['id'])) {
    $camps = new WP_Query(array(
      'p' => $atts['id'],
      'post_type' => 'camps',
      'posts_per_page' => 1
    ));
  } else {
    $camps = new WP_Query(array(
      'post_type' => 'camps',
      'post_status' => 'publish',
      'posts_per_page' => 3
    ));
  }

  if($camps->have_posts()) :

    $output = '
      <section class="b-container b-camps clearfix">
          <div class="b-camps_heading">
            <h2 class="m-typo m-typo_secondary">
              ' . pll__('td.camps_title') . '
            </h2>
            <p class="m-typo m-typo_text">
              ' . pll__('td.camps_title_excerpt') . '
            </p>
          </div>
          <div class="b-camps_boxes">';
    while($camps->have_posts()) : $camps->the_post();
        $output .='<div class="m-camps-box m-camps-box_primary">
          <div class="m-camps-box_image" style="background-image: url(' . get_the_post_thumbnail_url() . ');">
            <div class="m-camps-box">
              <h6 class="m-typo m-typo_primary">
                ' . pll__('td.camps_flight') . ': ' . get_field('flight') . '
              </h6>
            </div>
            <div class="m-camps_top-bar active">';
          if(get_field('label')) :
            $output .='<h6 class="m-typo m-typo_primary">
              ' . get_field('label') . '
            </h6>';
          endif;
          $output .='</div>
              </div>
            <h6 class="m-typo m-typo_secondary">
              ' . get_the_title() . '
            </h6>
            <a href="' . get_the_permalink() . '" class="m-btn m-btn_quaternary">'. pll__('td.blog_read_more') .'</a>
          </div>';
    endwhile;
    $output .= '</div>
      </section>';

  echo $output;

  endif;

}
