<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<footer class="b-container b-footer clearfix">
  <div class="b-columns-3 clearfix">
    <h3 class="m-typo m-typo_secondary js-collapseTrigger"><?php pll_e('td.footer.column-1'); ?></h3>
    <div class="b-collapse js-collapse">
      <?php //TODO: Implement wp_nav_menu with custom walker ?>
      <div class="b-columns-2">
        <?php custom_menu('footer_first','m-menu'); ?>
      </div>
      <div class="b-columns-2">
        <?php custom_menu('footer_second','m-menu'); ?>
      </div>
    </div>
  </div>
  <div class="b-columns-3">
    <h3 class="m-typo m-typo_secondary js-collapseTrigger"><?php pll_e('td.footer.column-2'); ?></h3>
    <div class="b-collapse js-collapse">
      <div class="m-typo m-typo_text">
        <?php if(get_field('footer_describe', 'options')) : ?>
          <?php the_field('footer_describe', 'options'); ?>
        <?php else: ?>
          <?php pll_e('td.footer.newsletter-default'); ?>
        <?php endif; ?>
        <?php //TODO: implement newsletter form ?>
      </div>
      <form class="m-form" action="" method="post">
        <fieldset>
          <input type="text" name="" value="" placeholder="Imię i nazwisko" class="m-form_input">
          <input type="text" name="" value="" placeholder="Adres email" class="m-form_input">
          <input type="submit" name="" value="Wyślij" class="m-btn m-btn_primary">
        </fieldset>
      </form>
    </div>
  </div>
  <div class="b-columns-3" style="margin-bottom: 0;">
    <h3 class="m-typo m-typo_secondary js-collapseTrigger"><?php pll_e('td.footer.column-3'); ?></h3>
    <div class="b-collapse js-collapse">
      <ul class="list">
        <li class="list_item">
          <div class="wrapper">
            <a href="https://web.facebook.com/DawidWozniakowskiFanpage/" target="_blank">
              <img src="<?php echo THEME_URL; ?>/assets/images/facebook-footer.png" alt="facebook-icon">
              <p class="m-typo m-typo_text">
                Facebook
              </p>
            </a>
          </div>
        </li>
        <li class="list_item">
          <div class="wrapper">
            <a href="viber://+48728522728">
              <img src="<?php echo THEME_URL; ?>/assets/images/viber-footer.png" alt="facebook-icon">
              <p class="m-typo m-typo_text">
                Viber
              </p>
            </a>
          </div>
        </li>
        <li class="list_item">
          <div class="wrapper">
            <a href="https://www.instagram.com/dawidwozniakowski/" target="_blank">
              <img src="<?php echo THEME_URL; ?>/assets/images/insta-footer.png" alt="facebook-icon">
              <p class="m-typo m-typo_text">
                Instagram
              </p>
            </a>
          </div>
        </li>
        <li class="list_item">
          <div class="wrapper">
            <a href="skype://kontakt@dawidwozniakowski.pl">
              <img src="<?php echo THEME_URL; ?>/assets/images/skype-footer.png" alt="facebook-icon">
              <p class="m-typo m-typo_text">
                Skype
              </p>
            </a>
          </div>
        </li>
        <li class="list_item">
          <div class="wrapper">
            <a href="whatsapp://+48728522728">
              <img src="<?php echo THEME_URL; ?>/assets/images/what-footer.png" alt="facebook-icon">
              <p class="m-typo m-typo_text">
                Whatsapp
              </p>
            </a>
          </div>
        </li>
        <li class="list_item">
          <div class="wrapper">
            <a href="signal://+48728522728">
              <img src="<?php echo THEME_URL; ?>/assets/images/signal-footer.png" alt="facebook-icon">
              <p class="m-typo m-typo_text">
                Signal
              </p>
            </a>
          </div>
        </li>
      </ul>
    </div>
    <h3 class="m-typo m-typo_secondary js-collapseTrigger">Dane adresowe</h3>
    <div class="b-collapse js-collapse">
      <div class="m-typo m-typo_text">
        <span style="font-weight: 600">Dawid Woźniakowski</span><br>
        ul. Dembego 23a / 49 <br>
        02-796 Warszawa <br>
        NIP: 951-224-20-76 <br>
        REGON: 142755317 <br><br>
        Dostępność: 8.00 - 21.00 <br>
      </div>
    </div>
  </div>
</footer>
