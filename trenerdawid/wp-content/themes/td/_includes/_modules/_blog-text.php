

<div class="b-container subpage_blog-single-text">

<div class="m-two-boxes">
  <div class="boxes-wrapper">
    <div class="box">
      <div class="image-wrapper">
        <div class="image-wrapper_img"  style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
        <div class="date">
					<?php echo get_the_date('F j, Y'); ?>
        </div>
      </div>
    </div>
    <div class="box">
      <div class="author">
        <div class="author_img" style="background-image: url(<?php echo THEME_URL; ?>/assets/images/blog-avatar.png);"></div>
        <h6 class="m-typo m-typo_text">
					<?php
						echo get_the_author_meta('display_name');
					 ?>
        </h6>
      </div>
      <div class="category">
        <h6 class="m-typo m-typo_text">
          <span>Kategoria:</span> 
					<?php
				    foreach((get_the_category()) as $category){
				        echo $category->name;
                if(count(get_the_category()) > 1) {
                  echo ", ";
                }
				      }
				    ?>
        </h6>
      </div>
    </div>
  </div>
</div>

<div class="m-ten-boxes">
<?php the_content(); ?>
</div>

<br><br><br>

<div class="m-tags">
  <p class="m-typo m-typo_text">
    Tagi: <span><?php the_tags(''); ?></span>
  </p>
</div>

<br><br><br>

<div class="m-bar">
  <div class="boxes-wrapper">
    <div class="box prev">
			<?php
			$prev_post = get_previous_post();
			if (!empty( $prev_post )): ?>
      <a href="<?php echo $prev_post->guid ?>">
        <img class="box_arrow" src="<?php echo THEME_URL; ?>assets/images/bar-arrow.png" alt="arrow">
        <p class="m-typo m-typo_text">
          Poprzedni wpis <br> <span><?php echo $prev_post->post_title ?></span>
        </p>
      </a>
			<?php endif ?>
    </div>
    <div class="box next">
			<?php
			$next_post = get_next_post();
			if (!empty( $next_post )): ?>
	      <a href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
	        <img class="box_arrow" src="<?php echo THEME_URL; ?>assets/images/bar-arrow.png" alt="arrow">
	        <p class="m-typo m-typo_text">
	          Następny wpis <br> <span><?php echo esc_attr( $next_post->post_title ); ?></span>
	        </p>
	      </a>
			<?php endif; ?>
    </div>
  </div>
</div>
<?php if (comments_open()) : ?>
  <div class="b-container mb50">
    <div id="disqus_thread"></div>
    <script>
        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
         */
        
        var disqus_config = function () {
            this.page.url = 'http://dawidwozniakowski.pl';  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = <?php global $post; echo $post->ID; ?> // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        
        (function() {  // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            
            s.src = '//EXAMPLE.disqus.com/embed.js';
            
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
  </div>
<?php endif; ?>
<div class="m-blog-posts">
  <h1 class="m-typo m-typo_secondary">
    Zobacz także inne wpisy z mojego bloga
  </h1>



  <div class="posts-wrapper">

		<?php
		$blog = new WP_Query(array(
        'orderby' => 'rand',
		    'post_type' => 'post',
		    'posts_per_page' => 3
		  ));
		 ?>
		 <?php while($blog->have_posts()) : $blog->the_post();?>
      <div class="post">
       	<a href="<?php the_permalink(); ?>">
    			<div class="post_img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);"></div>
    		</a>
    		<a href="<?php the_permalink(); ?>">
    			<h1 class="m-typo m-typo_primary">
    				<?php the_title(); ?>
          </h1>
         </a>
         <p class="m-typo m-typo_text">
					 <?php the_excerpt_max_charlength(150); ?>
         </p>
         <div class="date">
           <?php echo get_the_date('F j, Y'); ?>
         </div>
      </div>
		<?php endwhile;?>

    </div>
  </div>


</div>
<br>
<br>
