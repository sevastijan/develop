<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_banners_shortcode' );
function vc_boxes_banners_shortcode( $shortcodes ) {
   $shortcodes['vc_banners'] = array(
     'name' =>'[VC] Banners',
     'base' => 'vc_banners',
     'category' => 'Content',
     'description' => 'Display banners',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_banners', 'vc_boxes_banners_render' );
function vc_boxes_banners_render() {
 $output = ' <div class="b-container m-banners clearfix">
  <div class="m-four-boxes">
    <div class="box" style="background-image: url('.THEME_URL.'assets/images/banners-img0.png);">
      <p class="m-typo m-typo_secondary">
        #Mega oferta
      </p>
      <div class="text-wrapper">
        <h1 class="m-typo m-typo_primary">
          Konsualtcje online do 50% taniej!
        </h1>
        <p class="m-typo m-typo_text">
          Skorzystaj z mojej nowej oferty planów treningowych i dietetycznych przy wspołpracy online i zaoszczędź nawet do 50%!
        </p>
      </div>
      <a href="https://dawidwozniakowski.pl/oferta/wspolpraca-dlugookresowa/konsultacje-on-line/" class="m-btn m-btn_primary">Dowiedz się wiecej</a>
    </div>
    <div class="box" style="background-image: url('.THEME_URL.'assets/images/banners-img4.jpg);">
      <a href="#">
        <div class="text-wrapper">
          <h3 class="m-typo m-typo_primary">
            Plany <br>
            Treningowe
          </h3>
          <p class="m-typo m-typo_text">
            Już od <span>99</span> zł
          </p>
        </div>
      </a>
    </div>
    <div class="box" style="background-image: url('.THEME_URL.'assets/images/banners-img2.png);">
      <a href="#">
        <div class="text-wrapper">
          <h3 class="m-typo m-typo_primary">
            Plany <br>
            Dietetyczne
          </h3>
          <p class="m-typo m-typo_text">
            Już od <span>149</span> zł
          </p>
        </div>
      </a>
    </div>
    <div class="box" style="background-image: url('.THEME_URL.'assets/images/banners-img3.png);">
      <a href="#">
        <div class="text-wrapper">
          <h3 class="m-typo m-typo_primary">
            Plany <br>
            Suplementacyjne
          </h3>
          <p class="m-typo m-typo_text">
            Już od <span>49</span> zł
          </p>
        </div>
      </a>
    </div>
  </div>
  <div class="m-two-boxes">
    <div class="payment-info boxes-wrapper">
      <h1 class="m-typo m-typo_primary">
        Płać tak jak ci wygodnie
      </h1>
      <p class="m-typo m-typo_text">
        Akceptuję zarówno płatności kartą, paypal oraz przelewy internetowe
      </p>
      <div class="logotypes">
        <img class="logo" src="'.THEME_URL.'assets/images/payu-logo.png" alt="logo">
        <img class="logo" src="'.THEME_URL.'assets/images/visa-logo.png" alt="logo">
        <img class="logo" src="'.THEME_URL.'assets/images/ppal-logo.png" alt="logo">
      </div>
      <div class="box">
        <a href="https://dawidwozniakowski.pl/oferta/rozpiski/" class="m-btn m-btn_secondary">Plany i rozpiski</a>
      </div>
      <div class="box">
        <a href="https://dawidwozniakowski.pl/kontakt/formularz/" class="m-btn m-btn_secondary">formularz online</a>
      </div>
    </div>
  </div>
</div>';
  echo $output;
} ?>
