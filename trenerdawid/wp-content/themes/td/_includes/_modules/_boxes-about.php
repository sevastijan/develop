<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_blog_shortcode' );
function vc_boxes_metamorphosis_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_metamorphosis'] = array(
     'name' =>'[VC] Metamorphosis',
     'base' => 'vc_metamorphosis',
     'category' => 'Content',
     'description' => 'Display metamorphosis',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_metamorphosis', 'vc_boxes_metamorphosis_render' );
function vc_boxes_metamorphosis_render() {
 $output = '
  <section class="b-container b-about  clearfix">
      <div class="b-about_heading">
        <h2 class="m-typo m-typo_primary">
          <a class="m-typo m-typo_primary" target="_blank" href="/o-mnie/moja-historia/">
            ' . pll__('td.metamorphosis_myHistory') . '
          </a>
        </h2>
      </div>
      <div class="b-about_boxes">
        <a target="_blank" href="/o-mnie/moja-historia/">
          <div class="b-about-box b-about-box_primary">
            <h6 class="m-typo m-typo_primary m-typo_heading">
            ' . pll__('td.metamorphosis_despiteObesity') . '
            </h6>
            <div class="m-about-box">
              <p class="m-typo m-typo_primary m-about-box_text">
                2006
              </p>
            </div>
          </div>
          <div class="b-about-box b-about-box_secondary">
            <h6 class="m-typo m-typo_primary m-typo_heading m-typo_heading_secondary">
            ' . pll__('td.metamorphosis_despiteHormonalDisorders') . '
            </h6>
            <div class="m-about-box">
              <p class="m-typo m-typo_primary m-about-box_text">
                2010
              </p>
            </div>
          </div>
          <div class="b-about-box b-about-box_tertiary">    
            <h6 class="m-typo m-typo_primary m-typo_heading">
            ' . pll__('td.metamorphosis_despiteMetabolicDisease') . '
            </h6>
            <div class="m-about-box">
              <p class="m-typo m-typo_primary m-about-box_text">
                2014
              </p>
            </div>
          </div>
        </a>
      </div>
  </section>';
  echo $output;
} ?>
