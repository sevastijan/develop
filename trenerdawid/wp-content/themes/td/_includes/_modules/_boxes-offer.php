<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_blog_shortcode' );
function vc_boxes_offer_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_offer'] = array(
     'name' =>'[VC] Offer',
     'base' => 'vc_offer',
     'category' => 'Content',
     'description' => 'Display offer',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_offer', 'vc_boxes_offer_render' );
function vc_boxes_offer_render($atts) {
  $atts = shortcode_atts(
          array(
            'tab_1' => "",
            'tab_2' => "",
            'tab_3' => "",
            'tab_4' => ""
          ), $atts, 'vc_boxes_offer');
  $output ='<div class="b-container m-offer clearfix">
      <div class="content-wrapper">
        <div class="m-typo m-typo_primary">
          ' . pll__('td.offer.headline') . '
        </div>
        <div class="m-tabs">';
    if($atts['tab_1']) :
      $output .= '<div id="tabs-1" class="m-tab">
        <div class="m-typo m-typo_text">
          ' . $atts['tab_1'] . '
        </div>
      </div>';
    endif;
    if($atts['tab_2']) :
      $output .= '<div id="tabs-2" class="m-tab">
        <div class="m-typo m-typo_text">
          ' . $atts['tab_2'] . '
        </div>
      </div>';
    endif;
    if($atts['tab_3']) :
      $output .= '<div id="tabs-3" class="m-tab">
        <div class="m-typo m-typo_text">
          ' . $atts['tab_3'] . '
        </div>
      </div>';
    endif;
    if($atts['tab_4']) :
      $output .= '<div id="tabs-4" class="m-tab">
        <div class="m-typo m-typo_text">
          ' . $atts['tab_4'] . '
        </div>
      </div>';
    endif;
    $output .='<ul class="m-tabs_nav clearfix">
          <li><a href="/dieta" class="m-btn m-btn_tertiary">' . pll__('td.offer.diet') . '</a></li>
          <li><a href="/trening" class="m-btn m-btn_tertiary">' . pll__('td.offer.training') . '</a></li>
          <li><a href="/zdrowie" class="m-btn m-btn_tertiary">' . pll__('td.offer.health') . '</a></li>
          <li><a href="/blog" class="m-btn m-btn_tertiary">' . pll__('td.offer.blog') . '</a></li>
        </ul>
    </div>
  </div>
</div>';
echo $output;
} ?>
