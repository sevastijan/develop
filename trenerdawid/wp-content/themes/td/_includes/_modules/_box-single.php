<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_column_shortcode' );
function vc_boxes_column_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_column'] = array(
     'name' =>'[VC] Blog Boxes',
     'base' => 'vc_say_hello',
     'category' => 'Content',
     'description' => 'Display sidebar widget',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_column', 'vc_boxes_column_render' );
function vc_boxes_column_render($atts) {
    $atts = shortcode_atts(
    array(
      'title' => "",
      'url' => "",
    ), $atts, 'vc_boxes_features');

  $output = '
    <div class="b-four-boxes is-singleWidget">
      <div class="b-columns-2">
        <div class="b-box b-box_quaternary">
          <a href="' . $atts['url'] . '">
            <div class="info-box">
              <h6 class="m-typo m-typo_primary">
                ' . $atts['title'] . '
              </h6>
            </div>
          </a>
          <div class="b-box_img" style="background-image: url('. THEME_URL .'assets/images/box-fourth.jpg)"></div>
        </div>
      </div>
    </div>';
  echo $output;
} ?>
