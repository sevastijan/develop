<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_blog_shortcode' );
function vc_boxes_blog_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_blog'] = array(
     'name' =>'[VC] Blog Boxes',
     'base' => 'vc_say_hello',
     'category' => 'Content',
     'description' => 'Display last 4 posts from blog',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_blog', 'vc_boxes_blog_render' );
function vc_boxes_blog_render() {

  global $post;

  $blog = new WP_Query(array(
    'post_type' => 'post',
    'posts_per_page' => 4
  ));

  if($blog->have_posts()) :

    $output = '
    <section class="b-container b-blog  clearfix">
        <div class="b-blog_heading">
          <h2 class="m-typo m-typo_secondary">
           ' . pll__('td.blog_title') . '
          </h2>
          <p class="m-typo m-typo_text">
            ' . pll__('td.blog_title_excerpt') . '
          </p>
        </div>
        <div class="b-blog_boxes">';
    while($blog->have_posts()) : $blog->the_post();
      $category_detail = get_the_category($post->ID);
      $output .= '<div class="m-blog-box b-columns-2">
        <div class="m-blog-box_image" style="background-image: url(' . get_the_post_thumbnail_url() . ');"></div>
        <div class="m-blog-box_content">
          <h6 class="m-typo m-typo_secondary">
            ' . get_the_title()  . '
          </h6>
          <p class="m-typo m-typo_text">
            ' . get_the_excerpt_max_charlength(170) . '
          </p>
          <div class="m-blog-box_bar">
            <div class="m-blog-box_primary">
              <div class="m-blog-box_info">
                <span class="m-typo m-typo_text">
                  ' . get_the_date() . ' | ' . $category_detail[0]->name . '
                </span>
              </div>
            </div>
            <div class="m-blog-box_secondary">
              <a href="' . get_the_permalink() . '" class="m-btn m-btn_quaternary">'. pll__('td.blog_read_more') .'</a>
            </div>
          </div>
        </div>
      </div>';
    endwhile;
    $output .= '</div>
        <div class="b-blog_button">
          <a href="' . esc_url(home_url('/blog')) . '" class="m-btn m-btn_primary"> ' . pll__('td.blog_go_to_listing') . '</a>
        </div>
      </section>';
    echo $output;
  endif;
}
?>
