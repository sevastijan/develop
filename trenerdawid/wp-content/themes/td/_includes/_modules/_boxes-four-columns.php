<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php 
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_four_columns_shortcode' );
function vc_boxes_four_columns_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_four_columns'] = array(
     'name' =>'[VC] Blog Boxes',
     'base' => 'vc_say_hello',
     'category' => 'Content',
     'description' => 'Display last 4 posts from blog',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_four_columns', 'vc_boxes_four_columns_render' );
function vc_boxes_four_columns_render() {
  $output = '
    <section class="b-container  clearfix">
      <div class="b-four-boxes">
        <div class="b-columns-2">
          <div class="b-box b-box_primary">
            <div class="info-box">
              <h6 class="m-typo m-typo_primary">
                ' . get_field('title') . '
              </h6>
              <p class="m-typo m-typo_text">
                ' . get_field('excerpt') . '
              </p>
              <a href="' . get_field('url') . '" class="m-btn m-btn_quaternary">' . pll__('td.hero.see-more') . '</a>
            </div>
            <div class="b-box_img" style="background-image: url(' . get_field('photo') . ');"></div>
          </div>
          <div class="b-box b-box_secondary" style="background-image: url(' . get_field('bphoto') . ');">
            <div class="info-box">
              <h6 class="m-typo m-typo_primary">
                ' . get_field('btitle') . '
              </h6>
              <p class="m-typo m-typo_text">
                ' . get_field('bexcerpt') . '
              </p>
              <a href="' . get_field('burl') . '" class="m-btn m-btn_quaternary">' . pll__('td.hero.see-more') . '</a>
            </div>
          </div>
        </div>
        <div class="b-columns-2">
          <div class="b-box b-box_tertiary" style="background-image: url(' . get_field('bpphoto') . ');">
          </div>
          <div class="b-box b-box_quaternary">
            <a href="' . get_field('bplink') . '">
              <div class="info-box">
                <h6 class="m-typo m-typo_primary">
                  ' . get_field('bptitle') . '
                </h6>
              </div>
            </a>
            <div class="b-box_img" style="background-image: url(' . get_field('spphoto') . ');"></div>
          </div>
        </div>
      </div>
    </section>';
    echo $output; 
} ?>