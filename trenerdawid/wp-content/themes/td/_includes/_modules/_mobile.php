<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<div id="js-mobile" class="m-mobile clearfix">
  <header class="m-mobile_header clearfix">
    <div class="m-logo">
      <a href="#">
        <img src="<?php echo THEME_URL; ?>/assets/images/logo.png" alt="">
      </a>
    </div>
    <div class="m-burger js-burger is-close">
      <span class="m-burger_item"></span>
      <span class="m-burger_item"></span>
      <span class="m-burger_item"></span>
    </div>
  </header>
    <?php custom_menu('main_menu','m-menu'); ?>
</div>
