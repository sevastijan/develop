<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_plans_shortcode' );
function vc_boxes_plans_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_plans'] = array(
     'name' =>'[VC] Plans Boxes',
     'base' => 'vc_plans',
     'category' => 'Content',
     'description' => 'Display 3 plans',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_plans', 'vc_boxes_plans_render' );
function vc_boxes_plans_render() {

  global $post;

  $plans = new WP_Query(array(
    'post_type' => 'training',
    'posts_per_page' => 3,
    'post_status' => 'publish'
  ));

  if($plans->have_posts()) :

    $output = '
      <section class="b-container b-plans  clearfix">
          <div class="b-plans_heading">
            <h2 class="m-typo m-typo_secondary">
              ' . pll__('td.plansHeadline') . '
            </h2>
          </div>
          <div class="b-plans_boxes">';

    while($plans->have_posts()) : $plans->the_post();

      $output .= '<div class="m-plans-box m-plans-box_primary">
        <div class="m-plans-box_image" style="background-image: url(' . get_the_post_thumbnail_url() .');">

        </div>
        <h6 class="m-typo m-typo_secondary">
          ' . get_the_title() . '
        </h6>
        <div class="info-wrapper">
          <p class="m-typo m-typo_text info">
          Miejsce:
            <span>' . get_field('place') . '</span>
          </p>
          <p class="m-typo m-typo_text info">
          Data:
            <span>' . get_field('date') . '</span>
          </p>
        </div>
        <p class="m-typo m-typo_text plans">
          ' . get_the_excerpt_max_charlength(80) . '
        </p>
        <a href="' . get_permalink() . '" class="m-btn m-btn_quaternary">' . pll__('td.buy_course') . '</a>
      </div>';

    endwhile;

    $output .=' </div>
          </section>';

  endif;

  echo $output;

} ?>
