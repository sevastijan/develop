<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>

<div class="b-sidebar">
	<div class="b-search" style="margin-bottom: 25px;">
		<div class="m-typo m-typo_bold">
			<?php pll_e('td.headline-search'); ?>
		</div>
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		    <label>
		        <input type="search" class="search-field"
		            placeholder="<?php echo esc_attr_x( ' Szukaj...', 'placeholder' ) ?>"
		            value="<?php echo get_search_query() ?>" name="s"
		            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		    </label>
		    <input type="submit" class="search-submit"
		        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
		</form>
	</div>
	<div class="b-aboutMe b-sidebar_section clearfix">
		<div class="b-aboutMe_image">
			<img src="<?php the_field('photo', 'options'); ?>" alt="">
		</div>
		<div class="m-typo m-typo_bold">
			<?php the_field('headline', 'options'); ?>
		</div>
		<p>
			<?php the_field('tekst', 'options'); ?>
		</p>
		<a class="m-btn m-btn_primary" href="<?php the_field('link', 'options'); ?>"><?php pll_e('td.see_more'); ?></a>
	</div>
	<div class="b-newsletter b-sidebar_section clearfix">
		<h6 class="m-typo m-typo_secondary">
			<?php pll_e('td.footer.column-2'); ?>
		</h6>
		<div class="m-typo m-typo_text">
	        <?php if(get_field('footer_describe', 'options')) : ?>
	          <?php the_field('footer_describe', 'options'); ?>
	        <?php else: ?>
	          <?php pll_e('td.footer.newsletter-default'); ?>
	        <?php endif; ?>
	        <?php //TODO: implement newsletter form ?>
	    </div>
		<form class="m-form" action="" method="post">
			<fieldset>
				<input type="text" name="" value="" placeholder="Imię i nazwisko" class="m-form_input">
				<input type="text" name="" value="" placeholder="Adres email" class="m-form_input">
			</fieldset>
		</form>
		<input type="submit" name="" value="Zapisz się!" class="m-btn m-btn_primary">
	</div>
	<div class="b-sidebar_section clearfix">
		<?php echo do_shortcode('[vc_boxes_camps id="382"]'); ?>
	</div>
	<div class="b-sidebar_section clearfix">
		<div class="fb-page" data-href="https://www.facebook.com/DawidWozniakowskiFanpage" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/DawidWozniakowskiFanpage" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/DawidWozniakowskiFanpage">Dawid Woźniakowski</a></blockquote></div>
	</div>
	<div class="b-sidebar_section b-sidebar_tags clearfix">
		<?php the_tags( '<h6 class="m-typo m-typo_secondary">' . pll__('td.footer.tags-headeline') . '</h6><ul><li>', '</li><li>', '</li></ul>' ); ?>
	</div>
</div>
