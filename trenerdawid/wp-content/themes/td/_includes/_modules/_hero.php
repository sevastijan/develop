<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<section class="b-container b-hero clearfix">
  <header class="b-hero_header clearfix">
    <div class="m-logo">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <img src="<?php echo THEME_URL; ?>/assets/images/logo.png" alt="">
        <img src="<?php echo THEME_URL; ?>/assets/images/logo-blue.png" alt="">
      </a>
    </div>
    <?php custom_menu('main_menu','m-menu'); ?>
    <div class="m-burger js-burger">
      <span class="m-burger_item"></span>
      <span class="m-burger_item"></span>
      <span class="m-burger_item"></span>
    </div>
  </header>

  <?php if(get_field('headline')) :

    the_field('headline');

  elseif(is_singular() && !is_front_page()) : ?>

    <main class="b-hero_teaser">
      <h1 class="m-typo m-typo_primary">
        <?php the_title(); ?>
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('
            <p class="m-breadcrumbs">','</p>
          ');
          }
        ?>
      </h1>
    </main>

  <?php else : ?>


    <main class="b-hero_teaser">
      <h1 class="m-typo m-typo_primary">
        <?php pll_e('td.hero.teaser'); ?> <span><?php pll_e('td.hero.teaser-discount'); ?></span>
      </h1>
    </main>

 <?php endif; ?>

</section>
