<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<section class="b-container b-hero clearfix b-hero_white">
  <header class="b-hero_header clearfix b-hero_header-white">
    <div class="m-logo">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
        <img src="<?php echo THEME_URL; ?>/assets/images/logo-blue.png" alt="">
        <img src="<?php echo THEME_URL; ?>/assets/images/logo-blue.png" alt="">
      </a>
    </div>
    <?php custom_menu('main_menu','m-menu'); ?>
    <div class="m-burger js-burger">
      <span class="m-burger_item"></span>
      <span class="m-burger_item"></span>
      <span class="m-burger_item"></span>
    </div>
  </header>

    <main class="b-hero_teaser b-hero_teaser-white">
      <h1 class="m-typo m-typo_primary">
        <?php if(is_home() || is_category()) {
          echo 'Blog';
        } elseif(is_search()) {
          echo 'Wyniki wyszukiwania';
        } else {
          the_title();
        } ?>
        <p class="m-breadcrumbs m-breadcrumbs_home">
          <a href="/">
          <?php pll_e('td.breadcrumb.home'); ?> </a>
          / 
        </p>
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('
            <p class="m-breadcrumbs">','</p>
          ');
          }
        ?>
      </h1>
    </main>

</section>
