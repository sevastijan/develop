<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_blog_shortcode' );
function vc_boxes_features_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_features'] = array(
     'name' =>'[VC] Features',
     'base' => 'vc_features',
     'category' => 'Content',
     'description' => 'Display features',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_features', 'vc_boxes_features_render' );
function vc_boxes_features_render($atts) {
  $atts = shortcode_atts(
    array(
      'tab_1' => "",
      'tab_2' => "",
      'tab_2_title' => "",
      'tab_2_url' => "",
      'tab_3' => "",
      'tab_3_title' => "",
      'tab_3_url' => "",
      'tab_4' => "",
      'tab_4_title' => "",
      'tab_4_url' => "",
    ), $atts, 'vc_boxes_features');

  $output = '
    <section class="b-container b-features clearfix">
        <div class="b-features_heading">
          <h2 class="m-typo m-typo_primary">
            ' . pll__('td.features_headline') . '
          </h2>
        </div>
        <div class="b-features_boxes">
          <div class="m-features-box m-features-box_describe ">
            <p class="m-typo m-typo_text">
            ' . $atts['tab_1'] . '
            </p>
          </div>
          <div class="m-features-box m-features-box_primary ">
            <div class="m-features-layer">
              <div class="m-features-layer_primary">
                <h6 class="m-typo m-typo_primary">
                  ' . $atts['tab_2_title'] . '
                </h6>
                <img class="m-features-layer_icon" src="' . THEME_URL . '/assets/images/features-icon.png" alt="">
              </div>
              <div class="m-features-layer_secondary" style="background-image: url(' . THEME_URL . '/assets/images/features-box-img.jpg);">
                <p class="m-typo m-typo_text">
                ' . $atts['tab_2'] . '
                </p>
                <a href="' . $atts['tab_2_url'] . '" class="m-btn m-btn_tertiary">' . pll__('td.see_more') . '</a>
              </div>
            </div>
          </div>
          <div class="m-features-box m-features-box_secondary ">
            <div class="m-features-layer">
              <div class="m-features-layer_primary">
                <h6 class="m-typo m-typo_primary">
                  ' . $atts['tab_3_title'] . '
                </h6>
                <img class="m-features-layer_icon" src="' . THEME_URL . '/assets/images/features-icon.png" alt="">
              </div>
              <div class="m-features-layer_secondary" style="background-image: url(' . THEME_URL . '/assets/images/features-box-img.jpg);">
                <p class="m-typo m-typo_text">
                  ' . $atts['tab_3'] . '
                </p>
                <a href="' . $atts['tab_3_url'] . '" class="m-btn m-btn_tertiary">' . pll__('td.see_more') . '</a>
              </div>
            </div>
          </div>
          <div class="m-features-box m-features-box_tertiary ">
            <div class="m-features-layer">
              <div class="m-features-layer_primary">
                <h6 class="m-typo m-typo_primary">
                  ' . $atts['tab_4_title'] . '
                </h6>
                <img class="m-features-layer_icon" src="' . THEME_URL . '/assets/images/features-icon2.png" alt="">
              </div>
              <div class="m-features-layer_secondary" style="background-image: url(' . THEME_URL . '/assets/images/features-box-img.jpg);">
                <p class="m-typo m-typo_text">
                  ' . $atts['tab_4'] . '
                </p>
                <a href="' . $atts['tab_4_url'] . '" class="m-btn m-btn_tertiary">' . pll__('td.see_more') . '</a>
              </div>
            </div>
          </div>
        </div>
    </section>';
  echo $output;
} ?>
