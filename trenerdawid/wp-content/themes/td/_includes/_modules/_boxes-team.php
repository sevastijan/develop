<?php
/**
 * Wordpress template created for "Trener Personalny Dawid Woźniakowski"
 *
 * Version 1.0
 * Date: 08.08.2017
 *
 * @author Sebastian Ślęczka @ sebastians@interpages.pl
 *
 * @package WordPress
 *
 */
?>
<?php
add_filter( 'vc_grid_item_shortcodes', 'vc_boxes_blog_shortcode' );
function vc_boxes_trainers_shortcode( $shortcodes ) {
   $shortcodes['vc_boxes_trainers'] = array(
     'name' =>'[VC] Trainers',
     'base' => 'vc_trainers',
     'category' => 'Content',
     'description' => 'Display trainers',
     'post_type' => Vc_Grid_Item_Editor::postType(),
  );
   return $shortcodes;
}
add_shortcode( 'vc_boxes_trainers', 'vc_boxes_trainers_render' );
function vc_boxes_trainers_render() {
  global $post;

  $team = new WP_Query(array(
    'post_type' => 'trainers',
    'posts_per_page' => 3
  ));
  if($team->have_posts()) :
  $output = '
    <div class="b-container b-team clearfix">
      <a href="/o-mnie/moj-zespol/">
        <div class="b-team_describe">
          <h3 class="m-typo m-typo_primary">
            ' . pll__('td.team_myTrainers') . '
          </h3>
        </div>
      </a>
      <div class="b-team_boxes">';
    $index = 0;
    while($team->have_posts()) : $team->the_post();
      if($index == 0) {
        $output .='<div class="m-team_primary-box" style="background-image: url('. get_the_post_thumbnail_url() .');">';
      } else if ($index == 1) {
        $output .='<div class="m-team_secondary-box" style="background-image: url('. get_the_post_thumbnail_url() .');">';
      } else {
        $output .='<div class="m-team_tertiary-box" style="background-image: url('. get_the_post_thumbnail_url() .');">';
      }
      $output .='<div class="m-team-text">
          <h6 class="m-typo m-typo_primary m-typo_text">
            <a href="' . get_the_permalink() . '">' . get_the_title() . '</a>
          </h6>
        </div>
        </a>
      </div>';
      $index++;
    endwhile;
    $output .='</div>
      </div>';
    echo $output;
  endif;
} ?>
