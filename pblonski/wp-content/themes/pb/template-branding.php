<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

 Template Name: Branding

*/
get_header(); ?>


    <section class="branding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="same-heading">Branding<span class="name"><span class="hidden-xs"> -</span> <br class="visible-xs">Projektowanie znaku logo i identyfikacji wizualnej firmy</span></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <p class="same-paragraph">Dobra Identyfikacji wizualna marki w dzisiejszej nieprzebytej dżungli lepiej i grzej zaprojektowanych logotypów jest kluczem do zwrócenia na siebie uwagi. A przecież każdemu z nas właśnie oto chodzi. O zwrócenie na siebie uwagi potencjalnego
                        klienta. Pomyśl gdybym ja nie zwrócił na siebie swoim wizerunkiem Twojej uwagi zapewne nie czytałbyś tego tekstu, którego zadaniem jest jeszcze bardziej Cię zaciekawić i przekonać, że jesteś w dobrym miejscu. Ten tekst też jest
                        elementem brandingu, ale mojego. Też jest elementem wizerunku i stylu, mojego wizerunku i mojego stylu. Jeżeli tu dotarłeś to znaczy, że to działa, że potrafię to robić. A zatem, Tobie też jest potrzebny działający branding, magnetyczny
                        wizerunek i niebanalny styl, a jakie elementy składają się na dobrze działającą całość ? Już wyjaśniam.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div class="col-md-4">
                        <img src="<?php echo PB_THEME_URL; ?>img/batista.png" alt="brand-logo" class="same-img">
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo PB_THEME_URL; ?>img/batista.png" alt="brand-logo" class="same-img">
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo PB_THEME_URL; ?>img/batista.png" alt="brand-logo" class="same-img">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h2 class="same-heading branding-heading">Logo<span class="name"><span class="hidden-xs"> -</span> <br class="visible-xs">Czym jest logo i z czego powinien składać się logotyp?</span></h2>
                        <p class="same-paragraph">Pełny znak Logo to znak graficzny zazwyczaj złożony z trzech części. Części typograficznej. Czyli zaprojektowanego napisu zwykle informującego nas o nazwie firmy. Następnie, części czysto graficznej, tak zwanego sygnetu. Sygnet,
                            to zaprojektowany symbol stanowiący integralną cześć projektu logo. Oraz sloganu. Slogan to krótkie i zwięzłe hasło brandingowe stanowiące swoiste rozszerzenie komunikatu jaki niesie logotyp. Ogółem logo ma za zadanie wywoływać
                            u docelowej grupy odbiorców określone emocje, nasuwać odpowiednie skojarzenia i oddawać charakter firmy. Nadrzędnym zadaniem logotypu jest zostać zapamiętanym, bo nic innego jak zapamiętywalność jest tym, czynnikiem, który
                            sprawi, ze klient przyjdzie właśnie do Ciebie.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo PB_THEME_URL; ?>img/batista.png" alt="brand-logo" class="same-img">
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo PB_THEME_URL; ?>img/batista.png" alt="brand-logo" class="same-img">
                    </div>
                    <div class="col-md-4">
                        <img src="<?php echo PB_THEME_URL; ?>img/batista.png" alt="brand-logo" class="same-img">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="container">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h2 class="same-heading branding-heading">Barwy firmowe<span class="name"><span class="hidden-xs"> -</span> <br class="visible-xs">psychologia kolorów jest jednym znajistotniejszych zagadniej brandingu?</span></h2>
                        <p class="same-paragraph">Kolorem i kształtem określimy swój charakter, skuteczniej dotrzemy do pożądanej grupy docelowej, wywołamy odpowiednią reakcję i nadamy jednoznaczny charakter marce. Szeroki zakres wiedzy o kształtach, symbolach zakorzenionych ludzkiej
                            podświadomości i równie podświadomej i niekontrolowanej reakcji ludzkiego umysły na poszczególne kolory, umiejętnie wykorzystany w procesie projektowania identyfikacji wizualnej czyni taki projekt niezwykle skutecznym . Dzisiaj
                            wiemy o kolorach niemal wszystko. Wiemy jakie ma znaczenie dany kolor, idąc dalej, wiemy jakie reakcje wywołuje w ludzkim umyśle nie tylko dany kolor, ale konkretny odcień tego koloru. Posiadając taką wiedzę jesteśmy uzbrojeni
                            w całkiem niezłe narzędzie pozwalające precyzyjnie projektować skuteczne logotypy.
                        </p>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <img src="<?php echo PB_THEME_URL; ?>img/branding-orange.png" alt="color-psychology" class="same-img color-psychology">
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <img src="<?php echo PB_THEME_URL; ?>img/branding-brown.png" alt="color-psychology" class="same-img color-psychology">
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <img src="<?php echo PB_THEME_URL; ?>img/branding-green.png" alt="color-psychology" class="same-img color-psychology">
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <img src="<?php echo PB_THEME_URL; ?>img/branding-blue.png" alt="color-psychology" class="same-img color-psychology">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="container">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h2 class="same-heading branding-heading">Typografia<span class="name"><span class="hidden-xs"> </span> <br class="visible-xs"></span></h2>
                        <p class="same-paragraph">Dobrze dobrana typografia to spoiwo wizerunku. Typografia określa charakter nadaje niepowtarzalną atmosferę całości. Dopieszczony wizerunek pod kątem typograficznym daje odbiorcy poczucie tego, że patrzy na profesjonalną markę,
                            która zadbała o każdy detal swojego wizerunku. Typografia mówi o tym jacy jesteśmy. Czy jesteśmy marką stawiającą na nowacje, czy może jesteśmy marką służącą poradą, a może solidną firmą budowlaną, albo dbającym o jakość restauratorem.
                            Zakorzenione głęboko w ludzkiej świadomość stereotypy możemy wydobyć odpowiednim doborem kroju pisma jakim będziemy komunikować się z klientem.
                        </p>
                    </div>
                    <div class="hidden-xs col-sm-12">
                        <img src="<?php echo PB_THEME_URL; ?>img/branding-typography.png" alt="branding-typography" class="same-img branding-typography">
                    </div>
                    <div class="col-xs-12 visible-xs">
                        <img src="<?php echo PB_THEME_URL; ?>img/typografia.png" alt="branding-typography" class="same-img branding-typography">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="container">
                    <div class="col-sm-10 col-sm-offset-1">
                        <h2 class="same-heading branding-heading">Brand Hero<span class="name"><span class="hidden-xs"> -</span> <br class="visible-xs">Narzędzie reklamowe o nieograniczonych możliwościach komunikacji z klientem.</span></h2>
                        <p class="same-paragraph">Postać, wcielenie marki będące uzewnętrznieniem całego jestestwa firmy. Zadanie niełatwe, ale dobrze zaprojektowany Brand Hero jest nieograniczonym narzędziem reklamowych, dającym możliwości komunikacji z odbiorcą jakich nie daje
                            nic innego. W naszym kraju korzystanie z Brand Hero dopiero raczkuje, ale już można zauważyć, że niektóre polskie znane marki sięgają po to rozwiązanie i czerpią z niego pełnymi garściami. Moim Brand Hero jestem ja sam, to
                            ten komiksowy, uśmiechnięty, brodaty wariat, a obok najpopularniejsze postaci Brand Hero jakie zostały stworzone przez znane marki.
                        </p>
                    </div>
                    <div class="col-sm-12">
                        <img src="<?php echo PB_THEME_URL; ?>img/branding-hero.png" alt="branding-typography" class="same-img branding-hero">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="bar realization">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="bar-wrapper realization">
                        <div class="bar-heading realization-bar">
                            Kilka moich realizacji <br class="visible-xs"><span class="realization-bar-span">* te powyżej też są moje</span>
                        </div>
                        <img src="<?php echo PB_THEME_URL; ?>img/arrow-down.png" alt="arrow" class="arrow-down">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="my-realizations">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-3 my-realizations-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>img/mr1.png" alt="brand-logo" class="my-realizations-logo">
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 my-realizations-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>img/mr2.png" alt="brand-logo" class="my-realizations-logo">
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 my-realizations-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>img/mr3.png" alt="brand-logo" class="my-realizations-logo">
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 my-realizations-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>img/mr4.png" alt="brand-logo" class="my-realizations-logo">
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 my-realizations-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>img/mr5.png" alt="brand-logo" class="my-realizations-logo">
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 my-realizations-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>img/mr6.png" alt="brand-logo" class="my-realizations-logo">
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 my-realizations-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>img/mr7.png" alt="brand-logo" class="my-realizations-logo">
                </div>
                <div class="col-sm-6 col-md-4 col-lg-3 my-realizations-wrapper">
                    <img src="<?php echo PB_THEME_URL; ?>img/mr8.png" alt="brand-logo" class="my-realizations-logo">
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
