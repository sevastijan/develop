<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

 Template Name: Contact

*/
get_header() ?>

<section class="contact-page">
		<div class="container">
				<div class="row">
						<div class="col-xs-12">
								<h2 class="same-heading contact-page-heading">Kontakt</h2>
						</div>
				</div>
				<div class="row">
						<div class="col-md-3">
								<img src="<?php echo PB_THEME_URL; ?>img/ja.png" alt="me" class="me-img">
						</div>
						<div class="col-md-9">
								<div class="contact-content">
										<p class="same-paragraph">
												Kliknąłeś zakładkę kontakt, to bardzo dobrze. Jeżeli masz dużo pytań, chcesz realizować jakiś projekt, ale nie bardzo wiesz jak się do tego zabrać, to najlepiej zadzwoń - doradzę, podpowiem - to nic nie kosztuje. Preferuję kontakt telefoniczny ze względu
												na to, że łatwiej się zrozumieć i zajmuje to zdecydowanie mniej czasu, a efekty rozmowy są precyzyjne i dokładne. Jeżeli jednak wolisz formę pisemną, to oczywiście nie mam nic przeciwko, tylko postaraj się być precyzyjnym ponieważ
												na ogólnikowo zadane pytanie będę mógł odpowiedzieć, też w ogólny i mało precyzyjny sposób.
										</p>
								</div>
						</div>
				</div>
		</div>
</section>
<section class="download">
		<div class="container">
				<div class="row">
						<div class="col-lg-6">
								<div class="contact">
										<h3 class="contact-title">Paweł Błoński <span class="hidden-xs">-</span> tel.&nbsp;606&nbsp;398&nbsp;351</h3>
										<p class="contact-paragraph">ilustracje@pawelblonski.pl</p>
										<div class="social">
												<a href="#"><img src="img/facebook.png" alt="facebook-logo" class="facebook-img"></a>
										</div>
										<?php echo do_shortcode('[contact-form-7 id="19" title="Contact form 1"]'); ?>
								</div>

						</div>
						<div class="col-lg-6 download-align">
								<div class="col-xs-12">
										<span class="download-topic"><a href="#" class="download-topic-href">Kolorowanki do pobrania <span class="see-more">(zobacz więcej)</span></a>
										</span>
								</div>
								<?php
									$args = array(
										'post_type' => 'coloring',
										'posts_per_page' => 6
									);
									$news = new WP_Query($args);
								  $index = 1; if ($news->have_posts()) : while ($news->have_posts()) : $news->the_post();
								?>
								<div class="col-xs-12 col-sm-4 <?php if($index < 2){ echo 'download-mobile-margin'; } ?>">
										<?php the_post_thumbnail('coloring-widget', ['class' => 'download-img']); ?>
								</div>
							<?php endwhile; endif; ?>
						</div>
				</div>
		</div>
</section>

<?php get_footer() ?>
