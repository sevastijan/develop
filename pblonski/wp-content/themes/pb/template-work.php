<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

 Template Name: Work

*/
get_header() ?>

    <section class="workplace">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="same-heading">Pracownia rysownika<span class="name"><span class="hidden-xs"> -</span> <br class="visible-xs">Zobacz jak pracuję i jakiego sprzętu używam.</span></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <p class="same-paragraph">Rysownik dzisiaj niestety zmuszony jest korzystać z dobrodziejstw nowoczesnej technologii, która swoje kosztuje. Proces tworzenia ilustracji jeszcze 15 lat temu wyglądał zupełnie inaczej. Wykorzystywaliśmy tylko analogowe materiały
                        przez co czas potrzebny na stworzenie statystycznej ilustracji był o wiele dłuży niż dzisiaj, a tym samym ceny ilustracji były dużo wyższe. Dzisiaj z pomocą przychodzą nam dedykowane rysownikom i artystom urządzenia i oprogramowanie.
                        Postanowiłem co nie co odsłonić swoje zaplecze i pokazać wszystkim zainteresowanym jak wygląda moja praca i miejsce jej wykonywania.
                    </p>
                </div>
                <div class="col-xs-12">
                    <img src="<?php echo PB_THEME_URL; ?>img/work.png" alt="workplace" class="same-img workplace-img">
                </div>
                <div class="col-sm-10 col-sm-offset-1">
                    <h6 class="wokplace-topic">Pracownia rysownika</h6>
                    <p class="same-paragraph">Rysownik dzisiaj niestety zmuszony jest korzystać z dobrodziejstw nowoczesnej technologii, która swoje kosztuje. Proces tworzenia ilustracji jeszcze 15 lat temu wyglądał zupełnie inaczej. Wykorzystywaliśmy tylko analogowe materiały
                        przez co czas potrzebny na stworzenie statystycznej ilustracji był o wiele dłuży niż dzisiaj, a tym samym ceny ilustracji były dużo wyższe. Dzisiaj z pomocą przychodzą nam dedykowane rysownikom i artystom urządzenia i oprogramowanie.
                        Postanowiłem co nie co odsłonić swoje zaplecze i pokazać wszystkim zainteresowanym jak wygląda moja praca i miejsce jej wykonywania.
                    </p>
                </div>
                <div class="col-xs-12">
                    <img src="<?php echo PB_THEME_URL; ?>img/work.png" alt="workplace" class="same-img workplace-img">
                </div>
                <div class="col-sm-10 col-sm-offset-1">
                    <h6 class="wokplace-topic">Stanowisko cyfrowe:</h6>
                    <p class="same-paragraph">Tutaj mam komputer, dwa monitory jeden zwykły 22 calowy Samsung, a drugi graficzny, skalibrowany monitor Eizo. Eizo to już stara dziadzia, ale daje rade. Oprócz komputera i monitorów oczywiście musi być dobry tablet graficzny. Wcześniej pracowałem na biurkowym tablecie Wacoma, był to model intuos 4 L. Jakiś czas temu zainwestowałem w tablet ekranowy i na biurku zagościł nowiutki Wacom Citiq 13 HD. Oczywiście marzy mi się większy tablet ekranowy, ale nie od razu Rzym zbudowano.
                    </p>
                </div>
                <div class="col-xs-12">
                    <img src="<?php echo PB_THEME_URL; ?>img/work.png" alt="workplace" class="same-img workplace-img">
                </div>
                <div class="col-sm-10 col-sm-offset-1">
                    <h6 class="wokplace-topic">Stanowisko analogowe.</h6>
                    <p class="same-paragraph">Ktoś by pomyślał, że to przecież nic wielkiego, ale w moich skromnych metrażowo warunkach, urządzenie dobrego analogowego stanowiska było nie lada wyzwaniem. Dobre oświetlenie, setki przyborów rysunkowych w zasięgu ręki, no i rysownica z regulowanym kątem nachylenia. Ta ostatnia powstała na zamówienie, zmontował mi ją mój Teść, dokładnie wedle mojego projektu i jest zajebista.
Obydwa stanowiska ulokowane są na jednym długim blacie nierozciągniętym wzdłuż ściany z niewielkim oknem na wiejski świat, który jestem zmuszony oglądać każdego dnia. Tak to z grubsza wygląda.
                    </p>
                </div>
                <div class="col-xs-12">
                    <img src="<?php echo PB_THEME_URL; ?>img/work.png" alt="workplace" class="same-img workplace-img">
                </div>
                <div class="col-sm-10 col-sm-offset-1">
                    <h6 class="wokplace-topic">Oprogramowanie dla rysownika.</h6>
                    <p class="same-paragraph">Oprogramowanie dla rysownika. O czym jeszcze mogę napisać ? O oprogramowaniu. W dzisiejszych czasach jest to dość istotna kwestia. Profesjonalne graficzne oprogramowanie jest cholernie drogie i nie każdego stać na najnowsze wersje.
                        Niestety zaliczam się do tej grupy. Swoje oprogramowanie zakupiłem już dobrych kilka lat tamu i przeznaczyłem na ten cel walizkę pieniędzy. Póki co ostatnia pudełkowa wersja pakietu Adobe dobrze mi służy, i w najbliższym czasie
                        nie zamierzam wydawać kolejnej walizki pieniędzy na najnowszy abonament adobe. Ważne, że pracuję na dobrym i oryginalnym oprogramowaniu. Dla ścisłości jest to pakiet Adobe Design Premium CS6. Jeżeli ktoś by miał jakieś pytania,
                        lub chciał szerzej poczytać o pracy rysownika zapraszam na mojego bloga: www.przygodyrysownika.pl
                    </p>
                    <p class="same-paragraph margin-bottom">Jeżeli ktoś by miał jakieś pytania,
                        lub chciał szerzej poczytać o pracy rysownika zapraszam na mojego bloga: <br>www.przygodyrysownika.pl
                    </p>
                </div>
            </div>
        </div>
    </section>

<?php get_footer() ?>
