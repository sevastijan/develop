<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

*/
get_header(); ?>

  <?php if( have_rows('slider', 'options') ): ?>
    <section class="carousel-section">
        <div class="container-fluid">
            <div class="row">
                <div class="hidden-xs col-sm-12">
                    <div class="top-carousel">
                        <?php while ( have_rows('slider', 'options') ) : the_row(); ?>
                          <div>
                            <img src="<?php the_sub_field('obrazek') ?>" alt="carouse-photo" class="carousel-img">
                          </div>
                        <?php endwhile; ?>
                    </div>
                    <div class="container">
                        <div class="top-carousel-arrows">
                            <ul class="carousel-ul">
                                <li class="prev arrow"><img src="<?php echo PB_THEME_URL; ?>img/left.png" alt="arrow"></li>
                                <li class="next arrow"><img src="<?php echo PB_THEME_URL; ?>img/right.png" alt="arrow"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 visible-xs mobile-slide-wrapper">
                    <div class="mobile">
                        <img src="<?php echo PB_THEME_URL; ?>img/mobile-slider.jpg" alt="mobile-slide" class="mobile-slide">
                    </div>
                </div>
            </div>
        </div>
    </section>
  <?php endif; ?>

  <?php if(get_field('o_mnie', 'options')) : ?>
    <section class="afewwords">
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-2 col-sm-offset-1 visible-xs visible-sm iphone-center">
                    <img src="<?php echo PB_THEME_URL; ?>img/ja.png" alt="me" class="me">
                </div>
                <div class="col-xs-8 iphone-center col-sm-8 col-md-6 col-md-offset-3 col-lg-5 col-lg-offset-3">
                    <h2 class="afewwords-topic">Rysownik i ilustrator <span class="name"><span class="hidden-xs">-</span> <br class="visible-xs">Paweł Błoński</span></h2>
                </div>
                <div class="col-xs-12 col-md-10 col-md-offset-1">
                    <div class="human hidden-xs hidden-sm">
                        <img src="<?php echo PB_THEME_URL; ?>img/human-new.png" alt="human">
                    </div>
                    <div class="afewwords-content">
                        <p class="paragraph">
                          <?php the_field('o_mnie', 'options'); ?></p>
                        <p class="invite">Zapraszam Cię do pozwiedzania mojej strony.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif; ?>

    <?php if ( have_posts() ) : ?>
    <section class="ilustration">
        <div class="container-fluid">
            <div class="row">
              <?php
                while ( have_posts() ) : the_post();
                $thumbnailURL = get_the_post_thumbnail_url('', 'post-index');
              ?>
              <div class="col-sm-6 col-md-4 col-lg-3 small-lg">
                    <div class="ilustration-box">
                        <a href="<?php the_permalink();?>">
                            <div class="img-wrapper">
                                <div class="ilustration-title" style="background-image: url(<?php echo $thumbnailURL; ?>);">
                                  <span class="title-content">
                                    <?php the_title(); ?>
                                  </span>
                                </div>
                            </div>
                        </a>
                        <p class="ilustration-paragraph"><?php the_excerpt_max_charlength(150); ?></p>
                        <img src="<?php echo PB_THEME_URL; ?>img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
    <section class="navigation">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <?php the_numeric_pagination(); ?>
          </div>
        </div>
      </div>
    </section>
    <?php endif; ?>

    <?php if( have_rows('klienci', 'options') ): ?>
    <section class="collabo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-box">
                        Dla nich pracowałem i są zadowoleni ;)
                    </div>
                </div>
                <div class="collabo-carousel">
                    <?php while ( have_rows('klienci', 'options') ) : the_row(); ?>
                      <div class="col-sm-6 col-md-4 col-lg-2 collabo-align">
                        <?php if(get_sub_field('link')) : ?>
                          <a href="<?php the_sub_field('link'); ?>">
                        <?php endif; ?>
                          <img src="<?php the_sub_field('logo'); ?>" alt="bruk" class="collabo-img mobile-pt">
                        <?php if(get_sub_field('link')) : ?>
                          </a>
                        <?php endif; ?>
                      </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
  <?php endif; ?>

  <section class="download">
      <div class="container">
          <div class="row">
              <div class="col-lg-6">
                  <div class="contact">
                      <h3 class="contact-title">Paweł Błoński <span class="hidden-xs">-</span> tel.&nbsp;606&nbsp;398&nbsp;351</h3>
                      <p class="contact-paragraph">ilustracje@pawelblonski.pl</p>
                      <div class="social">
                          <a href="#"><img src="<?php echo PB_THEME_URL; ?>img/facebook.png" alt="facebook-logo" class="facebook-img"></a>
                      </div>
                      <?php echo do_shortcode('[contact-form-7 id="19" title="Contact form 1"]'); ?>
                  </div>

              </div>
              <div class="col-lg-6 download-align">
                  <div class="col-xs-12">
                      <span class="download-topic"><a href="<?php echo esc_url(home_url('/kolorowanki')); ?>" class="download-topic-href">Kolorowanki do pobrania <span class="see-more">(zobacz więcej)</span></a></span>
                  </div>
                  <?php
  									$args = array(
  										'post_type' => 'coloring',
  										'posts_per_page' => 6,
  									);
  									$news = new WP_Query($args);
  								  $index = 1; if ($news->have_posts()) : while ($news->have_posts()) : $news->the_post();
  								?>
  								<div class="col-xs-12 col-sm-4 <?php if($index < 2){ echo 'download-mobile-margin'; } ?>">
  										<?php the_post_thumbnail('coloring-widget', ['class' => 'download-img']); ?>
  								</div>
  								<?php endwhile; endif; ?>
              </div>
          </div>
      </div>
  </section>

<?php get_footer(); ?>
