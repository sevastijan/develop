<?php
/**
 * Wordpress template created for Gravem Gamers
 * Theme author: Sebastian Ślęczka (www.interpages.pl)
 * Code author: Krzysztof Majstrowicz
 * If you have any questions feel free to ask us sebastians@interpages.pl
 *
 * Register post types specifically for Gravem Gamers
 *
 * Version 0.0.1
 * Date: 20.12.2016
 *
 * @package WordPress
 *
 */

  add_action('init','init_posttypes');

  function init_posttypes(){

      $coloring = array(
          'labels' => array (
              'name' => 'Kolorowanki',
              'sungular_name' => 'Kolorowanki',
              'all_items' => 'Wszystkie',
              'add_new' => 'Dodaj nowy',
              'add_new_item' => 'Dodaj nowy',
              'edit_item' => 'Edytuj',
              'new_item' => 'Nowy',
              'view_item' => 'Zobacz',
              'search_items' => 'Szukaj',
              'not_found' => 'Nie znaleziono',
              'not_found_in_trash' => 'Nie znaleziono w koszu',
              'parent_item_colon' => ''
          ),
          'public' => true,
          'public_queryable' => true,
          'show_ui' => true,
          'query_var' => true,
          'rewrite' => array('slug' => 'kolorowanki'),
          'capatility_type' => 'post',
          'hierarchical' => true,
          'menu_position' => 5,
          'supports' => array(
              'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields'
          ),
          'has_archive' => true,
      );
      register_post_type('coloring', $coloring);

  }

?>
