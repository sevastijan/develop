<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

  <?php if(is_search()) : ?>
    <meta name="robots" content="noindex, nofollow" >
  <?php endif; ?>

    <meta charset="<?php bloginfo('charset') ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Paweł Błoński - Studio grafiki i ilustracji</title>

    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/scss/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bangers|Caveat:700|Kalam:400,700|Roboto:400,400i,500,700,900">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<nav class="nav-bar-wrapper">
    <div class="nav-bg">
        <div class="container navbar">
            <div class="row">
                <div class="desktop-nav">
                    <div class="col-xs-9 col-md-4 col-lg-3 brand-box">
                        <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/brand-logo.png" alt="brand-logo" class="brand-img img-responsive"></a>
                    </div>
                    <div class="col-xs-3 col-md-8 col-lg-9 burger-box">
                        <div id="js-burger" class="burger hidden-md hidden-lg">
                            <div class="burger-brick"></div>
                            <div class="burger-brick middle"></div>
                            <div class="burger-brick"></div>
                        </div>
                        <!-- <ul class="menu-ul">
                            <li class="menu-li"><a href="#" class="menu-li-href active">Ilustracje</a></li>
                            <li class="menu-li"><a href="#" class="menu-li-href">Branding</a></li>
                            <li class="menu-li"><a href="#" class="menu-li-href">Pracownia</a></li>
                            <li class="menu-li"><a href="#" class="menu-li-href">Projektownie stron</a></li>
                            <li class="menu-li"><a href="#" class="menu-li-href">Blog</a></li>
                            <li class="menu-li"><a href="#" class="menu-li-href">Kontakt</a></li>
                        </ul> -->
                        <?php wp_nav_menu(array(
                          'name' => 'Menu Główne',
                          'menu_class' => 'menu-ul',
                          'menu-item' => 'menu-li'
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nav-space">
    </div>
</nav>
