<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

*/
?>
<?php if( have_rows('footer', 'options') ): ?>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-box">
                      <?php while ( have_rows('footer', 'options') ) : the_row(); ?>
                        <div class="col-md-3 img-box">
                            <img src="<?php the_sub_field('obrazek') ?>" alt="footer-img" class="footer-img">
                        </div>
                      <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  <?php endif ?>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo PB_THEME_URL ?>js/bootstrap.min.js"></script>
    <script src="<?php echo PB_THEME_URL ?>js/app.js"></script>

    <?php wp_footer(); ?>
</body>

</html>
