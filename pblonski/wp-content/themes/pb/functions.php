<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

*/
?>
<?php

  if(!defined('PB_THEME_DIR')) {
  define('PB_THEME_DIR', get_theme_root(). '/' .get_template() . '/');
  }

  if(!defined('PB_THEME_URL')) {
  define('PB_THEME_URL', WP_CONTENT_URL. '/themes/' .get_template() . '/');
  }

  if(function_exists('register_nav_menus')) {
    register_nav_menus(array(
      'main_nav' => 'Główne menu nawigacji'
    ));
  }

  /**
   * Add settings page for theme
   */

  if( function_exists('acf_add_options_page') ) {
  	acf_add_options_page();
  }

  /**
  *  Custom post excerpt
  *
  */
  function the_excerpt_max_charlength($charlength) {
      echo cutText(get_the_excerpt(), $charlength);
  }
  function cutText($text, $maxLength) {
      $maxLength++;
      $return = '';
      if (mb_strlen($text) > $maxLength) {
          $subex = mb_substr($text, 0, $maxLength - 5);
          $exwords = explode(' ', $subex);
          $excut = -(mb_strlen($exwords[count($exwords) - 1]));
          if ($excut < 0) {
              $return = mb_substr($subex, 0, $excut);
          } else {
              $return = $subex;
          }
          $return .= '...';
      } else {
          $return = $text;
      }
      return $return;
  }


  /**
  *  Theme thumbnails
  *
  */
  add_theme_support('post-thumbnails', array('post', 'coloring'));

  add_image_size( 'post-index', 446, 350, true );
  add_image_size( 'coloring-widget', 148, 148, true );
  add_image_size( 'single-widget', 727, 425, true );


  /**
  *  Import custom post types
  *
  */
  require_once(PB_THEME_DIR.'/inc/cpt.php');


  /**
  *  Custom numeric pagination
  *
  */
  function the_numeric_pagination() {

  	if( is_singular() )
  		return;

  	global $wp_query;

  	if( $wp_query->max_num_pages <= 1 )
  		return;

  	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  	$max   = intval( $wp_query->max_num_pages );

  	if ( $paged >= 1 )
  		$links[] = $paged;

  	if ( $paged >= 3 ) {
  		$links[] = $paged - 1;
  		$links[] = $paged - 2;
  	}

  	if ( ( $paged + 2 ) <= $max ) {
  		$links[] = $paged + 2;
  		$links[] = $paged + 1;
  	}

  	echo '<div class="pagination-box"><ul class="pagination-ul">' . "\n";

  	if ( get_previous_posts_link() )
  		printf( '<li class="pagination-li">%s</li>' . "\n", get_previous_posts_link('<span class="label-prev arrow"></span>') );

  	if ( ! in_array( 1, $links ) ) {
  		$class = 1 == $paged ? 'active' : '';

  		printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

  		if ( ! in_array( 2, $links ) )
  			echo '<li>…</li>';
  	}

  	sort( $links );
  	foreach ( (array) $links as $link ) {
  		$class = $paged == $link ? 'active' : '';
  		printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  	}

  	if ( ! in_array( $max, $links ) ) {
  		if ( ! in_array( $max - 1, $links ) )
  			echo '<li>…</li>' . "\n";

  		$class = $paged == $max ? 'active' : '';
  		printf( '<li class="pagination-li %s"><a href="%s" class="pagination-href">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  	}

  	if ( get_next_posts_link() )
  		printf( '<li class="pagination-li">%s</li>' . "\n", get_next_posts_link('<span class="label-next arrow"></span>') );

  	echo '</ul></div>' . "\n";
  }


?>
