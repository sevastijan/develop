<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

 Template Name: Project

*/
get_header() ?>

<section class="projects-heading">
		<div class="container">
				<div class="row">
						<div class="col-xs-12">
								<h2 class="same-heading">Strony interentowe<span class="name"><span class="hidden-xs"> -</span> <br class="visible-xs">Skuteczne i kompleksowe rozwiązania</span></h2>
						</div>
				</div>
				<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
								<p class="same-paragraph">Tak, dokładnie tak jak pisałem na stronie głównej zajmuję się projektowaniem i wdrażaniem nowoczesnych stron internetowych. Tych, którzy mnie nie znają może to zdziwić, ale stronami internetowymi zajmuję się od przeszło dziesięciu
										lat. Kiedyś wszystko robiłem sam, czyli projektowałem i kodowałem, ale po pierwsze wtedy było prościej, a po drugie doszedłem do wniosku, że lepsze efekty osiągnę jeżeli skupię się na tym w czym jestem naprawdę dobry, czyli w projektowaniu.
										Kwestie programistyczne postanowiłem oddać w ręce osób, które na ten świat przyszły po to żeby kodować, programować i jeszcze raz kodować. Dlatego od lat współpracuję z kilkoma wybitnymi programistami. Zapoznaj się proszę poniżej
										z efektami naszej pracy.
								</p>
						</div>
				</div>
		</div>
</section>
<section class="projects-offer">
		<div class="container">
				<div class="row">
						<div class="col-sm-6 col-md-4 less-padding">
								<div class="offer-wrapper">
										<div class="topic">
												<img src="img/head.png" alt="icon" class="icon-img">
												<h4 class="same-heading offer-topic">
												<span class="orange">Wizerunkowe</span>
												 strony&nbsp;internetowe
											 </h4>
										</div>
										<p class="offer-paragraph">
												Strona internetowa dla Twojej firmy
										</p>
										<ul class="offer-ul">
												<li class="offer-li">Skuteczne strony internetowe</li>
												<li class="offer-li">Wsparcie dla urządzeń mobilnych</li>
												<li class="offer-li">Optymalizacja SEO</li>
										</ul>
								</div>
						</div>

						<div class="col-sm-6 col-md-4 less-padding">
								<div class="offer-wrapper">
										<div class="topic">
												<img src="img/wozek.png" alt="icon" class="icon-img">
												<h4 class="same-heading offer-topic">
												Sklepy
												 <span class="orange">internetowe</span>
											 </h4>
										</div>
										<p class="offer-paragraph">
												Intuicyjne sklepy internetowe
										</p>
										<ul class="offer-ul">
												<li class="offer-li">Skuteczne strony internetowe</li>
												<li class="offer-li">Wsparcie dla urządzeń mobilnych</li>
												<li class="offer-li">Optymalizacja SEO</li>
										</ul>
								</div>
						</div>

						<div class="col-sm-6 col-md-4 less-padding">
								<div class="offer-wrapper">
										<div class="topic">
												<img src="img/display.png" alt="icon" class="icon-img">
												<h4 class="same-heading offer-topic">
												 Nowoczesny
												 <span class="orange">Design</span>
											 </h4>
										</div>
										<p class="offer-paragraph">
												Styl, estetyka, jakość i rozmach
										</p>
										<ul class="offer-ul">
												<li class="offer-li">Skuteczne strony internetowe</li>
												<li class="offer-li">Wsparcie dla urządzeń mobilnych</li>
												<li class="offer-li">Optymalizacja SEO</li>
										</ul>
								</div>
						</div>
						<div class="col-sm-6 col-md-4 less-padding">
								<div class="offer-wrapper">
										<div class="topic">
												<img src="img/notebook.png" alt="icon" class="icon-img">
												<h4 class="same-heading offer-topic">
													CMS
												<span class="orange">WordPress</span>
											 </h4>
										</div>
										<p class="offer-paragraph">
												Intuicyjny system zarządzania treścią
										</p>
										<ul class="offer-ul">
												<li class="offer-li">Skuteczne strony internetowe</li>
												<li class="offer-li">Wsparcie dla urządzeń mobilnych</li>
												<li class="offer-li">Optymalizacja SEO</li>
										</ul>
								</div>
						</div>

						<div class="col-sm-6 col-md-4 less-padding">
								<div class="offer-wrapper">
										<div class="topic">
												<img src="img/wozek.png" alt="icon" class="icon-img">
												<h4 class="same-heading offer-topic">
												 <span class="orange">Copywriting</span>
											 </h4>
										</div>
										<p class="offer-paragraph">
												Profesjonalne teksty brandingowe
										</p>
										<ul class="offer-ul">
												<li class="offer-li">Skuteczne strony internetowe</li>
												<li class="offer-li">Wsparcie dla urządzeń mobilnych</li>
												<li class="offer-li">Optymalizacja SEO</li>
										</ul>
								</div>
						</div>

						<div class="col-sm-6 col-md-4 less-padding">
								<div class="offer-wrapper">
										<div class="topic">
												<img src="img/phone.png" alt="icon" class="icon-img">
												<h4 class="same-heading offer-topic">
												 Responsive
												 <span class="orange">web&nbsp;design</span>
											 </h4>
										</div>
										<p class="offer-paragraph">
												Styl, estetyka, jakość i rozmach
										</p>
										<ul class="offer-ul">
												<li class="offer-li">Skuteczne strony internetowe</li>
												<li class="offer-li">Wsparcie dla urządzeń mobilnych</li>
												<li class="offer-li">Optymalizacja SEO</li>
										</ul>
								</div>
						</div>
				</div>
		</div>
</section>

<aside class="bar">
		<div class="container-fluid">
				<div class="row">
						<div class="col-xs-12">
								<div class="bar-wrapper">
										<div class="bar-heading">
												Kilka wybranych realizacji stron internetowych
										</div>
										<img src="img/arrow-down.png" alt="arrow" class="arrow-down">
								</div>
						</div>
				</div>
		</div>
</aside>



<section class="ilustration">
		<div class="container-fluid">
				<div class="row">
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a href="#">
												<div class="img-wrapper">
														<div class="ilustration-title"><span class="title-content">Małe co nieco na dzień dobry, czyli&nbsp;o&nbsp;tym jak intymność stała</span></div>
												</div>
										</a>
										<p class="ilustration-paragraph">Kiedyś ludzie pisali pamiętniki. Ale nie takie klawiaturą i na monitorze, tylko ręcznie, długopisem i w zwykłym zeszycie. I ukrywali</p>
										<img src="img/paragraph-arrow.png" alt="arrow" class="paragraph-arrow">
								</div>
						</div>
				</div>
		</div>
</section>
<?php get_footer();?>
