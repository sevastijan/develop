<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

*/
get_header() ?>

<section class="ilustration coloring-page">
<?php if(get_field('kolorowanki', 'options')) : ?>
	<div class="container">
		<div class="row">
				<div class="col-xs-12">
						<h2 class="same-heading">Kolorwanki<span class="name"><span class="hidden-xs"> -</span> <br class="visible-xs">Projektowanie znaku logo i identyfiukacji wizualnej firmy</span></h2>
				</div>
		</div>
		<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
						<p class="same-paragraph padding-bottom">
							<?php the_field('kolorowanki', 'options'); ?></p>
						</p>
				</div>
		</div>
	</div>
<?php endif  ?>

	<div class="container-fluid">
			<div class="row">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<div class="col-sm-6 col-md-4 col-lg-3 small-lg">
								<div class="ilustration-box">
										<a target="_blank" href="<?php the_field('plik_pdf_kolorowanki'); ?>">
												<div class="img-wrapper">
														<div class="ilustration-title coloring-page-title" style="background-image: url('<?php the_post_thumbnail_url('post-index'); ?>');"></div>
												</div>
										</a>
										<p class="ilustration-paragraph coloring-page-paragraph"><?php the_excerpt_max_charlength(175); ?></p>
								</div>
						</div>
					<?php endwhile; ?>
				<?php else: ?>
					<h1 class="no-results">Brak wyników</h1>
				<?php endif; ?>
			</div>
	</div>
</section>

<section class="navigation">
		<div class="container">
				<div class="row">
						<div class="col-xs-12">
								<?php the_numeric_pagination(); ?>
						</div>
				</div>
		</div>
</section>

<?php if( have_rows('klienci', 'options') ): ?>
<section class="collabo">
		<div class="container-fluid">
				<div class="row">
						<div class="col-xs-12">
								<div class="text-box">
										Dla nich pracowałem i są zadowoleni ;)
								</div>
						</div>
						<div class="collabo-carousel">
								<?php while ( have_rows('klienci', 'options') ) : the_row(); ?>
									<div class="col-sm-6 col-md-4 col-lg-2 collabo-align">
										<?php if(get_sub_field('link')) : ?>
											<a href="<?php the_sub_field('link'); ?>">
										<?php endif; ?>
											<img src="<?php the_sub_field('logo'); ?>" alt="bruk" class="collabo-img mobile-pt">
										<?php if(get_sub_field('link')) : ?>
											</a>
										<?php endif; ?>
									</div>
								<?php endwhile; ?>
						</div>
				</div>
		</div>
</section>
<?php endif; ?>

<section class="download">
		<div class="container">
				<div class="row">
						<div class="col-lg-6">
								<div class="contact">
										<h3 class="contact-title">Paweł Błoński <span class="hidden-xs">-</span> tel.&nbsp;606&nbsp;398&nbsp;351</h3>
										<p class="contact-paragraph">ilustracje@pawelblonski.pl</p>
										<div class="social">
												<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook-logo" class="facebook-img"></a>
										</div>
										<?php echo do_shortcode('[contact-form-7 id="19" title="Contact form 1"]'); ?>
								</div>

						</div>
						<div class="col-lg-6 download-align">
								<div class="col-xs-12">
										<span class="download-topic"><a href="#" class="download-topic-href">Kolorowanki do pobrania <span class="see-more">(zobacz więcej)</span></a></span>
								</div>
								<?php
									$args = array(
										'post_type' => 'coloring',
										'posts_per_page' => 6
									);
									$news = new WP_Query($args);
								  $index = 1; if ($news->have_posts()) : while ($news->have_posts()) : $news->the_post();
								?>
								<div class="col-xs-12 col-sm-4 <?php if($index < 2){ echo 'download-mobile-margin'; } ?>">
										<?php the_post_thumbnail('coloring-widget', ['class' => 'download-img']); ?>
								</div>
								<?php endwhile; endif; ?>
						</div>
				</div>
		</div>
</section>


<?php get_footer() ?>
