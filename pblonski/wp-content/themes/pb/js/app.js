
// mobile nav
$(document).ready(function(){
  $('#js-burger').click(function(){
    $('.menu-ul').toggleClass('open');
    $('body').toggleClass('overflow');
  });
});
 //  </ mobile nav


 // FIXED-MENU
 var num = 90,
     show = true;


 $(window).bind('scroll', function () {
     if ($(window).scrollTop() > num) {
        $('.navbar, .brand-box, .burger-box, .burger, .brand-img, .menu-ul>li').addClass('reduced');
     } else {
        $('.navbar, .brand-box, .burger-box, .burger, .brand-img, .menu-ul>li').removeClass('reduced');
     }
 });
 // </ FIXED-MENU



//  slick
$('.top-carousel').slick({
  dots: false,
  infinite: true,
  speed: 1500,
  autoplaySpeed: 2500,
  autoplay: true,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true,
  prevArrow: $('.prev'),
  nextArrow: $('.next')
});
// </ slick



// collabo-carouse-slick
$('.collabo-carousel').slick({
  dots: false,
  infinite: true,
  speed: 1500,
  autoplaySpeed: 2500,
  autoplay: true,
  slidesToShow: 6,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 680,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
// </ collabo-carouse-slick

// lightbox
lightbox.option({
  'resizeDuration': 200,
  'wrapAround': true,
  'showImageNumberLabel': false
})
// </ lightbox
