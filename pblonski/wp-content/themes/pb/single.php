<?php
/*

 Theme Name:  Paweł Błoński - Grafik, ilustrator
 Theme URI:    http://pawelblonski.pl
 Description:  Motwy dla strony Pawła Błońskiego
 Coding:       Krzysztof Majstrowicz
 Wordpress:  	 Krzysztof Majstrowicz & Sebastian Ślęczka
 Version:      0.1

*/
get_header(); the_post(); ?>
<article class="article">
		<div class="container">
				<div class="row">
						<div class="col-xs-12">
								<h2 class="same-heading"><?php the_title(); ?></h2>
						</div>
				</div>
				<div class="row">
						<div class="col-xs-12">
							<?php the_post_thumbnail('single-widget', ['class' => 'same-img article-heading-img']); ?>
						</div>
						<div class="col-sm-6 col-sm-offset-3 no-padding">
							<div class="same-paragraph">
								<?php the_content(); ?>

							</div>
						</div>
				</div>
		</div>
</article>

<section class="article-gallery">
		<div class="container">
				<div class="row">
						<div class="col-sm-3">
								<a href="<?php echo esc_url(home_url('/')); ?>" class="button-href">
										<div class="button">
												Wróć do przeglądu
										</div>
								</a>
						</div>
				</div>
		</div>
</section>

<?php get_footer();?>
