document.addEventListener('DOMContentLoaded', function() {
  var typed = new Typed('#js-typed', {
    strings: [
      '<span class="js-typedElement">Higher experiences</span>',
      '<span class="js-typedElement">Higher create</span>',
      '<span class="js-typedElement">Higher design</span>',
      '<span class="js-typedElement">Higher code</span>'
    ],
    typeSpeed: 50,
    backSpeed: 50,
    startDelay: 1000,
    loop: true,
    loopCount: Infinity
  });
});

$(document).ready(function(){
  $('#js-menuTrigger').on('click', function(){
    $(this).find('span').toggleClass('is-hidden');
    $('#js-menu').toggleClass('is-active');
  });
});
$(document).on('mousewheel DOMMouseScroll', function(e){
  // e.preventDefault();
  // console.log('111')
})
