// burger
$(".burger").click(function(){
    $("#menu-list").toggleClass("is-active");
    $("body").toggleClass("is-active");
});

// hide menu after click
$(".list_item").click(function(){
    $("body").removeClass("is-active");
    $("#menu-list").removeClass("is-active");
});

// slick
$('#js-primarySlider').slick({
	arrows: false,
	dots: true,
	infinite: true,
	autoplay: true,
	autoplaySpeed: 3000,
});
$('#js-secondarySlider').slick({
	arrows: true,
	infinite: true,
	autoplay: true,
	autoplaySpeed: 5000,
	nextArrow: '<i class="arrow"><span></span><span></span></i>',
	prevArrow: '<i class="arrow"><span></span><span></span></i>',
});

// nav bg-color when scrolled
var num = 20,
    show = true;
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
       $('#js-navbar, #menu-list').addClass('reduced');
    } else {
       $('#js-navbar, #menu-list').removeClass('reduced');
    }
});
