<footer id="js-contact" class="footer">
	<div class="two-boxes clearfix">
		<div class="content-wrapper">
			<div class="boxes-wrapper">
				<div class="box">
					<h6 class="typo typo_secondary">
						Napisz do nas
					</h6>
					<?php echo do_shortcode( '[contact-form-7 id="30" title="Formularz 1"]' ); ?>
				</div>
				<div class="box">
					<h6 class="typo typo_secondary">
						Kontakt do nas.
					</h6>
					<a href="#">
						<img src="<?php echo PB_THEME_URL; ?>images/brand-logo.png" alt="logo">
					</a>
					<div class="info">
						<p class="typo typo_text">
							tel: <a href="tel:<?php the_field('primary_phone','options') ?>"><?php the_field('primary_phone','options') ?></a>
						</p>
						<p class="typo typo_text">
							e-mail: <a href="mailto:<?php the_field('primary_email','options') ?>"><?php the_field('primary_email','options') ?></a>
						</p>
						<p class="typo typo_text">
							tel: <a href="tel:<?php the_field('secondary_phone','options') ?>"><?php the_field('secondary_phone','options') ?></a>
						</p>
						<p class="typo typo_text">
							e-mail: <a href="mailto:<?php the_field('secondary_email','options') ?>"><?php the_field('secondary_email','options') ?></a>
						</p>
					</div>
					<div class="short-descr">
						<div class="typo typo_text">
							<?php the_field('contact_description','options') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="bottom-bar">
		<div class="content-wrapper">
			<p class="typo typo_text">
				projekt: <a href="https://pawelblonski.pl/" target="_blank" title="Rysownik i ilustrator Paweł Błoński - Studio grafiki i ilustracji">pawelblonski.pl</a>
			</p>
		</div>
	</section>
</footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri() . '/js/app.js'?>"></script>
  <?php wp_footer(); ?>
  </body>
</html>
