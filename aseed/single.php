<?php get_header(); ?>
<?php the_post(); ?>
<article class="article">
	<div class="content-wrapper">
		<div class="box">
			<h1>
				<?php the_title(); ?>
			</h1>
			<?php the_content(); ?>
		</div>
	</div>
</article>


<section class="bar">
	<div class="content-wrapper">
		<div class="box">
			<p class="typo typo_text">
				<?php the_field('tertiary_headline','options') ?>
			</p>
		</div>
	</div>
</section>
<?php get_footer(); ?>
