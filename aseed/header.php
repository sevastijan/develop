<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri() . '/css/style.css'?>" rel="stylesheet">
	  <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
<nav id="js-navbar" class="navbar">
  <div class="content-wrapper">
	  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="brand-logo">
		  <img src="<?php echo PB_THEME_URL; ?>images/brand-logo.png" alt="logo">
	  </a>
    <ul id="js-menu-list" class="list">
		<li class="list_item">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="typo typo_secondary">maxprod</a>
		</li>
		<li class="list_item ">
			<a href="<?php echo esc_url( home_url( '/#js-gallery' ) ); ?>" id="js-galleryBtn" class="typo typo_secondary">galeria</a>
		</li>
		<li class="list_item">
			<a href="/soja" class="typo typo_secondary">o soi</a>
		</li>
		<li class="list_item">
			<a href="<?php echo esc_url( home_url( '/#js-coop' ) ); ?>" id="js-coopBtn"class="typo typo_secondary">Oferta</a>
		</li>
		<li class="list_item">
			<a href="#" id="js-contactBtn" class="typo typo_secondary">kontakt</a>
		</li>
    </ul>
    <div id="js-burger" class="burger">
      <span class="burger_item"></span>
      <span class="burger_item"></span>
      <span class="burger_item"></span>
    </div>
  </div>
</nav>
