<?php get_header(); ?>
<header class="header">
	<div class="content-wrapper">
		<div class="box-wrapper">
			<div class="box">
				<iframe width="640" height="360" src="https://www.youtube.com/embed/dp3IIoB_xk0?rel=0&amp;showinfo=0" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
			</div>
		</div>
		<div class="typo typo_text">
			<?php the_field('header_descr', 'options'); ?>
		</div>
	</div>
</header>

<section class="mini-box">
	<div class="content-wrapper">
		<div class="box">
			<p class="typo typo_text">
				<?php the_field('primary_headline','options') ?>
			</p>
		</div>
	</div>
</section>

<secton class="mosaic">
	<div class="content-wrapper">
		<?php if( have_rows('mosaic', 'options') ): $index = 0;?>
		  <?php while ( have_rows('mosaic', 'options') ) : the_row();?>
			  <div class="box <?php if($index > 5){ echo 'more js-showMoreBox'; } ?>">
	  			<div class="image" style="background-image: url(<?php the_sub_field('mosaic_image', 'options'); ?>);"></div>
	  			<div class="text-wrapper">
	  				<div class="typo typo_text">
	  					<?php the_sub_field('mosaic_description', 'options'); ?>
	  				</div>
	  			</div>
	  		</div>
		  <?php $index++; endwhile;?>
		<?php endif;?>
		<br>
		<br>
		<div class="show-more">
			<div id="js-showMore" class="wrapper">
				<p class="typo typo_text">
					Rozwiń prezentację
				</p>
				<img src="<?php echo PB_THEME_URL; ?>images/arrow.png" alt="arrow">
			</div>
		</div>
	</div>
</secton>

<section id="js-gallery" class="gallery clearfix">
	<div class="content-wrapper">
		<?php if( have_rows('gallery', 'options') ):?>
		  <?php while ( have_rows('gallery', 'options') ) : the_row();?>
			<div class="box" style="background-image: url(<?php the_sub_field('gallery_image', 'options'); ?>)">
				  <a href="<?php the_sub_field('gallery_image', 'options'); ?>" data-lightbox="image-1" data-title="<?php the_sub_field('gallery_description', 'options'); ?>">
				  </a>
			  </div>
		  <?php endwhile;?>
		<?php endif;?>
	</div>
</section>

<section id="js-coop" class="mini-box coop">
	<div class="content-wrapper">
		<div class="box">
			<p class="typo typo_text">
				<?php the_field('secondary_headline','options') ?>
			</p>
		</div>
	</div>
</section>

<section class="two-boxes clearfix">
	<img src="<?php echo PB_THEME_URL; ?>images/coop-logo.png" alt="logo">
	<div class="typo typo_text coop-descr">
		<?php the_field('product_heading','options') ?>
	</div>
	<div class="content-wrapper">
		<div class="boxes-wrapper">
			<?php if( have_rows('product', 'options') ):?>
			  <?php while ( have_rows('product', 'options') ) : the_row();?>
				  <div class="box">
					  <h3 class="typo typo_primary">
						 <?php the_sub_field('product_title', 'options'); ?>
					  </h3>
					  <div class="typo typo_text">
						  <?php the_sub_field('product_description', 'options'); ?>
					  </div>
				  </div>
			  <?php endwhile;?>
			<?php endif;?>
		</div>
	</div>
</section>

<section class="download clearfix">
	<div class="content-wrapper">
		<h3 class="typo typo_text heading">
			dokumenty do pobrania
		</h3>
		<div class="boxes-wrapper">
			<?php if( have_rows('download_files', 'options') ):?>
			  <?php while ( have_rows('download_files', 'options') ) : the_row();?>
				  <div class="box">
					  <div class="sub-box">
						<a href="<?php the_sub_field('download_file', 'options'); ?>" target="_blank">
							<p class="typo typo_text">
								<?php the_sub_field('download_heading', 'options'); ?>
							</p>
						</a>
	  					<a href="<?php the_sub_field('download_file', 'options'); ?>" target="_blank">
							<img src="<?php echo PB_THEME_URL; ?>images/download-icon.png" alt="download icon">
	  					</a>
	  				</div>
				  </div>
			  <?php endwhile;?>
			<?php endif;?>
		</div>
	</div>
</section>


<section class="partners clearfix">
	<div class="wrapper">
		<h3 class="typo typo_text heading">
			nasi partnerzy
		</h3>
	</div>
	<div class="content-wrapper">
		<div class="boxes-wrapper">
			<div class="box">
				<div class="sub-box">
					<img src="<?php echo PB_THEME_URL; ?>images/partner1.png" alt="partner-logo">
				</div>
			</div>
			<div class="box">
				<div class="sub-box">
					<img src="<?php echo PB_THEME_URL; ?>images/partner2.png" alt="partner-logo">
			</div>
		</div>
	</div>
</section>

<section class="bar">
	<div class="content-wrapper">
		<div class="box">
			<p class="typo typo_text">
				<?php the_field('tertiary_headline','options') ?>
			</p>
		</div>
	</div>
</section>
<?php get_footer(); ?>
