// burger
$("#js-burger").click(function(){
    $("#menu-list").toggleClass("is-active");
    $("body").toggleClass("is-active");
});

// hide menu after click
$(".list_item").click(function(){
    $("body").removeClass("is-active");
    $("#js-menu-list").removeClass("is-active");
});

// // nav scroll
$("#js-coopBtn").click(function() {
    $('html, body').animate({
        scrollTop: $("#js-coop").offset().top - 170
    }, 2000);
});
$("#js-contactBtn, #js-writeBtn").click(function() {
    $('html, body').animate({
        scrollTop: $("#js-contact").offset().top - 170
    }, 2000);
});
$("#js-galleryBtn").click(function() {
    $('html, body').animate({
        scrollTop: $("#js-gallery").offset().top - 60
    }, 2000);
});


// fixed navbar
var num = 20,
    show = true;
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
       $('#js-navbar').addClass('reduced');
    } else {
       $('#js-navbar').removeClass('reduced');
    }
});

// showMore
$('#js-showMore').click(function(){
  var button = $( "#js-showMore" );
  var box = $( ".js-showMoreBox" );
  var arrow = $(this);

  box.slideToggle(200);

  box.css('display','inline-block');
  if(!$(this).hasClass('active')){
	   $(this).find("p").html("Zwiń prezentację");
  } else{
	  $(this).find("p").html("Rozwiń prezentację");
  }

  box.toggleClass("active");
  arrow.toggleClass("active");
});

lightbox.option({
  'resizeDuration': 200,
  'albumLabel': ''
})
