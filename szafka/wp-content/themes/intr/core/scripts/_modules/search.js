const searchModule = new Vue({
  	el: '#vue-search',
  	data: function(){
		return {
			posts: [],
			loading: false,
			emptyResponse: false
	  }
  },
	methods: {
		checkValue: function () {
			var self,
				globalTimeout = null,
				searchInput = $('.js-input');
				self = this;
			searchInput.keyup(function() {
				let url = 'http://localhost/app/develop/szafka/wp-json/wp/v2/posts?search=' + $(this).val();
				if(globalTimeout !== null){
					clearTimeout(globalTimeout);
				}
				globalTimeout = setTimeout(function (){
					self.loading = true;
					if(searchInput.val().length == 0){
						self.posts = [];
						self.loading = false;
						emptyResponse = false;
					}else{
						self.getData(url);
					}
				}, 1500);
			});
		},
		getData: function (url) {
			var self;
				self = this;
			axios({
				method: 'get',
				url: url
			})
			.then(function(response) {
				self.posts = response.data;
				self.loading = false;
				if(response.data.length === 0){
					self.emptyResponse = true;
				}else{
				   	self.emptyResponse = false;
				}
			  });
		}
	},
	mounted: function(){
    	this.checkValue();
    }
})
